
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-07 20:24:23.
 * Controller ID - 81
 */

'use strict';


// [ID: 196]
function iSwitchOnChange(id, status) {
    if(id.substring(0, 16) == 'menu-item-status') {
        var menuItemId = id.slice(17, 50);
        var el = $('#categories-tab');
        blockUI(el);

        // [ID: 211]
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/changemenuitemstatus',
            data: {menuitemid:menuItemId, status:status}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    unBlockUI(el);
                    if(status == 1) {
                        $('#menu-item-number-'+menuItemId).removeClass('menu-item-draft').text(res.order+'. ');
                        notifyDone('Your Category is LIVE now!');
                    }
                    else {
                        $('#menu-item-number-'+menuItemId).addClass('menu-item-draft').text('Draft - ');
                        notifyDone('Your Category is NOT LIVE!')
                    }

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

$('.grid-stack').on('dragstop', function (event, ui) {
    setTimeout(function () {
        $('.menu-item').each(function () {
            var id = $(this).attr('data-menuid');
            var gridX = parseInt($(this).attr('data-gs-x'));
            var gridY = parseInt($(this).attr('data-gs-y'));

            var gridOrder = (gridX+4)/4;
            if(gridY == 0) {
                gridOrder = gridOrder+(gridY);
            }
            else {
                gridOrder = gridOrder+(gridY-1);
            }


            console.log(id+ ' X:'+ gridX+ ' Y:'+gridY+' ## Order:'+gridOrder);
        });
    }, 200);
});

// [ID: 205]
$('.grid-stack').on('dragstop', function (event, ui) {
    var el = $('#tab_categories');
    blockUI(el);
    var menuOrder = [];

    setTimeout(function () {
        $('.menu-item').each(function () {
            var id = $(this).attr('data-menuid');
            var gridX = parseInt($(this).attr('data-gs-x'));
            var gridY = parseInt($(this).attr('data-gs-y'));

            var gridOrder = (gridX+4)/4;
            if(gridY == 0) {
                gridOrder = gridOrder+(gridY);
            }
            else {
                gridOrder = gridOrder+(gridY-1);
            }


            // console.log(id+ ' X:'+ gridX+ ' Y:'+gridY+' ## Order:'+gridOrder);
            gridOrder = JSON.parse(gridOrder);
            menuOrder.push({id:id, order:gridOrder});
        });

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/setupmenuorder',
            data: {menuorder:menuOrder}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var i = 1;
                    $('.menu-item').each(function () {
                        var id = $(this).attr('data-menuid');
                        $('#menu-item-number-'+id).text(i);
                        i++;
                    });
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }, 200);
});

// [ID: 207]
function updateMenuItemName(menuItemId) {
    //develop the function here
    var value = $.trim($('#txt-menu-item-name-'+menuItemId).val());
    var el = $('#tab_categories');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/updatemenuitemname',
        data: {menuitemid:menuItemId, value:value}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 209]
function addMenuItem(menuId) {
    //develop the function here
    var el = $('#tab_categories');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/addmenuitem',
        data: {menuid:menuId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var items = [
                    {x: 0, y: 0, width: 2, height: 2},
                    {x: 3, y: 1, width: 1, height: 2},
                    {x: 4, y: 1, width: 1, height: 1},
                    {x: 2, y: 3, width: 3, height: 1},
//                    {x: 1, y: 4, width: 1, height: 1},
//                    {x: 1, y: 3, width: 1, height: 1},
//                    {x: 2, y: 4, width: 1, height: 1},
                    {x: 0, y: 0, width: 4, height: 4}
                ];

                var grid = $('.grid-stack').data('gridstack');

                var node = items.pop() || {
                    x: 12 * Math.random(),
                    y: 5 * Math.random(),
                    width: 4,
                    height: 4
                };
                grid.addWidget($('<div id="menu-item-'+data+'" style="padding: 20px;" data-gs-no-resize="true">' +
                    '                       <div class="grid-stack-item-content" style="box-shadow: 1px 1px 2px 3px #f7f6f6; border-radius: 10px; padding: 10px;">' +
                    '                           <div style="position: absolute; right: 10px; top: 10px;">' +
                    '                               <input data-id = "menu-item-status-'+data+'" type = "checkbox" class = "ios-switch ios-switch-primary iswitch-xs pull-right" title="Publish Menu Item" />' +
                    '                           </div>' +
                    '                           <h4 style="text-align: center;"><span id="menu-item-number-'+data+'" class="menu-item-draft">Draft - </span> <input id="txt-menu-item-name-'+data+'" placeholder="Menu Item Name..." type="text" class="input-trans" value="" style="height: 25px;" onchange="updateMenuItemName('+data+');" /> </h4>' +
                    '                       </div>' +
                    '                   <div/>'),
                    node.x, node.y, node.width, node.height);

                $(".ios-switch").each(function(){
                    mySwitch = new Switch(this);
                });

                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}


function flipDiv(menuItemId) {
    $('#flipped-class-'+menuItemId).toggleClass('flipped');
}

// [ID: 213]
function sortMenuSubItems() {
    //develop the function here

    setTimeout(function () {
        var i = 1;
        var menuOrder = [];
        var menuIdList = [];

        $('.accordion').each(function () {
            menuIdList.push($(this).attr('data-menuid'));
        });

        for(var j = 0; j < menuIdList.length; j++) {
            $('#accordion-menu-category-'+menuIdList[j]+' .panel').each(function () {
                var menuItemId = $(this).attr('data-menuitemid');
                var containerId = $(this).parent().attr('data-container');
                var mainMenuId = $(this).parent().attr('data-menuid');

                //color change
                console.log('itemid: '+menuItemId+' ## containerId: '+containerId);
                if(containerId != 0) {
                    $('#category-'+menuItemId).attr('style', 'background:#F2F5F7 !important');
                }
                else {
                    $('#sub-category-'+menuItemId).attr('style', 'background:#f5f2f7 !important');
                }

                menuOrder.push({id:menuItemId, order:i, menuid:menuIdList[j], mainmenuid:mainMenuId, container:containerId});
                i++
            });
        }


        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/sortmenusubitems',
            data: {menuorder:menuOrder}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }, 100);
}
                    
    // [ID: 306]
    function updateCategoryDetails(object, categoryId, field) {
        var value = $.trim($("#"+object.id).val());

        var el = $('#menu-item-grid-stack');
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/updatecategorydetails',
            data: {value:value, categoryid:categoryId, field:field}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    unBlockUI(el);
                    if(field == 'name') {
                        $('#panel-title-'+categoryId).html(value);
                    }
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 308]
    function addCategory(menuItemId, subItemId) {
        //develop the function here
        var el = $('#menu-item-'+menuItemId);
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/addcategory',
            data: {menuitemid:menuItemId, subitemid:subItemId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(subItemId == null) {
                        var appendHtml = '  <div class="panel" data-menuitemid="'+data+'">' +
                            '    <a class="panel-heading collapsed" role="tab" id="category-'+data+'" data-toggle="collapse" data-parent="#accordion-menu-category-'+menuItemId+'" href="#collapse-'+data+'" aria-expanded="true" aria-controls="collapse-'+data+'">' +
                            '        <h4 id="panel-title-'+data+'" class="panel-title">Untitled</h4>' +
                            '    </a>' +
                            '    <div id="collapse-'+data+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-'+data+'">' +
                            '        <div class="panel-body">' +
                            '            <label>Category Image</label>' +
                            '            <div class="upload-img-container" style="position: relative; width: 300px;" onclick="openUploadFileDialog('+data+');">' +
                            '                <div class="upload-img-overlay" onmouseenter="animateLivIcon('+data+');">' +
                            '                    <h4 style="margin-bottom: 10%; font-size: 13px;">Add/Change Category Image</h4>' +
                            '                    <div id="upload-img-livicon-'+data+'" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 45px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>' +
                            '                    <p>Click Here to Upload!<br/><br/><span>notes</span></p>' +
                            '                </div>' +
                            '                <img id="slider-slide-img-'+data+'" src="'+window.domain+'assets/backend/images/website/slide-block/default.jpg" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>' +
                            '                <div id = "image-upload-progress-'+data+'" class = "progress" style = "height: 8px;width: 100%;">' +
                            '                    <div id = "image-upload-progress-bar-'+data+'" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">' +
                            '                        <span id="image-upload-sr-only-'+data+'" class = "sr-only">0% Complete</span>' +
                            '                    </div>' +
                            '                </div>' +
                            '            </div>' +
                            '            <form action="'+window.domain+'website/savefieldimage" method="post" id="upload-image-form-'+data+'" enctype="multipart/form-data" class="slider-slide-image-upload">' +
                            '                <input type="file" name="file" id="image-choose-'+data+'" onchange="submitImageUploadForm(\'#upload-image-form-'+data+'\', '+data+');" accept="image/jpg;capture=camera" style="display: none;">' +
                            '                <input type="hidden" name="categoryid" value="'+data+'" />' +
                            '            </form>' +
                            '            <div class="form-group" style="margin-top: 40px;">' +
                            '                <label>Category Name</label>' +
                            '                <input  id="category-name-'+data+'" type="text" class="form-control" value=""  onchange="updateCategoryDetails(this, '+data+', \'name\')" />' +
                            '            </div>' +
                            '            <div class="form-group" style="margin-top: 40px;">' +
                            '                <label>Category Short Description</label>' +
                            '                <textarea id="category-description-'+data+'" onchange="updateCategoryDetails(this, '+data+', \'description\')" class="form-control" data-rich="0"></textarea>' +
                            '            </div>' +
                            '            <div class="form-group" style="margin-top: 40px;">' +
                            '                <label>Category Long Description</label>' +
                            '                <textarea id="category-long-description-'+data+'" onchange="updateCategoryDetails(this, '+data+', \'long_description\')" class="form-control" data-rich="0"></textarea>' +
                            '            </div>' +
                            '            <h3 style="margin-top: 50px; font-size: 18px;">Sub Categories</h3>' +
                            '            <div class="col-md-12" style="margin-bottom: 30px;">' +
                            '                <button class="btn btn-sm btn-info pull-right" onclick="addCategory('+menuItemId+', '+data+');"><i class="fa fa-plus"></i> Add Sub Category</button>' +
                            '            </div>' +
                            '            <div></div>' +
                            '            <div class="accordion" id="accordion-menu-sub-category-'+data+'" data-menuid="'+data+'" data-container="'+data+'" role="tablist" aria-multiselectable="true" style="margin-bottom: 100px; padding: 5px; margin-top: 50px;">' +
                            '            </div>' +
                            '        </div>' +
                            '    </div>' +
                            '</div>';

                        $('#accordion-menu-category-'+menuItemId).append(appendHtml);
                    }
                    else {
                        var appendHtml = '  <div class="panel" data-menuitemid="'+data+'">' +
                            '                   <a class="panel-heading collapsed" role="tab" id="sub-category-'+data+'" data-toggle="collapse" data-parent="#accordion-menu-sub-category-'+subItemId+'" href="#collapse-'+data+'" aria-expanded="true" aria-controls="collapse-'+data+'" style="background-color: #f5f2f7;">' +
                            '                       <h4 id="panel-title-'+data+'" class="panel-title">Untitled</h4>' +
                            '                   </a>' +
                            '                   <div id="collapse-'+data+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-'+data+'">' +
                            '                       <div class="panel-body">' +
                            '                           <div class="form-group" style="margin-top: 40px;">' +
                            '                               <label>Sub-Category Name</label>' +
                            '                               <input  id="category-name-'+data+'" type="text" class="form-control" value=""  onchange="updateCategoryDetails(this, '+data+', \'name\')" />' +
                            '                           </div>' +
                            '                           <div class="form-group">' +
                            '                               <label>Description</label>' +
                            '                               <textarea id="category-description-'+data+'" onchange="updateCategoryDetails(this, '+data+', \'description\')" class="form-control" data-rich="0"></textarea>' +
                            '                           </div>' +
                            '                       </div>' +
                            '                   </div>' +
                            '               </div>';

                        $('#accordion-menu-sub-category-'+subItemId).append(appendHtml);
                    }

                    $('#panel-title-'+data).trigger('click');
                    // document.getElementById('category-name-'+data).scrollIntoView();
                    $('#category-name-'+data).focus();
                    unBlockUI(el);
                    
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function animateLivIcon(slideId) {
        var livIcon = $('#upload-img-livicon-'+slideId);
        livIcon.playLivIcon(1, 2);
    }
                    
    // [ID: 314]
    $(function(){
        // function from the jquery form plugin
        $('.slider-slide-image-upload').ajaxForm({
            beforeSend:function(){
                console.log(window.uploadingImgFieldId);
                $('#'+window.type+'-image-upload-progress-'+window.selectedCategory).show();
            },
            uploadProgress:function(event,position,total,percentComplete){
                console.log("#"+window.type+"-upload-progress-bar-"+window.selectedCategory);
                $("#"+window.type+"-upload-progress-bar-"+window.selectedCategory).width(percentComplete+'%'); //dynamicaly change the progress bar width
                $("#"+window.type+"-upload-sr-only-"+window.selectedCategory).html(percentComplete+'%'); // show the percentage number
                console.log("#"+window.type+"-upload-sr-only-"+window.selectedCategory);
            },
            success:function(){
                console.log('success...');
                $("#"+window.type+"-image-upload-progress-"+window.selectedCategory).hide();
            },
            complete:function(response){
                console.log(response);
                var res = JSON.parse(response.responseText);



                // console.log(new Date().getTime());
                $('#'+window.type+'-img-'+res.id).attr('src', window.domain+res.dirpath+res.name+'?timestamp=' + new Date().getTime());
            }
        });

        //set the progress bar to be hidden on loading
        $(".progress").hide();
        $("#image-upload-progress-bar-"+window.uploadingImgFieldId).width('0%');
    });

    function categoryImgFormSubmit(categoryId, type) {
        window.selectedCategory = categoryId;
        window.type = type;
        if(type == 'category') {
            $('#upload-image-form-'+categoryId).submit();
        }
        else {
            $('#banner-upload-image-form-'+categoryId).submit();
        }
    }

    function menuImgFormSubmit(menuId) {
        window.selectedMenuImgId = menuId;
        $('#menu-upload-image-form-'+menuId).submit();
    }

    // [ID: 380]
    $(function(){
        // function from the jquery form plugin
        $('.menu-item-img-upload').ajaxForm({
            beforeSend:function(){
                $('#menu-image-upload-progress-'+window.selectedMenuImgId).show();
            },
            uploadProgress:function(event,position,total,percentComplete){
                console.log("#menu-upload-progress-bar-"+window.selectedMenuImgId);
                $("#menu-upload-progress-bar-"+window.selectedMenuImgId).width(percentComplete+'%'); //dynamicaly change the progress bar width
                $("#menu-upload-sr-only-"+window.selectedMenuImgId).html(percentComplete+'%'); // show the percentage number
            },
            success:function(){
                $("#menu-image-upload-progress-"+window.selectedMenuImgId).hide();
            },
            complete:function(response){
                console.log(response);
                var res = JSON.parse(response.responseText);



                // console.log(new Date().getTime());
                $('#menu-img-'+res.id).attr('src', window.domain+res.dirpath+res.name+'?timestamp=' + new Date().getTime());
            }
        });

        //set the progress bar to be hidden on loading
        $(".progress").hide();
        $("#menu-upload-progress-bar-"+window.selectedMenuImgId).width('0%');
    });

