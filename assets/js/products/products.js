
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-13 11:44:36.
 * Controller ID - 69
 */

'use strict';
window.paginate = 1;
window.paginateOn = 0;
window.productCounter = 0; //count the number of product to display product index (thead - #)
window.searchedList = [1];
window.sortBy = 'null';
window.sortType = 'null';
window.editPriceValue = {};
window.editWsPriceValue = {};
window.selectedProductList = []; // store the id when click the checkbox and select a product

$(document).ready(function () {
    getProducts();
});

$(window).scroll(function () {
    var elementHeight = $('#products-row-container')[0].scrollHeight+396;
    var scrollPosition = $(window).height() + $(window).scrollTop();

    if (elementHeight -800 < scrollPosition) {
        if(window.paginateOn == 1) {
            window.paginateOn = 0;
            window.paginate++;
            getProducts();
        }

    }
});

// [ID: 179]
function addProduct() {
    //develop the function here
    var productName = $.trim($('#mm-product-name').val());

    if(productName == '') {
        $('#mm-product-name').trigger('blur');
    }
    else {
        var el = $('#model-add-new-product-content');
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/addproduct',
            data: {productname:productName}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    unBlockUI(el);
                    notifyDone('Product Added!');
                    cancelProduct();

                    setTimeout(function () {
                        window.location.replace(window.domain+'products/product/'+data);
                    }, 100);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }


}

// [ID: 181]
function cancelProduct() {
    $('#product-modal-close').trigger('click');
    $("#mm-product-name").val('');
}
                    
    // [ID: 353]
    function uploadExcelProducts() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'products/uploadexcelproducts',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 355]
    function bulkImageUpload() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/bulkimageupload',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 357]
    function getProducts() {
        //develop the function here
        $('#onstart-loading').fadeOut(100);
        var el = $('#products-row-container');
        blockUI(el);

        if(window.paginate != 1) {
            $('#products-row-container').append('<tr id="product-loading-row">' +
                '                                   <td colspan="9" align="center"><img src="'+window.domain+'/assets/backend/images/loading/ring-sm.svg"/> </td>' +
                '                               </tr>');
        }
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/getproducts',
            data: {paginate:window.paginate, perpage:100, sortby:window.sortBy, sorttype:window.sortType}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                    unBlockUI(el);
                }
                else {
                    var res = JSON.parse(data);
                    var productRows = '';

                    for(var i = 0; res.length > i; i++) {
                        if(res[i].imgid == null) {
                            var imgid = 'default';
                            var imgext = 'png';
                        }
                        else {
                            var imgid = res[i].imgid;
                            var imgext = res[i].imgext;
                        }
                        
                        productRows += '<tr id="product-row-'+res[i].id+'">' +
                            '               <td><input type="checkbox" id="chk-'+res[i].id+'" onclick="addToSelectedList('+res[i].id+');" /> </td>' +
                            '               <td onclick="openProduct('+res[i].id+');">'+(++window.productCounter)+'</td>' +
                            '               <td align="left" onclick="openProduct('+res[i].id+');"><a href="'+window.domain+'/products/product/'+res[i].id+'"> '+res[i].external_id+'</a></td>' +
                            '               <td align="right" onclick="openProduct('+res[i].id+');"><img src="'+window.domain+'/assets/backend/images/products/products/'+imgid+'_thumb.'+imgext+'" style="width: 40px; margin-right: 10px;"/> </td>' +
                            '               <td align="left" onclick="openProduct('+res[i].id+');"><a href="'+window.domain+'/products/product/'+res[i].id+'"> '+res[i].name+'</a></td>' +
                            '               <td align="right" style="position: relative;"><span id="product-price-'+res[i].id+'">'+res[i].price+'</span><i id="edit-price-icon-'+res[i].id+'" class="fa fa-edit hover-fade-up-icon" onclick="showEditPrice('+res[i].id+', 1);"></i> </td>' +
                            '               <td align="right" style="position: relative;"><span id="product-ws-price-'+res[i].id+'">'+res[i].wsprice+'</span><i id="edit-ws-price-icon-'+res[i].id+'" class="fa fa-edit hover-fade-up-icon" onclick="showEditPrice('+res[i].id+', 2);"></i></td>' +
                            '               <td align="center"><span class="label '+(res[i].status == 1 ? 'label-success">Published': 'label-default">Draft')+'</span></td>' +
                            '               <td align="right">'+res[i].createdat+'</td>' +
                            '           </tr>';
                    }

                    if(window.paginate == 1) {
                        $('#products-row-container').html(productRows);
                    }
                    else {
                        $('#product-loading-row').fadeOut(200, function () {
                            $('#product-loading-row').remove();
                        });

                        $('#products-row-container').append(productRows);
                    }

                    if(res.length > 0)
                        window.paginateOn = 1;
                    else
                        $('#products-row-container').append('<tr id="product-loading-row">' +
                            '                                   <td colspan="8" align="center">No more... </td>' +
                            '                               </tr>');

                    $('#search-result-count').text(window.productCounter);

                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function openProduct(productId) {
        window.location.href = window.domain+'products/product/'+productId;
    }
                    
    // [ID: 359]
    function serchProducts() {

        var keywords = $.trim($("#product-table-search-text").val());
        var el = $('#products-row-container');
        blockUI(el);

        var resultCount = parseInt($('#search-result-count').text());
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'products/serchproducts',
            data: {keywords:keywords, searchedlist:window.searchedList}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                    unBlockUI(el);
                }
                else {
                    var res = JSON.parse(data);
                    var productRows = '';

                    for(var i = 0; res.length > i; i++) {
                        if(res[i].imgid == null) {
                            var imgid = 'default';
                            var imgext = 'png';
                        }
                        else {
                            var imgid = res[i].imgid;
                            var imgext = res[i].imgext;
                        }

                        productRows += '<tr id="product-row-'+res[i].id+'">' +
                            '               <td><input type="checkbox" class="chk-box" id="chk-'+res[i].id+'" onclick="addToSelectedList('+res[i].id+');" /> </td>' +
                            '               <td onclick="openProduct('+res[i].id+')">'+(++window.productCounter)+'</td>' +
                            '               <td align="left" onclick="openProduct('+res[i].id+')">'+res[i].external_id+'</td>' +
                            '               <td align="right" onclick="openProduct('+res[i].id+')"><img src="'+window.domain+'/assets/backend/images/products/products/'+imgid+'_thumb.'+imgext+'" style="width: 40px; margin-right: 10px;"/> </td>' +
                            '               <td align="left" onclick="openProduct('+res[i].id+')">'+res[i].name+'</td>' +
                            '               <td align="right" style="position: relative;"><span id="product-price-'+res[i].id+'">'+res[i].price+'</span><i id="edit-price-icon-'+res[i].id+'" class="fa fa-edit hover-fade-up-icon" onclick="showEditPrice('+res[i].id+', 1);"></i> </td>' +
                            '               <td align="right" style="position: relative;"><span id="product-ws-price-'+res[i].id+'">'+res[i].wsprice+'</span><i id="edit-ws-price-icon-'+res[i].id+'" class="fa fa-edit hover-fade-up-icon" onclick="showEditPrice('+res[i].id+', 2);"></i></td>' +
                            '               <td align="center"><span class="label '+(res[i].status == 1 ? 'label-success">Published': 'label-default">Draft')+'</span></td>' +
                            '               <td align="right">'+res[i].createdat+'</td>' +
                            '           </tr>';

                        window.searchedList.push(res[i].id);
                        resultCount++;
                    }

                    $('#products-row-container').append(productRows);
                    $('#search-result-count').text(resultCount);

                    if(productRows == '' && window.searchedList.length < 2) {
                        $('#products-row-container').append('<tr id="product-nothing-found-row">' +
                            '                                   <td colspan="9" align="center">Nothing found!... </td>' +
                            '                               </tr>');
                    }

                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function onSearch() {
        var keyword = $.trim($('#product-table-search-text').val());
        if(keyword.length > 1) {
            window.paginateOn = 0;
        }
        else {
            window.paginateOn = 1;

            $(".search-results").each(function () {
                $(this).fadeOut(200, function () {
                    $(this).remove();
                    window.productCounter--;
                });
            });

            $("#search-result-count").text(window.productCounter);
        }
    }

    $('#product-table-search-text').bind('keypress', function (e) {
        if(e.keyCode == 13) {
            serchProducts();
        }
    });

    function productTableSearchFilter(object){
        var filter = $.trim($("#"+object.id).val()), count = 0;
        $('#product-nothing-found-row').fadeOut(200, function() {
            $('#product-nothing-found-row').remove();
        });

        window.searchedList = [1];

        $(".search-filter tr").each(function(){
            if(filter.length < 2) {
                $(this).show();
                count++;
            }
            else {
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    if($(this).is('.headings')){

                    }
                    else {
                        $(this).fadeOut(200);
                    }

                } else {
                    $(this).show();
                    var searchedId = ($(this).attr('id')).substring(12);
                    window.searchedList.push(searchedId);
                    count++;
                }
            }

        });

        onSearch();
        var numberItems = count;
        $("#search-result-count").text(count);
    }

    function sortProductList(type) {
        window.sortBy = type;
        window.paginate = 1;
        window.paginateOn = 1;
        window.productCounter = 0;

        $('.sort-icon').fadeTo(200, 0);

        if(window.sortType == 'null') {
            window.sortType = 'ASC';
            $('#sort-icon-'+type).removeClass('fa-caret-down').addClass('fa-caret-up');
        }
        else if(window.sortType == 'ASC') {
            $('#sort-icon-'+type).removeClass('fa-caret-up').addClass('fa-caret-down');
            window.sortType = 'DESC';
        }
        else {
            $('#sort-icon-'+type).removeClass('fa-caret-down').addClass('fa-caret-up');
            window.sortType = 'ASC';
        }

        $('#sort-icon-'+type).fadeTo(200, 1);
        getProducts();
    }

    function showEditPrice(productId, type) {
        if(type == 1) {
            var price = $.trim($('#product-price-'+productId).text());
            price = parseFloat((price.replace('$', '')));
            window.editPriceValue[productId] = price;

            $('#product-price-'+productId).html('$<input id="input-product-edit-price-'+productId+'" type="number" class="table-on-row-edit numbers-only" value="'+price+'" /><i class="fa fa-close hover-fade-up-icon-close" onclick="closeEditPrice('+productId+');" style="color: #dc0107;"></i> ');
            $('#edit-price-icon-'+productId).removeClass('fa-edit hover-fade-up-icon').addClass('fa-check hover-fade-up-icon-active');
            $('#edit-price-icon-'+productId).removeAttr('onclick').attr('onclick', 'editPrice('+productId+', '+type+');');
        }
        else {
            var price = $.trim($('#product-ws-price-'+productId).text());
            price = parseFloat((price.replace('$', '')));
            window.editWsPriceValue[productId] = price;

            $('#product-ws-price-'+productId).html('$<input id="input-product-edit-ws-price-'+productId+'" type="number" class="table-on-row-edit numbers-only" value="'+price+'" /><i class="fa fa-close hover-fade-up-icon-close" onclick="closeEditWsPrice('+productId+');" style="color: #dc0107;"></i> ');
            $('#edit-ws-price-icon-'+productId).removeClass('fa-edit hover-fade-up-icon').addClass('fa-check hover-fade-up-icon-active');
            $('#edit-ws-price-icon-'+productId).removeAttr('onclick').attr('onclick', 'editPrice('+productId+', '+type+');');
        }
    }

    function closeEditPrice(productId) {
        var price = ((parseFloat(window.editPriceValue[productId])).toFixed(2));
        if(isNaN(price)) price = '-';
        else price = '$'+price;

        $('#product-price-'+productId).html(price);
        $('#edit-price-icon-'+productId).removeClass('fa-check hover-fade-up-icon-active').addClass('fa-edit hover-fade-up-icon');
    }
    function closeEditWsPrice(productId) {
        var price = ((parseFloat(window.editWsPriceValue[productId])).toFixed(2));
        if(isNaN(price)) price = '-';
        else price = '$'+price;

        $('#product-ws-price-'+productId).html(price);
        $('#edit-ws-price-icon-'+productId).removeClass('fa-check hover-fade-up-icon-active').addClass('fa-edit hover-fade-up-icon');
    }

    // [ID: 361]
    function editPrice(productId, type) {
        //develop the function here
        if(type == 1)
            var price = $.trim($('#input-product-edit-price-'+productId).val());
        else
            var price = $.trim($('#input-product-edit-ws-price-'+productId).val());

        var el = $('#product-row-'+productId);
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/editprice',
            data: {price:price, productid:productId, type:type}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(type == 1) {
                        window.editPriceValue[productId] = price;
                        closeEditPrice(productId);
                        $('#edit-price-icon-'+productId).removeAttr('onclick').attr('onclick', 'showEditPrice('+productId+', '+type+');');
                    }
                    else {
                        window.editWsPriceValue[productId] = price;
                        closeEditWsPrice(productId);
                        $('#edit-ws-price-icon-'+productId).removeAttr('onclick').attr('onclick', 'showEditPrice('+productId+', '+type+');');
                    }

                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 371]
    function interviewFunction() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/interviewfunction',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function addToSelectedList(id) {
        if($('#chk-'+id).prop('checked')) {
            window.selectedProductList.push(id);
        }
        else {
            window.selectedProductList.remove(id);
        }

        var selectedProducts = window.selectedProductList.length;
        if(selectedProducts > 0) {
            $('.selected-count-text').addClass('loaded').text(selectedProducts+' Selected');
            $('#btn-delete-products').removeAttr('disabled');
            $('#btn-clear-selection').removeAttr('disabled');
            $('#btn-show-more-options').removeAttr('disabled');
        }
        else {
            clearSelection();
        }
    }

    function clearSelection() {
        window.selectedProductList = [];
        $('input:checkbox').removeAttr('checked');
        $('.selected-count-text').removeClass('loaded');
        $('#btn-delete-products').attr('disabled', 'disabled');
        $('#btn-clear-selection').attr('disabled', 'disabled');
        $('#btn-show-more-options').attr('disabled', 'disabled');

    }

    // [ID: 378]
    function confirmDeleteProducts() {
        var id = 'btn-delete-products';
        nconfirm(id, 'Delete Permanently', 'Do you really want to remove selected products permanently ?', 'Remove', 'danger', 100, -43);
    }
    window.confirmfunction = (function () {
        var el = $('#products-row-container');
        blockUI(el);
        $('.nconfirm-btn-cancle').trigger('click');

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/deleteproducts',
            data: {productids:window.selectedProductList}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    unBlockUI(el);
                    for(var i = 0; i < window.selectedProductList.length; i++) {
                        $('#product-row-'+window.selectedProductList[i]).fadeOut(300);
                    }

                    clearSelection();
                    notifyDone('Successfully removed from the system.');
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    });
    window.notConfirmed = (function () {});

