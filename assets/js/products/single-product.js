
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-03-05 19:47:52.
 * Controller ID - 91
 */

$(document).ready(function () {
    window.productId = $('#product-id').val();
    window.productDetailsId = $('#product-detail-id').val();
    window.productDetailsPayment = $('#product-detail-payment').val();
    window.selectedImgType = 1;

    getProductStatistics();
    var newProduct = $('#product-new').val();
    if(newProduct == 1) {
        getGlobalProducts();
    }
});


// [ID: 182]
function saveProductDetails(object, column, productDetailsId) {
    var value = $.trim($('#'+object.id).val());
    if(productDetailsId == '') {
        productDetailsId = null;
    }

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/saveproductdetails',
        data: {productid:window.productId, productdetailid:productDetailsId, value:value, column:column}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                window.productDetailsId = data;
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 184]
function saveProductInfo(object, column) {
    var value = $.trim($("#"+object.id).val());

    //ajax method
    jQuery.ajax({
        type: "POST",
        url: window.domain + "products/saveproductinfo",
        data: {productid:window.productId, value:value, column:column}, //add the parameter if required to pass
        success: function (data) {
            if(data === "Unauthorized") {
                window.location.reload();
            }
            else if(data === 0) {
                dbError();
            }
            else {

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 186]
function addMorePriceCategories() {
    if(window.productDetailsPayment == 2) var el = $("#price-volume");
    else if(window.productDetailsPayment == 3) var el = $("#price-weight");
    else if(window.productDetailsPayment == 4) var el = $("#price-size");
    else if(window.productDetailsPayment == 5) var el = $("#price-area");

    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: "POST",
        url: window.domain + "products/addmorepricecategories",
        data: {productid:window.productId}, //add the parameter if required to pass
        success: function (data) {
            if(data == "Unauthorized") {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                if(window.productDetailsPayment == 1) {
                    var appendHtml = "";
                }
                else if(window.productDetailsPayment == 2) {
                    var appendHtml = '  <div id="product-volume-container-'+data+'">' +
                        '                   <div class="form-group col-md-6">' +
                        '                       <label>Volume (ml)</label>' +
                        '                       <input id="product-volume-volume-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'volume\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-5 form-group">' +
                        '                       <label>Price ($)</label>' +
                        '                       <input id="product-volume-price-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'price_volume\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-1">' +
                        '                       <i id="product-volume-remove-icon-'+data+'" class="fa fa-minus-circle remove-icon" title="Remove This" onclick="removePriceCombination(\'volume\', '+data+');"></i>' +
                        '                   </div>' +
                        '               </div>';
                }
                else if(window.productDetailsPayment == 3) {
                    var appendHtml = '  <div id="product-weight-container-'+data+'">' +
                        '                   <div class="form-group col-md-6">' +
                        '                       <label>Weight (kg)</label>' +
                        '                       <input id="product-weight-weight-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'weight\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-5 form-group">' +
                        '                       <label>Price ($)</label>' +
                        '                       <input id="product-weight-price-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'price_weight\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-1">' +
                        '                       <i id="product-weight-remove-icon-'+data+'" class="fa fa-minus-circle remove-icon" title="Remove This" onclick="removePriceCombination(\'weight\', '+data+');"></i>' +
                        '                   </div>' +
                        '               </div>';
                }
                else if(window.productDetailsPayment == 4) {
                    var appendHtml = '  <div id="product-size-container-'+data+'">' +
                        '                   <div class="form-group col-md-6">' +
                        '                       <label>Size Name (Ex: Small, Large)</label>' +
                        '                       <input id="product-size-size-'+data+'" type="text" class="form-control" onchange="saveProductDetails(this, \'size\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-5 form-group">' +
                        '                       <label>Price ($)</label>' +
                        '                       <input id="product-size-price-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'price_size\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-1">' +
                        '                       <i id="product-size-remove-icon-'+data+'" class="fa fa-minus-circle remove-icon" title="Remove This" onclick="removePriceCombination(\'size\', '+data+');"></i>' +
                        '                   </div>' +
                        '               </div>';
                }
                else if(window.productDetailsPayment == 5) {
                    var appendHtml = '  <div id="product-area-container-'+data+'">' +
                        '                   <div class="form-group col-md-6">' +
                        '                       <label>Area (m2)</label>' +
                        '                       <input id="product-area-area-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'area\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-5 form-group">' +
                        '                       <label>Price ($)</label>' +
                        '                       <input id="product-area-price-'+data+'" type="number" class="form-control only-digits" onchange="saveProductDetails(this, \'price_area\', '+data+');" value="" />' +
                        '                   </div>' +
                        '                   <div class="col-md-1">' +
                        '                       <i id="product-area-remove-icon-'+data+'" class="fa fa-minus-circle remove-icon" title="Remove This" onclick="removePriceCombination(\'area\', '+data+');"></i>' +
                        '                   </div>' +
                        '               </div>';
                }


                el.append(appendHtml);
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 188]
function ChangeProductPriceOption() {
    //develop the function here
    var priceOption = $('#product-price-options').val();

    var el = $('#product-price-container');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/changeproductpriceoption',
        data: {productid:window.productId, priceoption:priceOption}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                window.productDetailsPayment = priceOption;

                if(priceOption == 1) {

                    $('#price-volume').fadeOut(200);
                    $('#price-weight').fadeOut(200);
                    $('#price-area').fadeOut(200);
                    $('#price-size').fadeOut(200);
                    $('#product-add-more-icon').fadeOut(200);

                    setTimeout(function () {
                        $('#price-each').fadeIn(200, function () {
                            unBlockUI(el);
                        });
                    }, 220);

                }
                else if(priceOption == 2) {
                    $('#price-each').fadeOut(200);
                    $('#price-weight').fadeOut(200);
                    $('#price-area').fadeOut(200);
                    $('#price-size').fadeOut(200);
                    $('#product-add-more-icon').fadeIn(200);

                    setTimeout(function () {
                        $('#price-volume').fadeIn(200, function () {
                            unBlockUI(el);
                        });
                    }, 220);

                }
                else if(priceOption == 3) {
                    $('#price-volume').fadeOut(200);
                    $('#price-size').fadeOut(200);
                    $('#price-area').fadeOut(200);
                    $('#price-each').fadeOut(200);
                    $('#product-add-more-icon').fadeIn(200);

                    setTimeout(function () {
                        $('#price-weight').fadeIn(200, function () {
                            unBlockUI(el);
                        });
                    }, 220);

                }
                else if(priceOption == 4) {
                    $('#price-volume').fadeOut(200);
                    $('#price-each').fadeOut(200);
                    $('#price-weight').fadeOut(200);
                    $('#price-area').fadeOut(200);
                    $('#product-add-more-icon').fadeIn(200);

                    setTimeout(function () {
                        $('#price-size').fadeIn(200, function () {
                            unBlockUI(el);
                        });
                    }, 220);

                }
                else if(priceOption == 5) {
                    $('#price-volume').fadeOut(200);
                    $('#price-each').fadeOut(200);
                    $('#price-weight').fadeOut(200);
                    $('#price-size').fadeOut(200);
                    $('#product-add-more-icon').fadeIn(200);

                    setTimeout(function () {
                        $('#price-area').fadeIn(200, function () {
                            unBlockUI(el);
                        });
                    }, 220);
                }
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 190]
function removePriceCombination(option, detailId) {
    //develop the function here
    var el = $('#product-'+option+'-container-'+detailId);
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/removepricecombination',
        data: {detailid:detailId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                el.fadeOut(200, function () {
                    unBlockUI(el);
                    el.remove();
                });
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 194]
function changeSelectedImage(img) {
    //develop the function here
    $('#product-img-thumb-overlay-0').addClass('light-overlay');
    $('#product-img-thumb-overlay-1').addClass('light-overlay');
    $('#product-img-thumb-overlay-2').addClass('light-overlay');
    $('#product-img-thumb-overlay-3').addClass('light-overlay');
    $('#product-img-thumb-overlay-'+img).removeClass('light-overlay');
    window.selectedImgType = img;

    if(img == 0) {
        $("#product-img-title").text('Main Image');
        var imgUrl = $('#img-thumb-'+img).attr('src');
        $('#img-main-img').attr('src', imgUrl);
        $('#img-product-img-type').val(1);
    }
    else if(img == 1) {
        $("#product-img-title").text('Hover Image');
        var imgUrl = $('#img-thumb-'+img).attr('src');
        $('#img-main-img').attr('src', imgUrl);
        $('#img-product-img-type').val(2);
    }
    else if(img == 2) {
        $("#product-img-title").text('Other Image');
        var imgUrl = $('#img-thumb-other-'+img).attr('src');
        $('#img-main-img').attr('src', imgUrl);
        $('#img-product-img-type').val(3);
    }
    else if(img == 3) {
        $("#product-img-title").text('Other Image');
        var imgUrl = $('#img-thumb-other-'+img).attr('src');
        $('#img-main-img').attr('src', imgUrl);
        $('#img-product-img-type').val(4);
    }
}

// [ID: 195]
function submitImageUploadForm(id) {
    $(id).submit();
}


// [ID: 196]
function iSwitchOnChange(id, status) {
    if(id.substring(0, 16) == 'menu-item-status') {
        var menuItemId = id.slice(17, 50);
        var el = $('#categories-tab');
        blockUI(el);

        // [ID: 211]
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/changemenuitemstatus',
            data: {menuitemid:menuItemId, status:status}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    unBlockUI(el);
                    if(status == 1) {
                        $('#menu-item-number-'+menuItemId).removeClass('menu-item-draft').text(res.order+'. ');
                        notifyDone('Your Category is LIVE now!');
                    }
                    else {
                        $('#menu-item-number-'+menuItemId).addClass('menu-item-draft').text('Draft - ');
                        notifyDone('Your Category is NOT LIVE!')
                    }

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }else {
        var el = $('#product-container');
        blockUI(el);
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/iswitchonchange',
            data: {productid:window.productId, status:status}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(status == 1) {
                        notifyDone('Your product successfully published to the main website!')
                    }
                    else {
                        notifyDone('Your website is not live now!')
                    }

                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

}

// [ID: 198]
$('#detailed-description').on('froalaEditor.blur', function (e, editor) {

    var value = $(this).find('.fr-view').html();
    var el = $('#detailed-description-container');

    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/savedetaileddescription',
        data: {productid:window.productId, value:value}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
});

// [ID: 203]
$('#additional-info').on('froalaEditor.blur', function (e, editor) {
    var value = $(this).find('.fr-view').html();
    var el = $('#additional-info');

    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/saveadditionalinfo',
        data: {productid:window.productId, value:value}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
});

// [ID: 200]
function getProductStatistics() {
    //develop the function here

    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'products/getproductstatistics',
        data: {productid:window.productId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

                var res = JSON.parse(data);

                // Chart details
                echartLine.setOption({
                    title: {
                        text: 'Product View & Sales',
                        subtext: ''
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        x: 220,
                        y: 40,
                        data: ['Views', 'Sales']
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            magicType: {
                                show: true,
                                title: {
                                    line: 'Line',
                                    bar: 'Bar',
                                    stack: 'Stack',
                                    tiled: 'Tiled'
                                },
                                type: ['line', 'bar', 'stack', 'tiled']
                            },
                            restore: {
                                show: true,
                                title: "Refresh"
                            },
                            saveAsImage: {
                                show: true,
                                title: "Save Image"
                            }
                        }
                    },
                    calculable: true,
                    xAxis: [{
                        type: 'category',
                        boundaryGap: false,
                        data: res.linechart.months
                    }],
                    yAxis: [{
                        type: 'value'
                    }],
                    series: [{
                        name: 'Sales',
                        type: 'line',
                        smooth: true,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    type: 'default'
                                }
                            }
                        },
                        data: res.linechart.sales
                    }, {
                        name: 'Views',
                        type: 'line',
                        smooth: true,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    type: 'default'
                                }
                            }
                        },
                        data: res.linechart.views
                    }]
                });
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}


//Line Char
//----------------------------------------------------------------
var theme = {
    color: [
        '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
        '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
    ],

    title: {
        itemGap: 8,
        textStyle: {
            fontWeight: 'normal',
            color: '#408829'
        }
    },

    dataRange: {
        color: ['#1f610a', '#97b58d']
    },

    toolbox: {
        color: ['#408829', '#408829', '#408829', '#408829']
    },

    tooltip: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#408829',
                type: 'dashed'
            },
            crossStyle: {
                color: '#408829'
            },
            shadowStyle: {
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

    dataZoom: {
        dataBackgroundColor: '#eee',
        fillerColor: 'rgba(64,136,41,0.2)',
        handleColor: '#408829'
    },
    grid: {
        borderWidth: 0
    },

    categoryAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },

    valueAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitArea: {
            show: true,
            areaStyle: {
                color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },
    timeline: {
        lineStyle: {
            color: '#408829'
        },
        controlStyle: {
            normal: {color: '#408829'},
            emphasis: {color: '#408829'}
        }
    },

    k: {
        itemStyle: {
            normal: {
                color: '#68a54a',
                color0: '#a9cba2',
                lineStyle: {
                    width: 1,
                    color: '#408829',
                    color0: '#86b379'
                }
            }
        }
    },
    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            },
            emphasis: {
                areaStyle: {
                    color: '#99d2dd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            }
        }
    },
    force: {
        itemStyle: {
            normal: {
                linkStyle: {
                    strokeColor: '#408829'
                }
            }
        }
    },
    chord: {
        padding: 4,
        itemStyle: {
            normal: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },
    gauge: {
        startAngle: 225,
        endAngle: -45,
        axisLine: {
            show: true,
            lineStyle: {
                color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                width: 8
            }
        },
        axisTick: {
            splitNumber: 10,
            length: 12,
            lineStyle: {
                color: 'auto'
            }
        },
        axisLabel: {
            textStyle: {
                color: 'auto'
            }
        },
        splitLine: {
            length: 18,
            lineStyle: {
                color: 'auto'
            }
        },
        pointer: {
            length: '90%',
            color: 'auto'
        },
        title: {
            textStyle: {
                color: '#333'
            }
        },
        detail: {
            textStyle: {
                color: 'auto'
            }
        }
    },
    textStyle: {
        fontFamily: 'Arial, Verdana, sans-serif'
    }
};

//Line chart data
//--------------------------------
var echartLine = echarts.init(document.getElementById('echart_line'), theme);

function gridDragged() {
    alert(1);
}

$('.grid-stack').on('dragstop', function (event, ui) {
    setTimeout(function () {
        $('.menu-item').each(function () {
            var id = $(this).attr('data-menuid');
            var gridX = parseInt($(this).attr('data-gs-x'));
            var gridY = parseInt($(this).attr('data-gs-y'));

            var gridOrder = (gridX+4)/4;
            if(gridY == 0) {
                gridOrder = gridOrder+(gridY);
            }
            else {
                gridOrder = gridOrder+(gridY-1);
            }


            console.log(id+ ' X:'+ gridX+ ' Y:'+gridY+' ## Order:'+gridOrder);
        });
    }, 200);
});

// [ID: 205]
$('.grid-stack').on('dragstop', function (event, ui) {
    var el = $('#tab_categories');
    blockUI(el);
    var menuOrder = [];

    setTimeout(function () {
        $('.menu-item').each(function () {
            var id = $(this).attr('data-menuid');
            var gridX = parseInt($(this).attr('data-gs-x'));
            var gridY = parseInt($(this).attr('data-gs-y'));

            var gridOrder = (gridX+4)/4;
            if(gridY == 0) {
                gridOrder = gridOrder+(gridY);
            }
            else {
                gridOrder = gridOrder+(gridY-1);
            }


            // console.log(id+ ' X:'+ gridX+ ' Y:'+gridY+' ## Order:'+gridOrder);
            gridOrder = JSON.parse(gridOrder);
            menuOrder.push({id:id, order:gridOrder});
        });

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/setupmenuorder',
            data: {menuorder:menuOrder}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var i = 1;
                    $('.menu-item').each(function () {
                        var id = $(this).attr('data-menuid');
                        $('#menu-item-number-'+id).text(i);
                        i++;
                    });
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }, 200);
});

// [ID: 207]
function updateMenuItemName(menuItemId) {
    //develop the function here
    var value = $.trim($('#txt-menu-item-name-'+menuItemId).val());
    var el = $('#tab_categories');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/updatemenuitemname',
        data: {menuitemid:menuItemId, value:value}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 209]
function addMenuItem(menuId) {
    //develop the function here
    var el = $('#tab_categories');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/addmenuitem',
        data: {menuid:menuId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var items = [
                    {x: 0, y: 0, width: 2, height: 2},
                    {x: 3, y: 1, width: 1, height: 2},
                    {x: 4, y: 1, width: 1, height: 1},
                    {x: 2, y: 3, width: 3, height: 1},
//                    {x: 1, y: 4, width: 1, height: 1},
//                    {x: 1, y: 3, width: 1, height: 1},
//                    {x: 2, y: 4, width: 1, height: 1},
                    {x: 0, y: 0, width: 4, height: 4}
                ];

                var grid = $('.grid-stack').data('gridstack');

                var node = items.pop() || {
                    x: 12 * Math.random(),
                    y: 5 * Math.random(),
                    width: 4,
                    height: 4
                };
                grid.addWidget($('<div id="menu-item-'+data+'" style="padding: 20px;" data-gs-no-resize="true">' +
                    '                       <div class="grid-stack-item-content" style="box-shadow: 1px 1px 2px 3px #f7f6f6; border-radius: 10px; padding: 10px;">' +
                    '                           <div style="position: absolute; right: 10px; top: 10px;">' +
                    '                               <input data-id = "menu-item-status-'+data+'" type = "checkbox" class = "ios-switch ios-switch-primary iswitch-xs pull-right" title="Publish Menu Item" />' +
                    '                           </div>' +
                    '                           <h4 style="text-align: center;"><span id="menu-item-number-'+data+'" class="menu-item-draft">Draft - </span> <input id="txt-menu-item-name-'+data+'" placeholder="Menu Item Name..." type="text" class="input-trans" value="" style="height: 25px;" onchange="updateMenuItemName('+data+');" /> </h4>' +
                    '                       </div>' +
                    '                   <div/>'),
                    node.x, node.y, node.width, node.height);

                $(".ios-switch").each(function(){
                    mySwitch = new Switch(this);
                });

                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}


function flipDiv(menuItemId) {
    $('#flipped-class-'+menuItemId).toggleClass('flipped');
}

// [ID: 213]
function sortMenuSubItems() {
    //develop the function here

    setTimeout(function () {
        var i = 1;
        var menuOrder = [];
        var menuIdList = [];

        $('.accordion').each(function () {
            menuIdList.push($(this).attr('data-menuid'));
        });

        for(var j = 0; j < menuIdList.length; j++) {
            $('#accordion-menu-category-'+menuIdList[j]+' .panel').each(function () {
                var menuItemId = $(this).attr('data-menuitemid');
                var containerId = $(this).parent().attr('data-container');
                var mainMenuId = $(this).parent().attr('data-menuid');

                //color change
                console.log('itemid: '+menuItemId+' ## containerId: '+containerId);
                if(containerId != 0) {
                    $('#category-'+menuItemId).attr('style', 'background:#F2F5F7 !important');
                }
                else {
                    $('#sub-category-'+menuItemId).attr('style', 'background:#f5f2f7 !important');
                }

                menuOrder.push({id:menuItemId, order:i, menuid:menuIdList[j], mainmenuid:mainMenuId, container:containerId});
                i++
            });
        }


        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/sortmenusubitems',
            data: {menuorder:menuOrder}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }, 100);
}

// [ID: 298]
function getMenu() {
    //develop the function here

    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'products/getmenu',
        data: {}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 300]
function getCategoryDetails() {
    //develop the function here

    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'products/getcategorydetails',
        data: {}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 302]
function saveProductCategory() {
    //develop the function here
    var value = $('#drp-product-category').val();

    var el = $('#select-category-container');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/saveproductcategory',
        data: {productid:window.productId, value:value}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);

                var appendOptions = '<option value="0">Non-selected</option>';

                for(var i = 0; i < res.length; i++) {
                    appendOptions += '<option value="'+res[i].id+'">'+res[i].name+'</option>';
                }

                $('#drp-product-sub-cateogory').html(appendOptions);

                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

//Public API [ID: 304]
function getCategoryProducts() {
    //develop the function here
    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/getcategoryproducts',
        data: {}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 310]
function saveSubCategory() {
    //develop the function here
    var value = $('#drp-product-sub-cateogory').val();

    var el = $('#select-category-container');
    blockUI(el);

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/savesubcategory',
        data: {productid:window.productId, value:value}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);

                var keywords = $('#search-keywords').val();
                keywords = keywords.replace(', '+res, '');

                var subCategoryName = $("#drp-product-sub-cateogory option:selected").text();
                $('#search-keywords').val(keywords+', '+subCategoryName);
                unBlockUI(el);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

// [ID: 312]
function getProductDetails() {
    //develop the function here

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/getproductdetails',
        data: {}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

//Public product search [ID: 316]
function searchProducts() {
    //develop the function here

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/searchproducts',
        data: {}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function getApiTest() {
    console.log('get...get..goo');
    var api_key = 'SEM30004A282E685FE2A6214387421CCB848';
    var api_secret = 'ZDY3MjQ4OGZjZDYzZTE4YmFkOTAxNWE4N2M1NDAzOTM';
    var sem3 = require(window.domain+'assets/js/products/main.js')(api_key,api_secret);

    /*
     Sample query for retrieving products using Semantics3 Products API:

     The sample query shown below returns Toshiba branded products, belonging to the electronics category (ID 13658), that weigh >=1.0kg and <1.5kg (1500000 mg) and have retailed recently on the website newegg.com for >=USD 100, sorted in descending order of product name, limited to 5 results.
    */

// Build the query
    sem3.products.products_field( "cat_id", 4992 );
    sem3.products.products_field( "brand", "Toshiba" );
    sem3.products.products_field( "weight", "gte", 1000000 );
    sem3.products.products_field( "weight", "lt", 1500000 );
    sem3.products.products_field( "sitedetails", "name", "newegg.com" );
    sem3.products.products_field( "sitedetails", "latestoffers", "currency", "USD" );
    sem3.products.products_field( "sitedetails", "latestoffers", "price", "gte", 100 );

// Let's make a modification - say we no longer want the weight to be greater than 1000000 attribute
    sem3.products.remove( "products", "brand", "weight" );

// Let's view the JSON query we just constructed. This is a good starting point to debug, if you are getting incorrect
// results for your query
    var constructedJson = sem3.products.get_query_json( "products" );
    console.log( constructedJson );

// Make the query
    sem3.products.get_products(
        function(err, products) {
            if (err) {
                console.log("Couldn't execute query: get_products");
                return;
            }
            // View the results of the query
            console.log("Results of query:" + JSON.stringify( products ));
        }
    );

}

// [ID: 348]
function getGlobalProducts() {
    //develop the function here

    //ajax method
    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'products/getglobalproducts',
        data: {productid:window.productId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                // dbError(); //because this is a background operation, do errors will be displayed
            }
            else {
                var res = JSON.parse(data);
                var appendHtml = '';
                
                for(var i = 0; i < res.length; i++) {
                    //setup image url
                    var imgUrl = window.domain+'/assets/backend/images/products/products/'+res[i].imgid;

                    //setup small description
                    var smallDescription = res[i].small_description;
                    smallDescription = smallDescription.replace(/<\/?[^>]+(>|$)/g, "");
                    smallDescription = smallDescription.substring(0, 150);


                    appendHtml += '<div class="col-md-12" onclick="saveGlobalProduct('+res[i].id+');">' +
                        '                        <div class="global-product col-md-12">' +
                        '                            <div class="col-md-4">' +
                        '                                <img src="'+imgUrl+'" style="max-width: 100%"/>' +
                        '                            </div>' +
                        '                            <div class="col-md-8">' +
                        '                                <h5>'+res[i].name+'</h5>' +
                        '                                <p style="text-align: justify;">'+smallDescription+'...</p>' +
                        '                            </div>' +
                        '                        </div>' +
                        '                    </div>';
                }

                $('#global-product-container').html(appendHtml);
                $('#trigger-global-product-list').trigger('click');
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
        }
    });
}
                    
    // [ID: 350]
    function saveGlobalProduct(globalProductId) {
        //develop the function here
        var el = $('#model-global-product-list');
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'products/saveglobalproduct',
            data: {productid:window.productId, globalid:globalProductId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    // window.location.reload();
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

