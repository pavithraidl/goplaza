
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-12 14:24:29.
 * Controller ID - 68
 */

'use strict';
//region Global Variables

//endregion

//region Default settings

//endregion

//region Events
$(document).ready(function () {
    window.orgId = $('#orgid').val();
    getProductSections(window.orgId);
});

//endregion

                    
    // [ID: 174]
    function getProductSections(orgId) {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'website/getproductsections',
            data: {orgid:orgId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    var appendHtml = '';

                    for(var i = 0; i < res.length; i++) {
                        var products = '';

                        console.log(res[i].products.length);
                        for (var j = 0; j < res[i].products.length; j++) {
                            products +=' <div class="product clearfix">' +
                                '                            <div class="product-image">' +
                                '                                <a href="#"><img src="'+window.domain+'/assets/frontend/img/products/'+res[i].products[j].frontimg+'" alt="'+ res[i].products[j].name +' front image"></a>' +
                                '                                <a href="#"><img src="'+window.domain+'/assets/frontend/img/products/'+res[i].products[j].hoverimg+'" alt="'+ res[i].products[j].name +' back image"></a>' +
                                '                                <div class="sale-flash">50% Off*</div>' +
                                '                                <div class="product-overlay">' +
                                '                                    <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>' +
                                '                                    <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>' +
                                '                                </div>' +
                                '                            </div>' +
                                '                            <div class="product-desc">' +
                                '                                <div class="product-title"><h3><a href="#">'+ res[i].products[j].name +'</a></h3></div>' +
                                '                                <div class="product-price"><ins>$'+ res[i].products[j].price.toFixed(2) +'</ins> </div>' +
                                '                                <div class="product-rating">' +
                                '                                    <i class="icon-star3"></i>' +
                                '                                    <i class="icon-star3"></i>' +
                                '                                    <i class="icon-star3"></i>' +
                                '                                    <i class="icon-star3"></i>' +
                                '                                    <i class="icon-star-half-full"></i>' +
                                '                                </div>' +
                                '                            </div>' +
                                '                        </div>';
                        }
                        appendHtml += '<div class="item-grid">' +
                            '                    <h2>'+res[i].title+'</h2>' +
                            '                    <div id="grid-section-'+res[i].id+'" class="shop grid-container clearfix" data-layout="fitRows">' + products +
                            '                    </div><!-- #shop end -->' +
                            '                </div>';
                    }


                    $('#product-grids').html(appendHtml);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

