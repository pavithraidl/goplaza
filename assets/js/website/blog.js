
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Isuru ANZPharma
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-03-22 12:22:00.
 * Controller ID - 94
 */

'use strict';

$('#new-post-container').slideUp();

$(document).ready(function () {
    window.blogId = $('#input-blog-id').val();
});
                    
    // [ID: 364]
    function showHideNewPost() {
        if($('#new-post-container').is(':visible')) {
            $('#btn-new-post').removeClass('btn-danger').addClass('btn-success');
            $('#btn-new-post').html('<i class="fa fa-plus"></i> New Post');
            $('#btn-save-new-post').fadeOut(200);
        }
        else {
            $('#btn-new-post').removeClass('btn-success').addClass('btn-danger');
            $('#btn-new-post').html('<i class="fa fa-close"></i> Close');
            $('#btn-save-new-post').fadeIn(200);
        }

        $('#new-post-container').slideToggle();
    }

    $('#input-blog-title').on('change', function () {
        var value = $.trim($('#input-blog-title').val());
        saveData(value, 'title');
    })

    $('.edit').on('froalaEditor.blur', function (e, editor) {
        var value = $(this).find('.fr-view').html();
        saveData(value, 'content');
    });
                    
    // [ID: 367]
    function saveData(value, field) {
        //develop the function here
        var el = $('#input-section-container');
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/savedata',
            data: {value:value, field:field, blogid:window.blogId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

