
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-19 10:08:44.
 * Controller ID - 72
 */

'use strict';

                    
    // [ID: 202]
    function changeProductVariation(type, object) {
        if(type == 1) {
            var price = $('#product-variation-options').find(':selected').attr('data-price');
            $('#product-price').animateNumbers(price);
        }
        else if(type == 2) {
            var price = $('#'+object.id).attr('data-price');
            $('#product-price').animateNumbers(price);
            $('.btn-sizes').removeClass('btn-info').addClass('btn-default');
            $('.btn-sizes').removeAttr('disabled');
            $('#'+object.id).addClass('btn-info').removeClass('btn-default');
            $('#'+object.id).attr('disabled', 'disabled');
        }
    }

