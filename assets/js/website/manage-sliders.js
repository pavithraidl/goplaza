
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-25 22:06:11.
 * Controller ID - 80
 */

'use strict';
window.selectedSlider = '';
window.selectedSlide = '';
window.uploadingImgFieldId = null;

$(document).ready(function () {
    window.selectedSlider = $('#slider-id').val();
    window.selectedSlide = $('#slide-id').val();

    // $('#slider-control-prev').hide();
    // $('#slider-control-next').hide();
    var opacity = $('#control-button-container').attr('style');

    if(opacity != undefined) {
        $('#control-button-container').fadeTo(20, 0);
    }
    $('#icon-list-container').slideUp(200);


});
                    
    // [ID: 276]
    function editTitles() {
        var value = $.trim($('#input-slider-name').val());
        if(value != '') {
            $('#section-name-'+window.selectedSlider).html(value+' <i class="fa fa-chevron-right pull-right"></i> ');
        }
        else {
            if(window.selectedSlider == '') {
                $('#section-name-'+window.selectedSlider).html('Untitled <i class="fa fa-chevron-right pull-right"></i> ');
            }
            else {
                $('#section-name-'+window.selectedSlider).html('First Section <i class="fa fa-chevron-right pull-right"></i> ');
            }

        }
    }
                    
    // [ID: 278]
    function saveSlider(object, field, table) {
        var value = $('#'+object.id).val();
        var el = $('#second-section');
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/saveslider',
            data: {modelid:window.selectedSlider, field:field, table:table, value:value}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(window.selectedSlider == '') {
                        window.selectedSlider = data;
                        $('#slide-number').html('Slide 01');
                        $("#control-button-container").fadeTo(200, 1);
                    }
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 280]
    function addSlider() {
        //develop the function here
        //new slider append html
        var el = $('#website-sliders-block-container');
        blockUI(el);
        var appendHtml = '  <input type="hidden" id="slide-id" value="" />' +
            '               <div class="item active">' +
            '                   <div id="slide-field-container-0" class="row" style="padding: 20px">' +
            '                   </div>' +
            '               </div>';
        $('#slider-slides-container').html(appendHtml);
        $('.selected').removeClass('selected');
        $('.fa-chevron-right').remove();
        var newSection = '<li id="section-name-" onclick="selectSection(0)" class="selected">New Slider <i class="fa fa-chevron-right pull-right"></i> </li>';
        $("#website-slider-container").prepend(newSection);
        $('#second-section').fadeOut(200);
        $('#add-more-sliders-button').fadeOut(200);
        $('#input-slider-name').val('');
        window.selectedSlider = '';
        window.selectedSlide = '';
        $("#control-button-container").fadeTo(200, 0);
        unBlockUI(el);
        $('#input-slider-name').focus();
    }
                    

    // [ID: 283]
    function addEditors(type) {
        //develop the function here
        var el = $('#second-section');
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/addeditors',
            data: {sliderid:window.selectedSlider, slideid:window.selectedSlide, type:type}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);

                    var field = '';

                    if(type == 1) field = '<input id="slide-field-'+res.fieldid+'" onchange="saveContent(this, '+res.fieldid+', \'content\');" type="text" class="form-control" placeholder="Type content here..." />';
                    else if(type == 2) field = '<textarea id="slide-field-'+res.fieldid+'" class="form-control"  onchange="saveContent(this, '+res.fieldid+', \'content\');" placeholder="Type content here..."></textarea>';
                    else if(type == 3) field = '<div id="slide-field-'+res.fieldid+'" data-id="'+res.fieldid+'" class="edit"></div>';
                    else if(type == 4) field = '<div class="upload-img-container" style="position: relative; width: 85%; margin: 0 auto;" onclick="openUploadFileDialog('+res.fieldid+');">' +
                        '                           <div class="upload-img-overlay" onmouseenter="animateLivIcon('+res.fieldid+');">' +
                        '                               <h4 style="margin-bottom: 10%">Add/Change Slide Image</h4>' +
                        '                               <div id="upload-img-livicon-'+res.fieldid+'" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 65px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>' +
                        '                                   <p>Click Here to Upload!<br/><br/><span></span></p>' +
                        '                               </div>' +
                        '                               <img id="slider-slide-img-'+res.fieldid+'" src="'+window.domain+'assets/backend/images/website/slide-block/default.jpg" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>' +
                        '                               <div id = "image-upload-progress-'+res.fieldid+'" class = "progress" style = "height: 8px;width: 100%;">' +
                        '                                   <div id = "image-upload-progress-bar-'+res.fieldid+'" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">' +
                        '                                       <span id="image-upload-sr-only-'+res.fieldid+'" class = "sr-only">0% Complete</span>' +
                        '                                   </div>' +
                        '                               </div>' +
                        '                           </div>' +
                        '                           <form action="'+window.domain+'website/savefieldimage" method="post" id="upload-image-form-'+res.fieldid+'" enctype="multipart/form-data" class="slider-slide-image-upload">' +
                        '                               <input type="file" name="file" id="image-choose-'+res.fieldid+'" onchange="submitImageUploadForm(\'#upload-image-form-'+res.fieldid+'\', '+res.fieldid+');" accept="image/jpg;capture=camera" style="display: none;">' +
                        '                               <input type="hidden" name="fieldid" value="'+res.fieldid+'" />' +
                        '                           </form>';
                    //region icon field
                    else if(type == 5) field = '<div class="col-md-12" style="margin-bottom: 15px;">' +
                        '                           <div class="col-md-2">' +
                        '                               <div id="selected-icon-container-'+res.fieldid+'" class="icon-container">' +
                        '                                    <i class="fa fa-square-o"></i>' +
                        '                                </div>' +
                        '                                <input type="hidden" id="selected-icon-'+res.fieldid+'" onchange="saveContent(this, '+res.fieldid+', \'content\');" />' +
                        '                            </div>' +
                        '                            <div class="col-md-10">' +
                        '                                <div id="icon-list-container-'+res.fieldid+'" class="col-md-12" style="max-height: 200px; overflow-y: auto; display: none;">' +
                        '                                    <section id="web-application">' +
                        '                                        <h2 class="page-header">Web Application Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'adjust\');" href="#" ><i class="fa fa-adjust"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'anchor\');" href="#" ><i class="fa fa-anchor"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'archive\');" href="#" ><i class="fa fa-archive"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'area-chart\');" href="#" ><i class="fa fa-area-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows\');" href="#" ><i class="fa fa-arrows"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows-h\');" href="#" ><i class="fa fa-arrows-h"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows-v\');" href="#" ><i class="fa fa-arrows-v"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'asterisk\');" href="#" ><i class="fa fa-asterisk"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'at\');" href="#" ><i class="fa fa-at"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'car\');" href="#" ><i class="fa fa-automobile"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ban\');" href="#" ><i class="fa fa-ban"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'university\');" href="#" ><i class="fa fa-bank"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'barcode\');" href="#" ><i class="fa fa-barcode"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bars\');" href="#" ><i class="fa fa-bars"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'beer\');" href="#" ><i class="fa fa-beer"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bell\');" href="#" ><i class="fa fa-bell"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bell-o\');" href="#" ><i class="fa fa-bell-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bell-slash\');" href="#" ><i class="fa fa-bell-slash"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bell-slash-o\');" href="#" ><i class="fa fa-bell-slash-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bicycle\');" href="#" ><i class="fa fa-bicycle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'binoculars\');" href="#" ><i class="fa fa-binoculars"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'birthday-cake\');" href="#" ><i class="fa fa-birthday-cake"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bolt\');" href="#" ><i class="fa fa-bolt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bomb\');" href="#" ><i class="fa fa-bomb"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'book\');" href="#" ><i class="fa fa-book"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bookmark\');" href="#" ><i class="fa fa-bookmark"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bookmark-o\');" href="#" ><i class="fa fa-bookmark-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'briefcase\');" href="#" ><i class="fa fa-briefcase"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bug\');" href="#" ><i class="fa fa-bug"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'building\');" href="#" ><i class="fa fa-building"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'building-o\');" href="#" ><i class="fa fa-building-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bullhorn\');" href="#" ><i class="fa fa-bullhorn"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bullseye\');" href="#" ><i class="fa fa-bullseye"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bus\');" href="#" ><i class="fa fa-bus"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'taxi\');" href="#" ><i class="fa fa-cab"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'calculator\');" href="#" ><i class="fa fa-calculator"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'calendar\');" href="#" ><i class="fa fa-calendar"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'calendar-o\');" href="#" ><i class="fa fa-calendar-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'camera\');" href="#" ><i class="fa fa-camera"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'camera-retro\');" href="#" ><i class="fa fa-camera-retro"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'car\');" href="#" ><i class="fa fa-car"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-down\');" href="#" ><i class="fa fa-caret-square-o-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-left\');" href="#" ><i class="fa fa-caret-square-o-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-right\');" href="#" ><i class="fa fa-caret-square-o-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-up\');" href="#" ><i class="fa fa-caret-square-o-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc\');" href="#" ><i class="fa fa-cc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'certificate\');" href="#" ><i class="fa fa-certificate"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check\');" href="#" ><i class="fa fa-check"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check-circle\');" href="#" ><i class="fa fa-check-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check-circle-o\');" href="#" ><i class="fa fa-check-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check-square\');" href="#" ><i class="fa fa-check-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check-square-o\');" href="#" ><i class="fa fa-check-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'child\');" href="#" ><i class="fa fa-child"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle\');" href="#" ><i class="fa fa-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle-o\');" href="#" ><i class="fa fa-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle-o-notch\');" href="#" ><i class="fa fa-circle-o-notch"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle-thin\');" href="#" ><i class="fa fa-circle-thin"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'clock-o\');" href="#" ><i class="fa fa-clock-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'times\');" href="#" ><i class="fa fa-close"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cloud\');" href="#" ><i class="fa fa-cloud"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cloud-download\');" href="#" ><i class="fa fa-cloud-download"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cloud-upload\');" href="#" ><i class="fa fa-cloud-upload"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'code\');" href="#" ><i class="fa fa-code"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'code-fork\');" href="#" ><i class="fa fa-code-fork"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'coffee\');" href="#" ><i class="fa fa-coffee"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cog\');" href="#" ><i class="fa fa-cog"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cogs\');" href="#" ><i class="fa fa-cogs"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'comment\');" href="#" ><i class="fa fa-comment"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'comment-o\');" href="#" ><i class="fa fa-comment-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'comments\');" href="#" ><i class="fa fa-comments"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'comments-o\');" href="#" ><i class="fa fa-comments-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'compass\');" href="#" ><i class="fa fa-compass"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'copyright\');" href="#" ><i class="fa fa-copyright"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'credit-card\');" href="#" ><i class="fa fa-credit-card"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'crop\');" href="#" ><i class="fa fa-crop"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'crosshairs\');" href="#" ><i class="fa fa-crosshairs"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cube\');" href="#" ><i class="fa fa-cube"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cubes\');" href="#" ><i class="fa fa-cubes"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cutlery\');" href="#" ><i class="fa fa-cutlery"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tachometer\');" href="#" ><i class="fa fa-dashboard"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'database\');" href="#" ><i class="fa fa-database"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'desktop\');" href="#" ><i class="fa fa-desktop"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'dot-circle-o\');" href="#" ><i class="fa fa-dot-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'download\');" href="#" ><i class="fa fa-download"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pencil-square-o\');" href="#" ><i class="fa fa-edit"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ellipsis-h\');" href="#" ><i class="fa fa-ellipsis-h"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ellipsis-v\');" href="#" ><i class="fa fa-ellipsis-v"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'envelope\');" href="#" ><i class="fa fa-envelope"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'envelope-o\');" href="#" ><i class="fa fa-envelope-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'envelope-square\');" href="#" ><i class="fa fa-envelope-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eraser\');" href="#" ><i class="fa fa-eraser"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'exchange\');" href="#" ><i class="fa fa-exchange"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'exclamation\');" href="#" ><i class="fa fa-exclamation"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'exclamation-circle\');" href="#" ><i class="fa fa-exclamation-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'exclamation-triangle\');" href="#" ><i class="fa fa-exclamation-triangle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'external-link\');" href="#" ><i class="fa fa-external-link"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'external-link-square\');" href="#" ><i class="fa fa-external-link-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eye\');" href="#" ><i class="fa fa-eye"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eye-slash\');" href="#" ><i class="fa fa-eye-slash"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eyedropper\');" href="#" ><i class="fa fa-eyedropper"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'fax\');" href="#" ><i class="fa fa-fax"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'female\');" href="#" ><i class="fa fa-female"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'fighter-jet\');" href="#" ><i class="fa fa-fighter-jet"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-archive-o\');" href="#" ><i class="fa fa-file-archive-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-audio-o\');" href="#" ><i class="fa fa-file-audio-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-code-o\');" href="#" ><i class="fa fa-file-code-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-excel-o\');" href="#" ><i class="fa fa-file-excel-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-image-o\');" href="#" ><i class="fa fa-file-image-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-video-o\');" href="#" ><i class="fa fa-file-movie-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-pdf-o\');" href="#" ><i class="fa fa-file-pdf-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-image-o\');" href="#" ><i class="fa fa-file-photo-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-image-o\');" href="#" ><i class="fa fa-file-picture-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-powerpoint-o\');" href="#" ><i class="fa fa-file-powerpoint-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-audio-o\');" href="#" ><i class="fa fa-file-sound-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-video-o\');" href="#" ><i class="fa fa-file-video-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-word-o\');" href="#" ><i class="fa fa-file-word-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-archive-o\');" href="#" ><i class="fa fa-file-zip-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'film\');" href="#" ><i class="fa fa-film"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'filter\');" href="#" ><i class="fa fa-filter"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'fire\');" href="#" ><i class="fa fa-fire"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'fire-extinguisher\');" href="#" ><i class="fa fa-fire-extinguisher"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'flag\');" href="#" ><i class="fa fa-flag"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'flag-checkered\');" href="#" ><i class="fa fa-flag-checkered"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'flag-o\');" href="#" ><i class="fa fa-flag-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bolt\');" href="#" ><i class="fa fa-flash"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'flask\');" href="#" ><i class="fa fa-flask"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'folder\');" href="#" ><i class="fa fa-folder"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'folder-o\');" href="#" ><i class="fa fa-folder-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'folder-open\');" href="#" ><i class="fa fa-folder-open"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'folder-open-o\');" href="#" ><i class="fa fa-folder-open-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'frown-o\');" href="#" ><i class="fa fa-frown-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'futbol-o\');" href="#" ><i class="fa fa-futbol-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'gamepad\');" href="#" ><i class="fa fa-gamepad"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'gavel\');" href="#" ><i class="fa fa-gavel"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cog\');" href="#" ><i class="fa fa-gear"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cogs\');" href="#" ><i class="fa fa-gears"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'gift\');" href="#" ><i class="fa fa-gift"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'glass\');" href="#" ><i class="fa fa-glass"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'globe\');" href="#" ><i class="fa fa-globe"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'graduation-cap\');" href="#" ><i class="fa fa-graduation-cap"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'users\');" href="#" ><i class="fa fa-group"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hdd-o\');" href="#" ><i class="fa fa-hdd-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'headphones\');" href="#" ><i class="fa fa-headphones"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'heart\');" href="#" ><i class="fa fa-heart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'heart-o\');" href="#" ><i class="fa fa-heart-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'history\');" href="#" ><i class="fa fa-history"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'home\');" href="#" ><i class="fa fa-home"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'picture-o\');" href="#" ><i class="fa fa-image"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'inbox\');" href="#" ><i class="fa fa-inbox"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'info\');" href="#" ><i class="fa fa-info"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'info-circle\');" href="#" ><i class="fa fa-info-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'university\');" href="#" ><i class="fa fa-institution"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'key\');" href="#" ><i class="fa fa-key"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'keyboard-o\');" href="#" ><i class="fa fa-keyboard-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'language\');" href="#" ><i class="fa fa-language"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'laptop\');" href="#" ><i class="fa fa-laptop"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'leaf\');" href="#" ><i class="fa fa-leaf"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'gavel\');" href="#" ><i class="fa fa-legal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'lemon-o\');" href="#" ><i class="fa fa-lemon-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'level-down\');" href="#" ><i class="fa fa-level-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'level-up\');" href="#" ><i class="fa fa-level-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'life-ring\');" href="#" ><i class="fa fa-life-bouy"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'life-ring\');" href="#" ><i class="fa fa-life-buoy"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'life-ring\');" href="#" ><i class="fa fa-life-ring"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'life-ring\');" href="#" ><i class="fa fa-life-saver"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'lightbulb-o\');" href="#" ><i class="fa fa-lightbulb-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'line-chart\');" href="#" ><i class="fa fa-line-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'location-arrow\');" href="#" ><i class="fa fa-location-arrow"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'lock\');" href="#" ><i class="fa fa-lock"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'magic\');" href="#" ><i class="fa fa-magic"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'magnet\');" href="#" ><i class="fa fa-magnet"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share\');" href="#" ><i class="fa fa-mail-forward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'reply\');" href="#" ><i class="fa fa-mail-reply"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'reply-all\');" href="#" ><i class="fa fa-mail-reply-all"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'male\');" href="#" ><i class="fa fa-male"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'map-marker\');" href="#" ><i class="fa fa-map-marker"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'meh-o\');" href="#" ><i class="fa fa-meh-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'microphone\');" href="#" ><i class="fa fa-microphone"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'microphone-slash\');" href="#" ><i class="fa fa-microphone-slash"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'minus\');" href="#" ><i class="fa fa-minus"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'minus-circle\');" href="#" ><i class="fa fa-minus-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'minus-square\');" href="#" ><i class="fa fa-minus-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'minus-square-o\');" href="#" ><i class="fa fa-minus-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'mobile\');" href="#" ><i class="fa fa-mobile"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'mobile\');" href="#" ><i class="fa fa-mobile-phone"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'money\');" href="#" ><i class="fa fa-money"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'moon-o\');" href="#" ><i class="fa fa-moon-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'graduation-cap\');" href="#" ><i class="fa fa-mortar-board"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'music\');" href="#" ><i class="fa fa-music"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bars\');" href="#" ><i class="fa fa-navicon"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'newspaper-o\');" href="#" ><i class="fa fa-newspaper-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paint-brush\');" href="#" ><i class="fa fa-paint-brush"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paper-plane\');" href="#" ><i class="fa fa-paper-plane"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paper-plane-o\');" href="#" ><i class="fa fa-paper-plane-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paw\');" href="#" ><i class="fa fa-paw"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pencil\');" href="#" ><i class="fa fa-pencil"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pencil-square\');" href="#" ><i class="fa fa-pencil-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pencil-square-o\');" href="#" ><i class="fa fa-pencil-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'phone\');" href="#" ><i class="fa fa-phone"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'phone-square\');" href="#" ><i class="fa fa-phone-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'picture-o\');" href="#" ><i class="fa fa-photo"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'picture-o\');" href="#" ><i class="fa fa-picture-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pie-chart\');" href="#" ><i class="fa fa-pie-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plane\');" href="#" ><i class="fa fa-plane"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plug\');" href="#" ><i class="fa fa-plug"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus\');" href="#" ><i class="fa fa-plus"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus-circle\');" href="#" ><i class="fa fa-plus-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus-square\');" href="#" ><i class="fa fa-plus-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus-square-o\');" href="#" ><i class="fa fa-plus-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'power-off\');" href="#" ><i class="fa fa-power-off"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'print\');" href="#" ><i class="fa fa-print"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'puzzle-piece\');" href="#" ><i class="fa fa-puzzle-piece"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'qrcode\');" href="#" ><i class="fa fa-qrcode"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'question\');" href="#" ><i class="fa fa-question"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'question-circle\');" href="#" ><i class="fa fa-question-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'quote-left\');" href="#" ><i class="fa fa-quote-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'quote-right\');" href="#" ><i class="fa fa-quote-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'random\');" href="#" ><i class="fa fa-random"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'recycle\');" href="#" ><i class="fa fa-recycle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'refresh\');" href="#" ><i class="fa fa-refresh"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'times\');" href="#" ><i class="fa fa-remove"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bars\');" href="#" ><i class="fa fa-reorder"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'reply\');" href="#" ><i class="fa fa-reply"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'reply-all\');" href="#" ><i class="fa fa-reply-all"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'retweet\');" href="#" ><i class="fa fa-retweet"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'road\');" href="#" ><i class="fa fa-road"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rocket\');" href="#" ><i class="fa fa-rocket"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rss\');" href="#" ><i class="fa fa-rss"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rss-square\');" href="#" ><i class="fa fa-rss-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'search\');" href="#" ><i class="fa fa-search"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'search-minus\');" href="#" ><i class="fa fa-search-minus"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'search-plus\');" href="#" ><i class="fa fa-search-plus"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paper-plane\');" href="#" ><i class="fa fa-send"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paper-plane-o\');" href="#" ><i class="fa fa-send-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share\');" href="#" ><i class="fa fa-share"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share-alt\');" href="#" ><i class="fa fa-share-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share-alt-square\');" href="#" ><i class="fa fa-share-alt-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share-square\');" href="#" ><i class="fa fa-share-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share-square-o\');" href="#" ><i class="fa fa-share-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'shield\');" href="#" ><i class="fa fa-shield"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'shopping-cart\');" href="#" ><i class="fa fa-shopping-cart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sign-in\');" href="#" ><i class="fa fa-sign-in"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sign-out\');" href="#" ><i class="fa fa-sign-out"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'signal\');" href="#" ><i class="fa fa-signal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sitemap\');" href="#" ><i class="fa fa-sitemap"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sliders\');" href="#" ><i class="fa fa-sliders"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'smile-o\');" href="#" ><i class="fa fa-smile-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'futbol-o\');" href="#" ><i class="fa fa-soccer-ball-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort\');" href="#" ><i class="fa fa-sort"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-alpha-asc\');" href="#" ><i class="fa fa-sort-alpha-asc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-alpha-desc\');" href="#" ><i class="fa fa-sort-alpha-desc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-amount-asc\');" href="#" ><i class="fa fa-sort-amount-asc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-amount-desc\');" href="#" ><i class="fa fa-sort-amount-desc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-asc\');" href="#" ><i class="fa fa-sort-asc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-desc\');" href="#" ><i class="fa fa-sort-desc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-desc\');" href="#" ><i class="fa fa-sort-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-numeric-asc\');" href="#" ><i class="fa fa-sort-numeric-asc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-numeric-desc\');" href="#" ><i class="fa fa-sort-numeric-desc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort-asc\');" href="#" ><i class="fa fa-sort-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'space-shuttle\');" href="#" ><i class="fa fa-space-shuttle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'spinner\');" href="#" ><i class="fa fa-spinner"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'spoon\');" href="#" ><i class="fa fa-spoon"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'square\');" href="#" ><i class="fa fa-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'square-o\');" href="#" ><i class="fa fa-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'star\');" href="#" ><i class="fa fa-star"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'star-half\');" href="#" ><i class="fa fa-star-half"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'star-half-o\');" href="#" ><i class="fa fa-star-half-empty"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'star-half-o\');" href="#" ><i class="fa fa-star-half-full"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'star-half-o\');" href="#" ><i class="fa fa-star-half-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'star-o\');" href="#" ><i class="fa fa-star-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'suitcase\');" href="#" ><i class="fa fa-suitcase"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sun-o\');" href="#" ><i class="fa fa-sun-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'life-ring\');" href="#" ><i class="fa fa-support"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tablet\');" href="#" ><i class="fa fa-tablet"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tachometer\');" href="#" ><i class="fa fa-tachometer"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tag\');" href="#" ><i class="fa fa-tag"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tags\');" href="#" ><i class="fa fa-tags"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tasks\');" href="#" ><i class="fa fa-tasks"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'taxi\');" href="#" ><i class="fa fa-taxi"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'terminal\');" href="#" ><i class="fa fa-terminal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'thumb-tack\');" href="#" ><i class="fa fa-thumb-tack"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'thumbs-down\');" href="#" ><i class="fa fa-thumbs-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'thumbs-o-down\');" href="#" ><i class="fa fa-thumbs-o-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'thumbs-o-up\');" href="#" ><i class="fa fa-thumbs-o-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'thumbs-up\');" href="#" ><i class="fa fa-thumbs-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ticket\');" href="#" ><i class="fa fa-ticket"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'times\');" href="#" ><i class="fa fa-times"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'times-circle\');" href="#" ><i class="fa fa-times-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'times-circle-o\');" href="#" ><i class="fa fa-times-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tint\');" href="#" ><i class="fa fa-tint"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-down\');" href="#" ><i class="fa fa-toggle-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-left\');" href="#" ><i class="fa fa-toggle-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'toggle-off\');" href="#" ><i class="fa fa-toggle-off"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'toggle-on\');" href="#" ><i class="fa fa-toggle-on"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-right\');" href="#" ><i class="fa fa-toggle-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-up\');" href="#" ><i class="fa fa-toggle-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'trash\');" href="#" ><i class="fa fa-trash"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'trash-o\');" href="#" ><i class="fa fa-trash-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tree\');" href="#" ><i class="fa fa-tree"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'trophy\');" href="#" ><i class="fa fa-trophy"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'truck\');" href="#" ><i class="fa fa-truck"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tty\');" href="#" ><i class="fa fa-tty"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'umbrella\');" href="#" ><i class="fa fa-umbrella"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'university\');" href="#" ><i class="fa fa-university"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'unlock\');" href="#" ><i class="fa fa-unlock"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'unlock-alt\');" href="#" ><i class="fa fa-unlock-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'sort\');" href="#" ><i class="fa fa-unsorted"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'upload\');" href="#" ><i class="fa fa-upload"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'user\');" href="#" ><i class="fa fa-user"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'users\');" href="#" ><i class="fa fa-users"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'video-camera\');" href="#" ><i class="fa fa-video-camera"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'volume-down\');" href="#" ><i class="fa fa-volume-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'volume-off\');" href="#" ><i class="fa fa-volume-off"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'volume-up\');" href="#" ><i class="fa fa-volume-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'exclamation-triangle\');" href="#" ><i class="fa fa-warning"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'wheelchair\');" href="#" ><i class="fa fa-wheelchair"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'wifi\');" href="#" ><i class="fa fa-wifi"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'wrench\');" href="#" ><i class="fa fa-wrench"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="file-type">' +
                        '                                        <h2 class="page-header">File Type Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file\');" href="#" ><i class="fa fa-file"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-archive-o\');" href="#" ><i class="fa fa-file-archive-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-audio-o\');" href="#" ><i class="fa fa-file-audio-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-code-o\');" href="#" ><i class="fa fa-file-code-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-excel-o\');" href="#" ><i class="fa fa-file-excel-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-image-o\');" href="#" ><i class="fa fa-file-image-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-video-o\');" href="#" ><i class="fa fa-file-movie-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-o\');" href="#" ><i class="fa fa-file-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-pdf-o\');" href="#" ><i class="fa fa-file-pdf-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-image-o\');" href="#" ><i class="fa fa-file-photo-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-image-o\');" href="#" ><i class="fa fa-file-picture-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-powerpoint-o\');" href="#" ><i class="fa fa-file-powerpoint-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-audio-o\');" href="#" ><i class="fa fa-file-sound-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-text\');" href="#" ><i class="fa fa-file-text"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-text-o\');" href="#" ><i class="fa fa-file-text-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-video-o\');" href="#" ><i class="fa fa-file-video-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-word-o\');" href="#" ><i class="fa fa-file-word-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-archive-o\');" href="#" ><i class="fa fa-file-zip-o"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="spinner">' +
                        '                                        <h2 class="page-header">Spinner Icons</h2>' +
                        '                                        <div class="alert alert-success">' +
                        '                                            <ul class="fa-ul">' +
                        '                                                <li>' +
                        '<i class="fa fa-info-circle fa-lg fa-li"></i> These icons work great with the <code>fa-spin</code> class. Check out the' +
                        '<a href="../examples/#spinning" class="alert-link">spinning icons example</a>.' +
                        '                                                </li>' +
                        '                                            </ul>' +
                        '                                        </div>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle-o-notch\');" href="#" ><i class="fa fa-circle-o-notch"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cog\');" href="#" ><i class="fa fa-cog"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cog\');" href="#" ><i class="fa fa-gear"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'refresh\');" href="#" ><i class="fa fa-refresh"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'spinner\');" href="#" ><i class="fa fa-spinner"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="form-control">' +
                        '                                        <h2 class="page-header">Form Control Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check-square\');" href="#" ><i class="fa fa-check-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'check-square-o\');" href="#" ><i class="fa fa-check-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle\');" href="#" ><i class="fa fa-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'circle-o\');" href="#" ><i class="fa fa-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'dot-circle-o\');" href="#" ><i class="fa fa-dot-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'minus-square\');" href="#" ><i class="fa fa-minus-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'minus-square-o\');" href="#" ><i class="fa fa-minus-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus-square\');" href="#" ><i class="fa fa-plus-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus-square-o\');" href="#" ><i class="fa fa-plus-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'square\');" href="#" ><i class="fa fa-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'square-o\');" href="#" ><i class="fa fa-square-o"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="payment">' +
                        '                                        <h2 class="page-header">Payment Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-amex\');" href="#" ><i class="fa fa-cc-amex"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-discover\');" href="#" ><i class="fa fa-cc-discover"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-mastercard\');" href="#" ><i class="fa fa-cc-mastercard"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-paypal\');" href="#" ><i class="fa fa-cc-paypal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-stripe\');" href="#" ><i class="fa fa-cc-stripe"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-visa\');" href="#" ><i class="fa fa-cc-visa"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'credit-card\');" href="#" ><i class="fa fa-credit-card"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'google-wallet\');" href="#" ><i class="fa fa-google-wallet"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paypal\');" href="#" ><i class="fa fa-paypal"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="chart">' +
                        '                                        <h2 class="page-header">Chart Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'area-chart\');" href="#" ><i class="fa fa-area-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'line-chart\');" href="#" ><i class="fa fa-line-chart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pie-chart\');" href="#" ><i class="fa fa-pie-chart"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="currency">' +
                        '                                        <h2 class="page-header">Currency Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'btc\');" href="#" ><i class="fa fa-bitcoin"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'btc\');" href="#" ><i class="fa fa-btc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'jpy\');" href="#" ><i class="fa fa-cny"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'usd\');" href="#" ><i class="fa fa-dollar"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eur\');" href="#" ><i class="fa fa-eur"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eur\');" href="#" ><i class="fa fa-euro"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'gbp\');" href="#" ><i class="fa fa-gbp"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ils\');" href="#" ><i class="fa fa-ils"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'inr\');" href="#" ><i class="fa fa-inr"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'jpy\');" href="#" ><i class="fa fa-jpy"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'krw\');" href="#" ><i class="fa fa-krw"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'money\');" href="#" ><i class="fa fa-money"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'jpy\');" href="#" ><i class="fa fa-rmb"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rub\');" href="#" ><i class="fa fa-rouble"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rub\');" href="#" ><i class="fa fa-rub"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rub\');" href="#" ><i class="fa fa-ruble"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'inr\');" href="#" ><i class="fa fa-rupee"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ils\');" href="#" ><i class="fa fa-shekel"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ils\');" href="#" ><i class="fa fa-sheqel"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'try\');" href="#" ><i class="fa fa-try"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'try\');" href="#" ><i class="fa fa-turkish-lira"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'usd\');" href="#" ><i class="fa fa-usd"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'krw\');" href="#" ><i class="fa fa-won"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'jpy\');" href="#" ><i class="fa fa-yen"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="text-editor">' +
                        '                                        <h2 class="page-header">Text Editor Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'align-center\');" href="#" ><i class="fa fa-align-center"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'align-justify\');" href="#" ><i class="fa fa-align-justify"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'align-left\');" href="#" ><i class="fa fa-align-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'align-right\');" href="#" ><i class="fa fa-align-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bold\');" href="#" ><i class="fa fa-bold"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'link\');" href="#" ><i class="fa fa-chain"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chain-broken\');" href="#" ><i class="fa fa-chain-broken"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'clipboard\');" href="#" ><i class="fa fa-clipboard"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'columns\');" href="#" ><i class="fa fa-columns"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'files-o\');" href="#" ><i class="fa fa-copy"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'scissors\');" href="#" ><i class="fa fa-cut"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'outdent\');" href="#" ><i class="fa fa-dedent"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eraser\');" href="#" ><i class="fa fa-eraser"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file\');" href="#" ><i class="fa fa-file"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-o\');" href="#" ><i class="fa fa-file-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-text\');" href="#" ><i class="fa fa-file-text"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'file-text-o\');" href="#" ><i class="fa fa-file-text-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'files-o\');" href="#" ><i class="fa fa-files-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'floppy-o\');" href="#" ><i class="fa fa-floppy-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'font\');" href="#" ><i class="fa fa-font"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'header\');" href="#" ><i class="fa fa-header"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'indent\');" href="#" ><i class="fa fa-indent"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'italic\');" href="#" ><i class="fa fa-italic"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'link\');" href="#" ><i class="fa fa-link"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'list\');" href="#" ><i class="fa fa-list"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'list-alt\');" href="#" ><i class="fa fa-list-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'list-ol\');" href="#" ><i class="fa fa-list-ol"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'list-ul\');" href="#" ><i class="fa fa-list-ul"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'outdent\');" href="#" ><i class="fa fa-outdent"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paperclip\');" href="#" ><i class="fa fa-paperclip"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paragraph\');" href="#" ><i class="fa fa-paragraph"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'clipboard\');" href="#" ><i class="fa fa-paste"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'repeat\');" href="#" ><i class="fa fa-repeat"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'undo\');" href="#" ><i class="fa fa-rotate-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'repeat\');" href="#" ><i class="fa fa-rotate-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'floppy-o\');" href="#" ><i class="fa fa-save"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'scissors\');" href="#" ><i class="fa fa-scissors"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'strikethrough\');" href="#" ><i class="fa fa-strikethrough"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'subscript\');" href="#" ><i class="fa fa-subscript"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'superscript\');" href="#" ><i class="fa fa-superscript"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'table\');" href="#" ><i class="fa fa-table"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'text-height\');" href="#" ><i class="fa fa-text-height"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'text-width\');" href="#" ><i class="fa fa-text-width"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'th\');" href="#" ><i class="fa fa-th"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'th-large\');" href="#" ><i class="fa fa-th-large"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'th-list\');" href="#" ><i class="fa fa-th-list"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'underline\');" href="#" ><i class="fa fa-underline"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'undo\');" href="#" ><i class="fa fa-undo"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chain-broken\');" href="#" ><i class="fa fa-unlink"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="directional">' +
                        '                                        <h2 class="page-header">Directional Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-double-down\');" href="#" ><i class="fa fa-angle-double-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-double-left\');" href="#" ><i class="fa fa-angle-double-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-double-right\');" href="#" ><i class="fa fa-angle-double-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-double-up\');" href="#" ><i class="fa fa-angle-double-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-down\');" href="#" ><i class="fa fa-angle-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-left\');" href="#" ><i class="fa fa-angle-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-right\');" href="#" ><i class="fa fa-angle-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angle-up\');" href="#" ><i class="fa fa-angle-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-down\');" href="#" ><i class="fa fa-arrow-circle-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-left\');" href="#" ><i class="fa fa-arrow-circle-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-o-down\');" href="#" ><i class="fa fa-arrow-circle-o-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-o-left\');" href="#" ><i class="fa fa-arrow-circle-o-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-o-right\');" href="#" ><i class="fa fa-arrow-circle-o-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-o-up\');" href="#" ><i class="fa fa-arrow-circle-o-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-right\');" href="#" ><i class="fa fa-arrow-circle-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-circle-up\');" href="#" ><i class="fa fa-arrow-circle-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-down\');" href="#" ><i class="fa fa-arrow-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-left\');" href="#" ><i class="fa fa-arrow-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-right\');" href="#" ><i class="fa fa-arrow-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrow-up\');" href="#" ><i class="fa fa-arrow-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows\');" href="#" ><i class="fa fa-arrows"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows-alt\');" href="#" ><i class="fa fa-arrows-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows-h\');" href="#" ><i class="fa fa-arrows-h"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows-v\');" href="#" ><i class="fa fa-arrows-v"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-down\');" href="#" ><i class="fa fa-caret-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-left\');" href="#" ><i class="fa fa-caret-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-right\');" href="#" ><i class="fa fa-caret-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-down\');" href="#" ><i class="fa fa-caret-square-o-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-left\');" href="#" ><i class="fa fa-caret-square-o-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-right\');" href="#" ><i class="fa fa-caret-square-o-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-up\');" href="#" ><i class="fa fa-caret-square-o-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-up\');" href="#" ><i class="fa fa-caret-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-circle-down\');" href="#" ><i class="fa fa-chevron-circle-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-circle-left\');" href="#" ><i class="fa fa-chevron-circle-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-circle-right\');" href="#" ><i class="fa fa-chevron-circle-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-circle-up\');" href="#" ><i class="fa fa-chevron-circle-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-down\');" href="#" ><i class="fa fa-chevron-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-left\');" href="#" ><i class="fa fa-chevron-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-right\');" href="#" ><i class="fa fa-chevron-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'chevron-up\');" href="#" ><i class="fa fa-chevron-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hand-o-down\');" href="#" ><i class="fa fa-hand-o-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hand-o-left\');" href="#" ><i class="fa fa-hand-o-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hand-o-right\');" href="#" ><i class="fa fa-hand-o-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hand-o-up\');" href="#" ><i class="fa fa-hand-o-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'long-arrow-down\');" href="#" ><i class="fa fa-long-arrow-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'long-arrow-left\');" href="#" ><i class="fa fa-long-arrow-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'long-arrow-right\');" href="#" ><i class="fa fa-long-arrow-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'long-arrow-up\');" href="#" ><i class="fa fa-long-arrow-up"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-down\');" href="#" ><i class="fa fa-toggle-down"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-left\');" href="#" ><i class="fa fa-toggle-left"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-right\');" href="#" ><i class="fa fa-toggle-right"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'caret-square-o-up\');" href="#" ><i class="fa fa-toggle-up"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="video-player">' +
                        '                                        <h2 class="page-header">Video Player Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'arrows-alt\');" href="#" ><i class="fa fa-arrows-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'backward\');" href="#" ><i class="fa fa-backward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'compress\');" href="#" ><i class="fa fa-compress"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'eject\');" href="#" ><i class="fa fa-eject"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'expand\');" href="#" ><i class="fa fa-expand"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'fast-backward\');" href="#" ><i class="fa fa-fast-backward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'fast-forward\');" href="#" ><i class="fa fa-fast-forward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'forward\');" href="#" ><i class="fa fa-forward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pause\');" href="#" ><i class="fa fa-pause"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'play\');" href="#" ><i class="fa fa-play"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'play-circle\');" href="#" ><i class="fa fa-play-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'play-circle-o\');" href="#" ><i class="fa fa-play-circle-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'step-backward\');" href="#" ><i class="fa fa-step-backward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'step-forward\');" href="#" ><i class="fa fa-step-forward"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'stop\');" href="#" ><i class="fa fa-stop"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'youtube-play\');" href="#" ><i class="fa fa-youtube-play"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="brand">' +
                        '                                        <h2 class="page-header">Brand Icons</h2>' +
                        '                                        <div class="alert alert-success">' +
                        '                                            <ul class="margin-bottom-none padding-left-lg">' +
                        '                                                <li>All brand icons are trademarks of their respective owners.</li>' +
                        '                                                <li>The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.</li>' +
                        '                                            </ul>' +
                        '                                        </div>' +
                        '                                        <div class="alert alert-warning">' +
                        '                                            <h4><i class="fa fa-warning"></i> Warning!</h4> Apparently, Adblock Plus can remove Font Awesome brand icons with their "Remove Social Media Buttons" setting. We will not use hacks to force them to display. Please' +
                        '                                            <a href="https://adblockplus.org/en/bugs" class="alert-link">report an issue with Adblock Plus</a> if you believe this to be an error. To work around this, you\'ll need to modify the social icon class names.' +
                        '                                        </div>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'adn\');" href="#" ><i class="fa fa-adn"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'android\');" href="#" ><i class="fa fa-android"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'angellist\');" href="#" ><i class="fa fa-angellist"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'apple\');" href="#" ><i class="fa fa-apple"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'behance\');" href="#" ><i class="fa fa-behance"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'behance-square\');" href="#" ><i class="fa fa-behance-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bitbucket\');" href="#" ><i class="fa fa-bitbucket"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'bitbucket-square\');" href="#" ><i class="fa fa-bitbucket-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'btc\');" href="#" ><i class="fa fa-bitcoin"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'btc\');" href="#" ><i class="fa fa-btc"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-amex\');" href="#" ><i class="fa fa-cc-amex"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-discover\');" href="#" ><i class="fa fa-cc-discover"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-mastercard\');" href="#" ><i class="fa fa-cc-mastercard"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-paypal\');" href="#" ><i class="fa fa-cc-paypal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-stripe\');" href="#" ><i class="fa fa-cc-stripe"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'cc-visa\');" href="#" ><i class="fa fa-cc-visa"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'codepen\');" href="#" ><i class="fa fa-codepen"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'css3\');" href="#" ><i class="fa fa-css3"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'delicious\');" href="#" ><i class="fa fa-delicious"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'deviantart\');" href="#" ><i class="fa fa-deviantart"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'digg\');" href="#" ><i class="fa fa-digg"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'dribbble\');" href="#" ><i class="fa fa-dribbble"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'dropbox\');" href="#" ><i class="fa fa-dropbox"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'drupal\');" href="#" ><i class="fa fa-drupal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'empire\');" href="#" ><i class="fa fa-empire"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'facebook\');" href="#" ><i class="fa fa-facebook"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'facebook-square\');" href="#" ><i class="fa fa-facebook-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'flickr\');" href="#" ><i class="fa fa-flickr"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'foursquare\');" href="#" ><i class="fa fa-foursquare"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'empire\');" href="#" ><i class="fa fa-ge"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'git\');" href="#" ><i class="fa fa-git"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'git-square\');" href="#" ><i class="fa fa-git-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'github\');" href="#" ><i class="fa fa-github"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'github-alt\');" href="#" ><i class="fa fa-github-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'github-square\');" href="#" ><i class="fa fa-github-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'gittip\');" href="#" ><i class="fa fa-gittip"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'google\');" href="#" ><i class="fa fa-google"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'google-plus\');" href="#" ><i class="fa fa-google-plus"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'google-plus-square\');" href="#" ><i class="fa fa-google-plus-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'google-wallet\');" href="#" ><i class="fa fa-google-wallet"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hacker-news\');" href="#" ><i class="fa fa-hacker-news"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'html5\');" href="#" ><i class="fa fa-html5"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'instagram\');" href="#" ><i class="fa fa-instagram"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ioxhost\');" href="#" ><i class="fa fa-ioxhost"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'joomla\');" href="#" ><i class="fa fa-joomla"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'jsfiddle\');" href="#" ><i class="fa fa-jsfiddle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'lastfm\');" href="#" ><i class="fa fa-lastfm"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'lastfm-square\');" href="#" ><i class="fa fa-lastfm-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'linkedin\');" href="#" ><i class="fa fa-linkedin"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'linkedin-square\');" href="#" ><i class="fa fa-linkedin-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'linux\');" href="#" ><i class="fa fa-linux"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'maxcdn\');" href="#" ><i class="fa fa-maxcdn"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'meanpath\');" href="#" ><i class="fa fa-meanpath"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'openid\');" href="#" ><i class="fa fa-openid"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pagelines\');" href="#" ><i class="fa fa-pagelines"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'paypal\');" href="#" ><i class="fa fa-paypal"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pied-piper\');" href="#" ><i class="fa fa-pied-piper"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pied-piper-alt\');" href="#" ><i class="fa fa-pied-piper-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pinterest\');" href="#" ><i class="fa fa-pinterest"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'pinterest-square\');" href="#" ><i class="fa fa-pinterest-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'qq\');" href="#" ><i class="fa fa-qq"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rebel\');" href="#" ><i class="fa fa-ra"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'rebel\');" href="#" ><i class="fa fa-rebel"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'reddit\');" href="#" ><i class="fa fa-reddit"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'reddit-square\');" href="#" ><i class="fa fa-reddit-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'renren\');" href="#" ><i class="fa fa-renren"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share-alt\');" href="#" ><i class="fa fa-share-alt"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'share-alt-square\');" href="#" ><i class="fa fa-share-alt-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'skype\');" href="#" ><i class="fa fa-skype"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'slack\');" href="#" ><i class="fa fa-slack"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'slideshare\');" href="#" ><i class="fa fa-slideshare"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'soundcloud\');" href="#" ><i class="fa fa-soundcloud"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'spotify\');" href="#" ><i class="fa fa-spotify"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'stack-exchange\');" href="#" ><i class="fa fa-stack-exchange"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'stack-overflow\');" href="#" ><i class="fa fa-stack-overflow"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'steam\');" href="#" ><i class="fa fa-steam"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'steam-square\');" href="#" ><i class="fa fa-steam-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'stumbleupon\');" href="#" ><i class="fa fa-stumbleupon"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'stumbleupon-circle\');" href="#" ><i class="fa fa-stumbleupon-circle"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tencent-weibo\');" href="#" ><i class="fa fa-tencent-weibo"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'trello\');" href="#" ><i class="fa fa-trello"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tumblr\');" href="#" ><i class="fa fa-tumblr"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'tumblr-square\');" href="#" ><i class="fa fa-tumblr-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'twitch\');" href="#" ><i class="fa fa-twitch"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'twitter\');" href="#" ><i class="fa fa-twitter"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'twitter-square\');" href="#" ><i class="fa fa-twitter-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'vimeo-square\');" href="#" ><i class="fa fa-vimeo-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'vine\');" href="#" ><i class="fa fa-vine"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'vk\');" href="#" ><i class="fa fa-vk"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'weixin\');" href="#" ><i class="fa fa-wechat"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'weibo\');" href="#" ><i class="fa fa-weibo"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'weixin\');" href="#" ><i class="fa fa-weixin"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'windows\');" href="#" ><i class="fa fa-windows"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'wordpress\');" href="#" ><i class="fa fa-wordpress"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'xing\');" href="#" ><i class="fa fa-xing"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'xing-square\');" href="#" ><i class="fa fa-xing-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'yahoo\');" href="#" ><i class="fa fa-yahoo"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'yelp\');" href="#" ><i class="fa fa-yelp"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'youtube\');" href="#" ><i class="fa fa-youtube"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'youtube-play\');" href="#" ><i class="fa fa-youtube-play"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'youtube-square\');" href="#" ><i class="fa fa-youtube-square"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                    <section id="medical">' +
                        '                                        <h2 class="page-header">Medical Icons</h2>' +
                        '                                        <div class="row fontawesome-icon-list">' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'ambulance\');" href="#" ><i class="fa fa-ambulance"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'h-square\');" href="#" ><i class="fa fa-h-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'hospital-o\');" href="#" ><i class="fa fa-hospital-o"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'medkit\');" href="#" ><i class="fa fa-medkit"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'plus-square\');" href="#" ><i class="fa fa-plus-square"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'stethoscope\');" href="#" ><i class="fa fa-stethoscope"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'user-md\');" href="#" ><i class="fa fa-user-md"></i></a>' +
                        '                                            </div>' +
                        '                                            <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('+res.fieldid+', \'wheelchair\');" href="#" ><i class="fa fa-wheelchair"></i></a>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </section>' +
                        '                                </div>' +
                        '                                <div class="show-hide-icons col-md-12" onclick="showHideIconContainer('+res.fieldid+');">Show Icons <i class="fa fa-angle-double-down"></i> </div>' +
                        '                            </div>' +
                        '                        </div>';
                    //endregion

                    var appendHtml = '<div class="form-group"><label><input id="slide-field-label-'+res.fieldid+'" type="text" onchange="saveContent(this, '+res.fieldid+', \'field_name\');" class="input-trans" placeholder="Type your field name here..." style="width: 200px;" /> </label>' + field +'</div>';

                    if(window.selectedSlide == '') {
                        $('#slide-field-container-0').attr('id', 'slide-field-container-'+res.slideid);
                        window.selectedSlide = res.slideid;
                    }

                    $('#slide-field-container-'+res.slideid).append(appendHtml);
                    $('#slide-field-label-'+res.fieldid).focus();

                    if(type == 3) {
                        $('.edit').froalaEditor({
                            theme: 'white'
                        });
                    }

                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function animateLivIcon(slideId) {
        var livIcon = $('#upload-img-livicon-'+slideId);
        livIcon.playLivIcon(1, 2);
    }
                    
    // [ID: 285]
    function saveContent(object, fieldId, field) {
        //develop the function here
        var value = $.trim($('#'+object.id).val());
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/savecontent',
            data: {fieldid:fieldId, field:field, value:value}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    $('.edit').on('froalaEditor.blur', function (e, editor) {

        var value = $(this).find('.fr-view').html();
        var fieldId = $(this).attr('data-id');
        var field = 'content';

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/savecontent',
            data: {fieldid:fieldId, field:field, value:value}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });

    });


    function openUploadFileDialog(fieldId) {
        $('#image-choose-'+fieldId).trigger('click');
        window.uploadingImgFieldId = fieldId;
    }

    // [ID: 287]
    function submitImageUploadForm(form, fieldId) {
        $(form).submit();
    }

    // [ID: 226]
    $(function(){
        // function from the jquery form plugin
        $('.slider-slide-image-upload').ajaxForm({
            beforeSend:function(){
                console.log(window.uploadingImgFieldId);
                $("#image-upload-progress-"+window.uploadingImgFieldId).show();
            },
            uploadProgress:function(event,position,total,percentComplete){
                console.log('upload progress...');
                $("#image-upload-progress-bar-"+window.uploadingImgFieldId).width(percentComplete+'%'); //dynamicaly change the progress bar width
                $("#image-upload-sr-only-"+window.uploadingImgFieldId).html(percentComplete+'%'); // show the percentage number
            },
            success:function(){
                console.log('success...');
                $("#image-upload-progress-"+window.uploadingImgFieldId).hide();
            },
            complete:function(response){
                console.log(response);
                var res = JSON.parse(response.responseText);

                // console.log(new Date().getTime());
                $('#slider-slide-img-'+res.id).attr('src', window.domain+res.dirpath+res.name+'?timestamp=' + new Date().getTime());
            }
        });

        //set the progress bar to be hidden on loading
        $(".progress").hide();
        $("#image-upload-progress-bar-"+window.uploadingImgFieldId).width('0%');
    });
                    
    // [ID: 289]
    function addSlide() {
        var el = $('#slider-slides');
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/addslide',
            data: {sliderid:window.selectedSlider}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    $('.active').removeClass('active');
                    $('#slider-slides-container').append(res);
                    $('#slider-control-prev').fadeIn(200);
                    $('#slider-control-next').fadeIn(200);

                    $('#slider-slides-container').carousel({
                        interval: false
                    });

                    $('#slider-control-prev').fadeIn(200);
                    $('#slider-control-next').fadeIn(200);

                    $('.edit').froalaEditor({
                        theme: 'white'
                    });

                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 291]
    function selectSlider(sliderId) {
        //develop the function here

        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'website/selectslider',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 295]
    function getSliders() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'website/getsliders',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    function pickedIcon(fieldId, icon) {
        $('#selected-icon-container-'+fieldId).html('<i class="fa fa-'+icon+'" ></i>');
        $('#selected-icon-'+fieldId).val(icon);
        $('#selected-icon-'+fieldId).trigger('click');
    }

    function showHideIconContainer(fieldId) {
        $('#icon-list-container-'+fieldId).slideToggle(300);
    }
                    
    // [ID: 382]
    function getMenu() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'website/getmenu',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 386]
    function getPopularCategories() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'website/getpopularcategories',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 388]
    function getNewProducts() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getnewproducts',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 390]
    function getFeaturedProducts() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getfeaturedproducts',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 392]
    function getProductDetails() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getproductdetails',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 394]
    function getProductFullDetails() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getproductfulldetails',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 401]
    function getManufactureDetails() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getmanufacturedetails',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 406]
    function getManufactureProducts() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getmanufactureproducts',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 408]
    function getManufactures() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getmanufactures',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 410]
    function getCategoryDetails() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getcategorydetails',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 412]
    function getCategoryProducts() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getcategoryproducts',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

