
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-12 13:11:55.
 * Controller ID - 84
 */

'use strict';

window.paginate = 1;
window.newestId = null;
getEnquiries();

$(document).ready(function () {
    setInterval(function() {
        getNewestEnquiries();
    }, 30000);
});
                    
    //Public API [ID: 318]
    function sendEmail() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/sendemail',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 329]
    function getEnquiries() {
        //develop the function here
        var el = $('.x_content');
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getenquiries',
            data: {paginate:window.paginate, perpage:25}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    var enquiry = '';

                    for(var i = 0; i < res.length; i++) {
                        if(i == 0) {
                            window.newestId = res[i].id;
                        }
                        enquiry += ' <a class="mail-list-item" href="#">' +
                            '               <div id="enquiry-'+res[i].id+'" class="mail_list '+(i == 0 ? 'selected':'')+'" onclick="getEnquiry('+res[i].id+');">' +
                            '                   <div class="left">' +
                            '                       '+(res[i].status == 1 ? '<i id="read-status-'+res[i].id+'" class="fa fa-circle"></i>':'<i id="read-status-'+res[i].id+'" class="fa fa-circle-o"></i>')+
                            '                   </div>' +
                            '                   <div class="right">' +
                            '                       <h3>'+res[i].name+' <small>'+res[i].created_at+'</small></h3>' +
                            '                       <p><strong>'+res[i].subject+':</strong> '+res[i].message+'</p>' +
                            '                   </div>' +
                            '               </div>' +
                            '           </a>';
                    }

                    if(enquiry == '') {
                        enquiry = '<p style="text-align: center;">No Enquiries...</p>'
                    }

                    if(window.paginate == 1) {
                        $('#enquiry-list-container').html(enquiry);
                        getEnquiry(window.newestId);
                    }
                    else {
                        $('#enquiry-list-container').append(enquiry);
                    }
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 331]
    function getNewestEnquiries() {
        //develop the function here
        var el = $('.x_content');
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getnewestenquiries',
            data: {newestid:window.newestId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    var enquiry = '';

                    for(var i = 0; i < res.length; i++) {
                        if(i == 0) {
                            window.newestId = res[i].id;
                        }
                        enquiry += ' <a class="mail-list-item" href="#">' +
                            '               <div id="enquiry-'+res[i].id+'" class="mail_list" onclick="getEnquiry('+res[i].id+');">' +
                            '                   <div class="left">' +
                            '                       '+(res[i].status == 1 ? '<i id="read-status-'+res[i].id+'" class="fa fa-circle"></i>':'<i id="read-status-'+res[i].id+'" class="fa fa-circle-o"></i>')+
                            '                   </div>' +
                            '                   <div class="right">' +
                            '                       <h3>'+res[i].name+' <small>'+res[i].created_at+'</small></h3>' +
                            '                       <p><strong>'+res[i].subject+':</strong> '+res[i].message+'</p>' +
                            '                   </div>' +
                            '               </div>' +
                            '           </a>';
                    }

                    $('#enquiry-list-container').prepend(enquiry);
                }

                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 333]
    function getEnquiry(enquiryId) {
        //develop the function here
        var el = $('.x_content');
        blockUI(el);
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/getenquiry',
            data: {enquiryid:enquiryId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    $('#body-created-at').html(res.created_at);
                    $('#body-subject').html(res.subject);
                    $('#body-name').html(res.name);
                    $('#body-email').html('(Email: '+res.email+''+(res.phone != null ? ', Phone: '+res.phone:'')+')');
                    $('#body-message').html(res.message);

                    $('.selected').removeClass('selected');
                    $('#read-status-'+enquiryId).removeClass('fa-circle').addClass('fa-circle-o');
                    $('#enquiry-'+enquiryId).addClass('selected');
                }

                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

