
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-18 15:42:59.
 * Controller ID - 79
 */

'use strict';
window.selectedSection = $('#section-id').val();
                    
    // [ID: 270]
    function postSection(object, field) {
        //develop the function here
        var value = $.trim($('#'+object.id).val());

        if(field == 'name' && value == '') {
            value = 'Untitled';
        }
        var el = $('#input-section-container');
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/postsection',
            data: {value:value, field:field, sectionid:window.selectedSection}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(field == 'status') {
                        window.location.reload();
                    }
                    else {
                        if(window.selectedSection == 0) {
                            window.selectedSection = data;
                            $('#section-name-0').attr('data-id', data);
                            $('#section-name-0').attr('onclick', 'selectSection('+data+');');
                            $('#section-name-0').attr('id', 'section-name-'+data);

                            $('#second-section').fadeTo(200, 1);
                            $('#second-section').fadeIn(200);

                            $('#add-more-section-button').fadeTo(200, 1);
                            $('#add-more-section-button').fadeIn(200);
                        }

                        unBlockUI(el);
                    }

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 272]
    function editTitle() {
        var value = $.trim($('#input-section-name').val());
        if(value != '') {
            $('#section-name-'+window.selectedSection).html(value+' <i class="fa fa-chevron-right pull-right"></i> ');
        }
        else {
            if(window.selectedSection == 0) {
                $('#section-name-'+window.selectedSection).html('First Section <i class="fa fa-chevron-right pull-right"></i> ');
            }
            else {
                $('#section-name-'+window.selectedSection).html('Untitled <i class="fa fa-chevron-right pull-right"></i> ');
            }

        }
        
    }

    $('#input-section-content').on('froalaEditor.blur', function (e, editor) {

        var value = $(this).find('.fr-view').html();
        var el = $('#input-section-container');
        blockUI(el);

        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'website/postsection',
            data: {value:value, field:'content', sectionid:window.selectedSection}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    });

    function addSection() {
        $('.selected').removeClass('selected');
        $('.fa-chevron-right').remove();
        var newSection = '<li id="section-name-0" onclick="selectSection(0)" class="selected">New Section <i class="fa fa-chevron-right pull-right"></i> </li>';
        $("#website-section-container").prepend(newSection);
        window.selectedSection = 0;
        $('#second-section').fadeOut(200);
        $('#add-more-section-button').fadeOut(200);
        $('#input-section-name').val('');
        $('#input-section-name').focus();
        $('#input-section-title').val('');
        $('#input-section-content').val('');
        $('.fr-view').html('');
    }
                    
    // [ID: 273]
    function selectSection(sectionId) {
        //develop the function here
        $('.selected').removeClass('selected');
        $('.fa-chevron-right').remove();

        $('#section-name-'+sectionId).addClass('selected');
        $('#section-name-'+sectionId).html($('#section-name-'+sectionId).html()+'<i class="fa fa-chevron-right pull-right"></i>');

        $('#input-section-name').val('');
        $('#input-section-title').val('');
        $('#input-section-content').val('');
        $('.fr-view').html('');
        var el = $('#input-section-container');
        window.selectedSection = sectionId;

        if(sectionId == 0) {
            $('#second-section').fadeOut(200);
            $('#add-more-section-button').fadeOut(200);
        }
        else {
            blockUI(el);

            //ajax method
            jQuery.ajax({
                type: 'GET',
                url: window.domain + 'website/selectsection',
                data: {sectionid:sectionId}, //add the parameter if required to pass
                success: function (data) {
                    if(data == 'Unauthorized') {
                        window.location.reload();
                    }
                    else if(data == 0) {
                        dbError();
                    }
                    else {
                        var res = JSON.parse(data);

                        $('#input-section-name').val(res.name);
                        $('#input-section-title').val(res.title);
                        $('.fr-view').html(res.content);
                        $('#section-id').text('ID: '+res.id);

                        $('#second-section').fadeIn(200);
                        $('#add-more-section-button').fadeIn(200);

                        unBlockUI(el);
                    }
                },
                error: function (xhr, textShort, errorThrown) {
                    noConnection();
                    unBlockUI(el);
                }
            });
        }

    }

