/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/05/17 13:11.
 */

'use strict';

//region Global Variables
window.openedSystem = 0;
window.openedController = 0;
//endregion

//region Default settings

//endregion

//region Events
// $('.panel-title').on('click', function () {
//     $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
//         type: 'bar',
//         height: '40',
//         barWidth: 9,
//         colorMap: {
//             '7': '#a1a1a1'
//         },
//         barSpacing: 2,
//         barColor: '#26B99A'
//     });
//
//     $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
//         type: 'line',
//         width: '200',
//         height: '40',
//         lineColor: '#26B99A',
//         fillColor: 'rgba(223, 223, 223, 0.57)',
//         lineWidth: 2,
//         spotColor: '#26B99A',
//         minSpotColor: '#26B99A'
//     });
// });

function iSwitchOnChange(id, status) {
    if(id.substring(0, 13) == 'system-status') {
        var systemId = id.slice(14);
        var el = $('#panel-body-'+systemId);
        blockUI(el);
        $('#system-led-status-'+systemId).attr('class', '').addClass('led-green g-blink-n');

        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/change-system-status',
            data: {systemid: systemId, status:status},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    $('#system-led-status-'+systemId).attr('class', '').addClass('led-green g-blink-f');
                    if(status == 1) {
                        setTimeout(function () {
                            $('#system-led-status-'+systemId).attr('class', '').addClass('led-green');
                        }, 1500);
                    }
                    else {
                        setTimeout(function () {
                            $('#system-led-status-'+systemId).attr('class', '').addClass('led-yellow');
                        }, 1500);
                    }
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
    else if(id.substring(0, 11) == 'view-status') {
        var viewId = id.slice(12);
        var el = $('#panel-body-'+viewId);
        blockUI(el);
        $('#view-led-status-'+viewId).attr('class', '').addClass('led-green g-blink-n');

        // [ID: 249]
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/setviewmaintenance',
            data: {viewid:viewId, status:status}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    $('#view-led-status-'+viewId).attr('class', '').addClass('led-green g-blink-f');
                    if(status == 1) {
                        setTimeout(function () {
                            $('#view-led-status-'+viewId).attr('class', '').addClass('led-green');
                        }, 1500);
                    }
                    else {
                        setTimeout(function () {
                            $('#view-led-status-'+viewId).attr('class', '').addClass('led-yellow y-blink-n');
                        }, 1500);
                    }
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}
//endregion

//region Functions

//region System
function loadSystemData(systemId) {
    if(systemId == window.openedSystem) {
        window.openedSystem = null;
    }
    else {
        window.openedSystem = systemId;
        var el = $('#panel-body-'+systemId);
        blockUI(el);

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'system/load-system-data',
            data: {systemid: systemId},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    $('#system-total-visitors-'+systemId).html(res.visitors);
                    $('#system-total-visits-'+systemId).html(res.visits);
                    var progress = (((res.runningex+res.pendingex+res.solvedex)/res.totalex)*100).toFixed(2);
                    $('#system-exception-ratio-progress-'+systemId).val(progress);
                    $('#system-exception-ratio-text-'+systemId).html(progress+'%');
                    $('#system-runing-ex-'+systemId).html(res.runningex);
                    $('#system-pending-ex-'+systemId).html(res.pendingex);
                    $('#system-fixed-ex-'+systemId).html(res.solvedex);
                    $('#system-route-'+systemId).html(res.route);
                    $('#system-list-order-'+systemId).html(res.listorder);
                    $('#system-controllers-'+systemId).html(res.controllers);
                    $('#system-functions-'+systemId).html(res.func);
                    $('#system-created-by-'+systemId).html(res.createdby);
                    $('#system-created-at-'+systemId).html(res.createdat);
                    $('#mm-system-exception-title').html('<i class="fa fa-bug"></i> '+res.name+' Exceptions');
                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

function addSystem() {
    var object = $("#mm-sys-system-name");
    removeError(object, 'duplicate');

    var systemName = $.trim($('#mm-sys-system-name').val());
    var icon = $.trim($('#mm-sys-system-icon').val());
    var listOrder = $.trim($('#mm-sys-system-list-order').val());
    var allowDefault = $.trim($('#mm-sys-system-allow-default').val());

    if(systemName == '' || icon == '') {
        $('#mm-sys-system-name').trigger('blur');
        $('#mm-sys-system-icon').trigger('blur');
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/new-system',
            data: {name: systemName, icon:icon, listorder:listOrder, allowdefault:allowDefault},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else if(data == -1) {
                    $('#btn-new-system').trigger('click');

                    errorExecution(object, 'System Name already exists', 'duplicate');

                    setTimeout(function () {
                        object.effect( "shake");
                    }, 200);
                }
                else {
                    var mySwitch;
                    var res = JSON.parse(data);
                    $('#accordion').append(res);
                    $(".ios-switch").each(function(){
                        mySwitch = new Switch(this);
                    });

                    //clear dialog input field data
                    $('#mm-sys-system-name').val('');
                    $('#mm-sys-system-icon').val('');
                    removeError(object, 'duplicate');
                    removeError(object, 'required');
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
            }
        });
    }
}
//endregion

//region Exception Section
function openExceptions(systemId) {
    var el = $('#mm-system-exception-body-container');
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'system/get-system-exceptions',
        data: {systemid: systemId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var tableRows;

                for(var i = 0; i < res.length; i++) {
                    tableRows += '<tr id="sys-ex-'+res[i].id+'" class="even pointer"> ' +
                        '               <td>'+res[i].id+'</td> ' +
                        '               <td>'+res[i].controller+'</td> ' +
                        '               <td>'+res[i].func+'</td> ' +
                        '               <td>'+res[i].username+'</td> ' +
                        '               <td>'+res[i].ip+'</td> ' +
                        '               <td onclick="openException('+res[i].id+');" style="cursor:pointer;">'+res[i].exception+'</td> ' +
                        '               <td onclick="changeExceptionStatus('+res[i].id+');"><span id="sys-ex-status-'+res[i].id+'" data-status="'+res[i].status+'" style="cursor: pointer;" class="label label-'+
                                            (res[i].status == 1 ? 'success': res[i].status == 2 ? 'warning' : 'danger')+
                        '                   ">'+(res[i].status == 1 ? 'Fixed': res[i].status == 2 ? 'Fixing' : 'Running')+'' +
                        '                   </span>' +
                        '               </td> ' +
                        '           </tr>';
                }

                if(tableRows == '' || tableRows == null) {
                    tableRows = '<tr><td colspan="8" class="center">Huree... No Exceptions!</td></tr>'
                }
                $('#mm-system-exception-table').html(tableRows);

            }
            unBlockUI(el);
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function openException(exceptionId) {
    popupDiv('sys-ex-'+exceptionId, exceptionId, 'Wait...', '', 'height: 350px; width: 90%; top: -5px; left:50px;');
    $('#btn-close-popup-div').hide();

    var el = $('#div-popup-content-'+exceptionId);
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'system/get-exception-data',
        data: {exceptionid: exceptionId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);

                var title = res.title.replace(/([A-Z])/g, ' $1').trim()
                $('#div-popup-title-'+exceptionId).text(exceptionId+' - '+title);

                var appendHtml = ('  <div class="col-md-12">' +
                '                       <div class="form-horizontal form"> ' +
                '                           <div class="col-md-6">'+
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">ID:</label> ' +
                '                                   <div id="sys-ex-popup-id" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">' +exceptionId+
                '                                   </div> ' +
                '                               </div> ' +
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">System:</label> ' +
                '                                   <div id="sys-ex-popup-system" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">' +res.system+
                '                                   </div> ' +
                '                               </div> ' +
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Controller:</label> ' +
                '                                   <div id="sys-ex-popup-controller" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">' +res.controller+
                '                                   </div> ' +
                '                               </div> ' +
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Function:</label> ' +
                '                                   <div id="sys-ex-popup-function" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">' +res.func+
                '                                   </div> ' +
                '                               </div> ' +
                '                           </div>' +
                '                           <div class="col-md-6">'+
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">IP:</label> ' +
                '                                   <div id="sys-ex-popup-ip" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">' +res.ip+
                '                                   </div> ' +
                '                               </div> ' +
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created By:</label> ' +
                '                                   <div id="system-ex-popup-created-by" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">' + res.user+
                '                                   </div> ' +
                '                               </div> ' +
                '                               <div class="form-group" style="margin-top: -10px;"> ' +
                '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created At:</label> ' +
                '                                   <div id="system-ex-popup-created-at" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;"> ' + res.createdat +
                '                                   </div> ' +
                '                               </div> ' +
                '                           </div>' +
                '                       </div>' +
                '                       <div class="col-md-12" style="max-height: 125px; overflow: auto; border: 1px solid #f1efef;">'+res.exception+'</div>' +
                '                   </div>'
                );

                unBlockUI(el);
                el.html(appendHtml);
            }
            unBlockUI(el);
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function changeExceptionStatus(exceptionId) {
    var el = $('#sys-ex-status-'+exceptionId)
    var status = el.attr('data-status');

    if(status == 3) {
        var status = 2;
        el.removeClass('label-danger').addClass('label-warning').text('Fixing');
        el.attr('data-status', status);
        var running = parseInt($('#system-runing-ex-'+window.openedSystem).text());
        var pending = parseInt($('#system-pending-ex-'+window.openedSystem).text());
        $('#system-runing-ex-'+window.openedSystem).text(running-1);
        $('#system-pending-ex-'+window.openedSystem).text(pending+1);
    }
    else if(status == 2) {
        var status = 1;
        el.removeClass('label-warning').addClass('label-success').text('Fixed');
        el.attr('data-status', status);
        var pending = parseInt($('#system-pending-ex-'+window.openedSystem).text());
        var fixed = parseInt($('#system-fixed-ex-'+window.openedSystem).text());
        $('#system-fixed-ex-'+window.openedSystem).html(fixed+1);
        $('#system-pending-ex-'+window.openedSystem).text(pending-1);
    }
    else if(status == 1) {
        var status = 3;
        el.removeClass('label-success').addClass('label-danger').text('Running');
        el.attr('data-status', status);
        var running = parseInt($('#system-runing-ex-'+window.openedSystem).text());
        var fixed = parseInt($('#system-fixed-ex-'+window.openedSystem).text());
        $('#system-fixed-ex-'+window.openedSystem).html(fixed-1);
        $('#system-runing-ex-'+window.openedSystem).text(running+1);
    }

    if(parseInt($('#system-runing-ex-'+window.openedSystem).text()) == 0 && parseInt($('#system-pending-ex-'+window.openedSystem).text()) == 0)
        $('#system-led-exceptions-'+window.openedSystem).removeClass('led-red r-blink-n').addClass('led-green');
    else
        $('#system-led-exceptions-'+window.openedSystem).removeClass('led-green').addClass('led-red r-blink-n');

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'system/change-exception-status',
        data: {exceptionid: exceptionId, status:status},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {

            }
            unBlockUI(el);
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function allExceptionFixed() {
    $("#mm-system-exception-table span").each(function(){
        $(this).removeClass('label-danger label-warning').addClass('label-success').text('Fixed');
    });

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'system/change-all-exception-status',
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var running = parseInt($('#system-runing-ex-'+window.openedSystem).text());
                var pending = parseInt($('#system-pending-ex-'+window.openedSystem).text());
                var fixed = parseInt($('#system-fixed-ex-'+window.openedSystem).text());
                $('#system-fixed-ex-'+window.openedSystem).html(fixed+running+pending);
                $('#system-runing-ex-'+window.openedSystem).text(0);
                $('#system-pending-ex-'+window.openedSystem).text(0);
                $('#system-led-exceptions-'+window.openedSystem).removeClass('led-red r-blink-n').addClass('led-green');
            }
            unBlockUI(el);
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}
//endregion

function getControllers(systemId) {
    var el = $('#sys-controller-container-'+systemId);
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'system/get-controllers',
        data: {systemid: systemId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                $('#accordion-controller-'+systemId).html(res);
            }
            unBlockUI(el);
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function addController() {
    var object = $("#mm-sys-controller-name");
    removeError(object, 'duplicate');

    var systemId = window.openedSystem;
    var name = $.trim($('#mm-sys-controller-name').val());
    var lang = $('#mm-sys-controller-lang').val();
    var description = $.trim($('#mm-sys-controller-description').val());

    if(name == '') {
        $('#mm-sys-controller-name').trigger('blur');
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/new-controller',
            data: {systemid: systemId, name:name, lang:lang, description:description},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else if(data == -1) {
                    $('#btn-new-controller-'+systemId).trigger('click');

                    errorExecution(object, 'Controller Name already exists', 'duplicate');

                    setTimeout(function () {
                        object.effect( "shake");
                    }, 200);
                }
                else {
                    getControllers(systemId);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

function openController(controllerId, type) {
    if(window.openedController != controllerId) {
        window.openedController = controllerId;
        window.openedControllerType = type;

        loadControllerData(controllerId, type);

    }
}

function loadControllerData(controllerId, type) {
    var el = $('#sys-controller-body-'+controllerId);
    blockUI(el);
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'system/get-controller-data',
        data: {controllerid:controllerId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                appendHtml = '';

                for(var i = 0; i < res.length; i++) {
                    var color = res[i].type == 'get' ? '#62bfa1' : res[i].type == 'post' ? '#628bbf' : '#8162bf';
                    var appendHtml = appendHtml+(' <div class="panel"> ' +
                        '                       <a class="panel-heading collapsed" role="tab" id="heading-function-'+res[i].id+'" onclick="openFunction('+res[i].id+');" data-toggle="collapse" data-parent="#accordion-function-'+controllerId+'" href="#collapse-function-'+res[i].id+'" aria-expanded="false" aria-controls="collapse-function-'+res[i].id+'" style="background:'+color+'; color: #fcfdff;"> ' +
                        '                           <h4 class="panel-title">'+res[i].id+'. &nbsp;&nbsp;&nbsp;'+res[i].name+'</h4> ' +
                        '                       </a> ' +
                        '                       <div id="collapse-function-'+res[i].id+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-function-'+res[i].id+'"> ' +
                        '                           <div id="sys-function-body-'+res[i].id+'" class="panel-body" style="min-height: 100px; position: relative;"> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                   </div>');
                }

                if(appendHtml == '')
                    appendHtml = '<p style="text-align: center;"><strong>No Functions Developed!</strong></p>';

                appendHtml = appendHtml + '<span class="slide-add-circle" data-toggle="modal" id="btn-new-function-'+controllerId+'" data-target=".bs-new-function-modal-lg" onclick="loadNewFunctionData('+type+');" style="bottom: 5px !important; background-color: #62bfa1 !important; width: 40px; height: 40px; padding: 4px; padding-left: 11px; right: 10px;"><i class="fa fa-plus"></i> </span>';

                el.html(appendHtml);
                unBlockUI(el);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function loadNewFunctionData(type) {
    var systemId = window.openedSystem;

    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'system/get-system-scripts',
        data: {systemid: systemId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var jsControllers = '';
                var phpControllers = '';
                var res = JSON.parse(data);


                if(res.js.length < 1) {
                 jsControllers = '<option disabled selected>No Scripts</option>'
                }
                else {
                    for(var i = 0; res.js.length > i; i++) {
                        jsControllers = jsControllers+'<option value="'+res.js[i].id+'">'+res.js[i].name+'</option>'
                    }
                }

                if(res.php.length < 1) {
                    phpControllers = '<option disabled selected>No Controllers</option>'
                }
                else {
                    for(var i = 0; res.php.length > i; i++) {
                        phpControllers = phpControllers+'<option value="'+res.php[i].id+'">'+res.php[i].name+'</option>'
                    }
                }

                if(type == 1) var appendType = '<option value="1" selected>Ajax</option> <option value="3">JS</option>';
                else var appendType = '<option value="1" selected>Ajax</option> <option value="2">PHP</option>';

                $('#mm-sys-js-script').html(jsControllers);
                $('#mm-sys-php-controller').html(phpControllers);
                $('#mm-sys-function-type').html(appendType);
                checkFunctionTypeStatus();
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function checkFunctionTypeStatus() {
    var status = $('#mm-sys-function-type').val();
    if(status == 1) {
        $('#mm-sys-ajax-method-container').slideDown(200);
    }
    else {
        $('#mm-sys-ajax-method-container').slideUp(200);
    }
}

function addFunction() {
    var object = $('#mm-sys-function-name');
    removeError(object, 'duplicate');

    var type = $('#mm-sys-function-type').val();
    var name = $.trim($('#mm-sys-function-name').val());
    var script = $('#mm-sys-js-script').val();
    var controller = $('#mm-sys-php-controller').val();
    var method = $('#mm-sys-ajax-method').val();
    var authenticated = $('#mm-sys-function-access').val();
    var description = $.trim($('#mm-sys-function-description').val());

    if(name == '') {
        $('#mm-sys-function-name').trigger('blur');
    }
    else {
        var el = $('#mm-system-exception-body-container');
        blockUI(el);

        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/new-function',
            data: {type: type, name:name, method:method, authenticated:authenticated, script:script, controller:controller, description:description, controllerid:window.openedController},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else if(data == -1) {
                    $('#btn-new-function-'+window.openedController).trigger('click');

                    errorExecution(object, 'Controller Name already exists', 'duplicate');

                    setTimeout(function () {
                        object.effect( "shake");
                    }, 200);
                }
                else {
                    loadControllerData(window.openedController, window.openedControllerType);
                    unBlockUI(el);

                    $('#mm-sys-function-name').val('');
                    $('#mm-sys-function-description').val('');
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}

    //get relevant data for the view [ID: 67]
    function loadNewView(systemId) {
        var el = $("#mm-system-view-body-container");
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'system/load-new-view',
            data: {systemid:systemId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    var options = "";

                    for(var i = 0; i < res.length; i++) {
                        options += '<option value="'+res[i].id+'">'+res[i].name+'</option>'
                    }

                    if(options == "") {
                        options = "<option disabled> No Controllers </option>"
                    }

                    $('#mm-sys-view-controllers').append(options);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

    //add a new view to the system [ID: 69]
    function addView() {
        var systemId = window.openedSystem;
        var name = $.trim($('#mm-sys-view-name').val());
        var controllerId = $('#mm-sys-view-controllers').val();
        var icon = $.trim($('#mm-sys-view-icon').val());
        var description = $.trim($('#mm-sys-view-description').val());
        var allowDefault = $('#mm-sys-system-allow-default').val();
        var listOrder = $('#mm-sys-system-list-order').val();
        var url = $.trim($('#mm-sys-view-url').val());
        var showMenu = $('#mm-sys-view-show-menu').val();

        if(name == '' || icon == '') {
            $('#mm-sys-view-name').trigger('blur');
            $('#mm-sys-view-icon').trigger('blur');
        }
        else {
            //ajax method
            jQuery.ajax({
                type: 'POST',
                url: window.domain + 'system/addview',
                data: {name:name, systemid:systemId, controllerid:controllerId, icon:icon, description:description, allowdefault:allowDefault, listorder:listOrder, url:url, showmenu:showMenu}, //add the parameter if required to pass
                success: function (data) {
                    if(data == 'Unauthorized') {
                        window.location.reload();
                    }
                    else if(data == 0) {
                        dbError();
                    }
                    else {
                        notifyDone('View added. Please refresh tha page.')
                    }
                },
                error: function (xhr, textShort, errorThrown) {
                    noConnection();
                    unBlockUI(el);
                }
            });
        }


    }

    //load a new operation [ID: 92]
    function loadNewOperation(systemId, viewId) {
        alert(systemId + ' ## ' + viewId);
        //develop the function here
        $('#mm-sys-ope-system-id').val(systemId);
        $('#mm-sys-ope-view-id').val(viewId);
    }

    //Add a new operation [ID: 93]
    function addOperation() {
        var name = $.trim($('#mm-sys-ope-name').val());
        var icon = $.trim($('#mm-sys-ope-icon').val());
        var description = $.trim($('#mm-sys-ope-description').val());
        var systemId = $('#mm-sys-ope-system-id').val();
        var viewId = $('#mm-sys-ope-view-id').val();

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/addoperation',
            data: {name:name, icon:icon, description:description, systemid:systemId, viewid:viewId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var operationHtml = '<li>' +
                        '                   <a>\n' +
                        '                       <span>\n' +
                        '                           <span><i class="fa '+icon+'"></i> '+name+'</span>\n' +
                        '                           <span class="time" style="right: 10%; font-size: 18px;">ID: '+data+'</span>\n' +
                        '                       </span>\n' +
                        '                       <span class="message">'+description+'</span>\n' +
                        '                   </a>\n' +
                        '               </li>';

                    $('#operation-list-'+viewId).append(operationHtml);
                    $('#msg-no-operations').fadeOut(200);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    //show add choices inputs [ID: 96]
    function showAddChoices(operationId) {
        $('#choices-container-'+operationId).html('<input type="text" id="txt-choices-'+operationId+'" style="margin-top: 15px; width: 200px; border-radius: 10px;" /><i class="fa fa-plus-circle" style="font-size: 20px; color: #00b922; margin-left: 10px; cursor: pointer;" onclick="addOperationChoice('+operationId+');" ></i>')
    }
                    
    //add choices to operations [ID: 97]
    function addOperationChoice(operationId) {
        var choice = $.trim($('#txt-choices-'+ operationId).val());
        var el = $('#choices-container-'+operationId);
        blockUI(el);

        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'system/addoperationchoice',
            data: {operationid:operationId, choice:choice}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    $("#choices-container-"+operationId).prepend('<h4 style="font-size: 13px; margin-left: 30px; color: #888;">'+choice+'</h4>');
                    $('#txt-choices-'+ operationId).val('');
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

