
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-12 21:24:09.
 * Controller ID - 78
 */

window.selectedOrg = null;

var el = $('#org-container');
$('.x_content').fadeTo(200, 1);
blockUI(el);

$(document).ready(function () {
    var el = $('#org-container');
    unBlockUI(el);
});

'use strict';
                    
    // [ID: 253]
    function addOrganization() {
        //develop the function here
        var orgName = $.trim($('#mm-org-name').val());
        var adminName = $.trim($('#mm-admin-name').val());
        var adminEmail = $.trim($('#mm-admin-email').val());
        var re = /\S+@\S+\.\S+/;
        var el = $('#model-add-new-org');

        if(orgName == '' || adminName == '' || adminEmail == '' || !re.test(adminEmail)) {
            $('#mm-org-name').trigger('blur');
            $('#mm-admin-name').trigger('blur');
            $('#mm-admin-email').trigger('blur');
        }
        else {
            blockUI(el);

            //ajax method
            jQuery.ajax({
                type: 'POST',
                url: window.domain + 'organizations/addorganization',
                data: {orgname:orgName, adminname:adminName, adminemail:adminEmail}, //add the parameter if required to pass
                success: function (data) {
                    if(data == 'Unauthorized') {
                        window.location.reload();
                    }
                    else if(data == 0) {
                        dbError();
                    }
                    else if(data == -1) {
                        unBlockUI(el);
                        errorExecution($('#mm-admin-email'), 'Sorry! This email has already been used.', 'duplicate');
                    }
                    else {
                        //getUsersList();
                        $('#mm-btn-org-cancel').trigger('click');
                        notifyDone('Organization has added.');
                        window.location.reload();
                    }
                },
                error: function (xhr, textShort, errorThrown) {
                    noConnection();
                    unBlockUI(el);
                }
            });
        }
    }
                    
    // [ID: 255]
    function changeOrgStatus(status) {
        //develop the function here
        var el = $("#panel-"+window.selectedOrg);
        blockUI(el);
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'organizations/changeorgstatus',
            data: {status:status, orgid:window.selectedOrg}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var ret = JSON.parse(data);

                    if(status == 1) {
                        if(ret.activation_status == 0)
                            $('#org-activation-status-'+window.selectedOrg).html('<button onclick="sendActivationEmail();" class="btn btn-sm btn-default">Send Activation Email</button>');
                        else if(ret.activation_status == 1)
                            $('#org-activation-status-'+window.selectedOrg).html('Active').attr('style', 'color: #00d719;');
                        else if(ret.activation_status == 2)
                            $('#org-activation-status-'+window.selectedOrg).html('<button onclick="sendActivationEmail();" class="btn btn-sm btn-default">Re-Send Activation Email</button>');

                        $('#org-status-indicator-'+window.selectedOrg).attr('style', 'color: #00c219');
                    }
                    else {
                        $('#org-activation-status-'+window.selectedOrg).html('Deactivated').attr('style', 'color: #D5000E;');
                        $('#org-status-indicator-'+window.selectedOrg).attr('style', 'color: #9c9c9c');
                    }

                }
                unBlockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 257]
    function iSwitchOnChange(id, status) {
        if (id == 'org-status') {
            changeOrgStatus(status);
        }
        else if(id.substring(0, 5) == 'sa-sy') {
            var viewId = id.slice(6);
            changeViewAccess(viewId, status);
        }
        else if(id.substring(0, 5) == 'sa-op') {
            var operationId = id.slice(6);
            changeOperationAccess(operationId, status);
        }
        else if(id.substring(0, 14) == 'payment-status') {
            var type = parseInt(id.slice(-2));
            saveMerchant(type, status);
        }

    }
                    
    // [ID: 258]
    function selectOrg(orgId) {
        window.selectedOrg = orgId;
    }
                    
    // [ID: 259]
    function changeAccess() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'organizations/changeaccess',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 261]
    function changeViewAccess(viewId, status) {
        //develop the function here
        var el = $("#panel-"+window.selectedOrg);
        blockUI(el);
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'organizations/changeviewaccess',
            data: {viewid:viewId, status:status, orgid:window.selectedOrg}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 263]
    function changeOperationAccess() {
        //develop the function here
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'organizations/changeoperationaccess',
            data: {}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 265]
    function saveSettings(object, field, table) {

        var value = $('#'+object.id).val();
        
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'organizations/savesettings',
            data: {value:value, field:field, table:table, orgid:window.selectedOrg}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(field == 'status') {
                        $('#mail-'+window.selectedOrg+' :input').each(function () {
                            $(this).val('');
                        });
                    }
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 267]
    function sendActivationEmail() {
        //develop the function here
        $(this).attr('disabled', 'disabled');
        var el = $('#panel-'+window.selectedOrg);
        blockUI(el);
        
        //ajax method
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'organizations/sendactivationemail',
            data: {orgid:window.selectedOrg}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);
                    notifyDone('Activation email has been sent to '+res);
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
                    
    // [ID: 323]
    function saveMerchant(merchantType, status) {
        //develop the function here
        var el = $('#merchant-' + window.selectedOrg);
        blockUI(el);
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'organizations/savemerchant',
            data: {orgid:window.selectedOrg, merchanttype:merchantType, status:status}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    if(status == 1) {
                        $('#merchant-title-'+window.selectedOrg).attr('style', 'font-size: 20px;');
                        $('.merchant-labels').attr('style', '');
                        $('#merchant-logo-'+window.selectedOrg).removeClass('gray-scale');
                    }
                    else {
                        $('#merchant-title-'+window.selectedOrg).attr('style', 'font-size: 20px; color: #7d7d7d;');
                        $('.merchant-labels').attr('style', 'color: #7d7d7d;');
                        $('#merchant-logo-'+window.selectedOrg).addClass('gray-scale');
                    }
                    unBlockUI(el);
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }

