/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 3/02/17 14:11.
 */

"use strict";


//region Global Variables

//endregion

//region Default settings

//endregion

//region Events

//endregion

//region Functions
function confirmPassword(object) {
    var id = $('#active-confirm-password');
    var confirmPassword = id.val();
    var password = $('#active-password').val();

    if(password != confirmPassword) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff4d4d;"><i class="fa fa-exclamation-circle"></i> Passwords does not match</span>').hide().fadeIn(300);
        }
    }
    else {
        removeInternalError(id);
    }
}

function removeInternalError(id) {
    id.parent().removeClass('has-error');
    var idName = id.attr('id');
    $('#'+idName+'-error-msg').fadeOut(200, function () {
        $(this).remove();
    })
}

function activate() {
    var fName = $.trim($('#active-fname').val());
    var lName = $.trim($('#active-lname').val());
    var password = $.trim($('#active-password').val());
    var confirmPassword = $.trim($('#active-confirm-password').val());
    var email = $('#active-email').val();
    var code = $('#active-code').val();

    if(fName == '' || lName == '' || password == '' || confirmPassword == '') {
        $('#active-fname').trigger('blur');
        $('#active-lname').trigger('blur');
        $('#active-password').trigger('blur');
        $('#active-confirm-password').trigger('blur');
    }
    else if(password != confirmPassword) {
        $('#active-confirm-password').trigger('blur');
    }
    else {
        var el = $('#active-form');
        blockUI(el);

        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'active/active-account',
            data: {fname:fName, lname:lName, password:password, code:code, email:email},
            success: function (data) {
                unBlockUI(el);
                $("#active-form").slideUp(200, function () {
                    $('#active-confirm').slideDown(200);
                })
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
}
//endregion