/**
 * Created by isuru on 18/12/16.
 */
"use strict";

//init variables
window.actionPropertyId = null;
window.openedInspectionBlock = null;
window.loadingValue = 1;
window.loading = false;
window.aLPaginate = 0;
window.openedPropertyId = null;

//default settings
getPropertyList();

$('#property-activity-list-container').scroll(function () {
    var elementContainer = $('#property-activity-list-container');
    var elementHeight = elementContainer[0].scrollHeight;
    var scrollPosition = elementContainer.height() + elementContainer.scrollTop();

    if (elementHeight -100 < scrollPosition) {
        console.log(elementHeight);
        if (window.aLPaginate != 0) {
            window.aLPaginate++;
            var propertyId = window.openedPropertyId;
            getActivityLog(window.aLPaginate, propertyId);
        }
    }
});
//events


//functions
function getPropertyList() {
    var el = $('#property-container');
    blockUI(el);
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'property/get-property-list',
        success: function (data) {
            if(data == 'null') {
                unBlockUI(el);
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                var inspectors = '<option>Assign a Manager</option>';
                var htmlAppend;


                if(res.length > 1) {
                    for(var i = 1; i < res.length; i++) {

                        if(res[i].due != null) {
                            var dueDate = res[i].due.date;
                        }
                        else {
                            dueDate = 'No Inspection Schedule'
                        }

                        htmlAppend += '<tr id="pro-row-'+res[i].id+'"> ' +
                            '               <td onclick="getPopupDiv('+res[i].id+');" style="cursor: pointer;">'+res[i].reference+'<br/><span class="blue text-sm">'+res[i].address+'</span> </td> ' +
                            '               <td align="center"  onclick="getPopupDiv('+res[i].id+');" style="cursor: pointer;""> ' +
                            '                   <span>'+res[i].ownername+'</span>' +
                            '               </td> ' +
                            '               <td align="center"  onclick="getPopupDiv('+res[i].id+');" style="cursor: pointer;"> ' +
                            '                   <span>'+res[i].managername+'</span> ' +
                            '               </td> ' +
                            '               <td  style="cursor: pointer;">'+dueDate+'</td>' +
                            '               <td onclick="getPopupDiv('+res[i].id+');">'+res[i].inspectorname+'</td>' +
                            '               <td onclick="getPopupDiv('+res[i].id+');" style="cursor: pointer;">'+(res[i].frequency == '1' ? 'Weekly' :res[i].frequency == '2' ? 'fortnightly' :res[i].frequency == '3' ? '3 Weeks' :res[i].frequency == '4' ? 'Monthly' :res[i].frequency == '5' ? '2 Months' :res[i].frequency == '6' ? '3 Months' : res[i].frequency == '7' ? '4 Months' : res[i].frequency == '8' ? '6 Months' : res[i].frequency == '9' ? '9 Months' : res[i].frequency == '10' ? 'Annually' : '<span style="color: #b9b9b9;">N/A</span>')   +'</td> ' +
                            '           </tr>';
                    }
                }
                else {
                    htmlAppend = '<tr id="pro-row-na"> ' +
                        '               <td colspan="6" style="text-align: center;">No records to display!</td> ' +
                        '           </tr>';
                }

                $('#property-data').html(htmlAppend);
                unBlockUI(el);
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

//Property Popup Div
function getPopupDiv(propertyId) {
    popupDiv('pro-row-'+propertyId, propertyId, 'Wait...', '', 'height: 583px; width: 100%; top: -135px; left:0;');

    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'property/get-property-details',
        data: {propertyid: propertyId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);

                //property image
                var propertyImg = window.domain+'assets/images/property/'+propertyId+'.jpg';
                if(!doesFileExist(propertyImg))
                    propertyImg = window.domain+'assets/images/property/default/200.png';

                //owner details loading
                var avatar = window.domain+'assets/images/contact/avatar/'+res.owneravatar+'.jpg';
                if(!doesFileExist(avatar))
                    avatar = window.domain+'assets/images/contact/avatar/default.jpg';

                var propertyOwner = $('<div id="pro-'+propertyId+'-owner" class="tenent-container"> ' +
                    '                       <img src="'+avatar+'" class="img-circle contact-image" style="width: 28px; height: 28px;margin-top: -5px; border: 1px solid rgba(64, 113, 148, 0.35);"/> ' +
                    '                       <span style="margin-left: 10px;">'+res.ownername+'</span> ' +
                    '                       <span onclick="removePropertyOwner(\'pro-'+propertyId+'\')" style="margin-left: 20px;"><i class="fa fa-close" style="color: #e30010; font-size: 14px;"></i></span> ' +
                    '                       <input type="hidden" id="'+propertyId+'-contact-id" value="'+res.ownerid+'" />' +
                    '                   </div>'
                );

                //Property manager details loading
                var manager = '<option value="0" disabled="disabled">Assign Property Manager</option>';
                for(var i = 0; i < res.userlist.length; i++) {
                    var selected = res.managerid == res.userlist[i].id ? 'selected="selected"' : '';
                    manager+= '<option value="'+res.userlist[i].id+'" '+selected+'>'+res.userlist[i].name+'</option>';
                }

                //tenant details loading
                var tenant = '';
                for(var i = 0; i < res.tenants.length; i++) {
                    var avatar = window.domain+'assets/images/contact/avatar/'+res.tenants[i].avatar+'.jpg';
                    if(!doesFileExist(avatar))
                        avatar = window.domain+'assets/images/contact/avatar/default.jpg';
                    tenant += '<div id="pro-tenant-'+res.tenants[i].id+'" class="tenent-container"> ' +
                        '               <img src="'+avatar+'" class="img-circle contact-image" style="width: 28px; height: 28px;margin-top: -5px; border: 1px solid rgba(64, 113, 148, 0.35);"/> ' +
                        '               <span style="margin-left: 10px;">'+res.tenants[i].name+'</span> ' +
                        '               <span style="margin-left: 20px;" onclick="removeTenant('+res.id+');"><i class="fa fa-close" style="color: #e30010; font-size: 14px;"></i></span> ' +
                        '           </div><br/><br/>';
                }

                //inspection details loading
                var inspectionList = '';
                for(var i = 0; i < res.inspection.length; i++) {
                    var style, icon, color, tooltip = '';
                    if(res.inspection[i].overdue == 1) {
                        style = 'width: 30px; height: 30px;border-radius: 50%;background-color: #fd132f; top:-6px; position: absolute; padding: 0px 11px;font-size: 22px;';;
                        icon = 'fa-exclamation';
                        color = '#fd132f';
                        tooltip = 'title="Inspection Overdue!"';
                    }
                    else if(res.inspection[i].inspector == 0 || res.inspection[i].inspector == null) {
                        style = 'width: 30px; height: 30px;border-radius: 50%;background-color: #b9b9b9; top:-6px; position: absolute; padding: 0 8px; font-size: 22px;';
                        icon = 'fa-question';
                        color = '';
                        tooltip = 'title="Not assigned to an Inspector!"';
                    }
                    else if(res.inspection[i].done == 'Pending') {
                        style = 'width: 30px; height: 30px;border-radius: 50%;background-color: #1fc1c1; top:-6px; position: absolute; padding: 0 6px; font-size: 22px;';
                        icon = 'fa-ellipsis-h';
                        color = '';
                        tooltip = 'title="Pending Inspection"';
                    }
                    else {
                        style = 'width: 30px; height: 30px;border-radius: 50%;background-color: #4ec11f; top:-6px; position: absolute; padding: 0 5px; font-size: 22px;';
                        icon = 'fa-check';
                        color = '';
                        tooltip = 'title="Completed Inspection"';
                    }
                    inspectionList = '  <div id="inspection-title-block-'+res.inspection[i].id+'" class="slide-block col-xs-12" style="cursor: auto !important;"> ' +
                        '                   <div class="col-md-6 hidden-xs" onclick="openInspectionSlide('+res.inspection[i].id+');" style="cursor: pointer;"> ' +
                        '                       <span class="slide-down-circle" data-toggle="tooltip" '+tooltip+' style="'+style+'"><i class="fa '+icon+'"></i> </span><span style="margin-left: 50px;">'+res.inspection[i].inspectionid+'</span>'+
                        '                   </div> ' +
                        '                   <div class="col-md-6 slide-date" onclick="openInspectionSlide('+res.inspection[i].id+');" style="cursor: pointer;">' +
                        '                       '+res.inspection[i].due+'/<span style="color: '+color+';">'+res.inspection[i].done+'</span>' +
                        '                   </div>' +
                        '                   <div id="slide-block-loading-'+res.inspection[i].id+'" style="display: none;" class="slide-loading"></div>' +
                        '                   <div id="inspection-content-'+res.inspection[i].id+'" class="col-md-12" style="overflow-y: auto; max-height: 250px; margin-top: 15px;"><!-- Content --></div> ' +
                        '               </div>' ;
                }
                if(inspectionList == '') {
                    inspectionList = 'No Inspection Records!'
                }

                var popupDivHtml = $('  <div id="pro-info-container-'+propertyId+'" class="col-md-12"> ' +
                        '       <div class="col-md-12 col-xs-12" >' +
                        '           <div class="" role="tabpanel" data-example-id="togglable-tabs"> ' +
                        '               <ul id="pro-manage-tab-header-container" class="nav nav-tabs bar_tabs left" role="tablist"> ' +
                        '                   <li role="presentation" class="active"><a href="#pro-manage-tab-info" id="pro-manage-info-tabb" role="tab" data-toggle="tab" aria-controls="info" aria-expanded="true"><i class="fa fa-info-circle"></i> Info </a></li> ' +
                        '                   <li role="presentation"><a href="#pro-manage-tab-tenant" id="pro-manage-tenant-tabb" role="tab" data-toggle="tab" aria-controls="tenant" aria-expanded="true"><i class="fa fa-user"></i> Tenants </a></li> ' +
                        '                   <li role="presentation"><a href="#pro-manage-tab-inspection" id="pro-manage-inspection-tabb" role="tab" data-toggle="tab" aria-controls="inspection" aria-expanded="true"><i class="fa fa-edit"></i> Inspections </a></li> ' +
                        '                   <li role="presentation"><a href="#pro-manage-tab-service" id="pro-manage-service-tabb" role="tab" data-toggle="tab" aria-controls="service" aria-expanded="true"><i class="fa fa-wrench"></i> Service </a></li> ' +
                        '                   <li role="presentation"><a href="#pro-manage-tab-log" id="pro-manage-log-tabb" role="tab" data-toggle="tab" aria-controls="log" aria-expanded="true"><i class="fa fa-bullhorn"></i> Log </a></li> ' +
                        '               </ul> ' +
                        '               <div id="pro-manage-tab-content-container" class="tab-content"> ' +
                        '                   <div role="tabpanel" class="tab-pane fade active in" id="pro-manage-tab-info" aria-labelledby="home-tab"> ' +
                        '                       <div id="pro-basic-details-container" class="col-xs-12 col-md-10 col-md-offset-1"> ' +
                        '                           <div style="max-height: 370px; overflow-y: auto;"> ' +
                        '                               <div class="property-img-container" style="text-align: center;"> ' +
                        '                                   <img src="'+window.domain+'assets/images/property/default/200.png" /> ' +
                        '                               </div> ' +
                        '                               <div class="form-group margin-top-sm col-md-12" style="margin-bottom: 20px;"> ' +
                        '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12 margin-top-sm" for="mm-pro-owner">Property Owner:</label> ' +
                        '                                   <div class="col-md-8 col-sm-8 col-xs-12">       ' +
                        '                                       <div class="row" style="margin-top: 30px;"> ' +
                        '                                           <div id="pro-'+propertyId+'-owner-container" class="col-md-12"> ' +
                        '                                               <div id="pro-'+propertyId+'-add-owner" onclick="setPropertyOwner(\'pro-'+propertyId+'\');" class="tenent-container"> ' +
                        '                                                   <span style="margin-left: 10px;"><i class="fa fa-user-md"></i> Add Property Owner</span> ' +
                        '                                               </div> ' +
                        '                                           </div> ' +
                        '                                       </div> ' +
                        '                                   </div> ' +
                        '                               </div> ' +
                        '                               <div class="form-group col-md-12"> ' +
                        '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pro-street">Address:</label> ' +
                        '                                   <div class="col-md-7 col-sm-8 col-xs-12"> ' +
                        '                                       <input type="text" id="pro-street" placeholder="Street" onchange="updateSingleField(this, \'contact_address\', \'id\', \'street\', \'property address street\', '+res.addressid+');" value="'+res.street+'" onblur="requiredValidator(this, \'Please fill the street\');" maxlength="32" class="form-control col-md-7 col-xs-12">' +
                        '                                   </div> ' +
                        '                               </div> ' +
                        '                               <div class="form-group col-md-12"> ' +
                        '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pro-suburb"></span></label> ' +
                        '                                   <div class="col-md-7 col-sm-8 col-xs-12"> ' +
                        '                                       <input type="text" id="pro-suburb" placeholder="Suburb" onchange="updateSingleField(this, \'contact_address\', \'id\', \'adline1\', \'property address suburb\', '+res.addressid+');" value="'+res.suburb+'" onblur="requiredValidator(this, \'Please fill the suburb\');" maxlength="64" class="form-control col-md-7 col-xs-12"> ' +
                        '                                   </div> ' +
                        '                               </div> ' +
                        '                               <div class="form-group col-md-12"> ' +
                        '                                   <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pro-city"></span></label> ' +
                        '                               <div class="col-md-4 col-sm-4 col-xs-6"> ' +
                        '                                   <input type="text" id="pro-city" placeholder="City" onchange="updateSingleField(this, \'contact_address\', \'id\', \'city\', \'property address city\', '+res.addressid+');" value="'+res.city+'" onblur="requiredValidator(this, \'Please fill the city\');" maxlength="64" class="form-control col-md-7 col-xs-12"> ' +
                        '                               </div> ' +
                        '                               <div class="col-md-4 col-sm-4 col-xs-6"> ' +
                        '                                   <input type="text" id="pro-postal-code" placeholder="Postal Code" onchange="updateSingleField(this, \'contact_address\', \'id\', \'postalcode\', \'property address postal code\', '+res.addressid+');" value="'+res.postalcode+'" maxlength="10" class="form-control col-md-7 col-xs-12"> ' +
                        '                               </div> ' +
                        '                           </div> ' +
                        '                           <div class="form-group margin-top-sm col-md-12"> ' +
                        '                               <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pro-reference">Reference:</label> ' +
                        '                               <div class="col-md-7 col-sm-8 col-xs-12"> ' +
                        '                                   <input type="text" id="pro-reference" onchange="updateSingleField(this, \'property\', \'id\', \'reference\', \'property reference\', '+propertyId+');" value="'+res.reference+'" onblur="requiredValidator(this, \'Please fill the reference\');" maxlength="32" placeholder="Your Internal Asset Reference" class="form-control col-md-7 col-xs-12"> ' +
                        '                               </div> ' +
                        '                           </div> ' +
                        '                           <div class="form-group col-md-12"> ' +
                        '                               <label class="control-label col-md-4 col-sm-4 col-xs-12" for="pro-manager">Property Manager:</label> ' +
                        '                               <div class="col-md-7 col-sm-8 col-xs-12">   ' +
                        '                                   <select id="pro-manager" class="form-control col-md-7 col-xs-12" onchange="updateSingleField(this, \'property\', \'id\', \'propertymanager\', \'property manager\', '+propertyId+');">'+manager+'</select> ' +
                        '                               </div> ' +
                        '                           </div> ' +
                        '                           <div class="form-group"> ' +
                        '                               <label class="control-label col-md-4 col-sm-4 col-xs-12 margin-top-sm" for="pro-tenant">Tenant:</label> ' +
                        '                               <div class="col-md-8 col-sm-8 col-xs-12"> ' +
                        '                                   <div class="row" style="margin-top: 40px;"> ' +
                        '                               <div id="pro-'+propertyId+'-tenant-container" class="col-md-12"> ' +
                        '                                   <div id="pro-'+propertyId+'-add-tenant" class="tenent-container"> ' +
                        '                                   <span style="margin-left: 10px;"><i class="fa fa-user"></i> Add Tenant</span> ' +
                        '                               </div> ' +
                        '                           </div> ' +
                        '                           <div class="col-md-12 col-xs-12">' +
                        '                               <p style="margin-top:30px;"><button id="pro-delete-property" class="btn btn-danger" onclick="deleteProperty('+propertyId+');"><i class="fa fa-trash"></i> Delete Property</button></p>' +
                        '                           </div>' +
                        '                       </div> ' +
                        '                   </div></div></div></div></div>' +
                        '                   <div role="tabpanel" class="tab-pane fade" id="pro-manage-tab-tenant" aria-labelledby="home-tab"> ' +
                        '                       <div style="height: 395px;">    ' +
                        '                           Coming soon ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '                   <div role="tabpanel" class="tab-pane fade" id="pro-manage-tab-inspection" aria-labelledby="home-tab"> ' +
                        '                       <div class="slide-down row"> ' +
                        '                           <div class="col-md-12" style="text-align: left"> ' +
                        '                               <span class="group-title"></span> ' +
                        '                           </div> ' +
                        '                           <div class="col-xs-12 group"> ' +
                        '                               <div class="col-md-10 col-xs-12 col-md-offset-1"> '+
                                                            inspectionList+
                        '                               </div> ' +
                        '                           </div> ' +
                        '                           <span class="slide-add-circle"><i class="fa fa-plus"></i> </span> ' +
                        '                       </div> ' +
                        '                   </div>' +
                        '                   <div role="tabpanel" class="tab-pane fade" id="pro-manage-tab-service" aria-labelledby="home-tab"> ' +
                        '                       <div style="height: 395px;">    ' +
                        '                           Coming soon ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '                   <div role="tabpanel" class="tab-pane fade" id="pro-manage-tab-log" aria-labelledby="home-tab"> ' +
                        '                       <div id = "property-tab-activity-log-container">    ' +
                        '                           <div id = "property-activity-list-container" class = "row" style="padding: 10px 30px; max-height: 380px; overflow-y: auto;"> ' +
                        '                               <ul id="property-activity-list" class = "media-list">'+
                        '                               </ul> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '               </div> ' +
                        '           </div> ' +
                        '   </div>' +
                        '</div>');

                var titleHtml = res.reference+'<small id="div-popup-sub-title-'+propertyId+'">'+res.street+' '+res.suburb+'</small>';
                $('#div-popup-title-'+propertyId).html(titleHtml);
                var el = $('#div-popup-content-'+propertyId);



                el.html(popupDivHtml);
                $('#pro-info-container-'+propertyId).fadeIn(200);

                if(res.ownerid > 0)
                    $('#pro-'+propertyId+'-owner-container').html(propertyOwner);

                if(res.tenants.length > 0)
                    $('#pro-'+propertyId+'-tenant-container').prepend(tenant);

                window.openedPropertyId = propertyId;
                getPropertyLog(1, propertyId);

                unBlockUI(el);
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function removePropertyOwner(appendDivId) {
    window.appendDivId = appendDivId;
    var propertyId = window.openedPropertyId;

    var id = appendDivId+"-owner";
    nconfirm(id, 'Delete Permanently', 'Do you really want to remove this property ?', 'Remove', 'danger', -800, 3);

    window.confirmfunction = (function () {
        var appendDivId = window.appendDivId;
        $('#'+appendDivId+'-owner').fadeOut(200, function () {
            $('#'+appendDivId+'-owner')
                .html('<span style="margin-left: 10px;"><i class="fa fa-user-md"></i> Add Property Owner</span>' +
                    '   <input type="hidden" id="'+appendDivId+'-owner-contact-id" value="" />').fadeIn(100);
        });

        setTimeout(function () {
            $('#'+appendDivId+'-owner').attr('onclick', 'setPropertyOwner(\''+appendDivId+'\');');
        }, 300);


        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'property/remove-property-owner',
            data:{propertyid:propertyId},
            success: function (data) {
                if(data == 'null') {

                }
                else if(data == 1) {
                    cancelNConfirm(id);
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    });
    window.notConfirmed = (function () {});
}


function deleteProperty(propertyId) {
    window.actionPropertyId = propertyId;
    var id = 'pro-delete-property';
    nconfirm(id, 'Delete Permanently', 'Do you really want to remove this property ?', 'Remove', 'danger', -200, -8);

    window.confirmfunction = (function () {
        var propertyId = window.actionPropertyId;
        var el = $('#div-popup-content-'+propertyId);
        blockUI(el);

        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'property/remove-property',
            data:{propertyid:propertyId},
            success: function (data) {
                if(data == 'null') {

                }
                else if(data == 1) {
                    closePopupDiv(propertyId);
                    getPropertyList();
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    });
    window.notConfirmed = (function () {});
}


function getPropertyLog(paginate, propertyId) {
    var el = $('#property-activity-list-container');
    if (window.aLPaginate != -1) {
        var temPaginate = window.aLPaginate;
        window.aLPaginate = 0;
        blockUI(el);

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'property/get-activity-list',
            data: {paginate: paginate, propertyid: propertyId},
            success: function (data) {
                if(data == -1) {
                    temPaginate = -1;

                    if(paginate == 1) {
                        activityList = '<p style="text-align: center">No Records to display..!</p>';
                        $('#property-activity-list').append(activityList);
                    }
                    else {
                        activityList = '<p style="text-align: center">End of the records..!</p>';
                        $('#property-activity-list').append(activityList);
                    }
                    unBlockUI(el);
                }
                else if (data != 0) {
                    var ret = JSON.parse(data);
                    var activityList = "";
                    for (var i = 0; i < ret.length; i++) {
                        activityList += (
                            '               <li class="media">' +
                            '                   <p>' + ret[i].activity + '<br />' +
                            '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                            '                   </p>' +
                            '               </li>'
                        );
                    }

                    if(activityList == '' || activityList == null)
                        activityList = '<p style="text-align: center">No Records to display..!</p>';
                    console.log(activityList);
                    $('#property-activity-list').append(activityList);
                    window.aLPaginate = temPaginate;
                    window.aLPaginate++;
                    unBlockUI(el);
                }
                else {
                    dbError();
                    unBlockUI(el)
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection(textStatus);
            }
        });
    }
    else {
        unBlockUI(el);
    }
}

function openInspectionSlide(inspectionId) {
    if(window.openedInspectionBlock != null && window.openedInspectionBlock != inspectionId) {
        $('#inspection-content-slide').slideUp(300, function () {
            $('#inspection-title-block-'+window.openedInspectionBlock).css({'height':'40px'});
            $('#inspection-title-block-'+window.openedInspectionBlock).css('transform', 'scale(1.0)');
            $('#slide-block-loading-'+window.openedInspectionBlock).css("width", "0%");
        });
        window.openedInspectionBlock = inspectionId;
    }
    if(window.openedInspectionBlock != inspectionId) {
        window.openedInspectionBlock = inspectionId;
        $('#inspection-title-block-'+inspectionId).css('transform', 'scale(1.07)');
        $('#slide-block-loading-'+inspectionId).css("width", "1%");
        $('#slide-block-loading-'+inspectionId).fadeIn(100);
        window.loading = true;
        loadingTimer('slide-block-loading-'+inspectionId);

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'property/get-single-inspection',
            data:{inspectionid:inspectionId},
            success: function (data) {
                if(data == 'null') {

                }
                else if(data != 0) {
                    window.loading = false;
                    $('#slide-block-loading-'+inspectionId).animate({width:'98%'}, 200);
                    $('#slide-block-loading-'+inspectionId).fadeOut(400);
                    var res = JSON.parse(data);

                    //Inspector
                    var options = '<option value="0" disabled selected>No one selected</option>';
                    for(var i = 0; i < res.userlist.length; i++) {
                        options += '<option value="'+res.userlist[i].id+'" '+(res.inspector == res.userlist[i].id ? 'selected': '')+'>'+res.userlist[i].fname+' '+res.userlist[i].lname+'</option>'
                    }

                    //Inspection Status
                    if(res.status > 0 && res.status < 5) {
                        var status = '<i class="fa fa-laptop" style="color: #f96d22;"></i>&nbsp;&nbsp;Inspection&nbsp;Ongoing';
                        var inspectionButton = '<p style="text-align:center;"><a href="'+window.domain+'inspection/new/'+inspectionId+'" target="_blank"><button class="btn btn-sm btn-info">Continue&nbsp;Inspection</button></a></p>';
                    }
                    else if(res.status == 5) {
                        var status = '<i class="fa fa-check-square" style="color: #21f977;"></i>&nbsp;&nbsp;Completed&nbsp;Inspection';
                        var inspectionButton = '<p style="text-align:center;"><a href="'+window.domain+'inspection/new/'+inspectionId+'" target="_blank"><button class="btn btn-sm btn-info">View&nbsp;Inspection</button></a><button class="btn btn-sm btn-success">View&nbsp;Report</button></p>';
                    }
                    else {
                        var status = '<i class="fa fa-refresh" style="color: #1864f9;"></i>&nbsp;&nbsp;Pending';
                        var inspectionButton = '<p style="text-align:center;"><a href="'+window.domain+'inspection/new/'+inspectionId+'" target="_blank"><button class="btn btn-sm btn-info">Start&nbsp;Inspection</button></a></p>';
                    }

                    var appendHtml = $('<div class="col-md-12" id="inspection-content-slide" style="height: 250px;">' +
                        '                   <div class="col-md-7 bs-callout bs-callout-info">' +
                        '                       <h4><i class="fa fa-info-circle"></i> Inspection Info</h4><div id="as-schedule-inspection-container"> ' +
                        '                       <div class="form-group has-feedback margin-top-sm col-xs-12"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12" for="slide-inspect-date">Inspection Date:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top:-10px;"> ' +
                        '                               <input id="slide-inspection-date" class="datepicker form-control col-md-7 col-xs-12 has-feedback-left" onchange="customUpdateSingleField(this, \'inspection_schedule\', \'id\', \'scheduledatetime\', \'inspection schedule date\', '+inspectionId+', \'property/update-inspection-date\');" value="'+res.date+'" required="required" type="text"> ' +
                        '                               <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                       <div class="form-group has-feedback col-xs-12" style="margin-top:8px;"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12" for="slide-inspect-time">Inspection Time:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top:-6px;"> ' +
                        '                               <input id="slide-inspection-time" class="timepicker form-control col-md-7 col-xs-12 has-feedback-left" onchange="customUpdateSingleField(this, \'inspection_schedule\', \'id\', \'scheduledatetime\', \'inspection schedule time\', '+inspectionId+', \'property/update-inspection-time\');" value="'+res.time+'" required="required" type="text"> ' +
                        '                               <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                       <div class="form-group margin-top-sm col-xs-12"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12" for="slide-frequency">Inspection Frequency:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top:-10px;"> ' +
                        '                               <select id="slide-frequency" onchange="updateSingleField(this, \'property\', \'id\', \'frequency\', \'inspection frequency\', '+res.propertyid+');" class="form-control col-md-7 col-xs-12"> ' +
                        '                                   <option value="0">No Repeat Inspcetion</option> ' +
                        '                                   <option value="1" '+(res.frequency == 1 ? 'selected': '')+'>1 Week</option> ' +
                        '                                   <option value="2" '+(res.frequency == 2 ? 'selected': '')+'>2 Weeks</option> ' +
                        '                                   <option value="3" '+(res.frequency == 3 ? 'selected': '')+'>3 Weeks</option> ' +
                        '                                   <option value="4" '+(res.frequency == 4 ? 'selected': '')+'>1 Month</option> ' +
                        '                                   <option value="5" '+(res.frequency == 5 ? 'selected': '')+'>2 Months</option> ' +
                        '                                   <option value="6" '+(res.frequency == 6 ? 'selected': '')+'>3 Months</option> ' +
                        '                                   <option value="7" '+(res.frequency == 7 ? 'selected': '')+'>4 Months</option> ' +
                        '                                   <option value="8" '+(res.frequency == 8 ? 'selected': '')+'>6 Months</option> ' +
                        '                                   <option value="9" '+(res.frequency == 9 ? 'selected': '')+'>9 Months</option> ' +
                        '                                   <option value="10" '+(res.frequency == 10 ? 'selected': '')+'>12 Months</option> ' +
                        '                               </select> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                       <div class="form-group margin-top-sm col-xs-12"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12" for="slide-inspection-type">Inspection Type:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top:-10px;"> ' +
                        '                               <select id="slide-inspection-type" onchange="updateSingleField(this, \'inspection_schedule\', \'id\', \'type\', \'inspection type\', '+inspectionId+');" class="form-control col-md-7 col-xs-12"> ' +
                        '                                   <option value="0" '+(res.type == 0 ? 'selected': '')+'>Simple Inspection</option> ' +
                        '                                   <option value="1" '+(res.type == 1 ? 'selected': '')+'>Move-In Inspection</option> ' +
                        '                                   <option value="2" '+(res.type == 2 ? 'selected': '')+'>Move-Out Inspection</option> ' +
                        '                               </select> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                       <div class="form-group margin-top-sm col-xs-12"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12" for="slide-inspection-inspector">Assigned Inspector:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12" style="margin-top:-10px;"> ' +
                        '                               <select onchange="customUpdateSingleField(this, \'inspection_schedule\', \'id\', \'inspector\', \'assigned inspector\', '+inspectionId+', \'property/update-inspection-inspector\');" id="slide-inspection-inspector" class="form-control col-md-7 col-xs-12"> ' +options+
                        '                               </select> ' +
                        '                           </div> ' +
                        '                       </div> ' +
                        '                       <div class="form-group margin-top-sm col-xs-12"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12">Created By:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12"> '+ res.createdby+
                        '                           </div> ' +
                        '                       </div> ' +
                        '                       <div class="form-group col-xs-12"> ' +
                        '                           <label class="control-label col-md-4 col-sm-4 col-xs-12">Created At:</label> ' +
                        '                           <div class="col-md-8 col-sm-8 col-xs-12"> '+ res.createdat+
                        '                           </div> ' +
                        '                       </div> ' +
                        '                   </div>' +
                        '               </div>' +
                        '               <div class="col-md-5">' +
                        '                   <div class="col-md-12 bs-callout bs-callout-success">' +
                        '                       <h4><i class="fa fa-file-text-o"></i> Inspection Report</h4>' +
                        '                       <div class="col-md-12" style="margin-top: 20px;">' +
                        '                           <div class="col-xs-4"><strong>Status</strong></div>' +
                        '                           <div class="col-xs-8">'+status+'</div>' +
                        '                       </div> ' +
                        '                       <div class="col-md-12" style="margin-top: 30px;">' +inspectionButton+
                        '                       </div>' +
                        '                   </div> ' +
                        '               </div> ' +
                        '           </div>'
                    ).hide();

                    var el = $('#inspection-content-'+inspectionId);
                    blockUI(el);

                    $('#inspection-content-'+inspectionId).append(appendHtml);
                    $('#inspection-title-block-'+inspectionId).css({'height':'300px'});
                    setTimeout(function () {
                        $('#inspection-content-slide').slideDown(300);
                    }, 10);
                    unBlockUI(el);
                    $('#slide-inspection-date').pickadate();
                    $('#slide-inspection-time').pickatime();
                    window.loading = false;
                    window.loadingValue = 4;

                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection(errorThrown);
            }
        });

    }
    else {
        $('#inspection-content-slide').slideUp(300, function () {
            $('#inspection-title-block-'+inspectionId).css({'height':'40px'});
            $('#inspection-title-block-'+inspectionId).css('transform', 'scale(1.0)');
            $('#slide-block-loading-'+inspectionId).css("width", "0%");
        });
        window.openedInspectionBlock = null;
    }
}

function loadingTimer(id) {
    window.loadingValue++;
    var value = window.loadingValue;
    if(value < 45) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 100);
                loadingTimer(id);
            }
        }, 100);
    }
    else if(value < 80) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 200);
                loadingTimer(id);
            }
        }, 200);
    }
    else if(value < 90) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 400);
                loadingTimer(id);
            }
        }, 400);
    }
    else if(value < 98) {
        setTimeout(function () {
            if(window.loading) {
                $('#'+id).animate({width:value+'%'}, 800);
                loadingTimer(id);
            }
        }, 800);
    }
}