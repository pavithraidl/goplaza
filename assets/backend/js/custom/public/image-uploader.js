/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 14/03/17 14:55.
 */

"use strict";

$(function(){
    // function from the jquery form plugin
    $('.image-upload').ajaxForm({
        beforeSend:function(){
            $("#image-upload-progress").show();
        },
        uploadProgress:function(event,position,total,percentComplete){
            $("#image-upload-progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
            $("#image-upload-sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            $("#image-upload-progress").hide();
        },
        complete:function(response){

            var res = response.responseText;
            var res = JSON.parse(res);

            console.log(res);
            // if(res.type == 1) {
            //     var imgToAppend = '<div class="col-md-4 ins-single-media-container" id="ins-single-media-container-'+res.id+'" style="margin-top: 10px;">' +
            //         '                   <img src="'+window.domain+res.src+'" style="width: 100%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" />' +
            //         '                   <i id="remove-media-'+res.id+'" class="fa fa-times-circle hover-fade" onclick="removeMedia('+res.id+');" style="color: #e30010; font-size: 30px; position: absolute; right: -3px; top: -15px;"></i> ' +
            //         '               </div>';
            // }
            // else if(res.type == 2) {
            //     var imgToAppend = '<div id="ins-single-media-container-'+res.id+'" class="col-md-4 ins-single-media-container" style="margin-top: 10px;">' +
            //         '                   <video width="400" controls> ' +
            //         '                       <source src="'+window.domain+res.src+'" type="video/mp4"> ' +
            //         '                           Your browser does not support video. ' +
            //         '                   </video>' +
            //         '                   <i id="remove-media-'+res.id+'" class="fa fa-times-circle hover-fade" onclick="removeMedia('+res.id+');" style="color: #e30010; font-size: 30px; position: absolute; right: -3px; top: -15px;"></i> ' +
            //         '               </div> ';
            // }
            //
            // if(res.type == 1) {
            //     var reportAppend = '<div id="ins-r-single-media-container-'+res.id+'" class="col-md-4 ins-single-media-container" style="margin-top: 10px;"> ' +
            //         '                   <img src="'+window.domain+res.src+'" style="width: 100%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" /> ' +
            //         '               </div>';
            // }
            // else if(res.type == 2) {
            //     var reportAppend = '<div id="ins-r-single-media-container-'+res.id+'" class="col-md-4 ins-single-media-container" style="margin-top: 10px;">' +
            //         '                   <video style="width: 75%; height: auto; border: 1px solid rgba(122, 192, 246, 0.81);" controls>' +
            //         '                       <source src="'+window.domain+res.src+'" type="video/mp4">' +
            //         '                       Your browser does not support video.' +
            //         '                   </video>' +
            //         '               </div>';
            // }

            $('#image-upload-progress').remove();

            var type = parseFloat(res.type-1);
            $('#img-main-img').attr('src', window.domain+res.src);
            $('#img-thumb-'+type).attr('src', window.domain+res.src);
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});

