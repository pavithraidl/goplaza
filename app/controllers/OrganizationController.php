<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-06-06 11:37:27.
 * Controller ID - 8
 */

class OrganizationController extends BaseController
{
    const SystemId = 3;
    const ControllerId = 5;


    public function getOrganizations() {
        $viewId = 49;
        $urlRouteId = 122;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.organizations.organizations', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function addOrganization() {
        //Function data
        $functionId = 254;

        $orgName = Input::get('orgname');
        $name = Input::get('adminname');
        $email = Input::get('adminemail');

        try {
            //Create a user account for the admin
            if (User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            } else {

                $words = explode(' ', $name);
                array_shift($words);
                $lName = implode(' ', $words);
                $name = explode(" ", $name);
                $fName = $name[0];

                $roll = 3;
                $code = str_random(60);
                $jobTitle = 'System Admin';

                $user = new User();
                $user -> fname = ucfirst($fName);
                $user -> lname = ucfirst($lName);
                $user -> email = $email;
                $user -> code = $code;
                $user -> active = 0;
                $user -> jobtitle = $jobTitle;
                $user -> roll = $roll;
                $user -> status = 3;
                $user -> created_at = \Carbon\Carbon::now('UTC');
                $user -> save();


                //Create the organization
                //generate secret
                $secret = str_random(32);

                $org = new Org();
                $org -> name = $orgName;
                $org -> admin = $user -> id;
                $org -> parent = Auth::user()->orgid;
                $org -> orgkey = $secret;
                $org -> status = 2;
                $org -> created_by = Auth::user()->id;
                $org -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $org -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $org -> save();
                $orgId = $org -> id;

                $orgKey = str_random(4).str_pad($orgId, 4, '0', STR_PAD_LEFT);

                $org -> orgkey = $orgKey;
                $org -> save();

                $user -> orgid = $orgId;
                $user -> save();

                return 1;

            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeOrgStatus() {
        //Function data
        $functionId = 256;

        $status = Input::get('status');
        $orgId = Input::get('orgid');

        try {
            if($status == 0) {
                $status = 2;
            }

            Org::where('id', $orgId)->update(array(
                'status' => $status
            ));

            $ret = array(
                'activation_status' => Org::where('id', $orgId)->pluck('activation_link')
            );

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeAccess() {
        //Function data
        $functionId = 260;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeViewAccess() {
        //Function data
        $functionId = 262;

        $orgId = Input::get('orgid');
        $status = Input::get('status');
        $viewId = Input::get('viewid');

        try {
            $userId = Org::where('id', $orgId)->pluck('admin');

            if(SystemViewAccess::where('viewid', $viewId)->where('userid', $userId)->pluck('id')) {
                SystemViewAccess::where('viewid', $viewId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemViewAccess();
                $systemAccess -> viewid = $viewId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeOperationAccess() {
        //Function data
        $functionId = 264;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveSettings() {
        //Function data
        $functionId = 266;

        $value = Input::get('value');
        $field = Input::get('field');
        $table = Input::get('table');
        $orgId = Input::get('orgid');

        try {
            $id = DB::table($table)->where('status', 1)->where('orgid', $orgId)->pluck('id');

            if(!$id) {
                DB::table($table)->insert(array(
                    'status' => 1,
                    'created_by' => Auth::user()->id,
                    'orgid' => $orgId,
                    'updated_at' => \Carbon\Carbon::now('Pacific/Auckland'),
                    'created_at' => \Carbon\Carbon::now('Pacific/Auckland')
                ));

                $id = DB::table($table)->where('status', 1)->where('orgid', $orgId)->pluck('id');
            }

            DB::table($table)->where('orgid', $orgId)->update(array(
                $field => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function sendActivationEmail() {
        //Function data
        $functionId = 268;

        $orgId = Input::get('orgid');

        try {
            $org = Org::where('id', $orgId)->first();

            $user = User::where('id', $org->admin)->first();
            $code = $user->code;
            $orgName = $org->name;
            $fName = $user->fname;
            $lName = $user->lname;
            $email = $user->email;

            Mail::send('backend.emails.active', array(
                'link' => URL::route('active', $code),
                'orgName' => $orgName,
                'name' => $fName . ' ' . $lName,
                'homeurl' => URL::To('/')
            ), function ($message) use ($email, $fName) {
                $message->to($email, $fName)->subject('Activate your account');
            });

            return json_encode($fName);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveMerchant() {
        //Function data
        $functionId = 324;

        $orgId = Input::get('orgid');
        $merchantType = Input::get('merchanttype');
        $status = Input::get('status');

        try {
            if($merchantType == 1) {
                $merchant = OrgMerchantPoli::where('orgid', $orgId)->where('status', 1)->first();

                if(!$merchant) {
                    $merchant = new OrgMerchantPoli();
                    $merchant -> orgid = $orgId;
                    $merchant -> status = 1;
                    $merchant -> created_by = Auth::user()->id;
                    $merchant -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $merchant -> save();

                    $orgMerchant = new OrgMerchant();
                    $orgMerchant -> merchant_detail_id = $merchantType;
                    $orgMerchant -> merchant_id = $merchant -> id;
                    $orgMerchant -> status = 1;
                    $orgMerchant -> orgid = $orgId;
                    $orgMerchant -> created_by = Auth::user()->id;
                    $orgMerchant -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $orgMerchant -> save();
                }

                $merchant -> status = $status;
                $merchant -> save();

                return 1;
            }

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}