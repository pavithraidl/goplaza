<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-27 13:51:58.
 * Controller ID - 58
 */

class GeneralController extends BaseController
{
    const SystemId = 21;
    const ControllerId = 58;

                    
    public function createContact($fname, $lname, $gender, $title, $phones, $emails, $addresses) {
        //Function data
        $functionId = 163;

        try {
            $contact = new GeneralContact();
            $contact -> fname = $fname;
            $contact -> lname = $lname;
            $contact -> gender = $gender;
            $contact -> title = $title;
            $contact -> status = 1;
            $contact -> created_by = Auth::user()->id;
            $contact -> orgid = Auth::user()->orgid;
            $contact -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $contact -> save();
            $contactId = $contact->id;

            if(sizeof($phones) > 0) {
                foreach ($phones as $phone) {
                    $newPhone = new GeneralTelephone();
                    $newPhone -> foreignid = $contactId;
                    $newPhone -> telephone = $phone->number;
                    $newPhone -> type = $phone -> type;
                    $newPhone -> status = 1;
                    $newPhone -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                }
            }

            if(sizeof($emails) > 0) {
                foreach ($emails as $email) {
                    $newEmail = new GeneralTelephone();
                    $newEmail -> foreignid = $contactId;
                    $newEmail -> telephone = $email->address;
                    $newEmail -> type = $email -> type;
                    $newEmail -> status = 1;
                    $newEmail -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                }
            }

            if(sizeof($addresses) > 0) {
                foreach ($addresses as $address) {
                    $newAddress = new GeneralTelephone();
                    $newAddress -> foreignid = $contactId;
                    $newAddress -> street = $address->street;
                    $newAddress -> adline1 = $address -> adline1;
                    $newAddress -> adline1 = $address -> adline2;
                    $newAddress -> adline1 = $address -> city;
                    $newAddress -> adline1 = $address -> postcode;
                    $newAddress -> adline1 = $address -> country;
                    $newAddress -> status = 1;
                    $newAddress -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                }
            }

            return $contactId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}