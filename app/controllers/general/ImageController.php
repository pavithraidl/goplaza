<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-14 14:05:50.
 * Controller ID - 70
 */

class ImageController extends BaseController
{
    const SystemId = 21;
    const ControllerId = 70;

                    
    public function uploadImage() {
        //Function data
        $functionId = 193;
        $productId = Input::get('productid');
        $imgType = Input::get('imgtype');
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;
        $ext = strtolower($imageFileType);
        $destinationDir = $_SERVER['DOCUMENT_ROOT'].'/assets/backend/images/products/products/';

        try {
            $srcDir = 'assets/backend/others/orgs/temp/' . Auth::user()->orgid . "/images/";

            if (!file_exists($srcDir)) {
                mkdir($srcDir, 0777);
            }

            $srcFileName = Auth::user()->orgid.str_random(10) . '.' . $ext;

            Input::file('file')->move($srcDir, $srcFileName);

            $product = Product::where('id', $productId)->first();

            if($product) {
                $detailId = ProductDetail::where('productid', $product->id)->where('default_product', 1)->pluck('id');

                $dbImgRelation = ProductImgRelation::where('productid', $productId)->where('type', $imgType)->first();
                if(!$dbImgRelation) {
                    $dbImgRelation = new ProductImgRelation();
                    $dbImgRelation -> productid = $product->id;
                    $dbImgRelation -> detailid = $detailId;
                    $dbImgRelation -> type = $imgType;
                    $dbImgRelation -> global_img = 0;
                    $dbImgRelation -> status = 1;
                    $dbImgRelation -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $dbImgRelation -> save();
                }
                $dbImg = ProductImage::where('id', $dbImgRelation->imgid)->first();

                if(!$dbImg) {
                    $dbImg = new ProductImage();
                    $dbImg -> ext = $ext;
                    $dbImg -> status = 1;
                    $dbImg -> created_by = 86;
                    $dbImg -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $dbImg -> save();
                }
                $imgId = $dbImg -> id;
                $dbImgRelation -> imgid = $imgId;
                $dbImgRelation -> save();

                $destinationUrl = $destinationDir.$imgId.'.'.$ext;



                $width = 600;
                $height = 600;
                $quality = 95;

                if($ext == 'jpg')
                    $img = imagecreatefromjpeg($srcDir.$srcFileName);
                else if($ext == 'png')
                    $img = imagecreatefrompng($srcDir.$srcFileName);

                //get this values from user by submitting form ( either by crop or by textboxes)
                list($old_width, $old_height, $type, $attr) = getimagesize($srcDir.$srcFileName);

                $dest = ImageCreateTrueColor($width, $height);
                imagecopyresampled(
                    $dest, //destination image
                    $img, //source image
                    0, //top left coordinate of the destination image in x direction
                    0, //top left coordinate of the destination image in y direction
                    0, //top left coordinate in x direction of source image that I want copying to start at
                    0, //top left coordinate in y direction of source image that I want copying to start at
                    $width, //190, thumbnail width
                    $height, //190, thumbnail height
                    $old_width, //how wide the rectangle from the source image we want thumbnailed is
                    $old_height //how high the rectangle from the source image we want thumbnailed is
                );
                imagejpeg($dest, $destinationUrl, $quality);

                //thumb img
                $destinationUrl = $destinationDir.$imgId.'_thumb.'.$ext;
                $dest = ImageCreateTrueColor(200, 200);
                imagecopyresampled(
                    $dest, //destination image
                    $img, //source image
                    0, //top left coordinate of the destination image in x direction
                    0, //top left coordinate of the destination image in y direction
                    0, //top left coordinate in x direction of source image that I want copying to start at
                    0, //top left coordinate in y direction of source image that I want copying to start at
                    200, //190, thumbnail width
                    200, //190, thumbnail height
                    $old_width, //how wide the rectangle from the source image we want thumbnailed is
                    $old_height //how high the rectangle from the source image we want thumbnailed is
                );
                imagejpeg($dest, $destinationUrl, $quality);

                unlink($srcDir.$srcFileName);

                $ret = array(
                    'src' => '/assets/backend/images/products/products/'.$imgId.'.'.$ext,
                    'type' => $imgType
                );
                return json_encode($ret);
            }
            else {
                return json_encode('not found!');
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function resizeImg($src, $ext, $srcWidth, $srcHeight, $distWidth, $distHeight, $srcX, $srcY, $distX, $distY, $quality, $distUrl ) {
        //Function data
        $functionId = 400;

        try {
            if($ext == 'jpg')
                $img = imagecreatefromjpeg($src);
            else if($ext == 'png')
                $img = imagecreatefrompng($src);

            if($distHeight == 'auto') {
                $hwRatio = $srcWidth/$distWidth;
                $distHeight = $srcHeight/$hwRatio;
            }

            if($distWidth == 'auto') {
                $hwRatio = $srcHeight/$distHeight;
                $distWidth = $srcWidth/$hwRatio;
            }

            //get this values from user by submitting form ( either by crop or by textboxes)
            list($old_width, $old_height, $type, $attr) = getimagesize($src);

            $dest = ImageCreateTrueColor($distWidth, $distHeight);
            imagecopyresampled(
                $dest, //destination image
                $img, //source image
                $distX, //top left coordinate of the destination image in x direction
                $distY, //top left coordinate of the destination image in y direction
                $srcX, //top left coordinate in x direction of source image that I want copying to start at
                $srcY, //top left coordinate in y direction of source image that I want copying to start at
                $distWidth, //190, thumbnail width
                $distHeight, //190, thumbnail height
                $old_width, //how wide the rectangle from the source image we want thumbnailed is
                $old_height //how high the rectangle from the source image we want thumbnailed is
            );
            imagejpeg($dest, $distUrl, $quality);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}