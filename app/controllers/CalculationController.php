<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 2/09/16
 * Time: 4:51 PM
 */

class CalculationController extends BaseController
{
    public function timeAgo($d)
    {
        $timeAgo = "";
        $curd = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');

        $to_time = strtotime($curd);
        $from_time = strtotime($d);
        $minits = round(abs($to_time - $from_time) / 60,2);

        if($minits > 1440 * 4){
            $year = substr($d, 0, 4);
            $curdYear = substr($curd,0,4);

            if($year == $curdYear){
                $timeAgo = date_format((date_create($d)), "M j");
            }
            else{
                $timeAgo = date_format((date_create($d)), "j M y");
            }
        }
        else if($minits < 1440*4 && $minits > 1440*3){
            $timeAgo = '3d ago';
        }
        else if($minits < 1440*3 && $minits > 1440*2){
            $timeAgo = '2d ago';
        }
        else if($minits < 1440*2 && $minits > 1440*1){
            $timeAgo = '1d ago';
        }
        else if($minits > 60){
            $hours = round($minits/60);
            $timeAgo = $hours."h ago";
        }
        else if($minits >1){
            $timeAgo = round($minits)."min ago";
        }
        else if($minits < 1){
            $timeAgo = "just now";
        }
        return $timeAgo;
    }

    function dayCount($d) {
        $dayAgo = "";
        $curd = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');

        $to_time = strtotime($curd);
        $from_time = strtotime($d);
        $minits = round(abs($to_time - $from_time) / 60,2);

        if($minits > 1440 * 3){
            $year = substr($d, 0, 4);
            $curdYear = substr($curd,0,4);

            if($year == $curdYear){
                $dayAgo = date_format((date_create($d)), "M j");
            }
            else{
                $dayAgo = date_format((date_create($d)), "j-M-y");
            }
        }
        else {
            $dDay = date('d', strtotime($d));
            $curDay = date('d', strtotime($curd));

            if($dDay == $curDay) {
                $dayAgo = 'Today';
            }
            else if($dDay == $curDay-1) {
                $dayAgo = "Yesterday";
            }
            else if($dDay == $curDay-2) {
                $dayAgo = "2 Days Ago";
            }
            else if($dDay == $curDay-3) {
                $dayAgo = "3 Days Ago";
            }
        }
//        else if($minits < 1440*4 && $minits > 1440*3){
//            $dayAgo = 'Three days ago';
//        }
//        else if($minits < 1440*3 && $minits > 1440*2){
//            $dayAgo = 'Two days ago';
//        }
//        else if($minits < 1440*2 && $minits > 1440*1){
//            $dayAgo = 'Yesterday';
//        }
//        else {
//            $dayAgo = 'Today';
//        }
        return $dayAgo;
    }

    public function shortDate($d){
        $curd = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');
        $year = substr($d, 0, 4);
        $curdYear = substr($curd,0,4);
        $timeAgo = "";

        if($year == $curdYear){
            $timeAgo = date_format((date_create($d)), "j M");
        }
        else{
            $timeAgo = date_format((date_create($d)), "j M y");
        }
        return $timeAgo;
    }

    public function shortFullDate($d){
        $curd = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');
        $year = substr($d, 0, 4);
        $curdYear = substr($curd,0,4);
        $timeAgo = "";
        $timeAgo = date_format((date_create($d)), "\<\s\\t\\r\o\\n\g\>j M\<\/\s\\t\\r\o\\n\g\> Y H:i:s");
        return $timeAgo;
    }

    public function getMonthListArray($monthNumber, $requiredLength, $type, $back) {
        $shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $longMonths = ['January', 'February', 'March', 'April', 'May', 'Jun', 'July', 'August', 'September', 'October', 'November', 'December'];
        $arrNumber = --$monthNumber;
        $ret = array();

        if($type == 1) {
            if($back == 1) {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $shortMonths[$arrNumber]);

                    if($arrNumber == 0) $arrNumber = 12;
                    $arrNumber--;
                }
            }
            else {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $shortMonths[$arrNumber]);
                    $arrNumber++;
                    if($arrNumber == 12) $arrNumber = 0;
                }
            }
        }
        else if($type == 2) {
            if($back == 1) {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $longMonths[$arrNumber]);

                    if($arrNumber == 0) $arrNumber = 12;
                    $arrNumber--;
                }
            }
            else {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $longMonths[$arrNumber]);
                    $arrNumber++;
                    if($arrNumber == 12) $arrNumber = 0;
                }
            }
        }

        return $ret;
    }

    public function getDayListArray($dayNumber, $requiredLength, $type, $back) {
        $shortDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        $longDays = ['Sunday', 'Monday', 'Tueday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        $arrNumber = --$dayNumber;
        $ret = array();

        if($type == 1) {
            if($back == 1) {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $shortDays[$arrNumber]);

                    if($arrNumber == 0) $arrNumber = 7;
                    $arrNumber--;
                }
            }
            else {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $shortDays[$arrNumber]);
                    $arrNumber++;
                    if($arrNumber == 7) $arrNumber = 0;
                }
            }
        }
        else if($type == 2) {
            if($back == 1) {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $longDays[$arrNumber]);

                    if($arrNumber == 0) $arrNumber = 7;
                    $arrNumber--;
                }
            }
            else {
                for($i = 0; $i < $requiredLength; $i++) {
                    array_push($ret, $longDays[$arrNumber]);
                    $arrNumber++;
                    if($arrNumber == 7) $arrNumber = 0;
                }
            }
        }

        return $ret;
    }

    public function getCalendarDays($start, $end) {
        $arr = array();
        $now = strtotime($start);
        $last = strtotime($end);

        while($now <= $last ) {
            $arr[] = array(
                'date' => date('Y-m-d', $now),
                'year' => date('Y', $now),
                'month' => date('m', $now),
                'date' => date('d', $now),
                'short_month' => date('M', $now),
                'day' => date('l', $now),
                'short_day' => date('D', $now)
            );

            $now = strtotime('+1 day', $now);
        }

        return $arr;
    }
}