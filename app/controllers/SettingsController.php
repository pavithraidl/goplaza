<?php

class SettingsController extends BaseController
{

    /************************| SettingsControlelr |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:48.
     */


    public function getSettings() {
        return View::make('backend.settings');
    }
}