<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-23 20:02:59.
 * Controller ID - 90
 */

class NotificationController extends BaseController
{
    const SystemId = 5;
    const ControllerId = 90;


    public function saveSystemNotification($msg, $orgId, $notificationId) {
        //Function data
        $functionId = 328;

        try {
            $notification = SystemNotification::where('notification_id', $notificationId)->where('status', 1)->first();

            if(!$notification) {
                $notification = new SystemNotification();
                $notification -> msg = $msg;
                $notification -> type = 1;
                $notification -> status = 1;
                $notification -> orgid = $orgId;
                $notification -> notification_id = $notificationId;
                $notification -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $notification -> save();
            }

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}