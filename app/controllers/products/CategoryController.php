<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-07 20:41:41.
 * Controller ID - 82
 */

class CategoryController extends BaseController
{
    const SystemId = 25;
    const ControllerId = 82;

                    
    public function updateCategoryDetails() {
        //Function data
        $functionId = 307;

        $value = Input::get('value');
        $categoryId = Input::get('categoryid');
        $field = Input::get('field');

        try {
            ProductCategory::where('id', $categoryId)->update(array(
                $field => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addCategory() {
        //Function data
        $functionId = 309;

        $menuItemId = Input::get('menuitemid');
        $subItemid = Input::get('subitemid');

        try {
            if($subItemid == '') {
                $subItemid = null;
            }

            $category = new ProductCategory();
            $category -> name = 'Untitled';
            $category -> href = '#';
            $category -> menu_item_id = $menuItemId;
            $category -> sub_item = $subItemid;
            $category -> list_order = 100;
            $category -> status = 1;
            $category -> created_by = Auth::user()->id;
            $category -> orgid = Auth::user()->orgid;
            $category -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $category -> save();
            $categoryId = $category -> id;

            return $categoryId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveCategoryImg() {
        //Function data
        $functionId = 315;

        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $ext = strtolower($imageFileType);

        $categoryId = Input::get('categoryid');
        $imgType = Input::get('imgtype');

        try {
            if($ext == "jpg" || $ext == "jpeg" || $ext == 'png') {
                $dirPath = "assets/backend/images/products/categories/".Auth::user()->orgid."/";
                //create the directory named by user id
                if (!file_exists($dirPath)) {
                    mkdir($dirPath, 0777);
                }

                if($imgType == 1) {
                    $fileName = $categoryId.'.jpg';
                }
                else {
                    $fileName = $categoryId.'-banner.jpg';
                }

                Input::file('file')->move($dirPath, $fileName);

                if($imgType == 1) {
                    ProductCategory::where('id', $categoryId)->update(array('img' => 1, 'img_ext' => $ext));
                }
                else {
                    ProductCategory::where('id', $categoryId)->update(array('banner' => 1, 'img_ext' => $ext));
                }


                $ret = array(
                    'id' => $categoryId,
                    'dirpath' => $dirPath,
                    'name' => $fileName
                );

                return json_encode($ret);
            }
            else {
                return -1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveMenuImage() {
        //Function data
        $functionId = 381;

        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $ext = strtolower($imageFileType);

        $menuId = Input::get('menuid');

        try {
            if($ext == "jpg" || $ext == "jpeg" || $ext == 'png') {
                $dirPath = "assets/backend/images/website/menu/" . Auth::user()->orgid . "/";
                //create the directory named by user id
                if (!file_exists($dirPath)) {
                    mkdir($dirPath, 0777);
                }

                $fileName = $menuId . '.' . $ext;

                Input::file('file')->move($dirPath, $fileName);

                MenuItem::where('id', $menuId)->update(array(
                    'img' => 1,
                    'img_ext' => $ext
                ));


                $ret = array(
                    'id' => $menuId,
                    'dirpath' => $dirPath,
                    'name' => $fileName
                );

                return json_encode($ret);
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}