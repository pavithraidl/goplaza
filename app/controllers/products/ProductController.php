<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-06 15:58:51.
 * Controller ID - 66
 */

class ProductController extends BaseController
{
    const SystemId = 25;
    const ControllerId = 66;


    public function getManageProducts() {
        $viewId = 41;
        $urlRouteId = 81;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.products.manage-products', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function getManufactures() {
        //Function data
        $functionId = 177;

        //ajaxData = Input::get('name');

        try {
            $arr = array();

            $manufactures = ProductManufactures::where('status', 1)->where('orgid', Auth::user()->orgid)->get();

            foreach ($manufactures as $manufacture) {
                $arr[] = array(
                    'id' => $manufacture->id,
                    'name' => $manufacture->name
                );
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }


    public function getProduct($productid) {
        $viewId = 43;
        $urlRouteId = 85;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.products.product', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'productId' => $productid
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function addProduct() {
        //Function data
        $functionId = 180;

        $productName = Input::get('productname');

        try {
            $product = new Product();
            $product -> name = $productName;
            $product -> status = 2;
            $product -> created_by = Auth::user()->id;
            $product -> orgid = Auth::user()->orgid;
            $product -> new_product = 1;
            $product -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $product -> save();
            $productId = $product -> id;

            $productDetails = new ProductDetail();
            $productDetails -> productid = $productId;
            $productDetails -> status = 1;
            $productDetails -> default_product = 1;
            $productDetails -> created_by = Auth::user()->id;
            $productDetails -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $productDetails -> save();

            return $productId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveProductDetails() {
        //Function data
        $functionId = 183;

        $productId = Input::get('productid');
        $productDetailId = Input::get('productdetailid');
        $value = Input::get('value');
        $column = Input::get('column');

        try {
            if($productDetailId == null) {
                $newProductDetail = new ProductDetail();
                $newProductDetail -> productid = $productId;
                $newProductDetail -> default_product = 1;
                $newProductDetail -> status = 1;
                $newProductDetail -> created_by = Auth::user()->id;
                $newProductDetail -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $newProductDetail -> save();
                $newProductDetailId = $newProductDetail -> id;
            }
            else {
                $newProductDetailId = $productDetailId;
            }

            ProductDetail::where('id', $productDetailId)->update(array(
                $column => $value
            ));

            return $newProductDetailId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveProductInfo() {
        //Function data
        $functionId = 185;

        $productId = Input::get('productid');
        $value = Input::get('value');
        $column = Input::get('column');

        try {
            Product::where('id', $productId)->update(array(
                $column => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addMorePriceCategories() {
        //Function data
        $functionId = 187;

        $productId = Input::get('productid');

        try {
            $newProductDetail = new ProductDetail();
            $newProductDetail -> productid = $productId;
            $newProductDetail -> default_product = 0;
            $newProductDetail -> status = 1;
            $newProductDetail -> created_by = Auth::user()->id;
            $newProductDetail -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $newProductDetail -> save();
            $newProductDetailId = $newProductDetail -> id;

            return $newProductDetailId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function ChangeProductPriceOption() {
        //Function data
        $functionId = 189;

        $productId = Input::get('productid');
        $priceOption = Input::get('priceoption');

        try {
            Product::where('id', $productId)->update(array(
                'payments' => $priceOption
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function removePriceCombination() {
        //Function data
        $functionId = 191;

        $detailId = Input::get('detailid');

        try {
            ProductDetail::where('id', $detailId)->update(array(
                'status' => 0
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function iSwitchOnChange() {
        //Function data
        $functionId = 197;

        $productId = Input::get('productid');
        $status = Input::get('status');

        try {
            if($status == 0) {
                $status = 2;
            }

            Product::where('id', $productId)->update(array(
                'status' => $status
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveDetailedDescription() {
        //Function data
        $functionId = 199;

        $productId = Input::get('productid');
        $value = Input::get('value');

        try {
            Product::where('id', $productId)->update(array(
                'description' => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getProductStatistics() {
        //Function data
        $functionId = 201;

        $productId = Input::get('productid');

        try {
            $calculation = new CalculationController();
            $currentMonthNumber = date("n");
            $months = $calculation->getMonthListArray($currentMonthNumber, 7, 1, 1);
            $lineViews = array();


            foreach ($months as $month) {
                $views = ProductView::where('product_id', $productId)->where( DB::raw('MONTH(created_at)'), '=', $currentMonthNumber )->count();
                $currentMonthNumber--;

                if($currentMonthNumber == -1) $currentMonthNumber = 11;

                $lineViews[] = $views;
                $lineSales[] = 0;
            }

            $lineChartData = array(
                'months' => array_reverse($months),
                'views' => array_reverse($lineViews),
                'sales' => array_reverse($lineSales)
            );

            $arr = array(
                'linechart' => $lineChartData
            );

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveAdditionalInfo() {
        //Function data
        $functionId = 204;
        $productId = Input::get('productid');
        $value = Input::get('value');

        try {
            Product::where('id', $productId)->update(array(
                'additional_info' => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function setupMenuOrder() {
        //Function data
        $functionId = 206;

        $menuOrder = Input::get('menuorder');

        try {
            for($i = 0; $i < sizeof($menuOrder); $i++) {
                MenuItem::where('id', $menuOrder[$i]['id'])->update(array(
                    'menu_order' => $menuOrder[$i]['order']
                ));
            }

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function updateMenuItemName() {
        //Function data
        $functionId = 208;

        $menuItemId = Input::get('menuitemid');
        $value = Input::get('value');

        try {
            MenuItem::where('id', $menuItemId)->update(array(
                'name' => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addMenuItem() {
        //Function data
        $functionId = 210;

        $menuId = Input::get('menuid');

        try {
            $newMenuItem = new MenuItem();
            $newMenuItem -> menuid = $menuId;
            $newMenuItem -> menu_order = 100;
            $newMenuItem -> status = 2;
            $newMenuItem -> created_by = Auth::user()->id;
            $newMenuItem -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $newMenuItem -> save();
            $newMenuItemId = $newMenuItem -> id;

            return $newMenuItemId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeMenuItemStatus() {
        //Function data
        $functionId = 212;

        $menuItemId = Input::get('menuitemid');
        $status = Input::get('status');

        if($status == 0) {
            $status = 2;
        }

        try {
            MenuItem::where('id', $menuItemId)->update(array(
                'status' => $status
            ));

            $menuItem = MenuItem::where('id', $menuItemId)->first();

            $arr = array(
                'order' => $menuItem->menu_order
            );

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function sortMenuSubItems() {
        //Function data
        $functionId = 214;

        $menuOrder = Input::get('menuorder');

        try {
            for($i = 0; $i < sizeof($menuOrder); $i++) {

                if($menuOrder[$i]['container'] == 0) {
                    ProductCategory::where('id', $menuOrder[$i]['id'])->update(array(
                        'list_order' => $menuOrder[$i]['order'],
                        'menu_item_id' => $menuOrder[$i]['mainmenuid'],
                        'sub_item' => null,
                    ));
                }
                else {
                    ProductCategory::where('id', $menuOrder[$i]['id'])->update(array(
                        'list_order' => $menuOrder[$i]['order'],
                        'menu_item_id' => $menuOrder[$i]['mainmenuid'],
                        'sub_item' => $menuOrder[$i]['container'],
                    ));
                }

            }

            return 1;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }


    public function getProductCategories() {
        $viewId = 52;
        $urlRouteId = 143;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.products.product-categories', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function getMenu() {
        //Function data
        $functionId = 299;

        $menuIds = Input::get('menuids');
        $ret = array();
        $categoryArr = array();

        $orgId = Input::get('orgid');
        $orgKey = Input::get('orgkey');
        $orgSecret = Input::get('orgsecret');

        try {
            $authenticateAPI = new AccessController();
            $access = $authenticateAPI -> apiAuthenticate($orgId, $orgKey, $orgSecret);

            if($access != -1) {
                foreach ($menuIds as $menuId) {
                    $categoryList = ProductCategory::where('menu_item_id', $menuId)->where('sub_item', NULL)->where('status', 1)->where('orgid', $orgId)->orderBy('list_order', 'asc')->get();

                    foreach ($categoryList as $category) {
                        $categoryArr[] = array(
                            'id' => $category->id,
                            'name' => $category->name,
                            'img' => $category->img,
                            'description' => $category->description
                        );
                    }

                    $ret[$menuId] = $categoryArr;
                    unset($categoryArr);
                    $categoryArr = array();
                }

                return json_encode($ret);
            }
            else {
                return -1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return $ex;
        }
    }

                    
    public function getCategoryDetails() {
        //Function data
        $functionId = 301;


        $categoryId = Input::get('categoryid');

        $orgId = Input::get('orgid');
        $orgKey = Input::get('orgkey');
        $orgSecret = Input::get('orgsecret');

        try {
            $authenticateAPI = new AccessController();
            $access = $authenticateAPI -> apiAuthenticate($orgId, $orgKey, $orgSecret);

            if($access != -1) {
                $category = ProductCategory::where('id', $categoryId)->where('orgid', $orgId)->first();

                $ret = array(
                    'id' => $category -> id,
                    'name' => $category -> name,
                    'href' => $category -> href,
                    'description' => $category -> description,
                    'img' => $category -> img,
                    'banner' => $category->banner
                );

                return json_encode($ret);
            }
            else {
                return -1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveProductCategory() {
        //Function data
        $functionId = 303;

        $productId = Input::get('productid');
        $categoryId = Input::get('value');
        $ret = array();

        try {
            $menuItemId = ProductCategory::where('id', $categoryId)->pluck('menu_item_id');
            $menuId = MenuItem::where('id', $menuItemId)->pluck('menuid');

            $productCategoryRelation = ProductCategoryRelation::where('product_id', $productId)->where('status', 1)->first();

            if($productCategoryRelation) {
                $productCategoryRelation -> menuid = $menuId;
                $productCategoryRelation -> menu_item_id = $menuItemId;
                $productCategoryRelation -> sub_category_id = null;
                $productCategoryRelation -> product_category_id = $categoryId;
                $productCategoryRelation -> save();
            }
            else {
                $productCategoryRelation = new ProductCategoryRelation();
                $productCategoryRelation -> menuid = $menuId;
                $productCategoryRelation -> menu_item_id = $menuItemId;
                $productCategoryRelation -> product_category_id = $categoryId;
                $productCategoryRelation -> sub_category_id = null;
                $productCategoryRelation -> product_id = $productId;
                $productCategoryRelation -> status = 1;
                $productCategoryRelation -> created_by = Auth::user()->id;
                $productCategoryRelation -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $productCategoryRelation -> save();
            }

            $subCategories = ProductCategory::where('sub_item', $categoryId)->get();
            foreach ($subCategories as $subCategory) {
                $ret[] = array(
                    'id' => $subCategory -> id,
                    'name' => $subCategory -> name
                );
            }


            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveSubCategory() {
        //Function data
        $functionId = 311;

        $productId = Input::get('productid');
        $categoryId = Input::get('value');

        try {
            $productCategoryRelation = ProductCategoryRelation::where('product_id', $productId)->where('status', 1)->first();

            if($categoryId == 0 || $categoryId == '') $categoryId = null;

            $oldCategory = $productCategoryRelation -> sub_category_id;
            $oldCategoryName = ProductCategory::where('id', $oldCategory)->pluck('name');

            if($productCategoryRelation) {
                $productCategoryRelation -> sub_category_id = $categoryId;
                $productCategoryRelation -> save();
            }


            return json_encode($oldCategoryName);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function searchProducts() {
        //Function data
        $functionId = 317;

        $keyword = Input::get('keyword');
        $paginate = Input::get('paginate');
        $perPage = Input::get('perpage');
        $ret = array();

        $orgId = Input::get('orgid');
        $orgKey = Input::get('orgkey');
        $orgSecret = Input::get('orgsecret');

        try {
            $authenticateAPI = new AccessController();
            $access = $authenticateAPI -> apiAuthenticate($orgId, $orgKey, $orgSecret);

            if($access != -1) {
//                $keyword = preg_replace('/[^A-Za-z0-9\ ]/', '', $keyword) ;

                $productKeywords = Product::where('orgid', $orgId)
                    ->where('status', 1)
                    ->where('keywords', 'LIKE', $keyword.'%')
                    ->orWhere('keywords', 'LIKE', '% '.$keyword.'%')
                    ->lists('id');

                $productsName = Product::where('orgid', $orgId)
                    ->where('status', 1)
                    ->where('name', 'LIKE', $keyword.'%')
                    ->orWhere('name', 'LIKE', '% '.$keyword.'%')
                    ->lists('id');

                $productsSmallDesc = Product::where('orgid', $orgId)
                    ->where('status', 1)
                    ->where('small_description', 'LIKE', $keyword.'%')
                    ->orWhere('small_description', 'LIKE', '% '.$keyword.'%')
                    ->lists('id');

                $productsDesc = Product::where('orgid', $orgId)
                    ->where('status', 1)
                    ->where('description', 'LIKE', $keyword.'%')
                    ->orWhere('description', 'LIKE', '% '.$keyword.'%')
                    ->lists('id');

                $productsAdditionalInfo = Product::where('orgid', $orgId)
                    ->where('status', 1)
                    ->where('additional_info', 'LIKE', $keyword.'%')
                    ->orWhere('additional_info', 'LIKE', '% '.$keyword.'%')
                    ->lists('id');

                $products = array_merge($productKeywords, $productsName);
                $products = array_merge($products, $productsSmallDesc);
                $products = array_merge($products, $productsDesc);
                $products = array_merge($products, $productsAdditionalInfo);

                $products = array_unique($products);

                foreach ($products as $product) {
                    $product = Product::where('id', $product)->first();
                    if($product->orgid == $orgId) {
                        $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->first();
                        $payments = $product->payments;

                        if($payments == 1) {
                            $productMeasure = $productDetail->qty;
                        }
                        else if($payments == 2) {
                            $productMeasure = $productDetail->volume;
                        }
                        else if($payments == 3) {
                            $productMeasure = $productDetail->weight;
                        }
                        else if($payments == 4) {
                            $productMeasure = $productDetail->size;
                        }
                        else if($payments == 5) {
                            $productMeasure = $productDetail->area;
                        }

                        $productPrice = $productDetail->price;

                        //product image
                        $productImg = ProductImage::where('productid', $product->id)->where('main', 1)->where('status', 1)->first();


                        $ret[] = array(
                            'id' => $product->id,
                            'name' => $product->name,
                            'measure' => $productMeasure,
                            'price' => $productPrice,
                            'img' => $productImg,
                            'detail_id' => $productDetail->id
                        );
                    }
                }


                return json_encode($ret);
            }
            else {
                return -1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return json_encode($ex);
        }
    }

                    
    public function getGlobalProducts() {
        //Function data
        $functionId = 349;

        $productId = Input::get('productid');
        $ret = array();

        try {
            $productName = Product::where('id', $productId)->pluck('name');


            $products = ProductGlobal::where('status', 1)
                ->where('name', 'LIKE', $productName.'%')
                ->get();

            foreach ($products as $product) {
                $productImgId = ProductImgRelation::where('productid', $product->id)->where('global_img', 1)->where('type', 1)->where('status', 1)->pluck('imgid');

                if($productImgId)
                    $productImgId = $productImgId.'_thumb.'.ProductImage::where('id', $productImgId)->pluck('ext');
                else
                    $productImgId = 'default.png';

                $ret[] = array(
                    'id' => $product->id,
                    'name' => $product->name,
                    'small_description' => $product->small_description,
                    'imgid' => $productImgId
                );
            }

            Product::where('id', $productId)->update(array('new_product', 0));

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveGlobalProduct() {
        //Function data
        $functionId = 351;

        $productId = Input::get('productid');
        $globalId = Input::get('globalid');

        try {
            $global = ProductGlobal::where('id', $globalId)->first();
            $product = Product::where('id', $productId)->where('orgid', Auth::user()->orgid)->first();

            if($product) {
                $product -> upc = $global -> upc;
                $product -> name = $global -> name;
                $product -> manufacture_id = $global -> manufacture_id;
                $product -> small_description = $global -> small_description;
                $product -> description = $global -> description;
                $product -> additional_info = $global -> additional_info;
                $product -> keywords = $global -> keywords;
                $product -> save();

                $detailId = ProductDetail::where('productid', $productId)->where('default_product', 1)->where('status', 1)->pluck('id');
                $globalImages = ProductImgRelation::where('productid', $global->id)->where('global_img', 1)->where('status', 1)->get();

                foreach ($globalImages as $globalImage) {
                    $image = new ProductImgRelation();
                    $image -> imgid = $globalImage -> imgid;
                    $image -> productid = $productId;
                    $image -> detailid = $detailId;
                    $image -> type = $globalImage -> type;
                    $image -> global_img = 0;
                    $image -> status = 1;
                    $image -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $image -> save();
                }

                return 1;
            }
            else {
                return -848;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
//----- REMOVE IF IT'S NOT USEFUL
//
//    public function uploadExcelProducts() {
//        //Function data
//        $functionId = 354;
//
//        //ajaxData = Input::get('name');
//
//        try {
//            $url = $_SERVER['DOCUMENT_ROOT'].'/assets/travel.xlsx';
//
//            require_once $_SERVER['DOCUMENT_ROOT'].'/app/library/PHPExcel/Classes/PHPExcel/IOFactory.php';
//
//            $excelFile = $url;
//
//            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
//            $objPHPExcel = $objReader->load($excelFile);
//
//            //Itrating through all the sheets in the excel workbook and storing the array data
//            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
//                $arrayData[$worksheet->getTitle()] = $worksheet->toArray();
//            }
//
//            foreach ($arrayData as $data) {
//                foreach ($data as $item) {
//                    $brand = $item[0];
//                    $externalId = $item[1];
//                    $name = $item[2];
//                    $retailPrice = $item[3];
//                    $wholesalePrice = $item[4];
//                    $costPrice = $item[5];
//                    $category = $item[6];
//                    $ages = $item[8];
//                    $manufactureId = null;
//                    $description = null;
//                    $keywords = null;
//
//                    //setup manufacture
//                    $manufactureName = strtolower($brand);
//                    if($manufactureName != '') {
//                        $manufacture = ProductManufactures::where('name', 'LIKE', $manufactureName.'%')->first();
//                        if(!$manufacture) {
//                            $manufacture = new ProductManufactures();
//                            $manufacture -> name = ucwords($manufactureName);
//                            $manufacture -> description = '';
//                            $manufacture -> img = 1;
//                            $manufacture -> status = 1;
//                            $manufacture -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
//                            $manufacture -> save();
//                            $manufactureId = $manufacture -> id;
//                        }
//                        else {
//                            $manufactureId = $manufacture ->id;
//                        }
//                    }
//
//                    //setup description
//                    if($ages == '0M+')
//                        $description = 'Suitable for newborns and older';
//                    else if($ages == '1M+')
//                        $description = 'Suitable for 1 month and older';
//                    else if($ages == '0-5YRS')
//                        $description = 'Suitable for newborns and 5 years old';
//                    else if($ages == '13-18KG')
//                        $description = 'Suitable for 13kg to 18kg weight';
//                    else if($ages == '10-15KG')
//                        $description = 'Suitable for 10kg to 15kg weight';
//                    else if($ages == '6-11KG')
//                        $description = 'Suitable for 6kg to 11kg weight';
//                    else if($ages == '4-8KG')
//                        $description = 'Suitable for 4kg to 8klg weight';
//                    else if($ages == '18M+')
//                        $description = 'Suitable for 18 months and older';
//
//                    //setup keywords
//                    $keywords = ucwords($category).', '.ucwords($manufactureName);
//
//                    $product = new Product();
//                    $product -> name = $name;
//                    $product -> external_id = $externalId;
//                    $product -> manufacture_id = $manufactureId;
//                    $product -> description = $description;
//                    $product -> keywords = $keywords;
//                    $product -> payments = 1;
//                    $product -> status = 1;
//                    $product -> new_product = 0;
//                    $product -> created_by = 65;
//                    $product -> orgid = 8;
//                    $product -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
//                    $product -> save();
//                    $productId = $product -> id;
//
//                    $detail = new ProductDetail();
//                    $detail -> productid = $productId;
//                    $detail -> price = $retailPrice;
//                    $detail -> wholesale_price = $wholesalePrice;
//                    $detail -> cost_price = $costPrice;
//                    $detail -> default_product = 1;
//                    $detail -> status = 1;
//                    $detail -> created_by = 65;
//                    $detail -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
//                    $detail -> save();
//                }
//            }
//
//            return 'done';
//
//        } Catch(Exception $ex) {
//            $exception = new ErrorController();
//            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
//            return 0;
//        }
//    }

                    
//------REMOVE IF IT'S NOT USEFUL--------
//
//    public function bulkImageUpload() {
//        //Function data
//        $functionId = 356;
//
//        //ajaxData = Input::get('name');
//
//        try {
//            $url = $_SERVER['DOCUMENT_ROOT'].'/assets/backend/images/all-images/';
//            $destination = $_SERVER['DOCUMENT_ROOT'].'/assets/backend/images/products/products/';
//            $ret = '';
//
//            $dir = $url;
//            $fileNames = array();
//            if(is_dir($dir)){
//                $handle = opendir($dir);
//                while(false !== ($file = readdir($handle))){
//                    if(is_file($dir.'/'.$file) && is_readable($dir.'/'.$file)){
//                        $fileNames[] = $file;
//                    }
//                }
//                closedir($handle);
//                $fileNames = array_reverse($fileNames);
//
//                foreach ($fileNames as $fileName) {
//                    $externalId = preg_replace("/\.[^.]+$/", "", $fileName);
//                    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
//
//                    $product = Product::where('external_id', $externalId)->first();
//
//                    if($product) {
//                        $detailId = ProductDetail::where('productid', $product->id)->where('default_product', 1)->pluck('id');
//
//                        $dbImg = new ProductImage();
//                        $dbImg -> ext = $ext;
//                        $dbImg -> status = 1;
//                        $dbImg -> created_by = 86;
//                        $dbImg -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
//                        $dbImg -> save();
//                        $imgId = $dbImg -> id;
//
//                        $destinationUrl = $destination.$imgId.'.'.$ext;
//
//                        $dbImgRelation = new ProductImgRelation();
//                        $dbImgRelation -> imgid = $imgId;
//                        $dbImgRelation -> productid = $product->id;
//                        $dbImgRelation -> detailid = $detailId;
//                        $dbImgRelation -> type = 1;
//                        $dbImgRelation -> global_img = 0;
//                        $dbImgRelation -> status = 1;
//                        $dbImgRelation -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
//                        $dbImgRelation -> save();
//
//                        $src = $url.$fileName;
//                        $width = 600;
//                        $height = 600;
//                        $quality = 95;
//
//                        if($ext == 'jpg')
//                            $img = imagecreatefromjpeg($src);
//                        else if($ext == 'png')
//                            $img = imagecreatefrompng($src);
//
//                        //get this values from user by submitting form ( either by crop or by textboxes)
//                        list($old_width, $old_height, $type, $attr) = getimagesize($src);
//
//                        $dest = ImageCreateTrueColor($width, $height);
//                        imagecopyresampled(
//                            $dest, //destination image
//                            $img, //source image
//                            0, //top left coordinate of the destination image in x direction
//                            0, //top left coordinate of the destination image in y direction
//                            0, //top left coordinate in x direction of source image that I want copying to start at
//                            0, //top left coordinate in y direction of source image that I want copying to start at
//                            $width, //190, thumbnail width
//                            $height, //190, thumbnail height
//                            $old_width, //how wide the rectangle from the source image we want thumbnailed is
//                            $old_height //how high the rectangle from the source image we want thumbnailed is
//                        );
//                        imagejpeg($dest, $destinationUrl, $quality);
//
//                        //thumb img
//                        $destinationUrl = $destination.$imgId.'_thumb.'.$ext;
//                        $dest = ImageCreateTrueColor(200, 200);
//                        imagecopyresampled(
//                            $dest, //destination image
//                            $img, //source image
//                            0, //top left coordinate of the destination image in x direction
//                            0, //top left coordinate of the destination image in y direction
//                            0, //top left coordinate in x direction of source image that I want copying to start at
//                            0, //top left coordinate in y direction of source image that I want copying to start at
//                            200, //190, thumbnail width
//                            200, //190, thumbnail height
//                            $old_width, //how wide the rectangle from the source image we want thumbnailed is
//                            $old_height //how high the rectangle from the source image we want thumbnailed is
//                        );
//                        imagejpeg($dest, $destinationUrl, $quality);
//
//                        $ret = $ret.$fileName.'<br/>';
//                    }
//                    else
//                        $ret = $ret. 'Not found!<br/>';
//                }
//            }else {
//                $ret = $ret. "<p>There is an directory read issue</p>";
//            }
//
//            return $ret;
//
//        } Catch(Exception $ex) {
//            $exception = new ErrorController();
//            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
//            return 0;
//        }
//    }

                    
    public function getProducts() {
        //Function data
        $functionId = 358;

        $paginate = Input::get('paginate');
        $perPage = Input::get('perpage');
        $sortBy = Input::get('sortby');
        $sortType = Input::get('sorttype');
        $onShowing = Input::get('onshowing');
        $ret = array();

        try {
            if($sortBy == 'null')
                $sortBy = 'id';
            if($sortType == 'null')
                $sortType = 'ASC';

            $products = Product::where('orgid', Auth::user()->orgid)->where('status', '!=', 0)->orderBy($sortBy, $sortType)->forPage($paginate, $perPage)->lists('id');

            foreach ($products as $productId) {
                $product = Product::where('id', $productId)->first();
                $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->first();
                $imgId = ProductImgRelation::where('productid', $product->id)->where('type', 1)->where('status', 1)->pluck('imgid');
                $imgExt = ProductImage::where('id', $imgId)->pluck('ext');
                $timeAgo = new CalculationController();
                $createdAt = $product->created_at;
                $createdAt = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $createdAt);
                $createdAt = $timeAgo->timeAgo($createdAt);

                $price = $productDetail->price != null ? '$'.number_format($productDetail->price, 2, '.', '') : '-';
                $wSPrice = $productDetail->wholesale_price != null ? '$'.number_format($productDetail->wholesale_price, 2, '.', '') : '-';

                $ret[] = array(
                    'id' => $product->id,
                    'external_id' => $product->external_id,
                    'name' => $product->name,
                    'price' => $price,
                    'wsprice' => $wSPrice,
                    'imgid' => $imgId,
                    'imgext' => $imgExt,
                    'status' => $product->status,
                    'createdat' => $createdAt
                );
            }

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function serchProducts() {
        //Function data
        $functionId = 360;

        $keyword = Input::get('keywords');
        $searchedList = Input::get('searchedlist');
        $ret = array();

        try {
            $statusChedk = strtolower($keyword);

            //Search by status
            if($statusChedk == 'published') {
                $keyword = 1;
                $products = Product::where('status', 1)->where('orgid', Auth::user()->orgid)->lists('id');
            }
            else if($statusChedk == 'draft') {
                $keyword = 2;
                $products = Product::where('status', 2)->where('orgid', Auth::user()->orgid)->lists('id');
            }
            else {
                //Search by Name
                $productsName = Product::where('orgid', Auth::user()->orgid)
                    ->where('status', '!=', 0)
                    ->where('name', 'LIKE', $keyword.'%')
                    ->orWhere('name', 'LIKE', '%'.$keyword.'%')
                    ->lists('id');

                //Search by external id
                $productExternalId = Product::where('orgid', Auth::user()->orgid)
                    ->where('status', '!=', 0)
                    ->where('external_id', 'LIKE', $keyword.'%')
                    ->orWhere('external_id', 'LIKE', '%'.$keyword.'%')
                    ->lists('id');

                //Search by Manufacture
                $productManufactureList = ProductManufactures::where('orgid', Auth::user()->orgid)
                    ->where('status', '!=', 0)
                    ->where('name', 'LIKE', $keyword.'%')
                    ->lists('id');
                $manufactureProducts = array();


                foreach ($productManufactureList as $manufactureId) {
                    $manuProductList = Product::where('manufacture_id', $manufactureId)->where('status', '!=', 0)->lists('id');
                    foreach ($manuProductList as $productId) {
                        $manufactureProducts[] = $productId;
                    }
                }

                $products = $productsName;
                $products = array_merge($products, $productExternalId);
                $products = array_merge($products, $manufactureProducts);
            }

            $products = array_unique($products);
            $products = array_diff($products, $searchedList);

            foreach ($products as $productId) {
                $product = Product::where('id', $productId)->first();

                $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->first();
                $imgId = ProductImgRelation::where('productid', $product->id)->where('type', 1)->where('status', 1)->pluck('imgid');
                $imgExt = ProductImage::where('id', $imgId)->pluck('ext');
                $timeAgo = new CalculationController();
                $createdAt = $product->created_at;
                $createdAt = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $createdAt);
                $createdAt = $timeAgo->timeAgo($createdAt);

                $price = $productDetail->price != null ? '$'.number_format($productDetail->price, 2, '.', '') : '-';
                $wSPrice = $productDetail->wholesale_price != null ? '$'.number_format($productDetail->wholesale_price, 2, '.', '') : '-';

                $ret[] = array(
                    'id' => $product->id,
                    'external_id' => $product->external_id,
                    'name' => $product->name,
                    'price' => $price,
                    'wsprice' => $wSPrice,
                    'imgid' => $imgId,
                    'imgext' => $imgExt,
                    'status' => $product->status,
                    'createdat' => $createdAt
                );
            }

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function editPrice() {
        //Function data
        $functionId = 362;

        $price = Input::get('price');
        $productId = Input::get('productid');
        $type = Input::get('type');

        try {
            if($type == 1) $type = 'price'; else $type = 'wholesale_price';

            $detailId = ProductDetail::where('productid', $productId)->where('default_product', 1)->where('status', 1)->pluck('id');
            ProductDetail::where('id', $detailId)->update(array(
                $type => $price
            ));

            if($type == 1 && !is_numeric($price)) {
                Product::where('id', $productId)->update('status', 2);
            }


            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function deleteProducts() {
        //Function data
        $functionId = 379;

        $productIds = Input::get('productids');

        try {
            foreach ($productIds as $productId) {
                $product = Product::where('id', $productId)->where('orgid', Auth::user()->orgid)->first();

                if($product) {
                    $product -> status = 0;
                    $product -> save();
                }
            }

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getProductList($productIds) {
        //Function data
        $functionId = 405;
        $arr = array();

        try {
            foreach ($productIds as $productId) {
                $product = Product::where('id', $productId)->first();
                $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->where('status', 1)->first();
                $productImgs = ProductImgRelation::where('productid', $product->id)->where('type', '<', 3)->where('status', 1)->get();
                $productSubCategoryId = ProductCategoryRelation::where('product_id', $product->id)->where('status', 1)->pluck('sub_category_id');
                $productSubCategory = ProductCategory::where('id', $productSubCategoryId)->first();
                $subCategoryName = '<span style="font-style: italic; color: #89898975;">- None -</span>';
                if($productSubCategory) {
                    $subCategoryName = $productSubCategory->name;
                }

                $imgArr = array();

                foreach ($productImgs as $productImg) {
                    $img = ProductImage::where('id', $productImg->imgid)->first();

                    $imgArr[] = array(
                        'imgid' => $img->id,
                        'ext' => $img->ext,
                        'type' => $productImg->type
                    );
                }

                if($productDetail) {
                    $arr[] = array(
                        'id' => $product -> id,
                        'name' => $product -> name,
                        'img' => $imgArr,
                        'price' => number_format($productDetail-> price, 2, '.', ''),
                        'new' => $product->new_product,
                        'sub_category' => $subCategoryName
                    );
                }
            }

            return $arr;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}