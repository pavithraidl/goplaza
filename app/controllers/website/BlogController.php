<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Isuru ANZPharma
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-03-22 10:27:38.
 * Controller ID - 93
 */

class BlogController extends BaseController
{
    const SystemId = 26;
    const ControllerId = 93;


    public function getBlog($blogId) {
        $viewId = 53;
        $urlRouteId = 173;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                if(!is_numeric($blogId)) $blogId = 0;

                return View::make('backend.website.blog', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'blogId' => $blogId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function saveData() {
        //Function data
        $functionId = 368;

        $value = Input::get('value');
        $field = Input::get('field');
        $blogId = Input::get('blogid');

        try {
            $blog = WebsiteBlog::where('id', $blogId)->first();
            if($blog) {
                WebsiteBlog::where('id', $blogId)->update(array(
                    $field => $value,
                    'status' => 2
                ));
            }
            else {
                $blog = new WebsiteBlog();
                $blog -> status = 2;
                $blog -> orgid = Auth::user()->orgid;
                $blog -> created_by = Auth::user()->id;
                $blog -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $blog -> save();
                $blogId = $blog -> id;

                WebsiteBlog::where('id', $blogId)->update(array(
                    $field => $value,
                    'status' => 2
                ));
            }

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getBlogPosts() {
        //Function data
        $functionId = 370;
        $arr = array();

        try {
            $blogs = WebsiteBlog::where('orgid', Auth::user()->orgid)->where('status', 1)->get();

            foreach ($blogs as $blog) {
                $timeAgo = new CalculationController();
                $createdAt = $blog->created_at;
                $createdAt = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $createdAt);
                $shortTime = $timeAgo->timeAgo($createdAt);

                $arr[] = array(
                    'id' => $blog->id,
                    'title' => $blog->title,
                    'content' => $blog->content,
                    'created_at' => $blog->created_at,
                    'short_time' => $shortTime
                );
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}