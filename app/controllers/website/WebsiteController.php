<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-12 14:22:28.
 * Controller ID - 67
 */

class WebsiteController extends BaseController
{
    const SystemId = 26;
    const ControllerId = 67;


    public function getManage() {
        $viewId = 42;
        $urlRouteId = 82;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.website.manage', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function getProductSections() {
        //Function data
        $functionId = 175;
        $orgId = Input::get('orgid');
        $arr = array();

        try {
            $productGrids = ProductsGrid::where('orgid', $orgId)->where('status', 1)->orderBy('page_order', 'ASC')->get();

            foreach ($productGrids as $productGrid) {
                $productArr = array();

                $productCord = $productGrid->productArrayCode;
                $paginate = 1;
                $count = $productGrid->default_product_count;

                $productCord = str_replace('-paginate-', $paginate, $productCord);
                $productCord = str_replace('-count-', $count, $productCord);


                $productsList = eval("return $productCord");

                foreach ($productsList as $product) {
                    if($product->payments == 1) $type = 'price_qty';
                    else if($product->payments == 2) $type = 'price_volume';
                    else if($product->payments == 3) $type = 'price_weight';
                    else if($product->payments == 4) $type = 'price_size';


                    $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->first();
                    $price = ProductDetail::where('productid', $product->id)->where('default_product', 1)->pluck("$type");

                    $frontImg = ProductImage::where('productid', $product->id)->where('detailid', $productDetail->id)->where('status', 1)->where('main', 1)->pluck('id');
                    if($frontImg) {
                        $frontImg =  $product->id.'/'.$productDetail->id.'/'.$frontImg.'.'.ProductImage::where('id', $frontImg)->pluck('ext');
                    }
                    else {
                        $frontImg = 'default/original.png';
                    }

                    $hoverImg = ProductImage::where('productid', $product->id)->where('detailid', $productDetail->id)->where('status', 1)->where('hover', 1)->pluck('id');
                    if($hoverImg) {
                        $hoverImg =  $product->id.'/'.$productDetail->id.'/'.$hoverImg.'.'.ProductImage::where('id', $hoverImg)->pluck('ext');
                    }
                    else {
                        $hoverImg = 'default/original.png';
                    }

                    $productArr[] = array(
                        'name' => $product->name,
                        'price' => $price,
                        'frontimg' => $frontImg,
                        'hoverimg' => $hoverImg
                    );
                }

                $arr[] = array(
                    'id' => $productGrid->id,
                    'title' => $productGrid->title,
                    'products' => $productArr
                );

            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }


    public function getSlider($sliderId) {
        $viewId = 47;
        $urlRouteId = 119;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.website.slider', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId,
                    'sliderId' => $sliderId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function postSection() {
        //Function data
        $functionId = 271;

        $value = Input::get('value');
        $field = Input::get('field');
        $sectionId = Input::get('sectionid');

        try {
            $section = WebsiteSection::where('id', $sectionId)->first();
            if(!$section) {
                $section = new WebsiteSection();
                $section -> status = 1;
                $section -> created_by = Auth::user()->id;
                $section -> orgid = Auth::user()->orgid;
                $section -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $section -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $section -> save();
                $sectionId = $section -> id;
            }

            WebsiteSection::where('id', $sectionId)->update(array(
                $field => $value
            ));

            return $sectionId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function selectSection() {
        //Function data
        $functionId = 274;

        $sectionId = Input::get('sectionid');

        try {
            $section = WebsiteSection::where('id', $sectionId)->first();

            $arr = array(
                'id' => $section->id,
                'name' => $section->name,
                'title' => $section->title,
                'content' => $section->content
            );

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }


    public function getWebsiteEnquiry() {
        $viewId = 51;
        $urlRouteId = 133;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.website.website-enquiry', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

                    
    public function editTitles() {
        //Function data
        $functionId = 277;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveSlider() {
        //Function data
        $functionId = 279;

        $modelId = Input::get('modelid');
        $field = Input::get('field');
        $table = Input::get('table');
        $value = Input::get('value');

        try {
            $model = DB::table($table)->where('id', $modelId)->first();
            if(!$model) {
                if($table == 'website_sliders') $model = new WebsiteSlider();
                else if($table == 'website_slider_img') $model = new WebsiteSliderImg();
                else if($table == 'website_slider_slide') $model = new WebsiteSliderSlide();

                $model -> status = 1;
                $model -> orgid = Auth::user()->orgid;
                $model -> created_by = Auth::user()->id;
                $model -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $model -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $model -> save();
                $modelId = $model->id;
            }
            else {
                $modelId = $model -> id;
            }

            DB::table($table)->where('id', $modelId)->update(array(
                $field => $value
            ));

            return $modelId;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addEditors() {
        //Function data
        $functionId = 284;

        $sliderId = Input::get('sliderid');
        $slideId = Input::get('slideid');
        $type = Input::get('type');


        try {
            if($slideId == '') {
                //Create a new slider
                $slide = new WebsiteSliderSlide();
                $slide -> sliderid = $sliderId;
                $slide -> status = 1;
                $slide -> created_by = Auth::user()->id;
                $slide -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $slide -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $slide -> save();
                $slideId = $slide -> id;
            }

            //create a new field
            $field = new WebsiteSliderSlideField();
            $field -> type = $type;
            $field -> slideid = $slideId;
            $field -> status = 1;
            $field -> created_by = Auth::user()->id;
            $field -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
            $field -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $field -> save();
            $fieldId = $field -> id;

            $ret = array(
                'slideid' => $slideId,
                'fieldid' => $fieldId
            );


            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveContent() {
        //Function data
        $functionId = 286;

        $fieldId = Input::get('fieldid');
        $field = Input::get('field');
        $value = Input::get('value');

        try {
            WebsiteSliderSlideField::where('id', $fieldId)->update(array(
                $field => $value
            ));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function saveFieldImage() {
        //Function data
        $functionId = 288;

        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $ext = strtolower($imageFileType);

        $fieldId = Input::get('fieldid');

        try {
            if($ext == "jpg" || $ext == "jpeg" || $ext == 'png' || $ext == 'gif') {
                $dirPath = "assets/backend/images/website/slide-block/".Auth::user()->orgid."/";
                //create the directory named by user id
                if (!file_exists($dirPath)) {
                    mkdir($dirPath, 0777);
                }

                $fileName = $fieldId.'.'.$ext;

                WebsiteSliderSlideField::where('id', $fieldId)->update(array(
                    'img_ext' => $ext
                ));


                Input::file('file')->move($dirPath, $fileName);

                WebsiteSliderSlideField::where('id', $fieldId)->update(array('img' => 1));

                $ret = array(
                    'id' => $fieldId,
                    'dirpath' => $dirPath,
                    'name' => $fileName
                );

                return json_encode($ret);
            }
            else {
                return -1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addSlide() {
        //Function data
        $functionId = 290;

        $sliderId = Input::get('sliderid');

        try {
            $oldSlide = WebsiteSliderSlide::where('sliderid', $sliderId)->where('status', 1)->first();
            $oldSlideId = $oldSlide -> id;

            //create a new slide
            $slide = new WebsiteSliderSlide();
            $slide -> sliderid = $sliderId;
            $slide -> status = 1;
            $slide -> created_by = Auth::user()->id;
            $slide -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
            $slide -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $slide -> save();
            $slideId = $slide -> id;

            $sliderFields = WebsiteSliderSlideField::where('slideid', $oldSlideId)->where('status', 1)->get();
            $fieldHtml = '';

            foreach ($sliderFields as $sliderField) {
                $field = new WebsiteSliderSlideField();
                $field -> slideid = $slideId;
                $field -> type = $sliderField -> type;
                $field -> field_name = $sliderField -> field_name;
                $field -> notes = $sliderField -> notes;
                $field -> img = 0;
                $field -> status = 1;
                $field -> created_by = Auth::user()->id;
                $field -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $field -> updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $field -> save();
                $fieldId = $field -> id;

                if($sliderField -> type == 1) $fieldInput = '<input id="slide-field-'.$fieldId.'" onchange="saveContent(this, '.$fieldId.', \'content\');" value="" type="text" class="form-control" placeholder="Type content here...">';
                else if($sliderField -> type == 2) $fieldInput = '<textarea id="slide-field-'.$fieldId.'" class="form-control" onchange="saveContent(this, '.$fieldId.', \'content\');" placeholder="Type content here..."></textarea>';
                else if($sliderField -> type == 3) $fieldInput = '<div id="slide-field-'.$fieldId.'" data-id="'.$fieldId.'" class="edit"></div>';
                else if($sliderField -> type == 4) $fieldInput = '<div class="upload-img-container" style="position: relative; width: 85%; margin: 0 auto;" onclick="openUploadFileDialog('.$fieldId.');">
                                                                        <div class="upload-img-overlay" onmouseenter="animateLivIcon('.$fieldId.');">
                                                                            <h4 style="margin-bottom: 10%">Add/Change Slide Image</h4>
                                                                            <div id="upload-img-livicon-'.$fieldId.'" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 65px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                                            <p>Click Here to Upload!<br/><br/><span>'.$sliderField->notes.'</span></p>
                                                                        </div>
                                                                        <img id="slider-slide-img-'.$fieldId.'" src="'.URL::To('/').'/assets/backend/images/website/slide-block/default.jpg" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                                        <div id = "image-upload-progress-'.$fieldId.'" class = "progress" style = "height: 8px;width: 100%;">
                                                                            <div id = "image-upload-progress-bar-'.$fieldId.'" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                                                <span id="image-upload-sr-only-'.$fieldId.'" class = "sr-only">0% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <form action="'.URL::Route('website-savefieldimage').'" method="post" id="upload-image-form-'.$fieldId.'" enctype="multipart/form-data" class="slider-slide-image-upload">
                                                                        <input type="file" name="file" id="image-choose-'.$fieldId.'" onchange="submitImageUploadForm(\'#upload-image-form-'.$fieldId.'\', '.$fieldId.');" accept="image/jpg;capture=camera" style="display: none;">
                                                                        <input type="hidden" name="fieldid" value="'.$fieldId.'" />
                                                                    </form>';
                else if($sliderField -> type == 5) $fieldInput = '<div class="col-md-12" style="margin-bottom: 15px;">
                                                                                <div class="col-md-2">
                                                                                    <div id="selected-icon-container-'.$fieldId.'" class="icon-container">
                                                                                        <i class="fa fa-square-o"></i>
                                                                                    </div>
                                                                                    <input type="hidden" id="selected-icon-'.$fieldId.'" onclick="saveContent(this, '.$fieldId.', \'content\');" />
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <div id="icon-list-container-'.$fieldId.'" class="col-md-12" style=" max-height: 200px; overflow-y: auto; display: none;">
                                                                                        <section id="web-application">
                                                                                            <h2 class="page-header">Web Application Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">
                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', '.$fieldId.', \'adjust\');" href="#" ><i class="fa fa-adjust"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'anchor\');" href="#" ><i class="fa fa-anchor"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'archive\');" href="#" ><i class="fa fa-archive"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'area-chart\');" href="#" ><i class="fa fa-area-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows\');" href="#" ><i class="fa fa-arrows"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows-h\');" href="#" ><i class="fa fa-arrows-h"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows-v\');" href="#" ><i class="fa fa-arrows-v"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'asterisk\');" href="#" ><i class="fa fa-asterisk"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'at\');" href="#" ><i class="fa fa-at"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'car\');" href="#" ><i class="fa fa-automobile"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ban\');" href="#" ><i class="fa fa-ban"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'university\');" href="#" ><i class="fa fa-bank"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'barcode\');" href="#" ><i class="fa fa-barcode"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bars\');" href="#" ><i class="fa fa-bars"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'beer\');" href="#" ><i class="fa fa-beer"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bell\');" href="#" ><i class="fa fa-bell"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bell-o\');" href="#" ><i class="fa fa-bell-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bell-slash\');" href="#" ><i class="fa fa-bell-slash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bell-slash-o\');" href="#" ><i class="fa fa-bell-slash-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bicycle\');" href="#" ><i class="fa fa-bicycle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'binoculars\');" href="#" ><i class="fa fa-binoculars"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'birthday-cake\');" href="#" ><i class="fa fa-birthday-cake"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bolt\');" href="#" ><i class="fa fa-bolt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bomb\');" href="#" ><i class="fa fa-bomb"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'book\');" href="#" ><i class="fa fa-book"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bookmark\');" href="#" ><i class="fa fa-bookmark"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bookmark-o\');" href="#" ><i class="fa fa-bookmark-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'briefcase\');" href="#" ><i class="fa fa-briefcase"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bug\');" href="#" ><i class="fa fa-bug"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'building\');" href="#" ><i class="fa fa-building"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'building-o\');" href="#" ><i class="fa fa-building-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bullhorn\');" href="#" ><i class="fa fa-bullhorn"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bullseye\');" href="#" ><i class="fa fa-bullseye"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bus\');" href="#" ><i class="fa fa-bus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'taxi\');" href="#" ><i class="fa fa-cab"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'calculator\');" href="#" ><i class="fa fa-calculator"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'calendar\');" href="#" ><i class="fa fa-calendar"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'calendar-o\');" href="#" ><i class="fa fa-calendar-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'camera\');" href="#" ><i class="fa fa-camera"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'camera-retro\');" href="#" ><i class="fa fa-camera-retro"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'car\');" href="#" ><i class="fa fa-car"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-down\');" href="#" ><i class="fa fa-caret-square-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-left\');" href="#" ><i class="fa fa-caret-square-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-right\');" href="#" ><i class="fa fa-caret-square-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-up\');" href="#" ><i class="fa fa-caret-square-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc\');" href="#" ><i class="fa fa-cc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'certificate\');" href="#" ><i class="fa fa-certificate"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check\');" href="#" ><i class="fa fa-check"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check-circle\');" href="#" ><i class="fa fa-check-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check-circle-o\');" href="#" ><i class="fa fa-check-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check-square\');" href="#" ><i class="fa fa-check-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check-square-o\');" href="#" ><i class="fa fa-check-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'child\');" href="#" ><i class="fa fa-child"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle\');" href="#" ><i class="fa fa-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle-o\');" href="#" ><i class="fa fa-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle-o-notch\');" href="#" ><i class="fa fa-circle-o-notch"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle-thin\');" href="#" ><i class="fa fa-circle-thin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'clock-o\');" href="#" ><i class="fa fa-clock-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'times\');" href="#" ><i class="fa fa-close"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cloud\');" href="#" ><i class="fa fa-cloud"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cloud-download\');" href="#" ><i class="fa fa-cloud-download"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cloud-upload\');" href="#" ><i class="fa fa-cloud-upload"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'code\');" href="#" ><i class="fa fa-code"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'code-fork\');" href="#" ><i class="fa fa-code-fork"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'coffee\');" href="#" ><i class="fa fa-coffee"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cog\');" href="#" ><i class="fa fa-cog"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cogs\');" href="#" ><i class="fa fa-cogs"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'comment\');" href="#" ><i class="fa fa-comment"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'comment-o\');" href="#" ><i class="fa fa-comment-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'comments\');" href="#" ><i class="fa fa-comments"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'comments-o\');" href="#" ><i class="fa fa-comments-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'compass\');" href="#" ><i class="fa fa-compass"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'copyright\');" href="#" ><i class="fa fa-copyright"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'credit-card\');" href="#" ><i class="fa fa-credit-card"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'crop\');" href="#" ><i class="fa fa-crop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'crosshairs\');" href="#" ><i class="fa fa-crosshairs"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cube\');" href="#" ><i class="fa fa-cube"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cubes\');" href="#" ><i class="fa fa-cubes"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cutlery\');" href="#" ><i class="fa fa-cutlery"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tachometer\');" href="#" ><i class="fa fa-dashboard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'database\');" href="#" ><i class="fa fa-database"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'desktop\');" href="#" ><i class="fa fa-desktop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'dot-circle-o\');" href="#" ><i class="fa fa-dot-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'download\');" href="#" ><i class="fa fa-download"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pencil-square-o\');" href="#" ><i class="fa fa-edit"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ellipsis-h\');" href="#" ><i class="fa fa-ellipsis-h"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ellipsis-v\');" href="#" ><i class="fa fa-ellipsis-v"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'envelope\');" href="#" ><i class="fa fa-envelope"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'envelope-o\');" href="#" ><i class="fa fa-envelope-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'envelope-square\');" href="#" ><i class="fa fa-envelope-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eraser\');" href="#" ><i class="fa fa-eraser"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'exchange\');" href="#" ><i class="fa fa-exchange"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'exclamation\');" href="#" ><i class="fa fa-exclamation"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'exclamation-circle\');" href="#" ><i class="fa fa-exclamation-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'exclamation-triangle\');" href="#" ><i class="fa fa-exclamation-triangle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'external-link\');" href="#" ><i class="fa fa-external-link"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'external-link-square\');" href="#" ><i class="fa fa-external-link-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eye\');" href="#" ><i class="fa fa-eye"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eye-slash\');" href="#" ><i class="fa fa-eye-slash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eyedropper\');" href="#" ><i class="fa fa-eyedropper"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'fax\');" href="#" ><i class="fa fa-fax"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'female\');" href="#" ><i class="fa fa-female"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'fighter-jet\');" href="#" ><i class="fa fa-fighter-jet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-archive-o\');" href="#" ><i class="fa fa-file-archive-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-audio-o\');" href="#" ><i class="fa fa-file-audio-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-code-o\');" href="#" ><i class="fa fa-file-code-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-excel-o\');" href="#" ><i class="fa fa-file-excel-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-image-o\');" href="#" ><i class="fa fa-file-image-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-video-o\');" href="#" ><i class="fa fa-file-movie-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-pdf-o\');" href="#" ><i class="fa fa-file-pdf-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-image-o\');" href="#" ><i class="fa fa-file-photo-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-image-o\');" href="#" ><i class="fa fa-file-picture-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-powerpoint-o\');" href="#" ><i class="fa fa-file-powerpoint-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-audio-o\');" href="#" ><i class="fa fa-file-sound-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-video-o\');" href="#" ><i class="fa fa-file-video-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-word-o\');" href="#" ><i class="fa fa-file-word-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-archive-o\');" href="#" ><i class="fa fa-file-zip-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'film\');" href="#" ><i class="fa fa-film"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'filter\');" href="#" ><i class="fa fa-filter"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'fire\');" href="#" ><i class="fa fa-fire"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'fire-extinguisher\');" href="#" ><i class="fa fa-fire-extinguisher"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'flag\');" href="#" ><i class="fa fa-flag"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'flag-checkered\');" href="#" ><i class="fa fa-flag-checkered"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'flag-o\');" href="#" ><i class="fa fa-flag-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bolt\');" href="#" ><i class="fa fa-flash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'flask\');" href="#" ><i class="fa fa-flask"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'folder\');" href="#" ><i class="fa fa-folder"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'folder-o\');" href="#" ><i class="fa fa-folder-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'folder-open\');" href="#" ><i class="fa fa-folder-open"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'folder-open-o\');" href="#" ><i class="fa fa-folder-open-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'frown-o\');" href="#" ><i class="fa fa-frown-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'futbol-o\');" href="#" ><i class="fa fa-futbol-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'gamepad\');" href="#" ><i class="fa fa-gamepad"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'gavel\');" href="#" ><i class="fa fa-gavel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cog\');" href="#" ><i class="fa fa-gear"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cogs\');" href="#" ><i class="fa fa-gears"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'gift\');" href="#" ><i class="fa fa-gift"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'glass\');" href="#" ><i class="fa fa-glass"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'globe\');" href="#" ><i class="fa fa-globe"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'graduation-cap\');" href="#" ><i class="fa fa-graduation-cap"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'users\');" href="#" ><i class="fa fa-group"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hdd-o\');" href="#" ><i class="fa fa-hdd-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'headphones\');" href="#" ><i class="fa fa-headphones"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'heart\');" href="#" ><i class="fa fa-heart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'heart-o\');" href="#" ><i class="fa fa-heart-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'history\');" href="#" ><i class="fa fa-history"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'home\');" href="#" ><i class="fa fa-home"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'picture-o\');" href="#" ><i class="fa fa-image"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'inbox\');" href="#" ><i class="fa fa-inbox"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'info\');" href="#" ><i class="fa fa-info"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'info-circle\');" href="#" ><i class="fa fa-info-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'university\');" href="#" ><i class="fa fa-institution"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'key\');" href="#" ><i class="fa fa-key"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'keyboard-o\');" href="#" ><i class="fa fa-keyboard-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'language\');" href="#" ><i class="fa fa-language"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'laptop\');" href="#" ><i class="fa fa-laptop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'leaf\');" href="#" ><i class="fa fa-leaf"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'gavel\');" href="#" ><i class="fa fa-legal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'lemon-o\');" href="#" ><i class="fa fa-lemon-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'level-down\');" href="#" ><i class="fa fa-level-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'level-up\');" href="#" ><i class="fa fa-level-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'life-ring\');" href="#" ><i class="fa fa-life-bouy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'life-ring\');" href="#" ><i class="fa fa-life-buoy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'life-ring\');" href="#" ><i class="fa fa-life-ring"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'life-ring\');" href="#" ><i class="fa fa-life-saver"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'lightbulb-o\');" href="#" ><i class="fa fa-lightbulb-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'line-chart\');" href="#" ><i class="fa fa-line-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'location-arrow\');" href="#" ><i class="fa fa-location-arrow"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'lock\');" href="#" ><i class="fa fa-lock"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'magic\');" href="#" ><i class="fa fa-magic"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'magnet\');" href="#" ><i class="fa fa-magnet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share\');" href="#" ><i class="fa fa-mail-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'reply\');" href="#" ><i class="fa fa-mail-reply"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'reply-all\');" href="#" ><i class="fa fa-mail-reply-all"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'male\');" href="#" ><i class="fa fa-male"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'map-marker\');" href="#" ><i class="fa fa-map-marker"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'meh-o\');" href="#" ><i class="fa fa-meh-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'microphone\');" href="#" ><i class="fa fa-microphone"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'microphone-slash\');" href="#" ><i class="fa fa-microphone-slash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'minus\');" href="#" ><i class="fa fa-minus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'minus-circle\');" href="#" ><i class="fa fa-minus-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'minus-square\');" href="#" ><i class="fa fa-minus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'minus-square-o\');" href="#" ><i class="fa fa-minus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'mobile\');" href="#" ><i class="fa fa-mobile"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'mobile\');" href="#" ><i class="fa fa-mobile-phone"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'money\');" href="#" ><i class="fa fa-money"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'moon-o\');" href="#" ><i class="fa fa-moon-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'graduation-cap\');" href="#" ><i class="fa fa-mortar-board"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'music\');" href="#" ><i class="fa fa-music"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bars\');" href="#" ><i class="fa fa-navicon"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'newspaper-o\');" href="#" ><i class="fa fa-newspaper-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paint-brush\');" href="#" ><i class="fa fa-paint-brush"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paper-plane\');" href="#" ><i class="fa fa-paper-plane"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paper-plane-o\');" href="#" ><i class="fa fa-paper-plane-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paw\');" href="#" ><i class="fa fa-paw"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pencil\');" href="#" ><i class="fa fa-pencil"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pencil-square\');" href="#" ><i class="fa fa-pencil-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pencil-square-o\');" href="#" ><i class="fa fa-pencil-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'phone\');" href="#" ><i class="fa fa-phone"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'phone-square\');" href="#" ><i class="fa fa-phone-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'picture-o\');" href="#" ><i class="fa fa-photo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'picture-o\');" href="#" ><i class="fa fa-picture-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pie-chart\');" href="#" ><i class="fa fa-pie-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plane\');" href="#" ><i class="fa fa-plane"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plug\');" href="#" ><i class="fa fa-plug"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus\');" href="#" ><i class="fa fa-plus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus-circle\');" href="#" ><i class="fa fa-plus-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus-square\');" href="#" ><i class="fa fa-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus-square-o\');" href="#" ><i class="fa fa-plus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'power-off\');" href="#" ><i class="fa fa-power-off"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'print\');" href="#" ><i class="fa fa-print"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'puzzle-piece\');" href="#" ><i class="fa fa-puzzle-piece"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'qrcode\');" href="#" ><i class="fa fa-qrcode"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'question\');" href="#" ><i class="fa fa-question"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'question-circle\');" href="#" ><i class="fa fa-question-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'quote-left\');" href="#" ><i class="fa fa-quote-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'quote-right\');" href="#" ><i class="fa fa-quote-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'random\');" href="#" ><i class="fa fa-random"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'recycle\');" href="#" ><i class="fa fa-recycle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'refresh\');" href="#" ><i class="fa fa-refresh"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'times\');" href="#" ><i class="fa fa-remove"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bars\');" href="#" ><i class="fa fa-reorder"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'reply\');" href="#" ><i class="fa fa-reply"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'reply-all\');" href="#" ><i class="fa fa-reply-all"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'retweet\');" href="#" ><i class="fa fa-retweet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'road\');" href="#" ><i class="fa fa-road"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rocket\');" href="#" ><i class="fa fa-rocket"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rss\');" href="#" ><i class="fa fa-rss"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rss-square\');" href="#" ><i class="fa fa-rss-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'search\');" href="#" ><i class="fa fa-search"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'search-minus\');" href="#" ><i class="fa fa-search-minus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'search-plus\');" href="#" ><i class="fa fa-search-plus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paper-plane\');" href="#" ><i class="fa fa-send"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paper-plane-o\');" href="#" ><i class="fa fa-send-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share\');" href="#" ><i class="fa fa-share"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share-alt\');" href="#" ><i class="fa fa-share-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share-alt-square\');" href="#" ><i class="fa fa-share-alt-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share-square\');" href="#" ><i class="fa fa-share-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share-square-o\');" href="#" ><i class="fa fa-share-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'shield\');" href="#" ><i class="fa fa-shield"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'shopping-cart\');" href="#" ><i class="fa fa-shopping-cart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sign-in\');" href="#" ><i class="fa fa-sign-in"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sign-out\');" href="#" ><i class="fa fa-sign-out"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'signal\');" href="#" ><i class="fa fa-signal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sitemap\');" href="#" ><i class="fa fa-sitemap"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sliders\');" href="#" ><i class="fa fa-sliders"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'smile-o\');" href="#" ><i class="fa fa-smile-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'futbol-o\');" href="#" ><i class="fa fa-soccer-ball-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort\');" href="#" ><i class="fa fa-sort"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-alpha-asc\');" href="#" ><i class="fa fa-sort-alpha-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-alpha-desc\');" href="#" ><i class="fa fa-sort-alpha-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-amount-asc\');" href="#" ><i class="fa fa-sort-amount-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-amount-desc\');" href="#" ><i class="fa fa-sort-amount-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-asc\');" href="#" ><i class="fa fa-sort-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-desc\');" href="#" ><i class="fa fa-sort-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-desc\');" href="#" ><i class="fa fa-sort-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-numeric-asc\');" href="#" ><i class="fa fa-sort-numeric-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-numeric-desc\');" href="#" ><i class="fa fa-sort-numeric-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort-asc\');" href="#" ><i class="fa fa-sort-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'space-shuttle\');" href="#" ><i class="fa fa-space-shuttle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'spinner\');" href="#" ><i class="fa fa-spinner"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'spoon\');" href="#" ><i class="fa fa-spoon"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'square\');" href="#" ><i class="fa fa-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'square-o\');" href="#" ><i class="fa fa-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'star\');" href="#" ><i class="fa fa-star"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'star-half\');" href="#" ><i class="fa fa-star-half"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'star-half-o\');" href="#" ><i class="fa fa-star-half-empty"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'star-half-o\');" href="#" ><i class="fa fa-star-half-full"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'star-half-o\');" href="#" ><i class="fa fa-star-half-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'star-o\');" href="#" ><i class="fa fa-star-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'suitcase\');" href="#" ><i class="fa fa-suitcase"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sun-o\');" href="#" ><i class="fa fa-sun-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'life-ring\');" href="#" ><i class="fa fa-support"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tablet\');" href="#" ><i class="fa fa-tablet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tachometer\');" href="#" ><i class="fa fa-tachometer"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tag\');" href="#" ><i class="fa fa-tag"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tags\');" href="#" ><i class="fa fa-tags"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tasks\');" href="#" ><i class="fa fa-tasks"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'taxi\');" href="#" ><i class="fa fa-taxi"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'terminal\');" href="#" ><i class="fa fa-terminal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'thumb-tack\');" href="#" ><i class="fa fa-thumb-tack"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'thumbs-down\');" href="#" ><i class="fa fa-thumbs-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'thumbs-o-down\');" href="#" ><i class="fa fa-thumbs-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'thumbs-o-up\');" href="#" ><i class="fa fa-thumbs-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'thumbs-up\');" href="#" ><i class="fa fa-thumbs-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ticket\');" href="#" ><i class="fa fa-ticket"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'times\');" href="#" ><i class="fa fa-times"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'times-circle\');" href="#" ><i class="fa fa-times-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'times-circle-o\');" href="#" ><i class="fa fa-times-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tint\');" href="#" ><i class="fa fa-tint"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-down\');" href="#" ><i class="fa fa-toggle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-left\');" href="#" ><i class="fa fa-toggle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'toggle-off\');" href="#" ><i class="fa fa-toggle-off"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'toggle-on\');" href="#" ><i class="fa fa-toggle-on"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-right\');" href="#" ><i class="fa fa-toggle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-up\');" href="#" ><i class="fa fa-toggle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'trash\');" href="#" ><i class="fa fa-trash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'trash-o\');" href="#" ><i class="fa fa-trash-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tree\');" href="#" ><i class="fa fa-tree"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'trophy\');" href="#" ><i class="fa fa-trophy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'truck\');" href="#" ><i class="fa fa-truck"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tty\');" href="#" ><i class="fa fa-tty"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'umbrella\');" href="#" ><i class="fa fa-umbrella"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'university\');" href="#" ><i class="fa fa-university"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'unlock\');" href="#" ><i class="fa fa-unlock"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'unlock-alt\');" href="#" ><i class="fa fa-unlock-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'sort\');" href="#" ><i class="fa fa-unsorted"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'upload\');" href="#" ><i class="fa fa-upload"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'user\');" href="#" ><i class="fa fa-user"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'users\');" href="#" ><i class="fa fa-users"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'video-camera\');" href="#" ><i class="fa fa-video-camera"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'volume-down\');" href="#" ><i class="fa fa-volume-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'volume-off\');" href="#" ><i class="fa fa-volume-off"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'volume-up\');" href="#" ><i class="fa fa-volume-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'exclamation-triangle\');" href="#" ><i class="fa fa-warning"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'wheelchair\');" href="#" ><i class="fa fa-wheelchair"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'wifi\');" href="#" ><i class="fa fa-wifi"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'wrench\');" href="#" ><i class="fa fa-wrench"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="file-type">
                                                                                            <h2 class="page-header">File Type Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file\');" href="#" ><i class="fa fa-file"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-archive-o\');" href="#" ><i class="fa fa-file-archive-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-audio-o\');" href="#" ><i class="fa fa-file-audio-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-code-o\');" href="#" ><i class="fa fa-file-code-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-excel-o\');" href="#" ><i class="fa fa-file-excel-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-image-o\');" href="#" ><i class="fa fa-file-image-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-video-o\');" href="#" ><i class="fa fa-file-movie-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-o\');" href="#" ><i class="fa fa-file-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-pdf-o\');" href="#" ><i class="fa fa-file-pdf-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-image-o\');" href="#" ><i class="fa fa-file-photo-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-image-o\');" href="#" ><i class="fa fa-file-picture-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-powerpoint-o\');" href="#" ><i class="fa fa-file-powerpoint-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-audio-o\');" href="#" ><i class="fa fa-file-sound-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-text\');" href="#" ><i class="fa fa-file-text"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-text-o\');" href="#" ><i class="fa fa-file-text-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-video-o\');" href="#" ><i class="fa fa-file-video-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-word-o\');" href="#" ><i class="fa fa-file-word-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-archive-o\');" href="#" ><i class="fa fa-file-zip-o"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="spinner">
                                                                                            <h2 class="page-header">Spinner Icons</h2>

                                                                                            <div class="alert alert-success">
                                                                                                <ul class="fa-ul">
                                                                                                    <li>
                                                                                                        <i class="fa fa-info-circle fa-lg fa-li"></i> These icons work great with the <code>fa-spin</code> class. Check out the
                                                                                                        <a href="../examples/#spinning" class="alert-link">spinning icons example</a>.
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle-o-notch\');" href="#" ><i class="fa fa-circle-o-notch"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cog\');" href="#" ><i class="fa fa-cog"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cog\');" href="#" ><i class="fa fa-gear"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'refresh\');" href="#" ><i class="fa fa-refresh"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'spinner\');" href="#" ><i class="fa fa-spinner"></i></a>
                                                                                                </div>

                                                                                            </div>
                                                                                        </section>

                                                                                        <section id="form-control">
                                                                                            <h2 class="page-header">Form Control Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check-square\');" href="#" ><i class="fa fa-check-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'check-square-o\');" href="#" ><i class="fa fa-check-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle\');" href="#" ><i class="fa fa-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'circle-o\');" href="#" ><i class="fa fa-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'dot-circle-o\');" href="#" ><i class="fa fa-dot-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'minus-square\');" href="#" ><i class="fa fa-minus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'minus-square-o\');" href="#" ><i class="fa fa-minus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus-square\');" href="#" ><i class="fa fa-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus-square-o\');" href="#" ><i class="fa fa-plus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'square\');" href="#" ><i class="fa fa-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'square-o\');" href="#" ><i class="fa fa-square-o"></i></a>
                                                                                                </div>

                                                                                            </div>
                                                                                        </section>

                                                                                        <section id="payment">
                                                                                            <h2 class="page-header">Payment Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-amex\');" href="#" ><i class="fa fa-cc-amex"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-discover\');" href="#" ><i class="fa fa-cc-discover"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-mastercard\');" href="#" ><i class="fa fa-cc-mastercard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-paypal\');" href="#" ><i class="fa fa-cc-paypal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-stripe\');" href="#" ><i class="fa fa-cc-stripe"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-visa\');" href="#" ><i class="fa fa-cc-visa"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'credit-card\');" href="#" ><i class="fa fa-credit-card"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'google-wallet\');" href="#" ><i class="fa fa-google-wallet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paypal\');" href="#" ><i class="fa fa-paypal"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="chart">
                                                                                            <h2 class="page-header">Chart Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'area-chart\');" href="#" ><i class="fa fa-area-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bar-chart\');" href="#" ><i class="fa fa-bar-chart-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'line-chart\');" href="#" ><i class="fa fa-line-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pie-chart\');" href="#" ><i class="fa fa-pie-chart"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="currency">
                                                                                            <h2 class="page-header">Currency Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'btc\');" href="#" ><i class="fa fa-bitcoin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'btc\');" href="#" ><i class="fa fa-btc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'jpy\');" href="#" ><i class="fa fa-cny"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'usd\');" href="#" ><i class="fa fa-dollar"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eur\');" href="#" ><i class="fa fa-eur"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eur\');" href="#" ><i class="fa fa-euro"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'gbp\');" href="#" ><i class="fa fa-gbp"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ils\');" href="#" ><i class="fa fa-ils"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'inr\');" href="#" ><i class="fa fa-inr"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'jpy\');" href="#" ><i class="fa fa-jpy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'krw\');" href="#" ><i class="fa fa-krw"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'money\');" href="#" ><i class="fa fa-money"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'jpy\');" href="#" ><i class="fa fa-rmb"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rub\');" href="#" ><i class="fa fa-rouble"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rub\');" href="#" ><i class="fa fa-rub"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rub\');" href="#" ><i class="fa fa-ruble"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'inr\');" href="#" ><i class="fa fa-rupee"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ils\');" href="#" ><i class="fa fa-shekel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ils\');" href="#" ><i class="fa fa-sheqel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'try\');" href="#" ><i class="fa fa-try"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'try\');" href="#" ><i class="fa fa-turkish-lira"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'usd\');" href="#" ><i class="fa fa-usd"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'krw\');" href="#" ><i class="fa fa-won"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'jpy\');" href="#" ><i class="fa fa-yen"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="text-editor">
                                                                                            <h2 class="page-header">Text Editor Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'align-center\');" href="#" ><i class="fa fa-align-center"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'align-justify\');" href="#" ><i class="fa fa-align-justify"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'align-left\');" href="#" ><i class="fa fa-align-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'align-right\');" href="#" ><i class="fa fa-align-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bold\');" href="#" ><i class="fa fa-bold"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'link\');" href="#" ><i class="fa fa-chain"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chain-broken\');" href="#" ><i class="fa fa-chain-broken"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'clipboard\');" href="#" ><i class="fa fa-clipboard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'columns\');" href="#" ><i class="fa fa-columns"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'files-o\');" href="#" ><i class="fa fa-copy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'scissors\');" href="#" ><i class="fa fa-cut"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'outdent\');" href="#" ><i class="fa fa-dedent"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eraser\');" href="#" ><i class="fa fa-eraser"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file\');" href="#" ><i class="fa fa-file"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-o\');" href="#" ><i class="fa fa-file-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-text\');" href="#" ><i class="fa fa-file-text"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'file-text-o\');" href="#" ><i class="fa fa-file-text-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'files-o\');" href="#" ><i class="fa fa-files-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'floppy-o\');" href="#" ><i class="fa fa-floppy-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'font\');" href="#" ><i class="fa fa-font"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'header\');" href="#" ><i class="fa fa-header"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'indent\');" href="#" ><i class="fa fa-indent"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'italic\');" href="#" ><i class="fa fa-italic"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'link\');" href="#" ><i class="fa fa-link"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'list\');" href="#" ><i class="fa fa-list"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'list-alt\');" href="#" ><i class="fa fa-list-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'list-ol\');" href="#" ><i class="fa fa-list-ol"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'list-ul\');" href="#" ><i class="fa fa-list-ul"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'outdent\');" href="#" ><i class="fa fa-outdent"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paperclip\');" href="#" ><i class="fa fa-paperclip"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paragraph\');" href="#" ><i class="fa fa-paragraph"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'clipboard\');" href="#" ><i class="fa fa-paste"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'repeat\');" href="#" ><i class="fa fa-repeat"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'undo\');" href="#" ><i class="fa fa-rotate-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'repeat\');" href="#" ><i class="fa fa-rotate-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'floppy-o\');" href="#" ><i class="fa fa-save"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'scissors\');" href="#" ><i class="fa fa-scissors"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'strikethrough\');" href="#" ><i class="fa fa-strikethrough"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'subscript\');" href="#" ><i class="fa fa-subscript"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'superscript\');" href="#" ><i class="fa fa-superscript"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'table\');" href="#" ><i class="fa fa-table"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'text-height\');" href="#" ><i class="fa fa-text-height"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'text-width\');" href="#" ><i class="fa fa-text-width"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'th\');" href="#" ><i class="fa fa-th"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'th-large\');" href="#" ><i class="fa fa-th-large"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'th-list\');" href="#" ><i class="fa fa-th-list"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'underline\');" href="#" ><i class="fa fa-underline"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'undo\');" href="#" ><i class="fa fa-undo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chain-broken\');" href="#" ><i class="fa fa-unlink"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="directional">
                                                                                            <h2 class="page-header">Directional Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-double-down\');" href="#" ><i class="fa fa-angle-double-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-double-left\');" href="#" ><i class="fa fa-angle-double-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-double-right\');" href="#" ><i class="fa fa-angle-double-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-double-up\');" href="#" ><i class="fa fa-angle-double-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-down\');" href="#" ><i class="fa fa-angle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-left\');" href="#" ><i class="fa fa-angle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-right\');" href="#" ><i class="fa fa-angle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angle-up\');" href="#" ><i class="fa fa-angle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-down\');" href="#" ><i class="fa fa-arrow-circle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-left\');" href="#" ><i class="fa fa-arrow-circle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-o-down\');" href="#" ><i class="fa fa-arrow-circle-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-o-left\');" href="#" ><i class="fa fa-arrow-circle-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-o-right\');" href="#" ><i class="fa fa-arrow-circle-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-o-up\');" href="#" ><i class="fa fa-arrow-circle-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-right\');" href="#" ><i class="fa fa-arrow-circle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-circle-up\');" href="#" ><i class="fa fa-arrow-circle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-down\');" href="#" ><i class="fa fa-arrow-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-left\');" href="#" ><i class="fa fa-arrow-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-right\');" href="#" ><i class="fa fa-arrow-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrow-up\');" href="#" ><i class="fa fa-arrow-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows\');" href="#" ><i class="fa fa-arrows"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows-alt\');" href="#" ><i class="fa fa-arrows-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows-h\');" href="#" ><i class="fa fa-arrows-h"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows-v\');" href="#" ><i class="fa fa-arrows-v"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-down\');" href="#" ><i class="fa fa-caret-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-left\');" href="#" ><i class="fa fa-caret-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-right\');" href="#" ><i class="fa fa-caret-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-down\');" href="#" ><i class="fa fa-caret-square-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-left\');" href="#" ><i class="fa fa-caret-square-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-right\');" href="#" ><i class="fa fa-caret-square-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-up\');" href="#" ><i class="fa fa-caret-square-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-up\');" href="#" ><i class="fa fa-caret-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-circle-down\');" href="#" ><i class="fa fa-chevron-circle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-circle-left\');" href="#" ><i class="fa fa-chevron-circle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-circle-right\');" href="#" ><i class="fa fa-chevron-circle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-circle-up\');" href="#" ><i class="fa fa-chevron-circle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-down\');" href="#" ><i class="fa fa-chevron-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-left\');" href="#" ><i class="fa fa-chevron-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-right\');" href="#" ><i class="fa fa-chevron-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'chevron-up\');" href="#" ><i class="fa fa-chevron-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hand-o-down\');" href="#" ><i class="fa fa-hand-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hand-o-left\');" href="#" ><i class="fa fa-hand-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hand-o-right\');" href="#" ><i class="fa fa-hand-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hand-o-up\');" href="#" ><i class="fa fa-hand-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'long-arrow-down\');" href="#" ><i class="fa fa-long-arrow-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'long-arrow-left\');" href="#" ><i class="fa fa-long-arrow-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'long-arrow-right\');" href="#" ><i class="fa fa-long-arrow-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'long-arrow-up\');" href="#" ><i class="fa fa-long-arrow-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-down\');" href="#" ><i class="fa fa-toggle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-left\');" href="#" ><i class="fa fa-toggle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-right\');" href="#" ><i class="fa fa-toggle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'caret-square-o-up\');" href="#" ><i class="fa fa-toggle-up"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="video-player">
                                                                                            <h2 class="page-header">Video Player Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'arrows-alt\');" href="#" ><i class="fa fa-arrows-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'backward\');" href="#" ><i class="fa fa-backward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'compress\');" href="#" ><i class="fa fa-compress"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'eject\');" href="#" ><i class="fa fa-eject"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'expand\');" href="#" ><i class="fa fa-expand"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'fast-backward\');" href="#" ><i class="fa fa-fast-backward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'fast-forward\');" href="#" ><i class="fa fa-fast-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'forward\');" href="#" ><i class="fa fa-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pause\');" href="#" ><i class="fa fa-pause"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'play\');" href="#" ><i class="fa fa-play"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'play-circle\');" href="#" ><i class="fa fa-play-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'play-circle-o\');" href="#" ><i class="fa fa-play-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'step-backward\');" href="#" ><i class="fa fa-step-backward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'step-forward\');" href="#" ><i class="fa fa-step-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'stop\');" href="#" ><i class="fa fa-stop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'youtube-play\');" href="#" ><i class="fa fa-youtube-play"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="brand">
                                                                                            <h2 class="page-header">Brand Icons</h2>

                                                                                            <div class="alert alert-success">
                                                                                                <ul class="margin-bottom-none padding-left-lg">
                                                                                                    <li>All brand icons are trademarks of their respective owners.</li>
                                                                                                    <li>The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.</li>
                                                                                                </ul>

                                                                                            </div>

                                                                                            <div class="alert alert-warning">
                                                                                                <h4><i class="fa fa-warning"></i> Warning!</h4> Apparently, Adblock Plus can remove Font Awesome brand icons with their "Remove Social Media Buttons" setting. We will not use hacks to force them to display. Please
                                                                                                <a href="https://adblockplus.org/en/bugs" class="alert-link">report an issue with Adblock Plus</a> if you believe this to be an error. To work around this, you\'ll need to modify the social icon class names.

                                                                                            </div>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'adn\');" href="#" ><i class="fa fa-adn"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'android\');" href="#" ><i class="fa fa-android"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'angellist\');" href="#" ><i class="fa fa-angellist"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'apple\');" href="#" ><i class="fa fa-apple"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'behance\');" href="#" ><i class="fa fa-behance"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'behance-square\');" href="#" ><i class="fa fa-behance-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bitbucket\');" href="#" ><i class="fa fa-bitbucket"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'bitbucket-square\');" href="#" ><i class="fa fa-bitbucket-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'btc\');" href="#" ><i class="fa fa-bitcoin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'btc\');" href="#" ><i class="fa fa-btc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-amex\');" href="#" ><i class="fa fa-cc-amex"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-discover\');" href="#" ><i class="fa fa-cc-discover"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-mastercard\');" href="#" ><i class="fa fa-cc-mastercard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-paypal\');" href="#" ><i class="fa fa-cc-paypal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-stripe\');" href="#" ><i class="fa fa-cc-stripe"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'cc-visa\');" href="#" ><i class="fa fa-cc-visa"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'codepen\');" href="#" ><i class="fa fa-codepen"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'css3\');" href="#" ><i class="fa fa-css3"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'delicious\');" href="#" ><i class="fa fa-delicious"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'deviantart\');" href="#" ><i class="fa fa-deviantart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'digg\');" href="#" ><i class="fa fa-digg"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'dribbble\');" href="#" ><i class="fa fa-dribbble"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'dropbox\');" href="#" ><i class="fa fa-dropbox"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'drupal\');" href="#" ><i class="fa fa-drupal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'empire\');" href="#" ><i class="fa fa-empire"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'facebook\');" href="#" ><i class="fa fa-facebook"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'facebook-square\');" href="#" ><i class="fa fa-facebook-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'flickr\');" href="#" ><i class="fa fa-flickr"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'foursquare\');" href="#" ><i class="fa fa-foursquare"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'empire\');" href="#" ><i class="fa fa-ge"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'git\');" href="#" ><i class="fa fa-git"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'git-square\');" href="#" ><i class="fa fa-git-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'github\');" href="#" ><i class="fa fa-github"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'github-alt\');" href="#" ><i class="fa fa-github-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'github-square\');" href="#" ><i class="fa fa-github-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'gittip\');" href="#" ><i class="fa fa-gittip"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'google\');" href="#" ><i class="fa fa-google"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'google-plus\');" href="#" ><i class="fa fa-google-plus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'google-plus-square\');" href="#" ><i class="fa fa-google-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'google-wallet\');" href="#" ><i class="fa fa-google-wallet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hacker-news\');" href="#" ><i class="fa fa-hacker-news"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'html5\');" href="#" ><i class="fa fa-html5"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'instagram\');" href="#" ><i class="fa fa-instagram"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ioxhost\');" href="#" ><i class="fa fa-ioxhost"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'joomla\');" href="#" ><i class="fa fa-joomla"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'jsfiddle\');" href="#" ><i class="fa fa-jsfiddle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'lastfm\');" href="#" ><i class="fa fa-lastfm"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'lastfm-square\');" href="#" ><i class="fa fa-lastfm-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'linkedin\');" href="#" ><i class="fa fa-linkedin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'linkedin-square\');" href="#" ><i class="fa fa-linkedin-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'linux\');" href="#" ><i class="fa fa-linux"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'maxcdn\');" href="#" ><i class="fa fa-maxcdn"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'meanpath\');" href="#" ><i class="fa fa-meanpath"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'openid\');" href="#" ><i class="fa fa-openid"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pagelines\');" href="#" ><i class="fa fa-pagelines"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'paypal\');" href="#" ><i class="fa fa-paypal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pied-piper\');" href="#" ><i class="fa fa-pied-piper"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pied-piper-alt\');" href="#" ><i class="fa fa-pied-piper-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pinterest\');" href="#" ><i class="fa fa-pinterest"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'pinterest-square\');" href="#" ><i class="fa fa-pinterest-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'qq\');" href="#" ><i class="fa fa-qq"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rebel\');" href="#" ><i class="fa fa-ra"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'rebel\');" href="#" ><i class="fa fa-rebel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'reddit\');" href="#" ><i class="fa fa-reddit"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'reddit-square\');" href="#" ><i class="fa fa-reddit-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'renren\');" href="#" ><i class="fa fa-renren"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share-alt\');" href="#" ><i class="fa fa-share-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'share-alt-square\');" href="#" ><i class="fa fa-share-alt-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'skype\');" href="#" ><i class="fa fa-skype"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'slack\');" href="#" ><i class="fa fa-slack"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'slideshare\');" href="#" ><i class="fa fa-slideshare"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'soundcloud\');" href="#" ><i class="fa fa-soundcloud"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'spotify\');" href="#" ><i class="fa fa-spotify"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'stack-exchange\');" href="#" ><i class="fa fa-stack-exchange"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'stack-overflow\');" href="#" ><i class="fa fa-stack-overflow"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'steam\');" href="#" ><i class="fa fa-steam"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'steam-square\');" href="#" ><i class="fa fa-steam-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'stumbleupon\');" href="#" ><i class="fa fa-stumbleupon"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'stumbleupon-circle\');" href="#" ><i class="fa fa-stumbleupon-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tencent-weibo\');" href="#" ><i class="fa fa-tencent-weibo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'trello\');" href="#" ><i class="fa fa-trello"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tumblr\');" href="#" ><i class="fa fa-tumblr"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'tumblr-square\');" href="#" ><i class="fa fa-tumblr-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'twitch\');" href="#" ><i class="fa fa-twitch"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'twitter\');" href="#" ><i class="fa fa-twitter"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'twitter-square\');" href="#" ><i class="fa fa-twitter-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'vimeo-square\');" href="#" ><i class="fa fa-vimeo-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'vine\');" href="#" ><i class="fa fa-vine"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'vk\');" href="#" ><i class="fa fa-vk"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'weixin\');" href="#" ><i class="fa fa-wechat"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'weibo\');" href="#" ><i class="fa fa-weibo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'weixin\');" href="#" ><i class="fa fa-weixin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'windows\');" href="#" ><i class="fa fa-windows"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'wordpress\');" href="#" ><i class="fa fa-wordpress"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'xing\');" href="#" ><i class="fa fa-xing"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'xing-square\');" href="#" ><i class="fa fa-xing-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'yahoo\');" href="#" ><i class="fa fa-yahoo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'yelp\');" href="#" ><i class="fa fa-yelp"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'youtube\');" href="#" ><i class="fa fa-youtube"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'youtube-play\');" href="#" ><i class="fa fa-youtube-play"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'youtube-square\');" href="#" ><i class="fa fa-youtube-square"></i></a>
                                                                                                </div>

                                                                                            </div>
                                                                                        </section>

                                                                                        <section id="medical">
                                                                                            <h2 class="page-header">Medical Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'ambulance\');" href="#" ><i class="fa fa-ambulance"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'h-square\');" href="#" ><i class="fa fa-h-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'hospital-o\');" href="#" ><i class="fa fa-hospital-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'medkit\');" href="#" ><i class="fa fa-medkit"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'plus-square\');" href="#" ><i class="fa fa-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'stethoscope\');" href="#" ><i class="fa fa-stethoscope"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'user-md\');" href="#" ><i class="fa fa-user-md"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon('.$fieldId.', \'wheelchair\');" href="#" ><i class="fa fa-wheelchair"></i></a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </section>
                                                                                    </div>
                                                                                    <div class="show-hide-icons col-md-12" onclick="showHideIconContainer('.$fieldId.');">Show Icons <i class="fa fa-angle-double-down"></i> </div>
                                                                                </div>
                                                                            </div>';
                
                $fieldHtml = $fieldHtml.
                                '<div class="form-group">
                                    <label><input id="slide-field-label-'.$fieldId.'" onchange="saveContent(this, '.$fieldId.', \'field_name\');" type="text" class="input-trans" value="'.$sliderField->field_name.'" placeholder="Type your field name here..." style="width: 200px;"> </label><i class="fa fa-minus-circle"></i>
                                    '.$fieldInput.'
                                </div>';
                
            }

            $slideCount = WebsiteSliderSlide::where('sliderid', $sliderId)->where('status', 1)->count('id');

            $appendHtml = '<div class="item  active">
                                <div id="slide-field-container-10" class="row" style="padding: 20px">
                                    <h5>Slide '.str_pad($slideCount, 2, "0", STR_PAD_LEFT).'</h5>
                                    '.$fieldHtml.'
                                </div>
                            </div>';

            return json_encode($appendHtml);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function selectSlider() {
        //Function data
        $functionId = 292;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getSliders() {
        //Function data
        $functionId = 296;

        $sliderIds = Input::get('sliderids');



        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $sliderArr = array();
                $slideArr = array();
                $fieldArr = array();
                $sliderIdAuth = true;

                foreach ($sliderIds as $sliderId) {
                    $sliderIdAuth = WebsiteSlider::where('id', $sliderId)->where('orgid', $orgId)->first();
                    if($sliderIdAuth) {
                        $slides = WebsiteSliderSlide::where('sliderid', $sliderId)->where('status', 1)->get();

                        foreach ($slides as $slide) {
                            $fields = WebsiteSliderSlideField::where('slideid', $slide->id)->where('status', 1)->get();

                            foreach ($fields as $field) {

                                $fieldTitle = strtolower($field->field_name);
                                $fieldTitle = preg_replace('/\s+/', '_', $fieldTitle);

                                if($field->type == 4) {
                                    $fieldArr[$fieldTitle] = $field->img;
                                    $fieldArr[$fieldTitle.'_id'] = $field->id;
                                    $fieldArr[$fieldTitle.'_ext'] = $field->img_ext;
                                    if($fieldArr[$fieldTitle] == 1) {
                                        $fieldArr[$fieldTitle.'_url'] = URL::To('/').'/assets/backend/images/website/slide-block/'.$orgId.'/'.$field->id.'.'.$field->img_ext;
                                    }
                                    else {
                                        $fieldArr[$fieldTitle.'_url'] = URL::To('/').'assets/backend/images/website/slide-block/no-img.png';
                                    }
                                }
                                else {
                                    $fieldArr[$fieldTitle] = $field->content;
                                    $fieldArr['id'] = $field->id;
                                }
                            }
                            $slideArr[$slide->id] = $fieldArr;
                            unset($fieldArr);
                            $fieldArr = array();
                        }

                        $sliderArr[$sliderId] = $slideArr;
                        unset($slideArr);
                        $slideArr = array();
                    }
                    else {
                        return json_encode(array(
                            'status' => VariableController::API_Unauthorized_Content_Access,
                            'data' => $sliderArr
                        ));
                        exit;
                    }
                }

                return json_encode(array(
                    'status' => VariableController::API_Success,
                    'data' => $sliderArr
                ));
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }


        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return json_encode(array(
                'status' => VariableController::API_Exception,
                'data' => array('')
            ));
        }
    }

                    
    public function getMenu() {
        //Function data
        $functionId = 383;
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $menuId = Menu::where('orgid', $orgId)->where('status', 1)->pluck('id');
                $menuItems = MenuItem::where('menuid', $menuId)->where('status', 1)->get();

                foreach ($menuItems as $menuItem) {

                    $categories = ProductCategory::where('menu_item_id', $menuItem->id)->where('sub_item', null)->where('status', 1)->orderBy('list_order', 'ASC')->get();
                    $categoryArr = array();

                    foreach ($categories as $category) {

                        $subCategories = ProductCategory::where('menu_item_id', $menuItem->id)->where('sub_item', $category->id)->where('status', 1)->orderBy('list_order', 'ASC')->get();
                        $subCategoryArr = array();

                        foreach ($subCategories as $subCategory) {
                            $subCategoryArr[] = array(
                                'id' => $subCategory->id,
                                'name' => $subCategory->name
                            );
                        }
                        $categoryArr[] = array(
                            'id' => $category -> id,
                            'name' => $category-> name,
                            'subcategory' => $subCategoryArr
                        );
                    }

                    $arr[] = array(
                        'id' => $menuItem -> id,
                        'name' => $menuItem->name,
                        'img' => $menuItem->img,
                        'ext' => $menuItem->img_ext,
                        'category' => $categoryArr
                    );
                }

                return json_encode(array(
                    'status' => VariableController::API_Success,
                    'data' => $arr
                ));
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getPopularCategories() {
        //Function data
        $functionId = 387;

        $perPage = Input::get('perpage');
        $paginate = Input::get('paginate');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $categories = ProductCategory::where('orgid', $orgId)->where('status', 1)->orderBy('views', 'DESC')->forPage($paginate, $perPage)->get();

                foreach ($categories as $category) {

                    $arr[] = array(
                        'id' => $category -> id,
                        'name' => $category -> name,
                        'img' => $category -> img,
                        'ext' => $category -> img_ext
                    );
                }

                return json_encode(array(
                    'status' => VariableController::API_Success,
                    'data' => $arr
                ));
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getNewProducts() {
        //Function data
        $functionId = 389;

        $perPage = Input::get('perpage');
        $paginate = Input::get('paginate');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $products = Product::where('orgid', $orgId)->where('status', 1)->where('new_product', 2)->orderBy('views', 'DESC')->forPage($paginate, $perPage)->get();

                foreach ($products as $product) {
                    $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->where('status', 1)->first();
                    $productImgs = ProductImgRelation::where('productid', $product->id)->where('type', '<', 3)->where('status', 1)->get();

                    $productSubCategoryId = ProductCategoryRelation::where('product_id', $product->id)->where('status', 1)->pluck('sub_category_id');
                    $productSubCategory = ProductCategory::where('id', $productSubCategoryId)->first();
                    $subCategoryName = '<span style="font-style: italic; color: #89898975;">- None -</span>';
                    if($productSubCategory) {
                        $subCategoryName = $productSubCategory->name;
                    }

                    $imgArr = array();

                    foreach ($productImgs as $productImg) {
                        $img = ProductImage::where('id', $productImg->imgid)->first();

                        $imgArr[] = array(
                            'imgid' => $img->id,
                            'ext' => $img->ext,
                            'type' => $productImg->type
                        );
                    }

                    if($productDetail) {
                        $arr[] = array(
                            'id' => $product -> id,
                            'name' => $product -> name,
                            'img' => $imgArr,
                            'price' => number_format($productDetail-> price, 2, '.', ''),
                            'sub_category' => $subCategoryName
                        );
                    }
                }

                return json_encode(array(
                    'status' => VariableController::API_Success,
                    'data' => $arr
                ));
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getFeaturedProducts() {
        //Function data
        $functionId = 391;

        $perPage = Input::get('perpage');
        $paginate = Input::get('paginate');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $products = Product::where('orgid', $orgId)->where('status', 1)->orderBy('views', 'DESC')->forPage($paginate, $perPage)->get();

                foreach ($products as $product) {
                    $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->where('status', 1)->first();
                    $productImgs = ProductImgRelation::where('productid', $product->id)->where('type', '<', 3)->where('status', 1)->get();
                    $productSubCategoryId = ProductCategoryRelation::where('product_id', $product->id)->where('status', 1)->pluck('sub_category_id');
                    $productSubCategory = ProductCategory::where('id', $productSubCategoryId)->first();
                    $subCategoryName = '<span style="font-style: italic; color: #89898975;">- None -</span>';
                    if($productSubCategory) {
                        $subCategoryName = $productSubCategory->name;
                    }

                    $imgArr = array();

                    foreach ($productImgs as $productImg) {
                        $img = ProductImage::where('id', $productImg->imgid)->first();

                        $imgArr[] = array(
                            'imgid' => $img->id,
                            'ext' => $img->ext,
                            'type' => $productImg->type
                        );
                    }

                    if($productDetail) {
                        $arr[] = array(
                            'id' => $product -> id,
                            'name' => $product -> name,
                            'img' => $imgArr,
                            'price' => number_format($productDetail-> price, 2, '.', ''),
                            'new' => $product->new_product,
                            'sub_category' => $subCategoryName
                        );
                    }
                }

                return json_encode(array(
                    'status' => VariableController::API_Success,
                    'data' => $arr
                ));
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getProductDetails() {
        //Function data
        $functionId = 393;

        $productId = Input::get('productid');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $product = Product::where('id', $productId)->where('orgid', $orgId)->where('status', 1)->first();
                if($product) {
                    $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->where('status', 1)->first();
                    $productImgs = ProductImgRelation::where('productid', $product->id)->where('type', '<', 3)->where('status', 1)->get();
                    $productSubCategoryId = ProductCategoryRelation::where('product_id', $product->id)->where('status', 1)->pluck('sub_category_id');
                    $manufacture = ProductManufactures::where('id', $product->manufacture_id)->where('status', 1)->first();
                    $productSubCategory = ProductCategory::where('id', $productSubCategoryId)->first();
                    $subCategoryName = '<span style="font-style: italic; color: #89898975;">- None -</span>';
                    if($productSubCategory) {
                        $subCategoryName = $productSubCategory->name;
                    }

                    $imgArr = array();

                    foreach ($productImgs as $productImg) {
                        $img = ProductImage::where('id', $productImg->imgid)->first();

                        $imgArr[] = array(
                            'imgid' => $img->id,
                            'ext' => $img->ext,
                            'type' => $productImg->type
                        );
                    }

                    if($productDetail) {
                        $arr = array(
                            'id' => $product -> id,
                            'external_id' => $product->external_id,
                            'name' => $product -> name,
                            'img' => $imgArr,
                            'small_description' => $product->small_description,
                            'price' => number_format($productDetail-> price, 2, '.', ''),
                            'new' => $product->new_product,
                            'sub_category' => $subCategoryName,
                            'manufacture_id' => $manufacture->id,
                            'manufacture_name' => $manufacture->name,
                            'manufacture_img' => $manufacture->img,
                            'manufacture_ext' => $manufacture->img_ext
                        );
                    }

                    return json_encode(array(
                        'status' => VariableController::API_Success,
                        'data' => $arr
                    ));
                }
                else {
                    return json_encode(array(
                        'status' => VariableController::API_Unauthorized_Content_Access,
                        'data' => $arr
                    ));
                    exit;
                }
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getProductFullDetails() {
        //Function data
        $functionId = 395;

        $productId = Input::get('productid');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $product = Product::where('id', $productId)->where('orgid', $orgId)->where('status', 1)->first();
                if($product) {
                    $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->where('status', 1)->first();
                    $manufacture = ProductManufactures::where('id', $product->manufacture_id)->where('status', 1)->first();
                    $productImgs = ProductImgRelation::where('productid', $product->id)->where('type', '<', 3)->where('status', 1)->get();
                    $productSubCategoryId = ProductCategoryRelation::where('product_id', $product->id)->where('status', 1)->pluck('sub_category_id');
                    $productSubCategory = ProductCategory::where('id', $productSubCategoryId)->first();

                    $subCategoryName = $mainCategoryName = '<span style="font-style: italic; color: #89898975;">- None -</span>';
                    $mainCategoryId = $subCategoryId = null;

                    if($productSubCategory) {
                        $mainCategory = ProductCategory::where('id', $productSubCategory->sub_item)->first();

                        if($mainCategory) {
                            $mainCategoryName = $mainCategory -> name;
                            $mainCategoryId = $mainCategory -> id;
                        }
                        if($productSubCategory) {
                            $subCategoryName = $productSubCategory->name;
                            $subCategoryId = $productSubCategory -> id;
                        }
                    }

                    $imgArr = array();

                    foreach ($productImgs as $productImg) {
                        $img = ProductImage::where('id', $productImg->imgid)->first();

                        $imgArr[] = array(
                            'imgid' => $img->id,
                            'ext' => $img->ext,
                            'type' => $productImg->type
                        );
                    }

                    //related products
                    $relatedProducts = $this->relatedProducts($subCategoryId);

                    if($productDetail) {
                        $arr = array(
                            'id' => $product -> id,
                            'external_id' => $product->external_id,
                            'name' => $product -> name,
                            'img' => $imgArr,
                            'small_description' => $product->small_description,
                            'description' => $product->description,
                            'additional_info' => $product->additional_info,
                            'price' => number_format($productDetail-> price, 2, '.', ''),
                            'new' => $product->new_product,
                            'main_category_id' => $mainCategoryId,
                            'main_category_name' => $mainCategoryName,
                            'sub_category_id' => $subCategoryId,
                            'sub_category_name' => $subCategoryName,
                            'manufacture_id' => $manufacture->id,
                            'manufacture_name' => $manufacture->name,
                            'manufacture_img' => $manufacture->img,
                            'manufacture_ext' => $manufacture->img_ext,
                            'related_products' => $relatedProducts
                        );
                    }

                    return json_encode(array(
                        'status' => VariableController::API_Success,
                        'data' => $arr
                    ));
                }
                else {
                    return json_encode(array(
                        'status' => VariableController::API_Unauthorized_Content_Access,
                        'data' => $arr
                    ));
                    exit;
                }
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function relatedProducts($subCategoryId) {
        //Function data
        $functionId = 396;

        try {
            $products = ProductCategoryRelation::where('sub_category_id', $subCategoryId)->where('status', 1)->lists('product_id');
            $counter = 0;
            $arr = array();

            foreach ($products as $productId) {
                $product = Product::where('id', $productId)->first();
                if($product->status == 1) {
                    $productDetail = ProductDetail::where('productid', $product->id)->where('default_product', 1)->where('status', 1)->first();
                    $productImgs = ProductImgRelation::where('productid', $product->id)->where('type', '<', 3)->where('status', 1)->get();
                    $productSubCategoryId = ProductCategoryRelation::where('product_id', $product->id)->where('status', 1)->pluck('sub_category_id');
                    $productSubCategory = ProductCategory::where('id', $productSubCategoryId)->first();
                    $subCategoryName = '<span style="font-style: italic; color: #89898975;">- None -</span>';
                    if($productSubCategory) {
                        $subCategoryName = $productSubCategory->name;
                    }

                    $imgArr = array();

                    foreach ($productImgs as $productImg) {
                        $img = ProductImage::where('id', $productImg->imgid)->first();

                        $imgArr[] = array(
                            'imgid' => $img->id,
                            'ext' => $img->ext,
                            'type' => $productImg->type
                        );
                    }

                    if($productDetail) {
                        $arr[] = array(
                            'id' => $product -> id,
                            'name' => $product -> name,
                            'img' => $imgArr,
                            'price' => number_format($productDetail-> price, 2, '.', ''),
                            'new' => $product->new_product,
                            'sub_category' => $subCategoryName
                        );
                    }

                    $counter++;
                    if($counter > 8) {
                        break;
                    }
                }
            }

            return $arr;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getManufactureDetails() {
        //Function data
        $functionId = 402;

        $manufactureId = Input::get('manufactureid');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $manufacture = ProductManufactures::where('id', $manufactureId)->where('orgid', $orgId)->where('status', 1)->first();
                if($manufacture) {

                    if($manufacture->img == 1) {
                        $img_url200 = URL::To('/').'/assets/backend/images/products/manufactures/'.$manufacture->id.'/logo@200.'.$manufacture->img_ext;
                    }
                    else {
                        $img_url200 = URL::To('/').'/assets/backend/images/products/manufactures/default/logo@200.jpg';
                    }

                    if($manufacture->banner == 1) {
                        $banner_url = URL::To('/').'/assets/backend/images/products/manufactures/'.$manufacture->id.'/banner.'.$manufacture->banner_ext;
                    }
                    else {
                        $banner_url = URL::To('/').'/assets/backend/images/products/manufactures/default/banner.jpg';
                    }

                    $breadcrumb = '<li><a href="'.Org::where('id', $orgId)->pluck('domain') .'/products/brands">Brands</a></li>
                                    <li class="active"><span>'.$manufacture->name.'</span></li>';
                    $arr = array(
                        'id' => $manufacture->id,
                        'name' => $manufacture->name,
                        'logo_imgurl200' => $img_url200,
                        'description' => $manufacture->description,
                        'banner_imgurl' => $banner_url,
                        'title_color' => $manufacture->title_color,
                        'breadcrumb' => $breadcrumb
                    );

                    return json_encode(array(
                        'status' => VariableController::API_Success,
                        'data' => $arr
                    ));
                }
                else {
                    return json_encode(array(
                        'status' => VariableController::API_Unauthorized_Content_Access,
                        'data' => $arr
                    ));
                    exit;
                }
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getManufactureProducts() {
        //Function data
        $functionId = 407;

        $manufactureId = Input::get('manufactureid');


        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();
            $perPage = Input::get('perpage');
            $paginate = Input::get('paginate');
            $orderBy = Input::get('orderby');
            $orderType = Input::get('ordertype');
            $arr = array();

            if($orgId) {
                $manufacture = ProductManufactures::where('id', $manufactureId)->where('orgid', $orgId)->where('status', 1)->first();
                if($manufacture) {

                    if($paginate == null || $paginate == '') $paginate = 1;
                    if($perPage == null || $perPage == '') $perPage = 50;

                    if($orderBy == '' || $orderBy == null) $orderBy = 'id';
                    if($orderType == '' || $orderType == null) $orderType = 'ASC';

                    $totalProducts = Product::where('manufacture_id', $manufactureId)->where('status', 1)->count();
                    $productIds = Product::where('manufacture_id', $manufactureId)->where('status', 1)->orderBy($orderBy, $orderType)->forPage($paginate, $perPage)->lists('id');

                    $productList = new ProductController();
                    $arr = array(
                        'products' => $productList -> getProductList($productIds),
                        'total' => $totalProducts
                    );

                    return json_encode(array(
                        'status' => VariableController::API_Success,
                        'data' => $arr
                    ));
                }
                else {
                    return json_encode(array(
                        'status' => VariableController::API_Unauthorized_Content_Access,
                        'data' => $arr
                    ));
                    exit;
                }
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getManufactures() {
        //Function data
        $functionId = 409;

        $arr = array();
        $paginate = Input::get('paginate');

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $manufactures = ProductManufactures::where('orgid', $orgId)->where('status', 1)->orderBy('name', 'ASC')->forPage($paginate, 40)->get();
                foreach ($manufactures as $manufacture) {
                    if($manufacture->img == 1) {
                        $url200 = URL::To('/').'/assets/backend/images/products/manufactures/'.$manufacture->id.'/logo@200.'.$manufacture->img_ext;
                    }
                    else {
                        $url200 = URL::To('/').'/assets/backend/images/products/manufactures/default/logo@200.jpg';
                    }
                    $arr[] = array(
                        'id' => $manufacture->id,
                        'name' => $manufacture->name,
                        'img200_url' => $url200
                    );
                }
                return json_encode(array(
                    'status' => VariableController::API_Success,
                    'data' => $arr
                ));
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getCategoryDetails() {
        //Function data
        $functionId = 411;

        $categoryId = Input::get('categoryid');
        $arr = array();

        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();

            if($orgId) {
                $category = ProductCategory::where('id', $categoryId)->where('orgid', $orgId)->where('status', 1)->first();
                if($category) {

                    if($category->img == 1) {
                        $img_url = URL::To('/').'/assets/backend/images/products/categories/'.$orgId.'/'.$categoryId.'.'.$category->img_ext;
                    }
                    else {
                        $img_url = URL::To('/').'/assets/backend/images/products/categories/default/img.jpg';
                    }

                    if($category->banner == 1) {
                        $banner_url = URL::To('/').'/assets/backend/images/products/categories/'.$orgId.'/'.$categoryId.'-banner.'.$category->img_ext;
                    }
                    else {
                        $banner_url = URL::To('/').'/assets/backend/images/products/categories/default/banner.jpg';
                    }

                    $breadcrumb = '';


                    $arr = array(
                        'id' => $category->id,
                        'name' => $category->name,
                        'logo_imgurl200' => $img_url,
                        'description' => $category->description,
                        'banner_imgurl' => $banner_url,
                        'title_color' => $category->title_color,
                        'breadcrumb' => $breadcrumb
                    );

                    return json_encode(array(
                        'status' => VariableController::API_Success,
                        'data' => $arr
                    ));
                }
                else {
                    return json_encode(array(
                        'status' => VariableController::API_Unauthorized_Content_Access,
                        'data' => $arr
                    ));
                    exit;
                }
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getCategoryProducts() {
        //Function data
        $functionId = 413;

        $categoryId = Input::get('categoryid');


        try {
            $access = new AccessController();
            $orgId = $access->apiAuthenticate();
            $perPage = Input::get('perpage');
            $paginate = Input::get('paginate');
            $orderBy = Input::get('orderby');
            $orderType = Input::get('ordertype');
            $arr = array();

            if($orgId) {
                $category = ProductCategory::where('id', $categoryId)->where('orgid', $orgId)->where('status', 1)->first();
                if($category) {

                    if($paginate == null || $paginate == '') $paginate = 1;
                    if($perPage == null || $perPage == '') $perPage = 50;

                    if($orderBy == '' || $orderBy == null) $orderBy = 'id';
                    if($orderType == '' || $orderType == null) $orderType = 'ASC';

                    $subCategories = ProductCategory::where('sub_item', $categoryId)->where('status', 1)->get();
                    $productIds = ProductCategoryRelation::where('product_category_id', $category->id)->where('status', 1)->lists('product_id');

                    foreach ($subCategories as $subCategory) {
                        $productCategoryIds = ProductCategoryRelation::where('sub_category_id', $subCategory->id)->where('status', 1)->lists('product_id');
                        $productSubCategoryIds = ProductCategoryRelation::where('product_category_id', $subCategory->id)->where('status', 1)->lists('product_id');
                    }


                    $productIds = array_merge($productIds, $productCategoryIds);
                    $productIds = array_merge($productIds, $productSubCategoryIds);
                    $productIds = array_unique($productIds);

                    $totalProducts = sizeof($productIds);

                    $start = ($paginate-1) * $perPage;

                    $productIds = array_slice($productIds, $start, ($start+$perPage));


                    $productList = new ProductController();

                    $arr = array(
                        'products' => $productList -> getProductList($productIds),
                        'total' => $totalProducts
                    );

                    return json_encode(array(
                        'status' => VariableController::API_Success,
                        'data' => $arr
                    ));
                }
                else {
                    return json_encode(array(
                        'status' => VariableController::API_Unauthorized_Content_Access,
                        'data' => $arr
                    ));
                    exit;
                }
            }
            else {
                return json_encode(array(
                    'status' => VariableController::API_Unauthorized,
                    'data' => array('')
                ));
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}