<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-12 13:11:40.
 * Controller ID - 83
 */

class EnquiryController extends BaseController
{
    const SystemId = 26;
    const ControllerId = 83;

                    
    public function sendEmail() {
        //Function data
        $functionId = 319;

        $name = Input::get('name');
        $email = Input::get('email');
        $phone = Input::get('phone');
        $subject = Input::get('subject');
        $message = Input::get('message');
        $ip = Input::get('ip');
        $orgKey = Input::get('orgkey');
        $orgId = Input::get('orgid');
        $orgSecret = Input::get('orgsecret');


        try {
            $authenticateAPI = new AccessController();
            $access = $authenticateAPI -> apiAuthenticate($orgId, $orgKey, $orgSecret);

            if($access != -1) {
                $orgId = Org::where('id', $orgId)->where('orgkey', $orgKey)->where('status', 1)->where('active', 1)->pluck('id');

                $settings = WebsiteEnquirySettings::where('orgid', $orgId)->where('status', 1)->first();

                if($settings) {
                    $adminEmail = $settings->receving_email;
                    $GLOBALS['orgid'] = $orgId;
                    $org = Org::where('id', $orgId)->first();

                    if($org->logo_img == 1)
                        $imgUrl = URL::To('/').'/assets/backend/images/org/logo/'.$org->id.'/logo.png';
                    else
                        $imgUrl = URL::To('/').'/assets/backend/images/org/logo/1/logo.png';

                    //sending email to admin
                    Mail::send('backend.emails.message', array(
                        'homeUrl' => $org->domain,
                        'orgLogo' => $imgUrl,
                        'orgName' => $org->name,
                        'title' => 'New Client Enquiry',
                        'content' => "<p style='text-align: justify !important;'>Name: <strong>$name</strong><br/>Email: <strong>$email</strong><br/>Phone: <strong>$phone</strong><br/>Subject: <strong>$subject</strong><br/><br/>Message: <strong>$message</strong></p>",
                        'senderName' => $name,
                        'facebook' => null,
                        'twitter' => null
                    ), function ($message) use ($adminEmail, $name) {
                        $message->to($adminEmail, $name)->subject('New client enquiry');
                    });

                    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    } else {
                        $ip = $_SERVER['REMOTE_ADDR'];
                    }

                    $enquiry = new WebsiteEnquiry();
                    $enquiry -> name = $name;
                    $enquiry -> email = $email;
                    $enquiry -> phone = $phone;
                    $enquiry -> subject = $subject;
                    $enquiry -> message = $message;
                    $enquiry -> ip = $ip;
                    $enquiry -> status = 1;
                    $enquiry -> orgid = $orgId;
                    $enquiry -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $enquiry -> save();

                    //sending reply to client
                    Mail::send('backend.emails.message', array(
                        'homeUrl' => $org->domain,
                        'orgLogo' => $imgUrl,
                        'orgName' => $org->name,
                        'title' => $settings->response_title,
                        'content' => $settings->response_msg,
                        'senderName' => $settings->sender_name,
                        'facebook' => null,
                        'twitter' => null
                    ), function ($message) use ($email, $name) {
                        $message->to($email, $name)->subject('Thank you for contacting us.');
                    });

                    return 1;
                }
                else {
                    //Setup system notification
                    $notification = new NotificationController();
                    $notification -> saveSystemNotification('Please setup email enquiry settings', $orgId, 1);

                    return 'Enquiry settings not found!';
                }
            }
            else {
                return -1;
            }


        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getEnquiries() {
        //Function data
        $functionId = 330;

        $paginate = Input::get('paginate');
        $perPage = Input::get('perpage');
        $arr = array();

        try {
            $enquiries = WebsiteEnquiry::where('orgid', Auth::user()->orgid)->where('status', '!=', 0)->orderBy('created_at', 'desc')->forPage($paginate, $perPage)->get();

            foreach ($enquiries as $enquiry) {
                $messages = explode(' ', $enquiry->message);
                $message = null;
                for($i = 0; $i < 8; $i++) {
                    $message = $message.' '.$messages[$i];
                    if(sizeof($messages) < $i+2) {
                        break;
                    }
                }

                if(sizeof($messages) > 8) {
                    $message = $message.'...';
                }


                //creating objects
                $timeScence = new CalculationController();

                //calculations
                $setCreatedAt = new DateTime($enquiry->created_at, new DateTimeZone('Pacific/Auckland'));
                $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                $arr[] = array(
                    'id' => $enquiry->id,
                    'name' => $enquiry->name,
                    'subject' => $enquiry->subject,
                    'message' => $message,
                    'status' => $enquiry->status,
                    'created_at' => $setCreatedAt
                );
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getNewestEnquiries() {
        //Function data
        $functionId = 332;

        $newestId = Input::get('newestid');
        $arr = array();

        try {
            $enquiries = WebsiteEnquiry::where('orgid', Auth::user()->orgid)->where('status', '!=', 0)->orderBy('created_at', 'desc')->where('id', '>', $newestId)->get();

            foreach ($enquiries as $enquiry) {
                $messages = explode(' ', $enquiry->message);
                $message = null;
                for($i = 0; $i < 8; $i++) {
                    $message = $message.' '.$messages[$i];
                    if(sizeof($messages) < $i+2) {
                        break;
                    }
                }

                if(sizeof($messages) > 8) {
                    $message = $message.'...';
                }


                //creating objects
                $timeScence = new CalculationController();

                //calculations
                $setCreatedAt = new DateTime($enquiry->created_at, new DateTimeZone('Pacific/Auckland'));
                $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                $arr[] = array(
                    'id' => $enquiry->id,
                    'name' => $enquiry->name,
                    'subject' => $enquiry->subject,
                    'message' => $message,
                    'status' => $enquiry->status,
                    'created_at' => $setCreatedAt
                );
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getEnquiry() {
        //Function data
        $functionId = 334;

        $enquiryId = Input::get('enquiryid');

        try {
            $enquiry = WebsiteEnquiry::where('id', $enquiryId)->where('orgid', Auth::user()->orgid)->where('status', '!=', 0)->first();
            $arr = null;

            if($enquiry) {
                $created_at = new DateTime($enquiry->created_at);
                $created_at = $created_at->format('g:ia \o\n l jS F Y');

                $arr = array(
                    'name' => $enquiry->name,
                    'email' => $enquiry->email,
                    'phone' => $enquiry->phone,
                    'subject' => $enquiry->subject,
                    'message' => $enquiry->message,
                    'status' => $enquiry->status,
                    'created_at' => $created_at
                );

                $enquiry->status = 2;
                $enquiry->save();
            }

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}