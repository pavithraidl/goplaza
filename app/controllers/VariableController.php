<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-09-17 11:19:01.
 * Controller ID - 32
 */

class VariableController extends BaseController
{
    //public variables
    //-----------------------------------------

    //API
    const API_Success = array('code' => 200, 'type' => 'Success', 'Info' => 'Request success. Data returned.');
    const API_Unauthorized = array('code' => 401, 'type' => 'Unauthorized', 'Info' => 'Invalid Org Key or Org Secret');
    const API_Exception = array('code' => 500, 'type' => 'Internal Server Error', 'Info' => 'Server cannot process the request for an unknown reason');
    const API_Unauthorized_Content_Access = array('code' => 404, 'type' => 'Unknown Content', 'Info' => 'Requested content cannot be found in the server');

    public function getVariable($name) {
        $exSystemId = 5;
        $exControllerId = 32;
        $exFunctionId = 19;

        try {
            //primary variables
            $domain = URL::to('/'); //website domain
            $stagingExtenstion = '';
            $version = '1.0';
            $localPassword = '123456';


            //returning relevant variable
            switch ($name) {
                case "domain":
                    return $domain;
                    break;

                case "version":
                    return $version;
                    break;

                case "local-password":
                    return $localPassword;
                    break;

                case 'staging-extension':
                    return $stagingExtenstion;
                    break;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException($exSystemId, $exControllerId, $exFunctionId, $ex);
            return 0;
        }


    }

    //check the database connection
    public function checkDb() {
        try {
            DB::connection()->getPdo();
            if(DB::connection()->getDatabaseName()){
                //Database connection is ok.
            }
        } catch (\Exception $e) {
            $errorMsg = 'Unable to connect with the database!'.'<br/><br/><br/>'.'<p style="text-align: left">'.$e.'</p>';
            echo View::make('backend.error.server', array('errorMsg' => $errorMsg));
            die();
        }
    }
}

