<?php

class UserController extends BaseController
{
    /************************| Users |***********************
     *
     * CONFIDENTIAL
     * __________________
     *
     *  [2014] - [2024]
     *  All Rights Reserved.
     *
     * Developed by - Pavithra Isuru
     * Created on - 30/10/16 15:46.
     */

    const SystemId = 2;
    const ControllerId = 4;

    public function getUsers() {
        $viewId = 4;
        $urlRouteId = 15;

        $access = new AccessController();
        $visitMonitor = $access -> visitMonitor($viewId);
        $accessCheck = $access ->getSecureRoute($viewId);

        if($visitMonitor) {
            if($accessCheck == -1) {
                return View::make('backend.error.under-maintenance');
            }
            else if($accessCheck == -2) {
                return View::make('backend.error.section-under-maintenance', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else if($accessCheck == 1) {
                return View::make('backend.user.users', array(
                    'systemId' => self::SystemId,
                    'viewId' => $viewId
                ));
            }
            else {
                $access -> denyAccess($urlRouteId);
                return View::make('backend.error.access-deny');
            }
        }
        else {
            return View::make('backend.error.500');
        }
    }

    public function getLogin() {
        return View::make('backend.auth.login');
    }

    public function postLogin() {
        $functionId = 16;

        try{
            //validate the login details
            $validator = Validator::make(Input::all(), array(
                'email' => 'required|email',
                'password' => 'required'
            ));

            if ($validator->fails()) {
                // Redirect to the sign in page with errors
                return Redirect::route('admin-get-login')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //remember user
                $remember = (Input::has('chk-remember')) ? true : false;

                // Sign in user to the dash board
                $email = Input::get('email');
                $password = Input::get('password');
                $previous = Input::get('previous');

                $auth = Auth::attempt(array(
                    'email' => $email,
                    'password' => $password,
                    'active' => 1
                ), $remember);

                if ($auth) {
                    if(User::where('email', $email)->pluck('active') == 0){
                        return Redirect::route('login')
                            ->with('global', 'This account is not Activated yet. Check your emails to active!');
                    }
                    else{
                        User::where('id', Auth::user()->id)->update(array(
                            'online' => \Carbon\Carbon::now('Pacific/Auckland')
                        ));
                        $this->saveActivity('Login to the system');
                        return Redirect::intended('dashboard');
                    }
                }
                else {
                    $this->saveActivity('Trying to login with wrong user details or deactivated account');
                    return Redirect::route('get-login')
                        ->with('global', 'Email/password wrong, or account not activated!');
                }
            }
        } Catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }


    public function getActivate($code) {
        $functionId = 123;
        try {
            if (User::where('code', '=', $code)->where('active', '=', 0)->pluck('id')) {
                $user = User::where('code', $code)->first();
                $fName = $user->fname;
                $lName = $user->lname;

                return View::make('backend.auth.activate', array(
                    'code' => $code,
                    'email' => $user->email,
                    'fName' => $fName,
                    'lName' => $lName
                ));
            } else {
                return View::make('backend.invalid-activation');
            }
        } Catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

    public function postActive() {
        $functionId = 124;

        $password = Input::get('password');
        $email = Input::get('email');
        $fName = Input::get('fname');
        $lName = Input::get('lname');
        $code = Input::get('code');

        try {
            $userId = User::where('code', '=', $code)->where('active', 0)->where('email', $email)->pluck('id');

            if($userId) {
                User::where('id', $userId)->update(array(
                    'fname' => $fName,
                    'lname' => $lName,
                    'active' => 1,
                    'code' => '',
                    'status' => 1,
                    'password' => Hash::make($password)
                ));

                return 1;
            }
            else {
                return -1;
            }
        } Catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }

    }

    public function getLogOut()
    {
        Auth::logout();
        $this->saveActivity('Logout from the system', null, null);
        return Redirect::route('get-login');
    }

    public function saveActivity($activity) {
        try {
            $record = new UserActivityLog();
            $record -> userid = Auth::user()->id;
            $record -> orgid = Auth::user()->orgid;
            $record -> activity = $activity;
            $record -> created_at = \Carbon\Carbon::now('UTC');
            $record -> save();

            return 1;
        } catch (Exception $ex) {

        }
    }

    public function getUserList()
    {
        //Function data
        $functionId = 91;

        try {
            $userList = null;
            $res = null;

            if (Auth::user()->roll < 2) {
                $userList = User::where('status', '!=', 0)->lists('id');
            } else {
                $access = SystemOperationAccess::where('userid', Auth::user()->id)->where('operationid', 2)->pluck('id');
                $accessAllow = SystemOperationAccess::where('id', $access)->pluck('allow');

                if ($accessAllow == 1) {
                    $choice = SystemOperationAccess::where('id', $access)->pluck('choice');

                    if ($choice == 1) {
                        $userList = User::where('status', '!=', 0)->where('orgid', Auth::user()->orgid)->where('roll', '>=', Auth::user()->roll)->lists('id');
                    } else if($choice == 2) {
                        $userList = User::where('status', '!=', 0)->where('orgid', Auth::user()->orgid)->where('roll', '==', Auth::user()->roll)->lists('id');
                    }
                    else {
                        $userList = User::where('id', Auth::user()->id)->lists('id');
                    }
                }

//                if (Auth::user()->roll == 0) {
//                    $userList = User::where('status', '!=', 0)->lists('id');
//                } else {
//                    if(sizeof($userList) > 1) {
//                        $userList = array_diff($userList, Auth::user()->id);
//                    }
//                }
            }

            if($userList != null) {
                $key = array_search(Auth::user()->id, $userList);
                unset($userList[$key]);
                array_unshift($userList, Auth::user()->id);
            }
            else {
                $userList = [Auth::user()->id];
            }

            //get the relevant data
            foreach ($userList as $userId) {
                $name = User::where('id', $userId)->pluck('fname') . ' ' . User::where('id', $userId)->pluck('lname');
                $departmentId = User::where('id', $userId)->pluck('dept');
                $department = $departmentId == 0 ? 'System' : OrgDepartments::where('id', $departmentId)->pluck('name');
                $status = User::where('id', $userId)->pluck('status');
                $roll = Auth::user()->roll;

                //get online state
                $timeAgo = new CalculationController();
                $online = User::where('id', $userId)->pluck('online');
                $online = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $online);
                $now = \Carbon\Carbon::now('Pacific/Auckland');

                $onlineGap = $diff = strtotime($now) - strtotime($online);
                $onlineTimeAgo = $timeAgo->timeAgo($online);
                if($onlineGap > 26000) {
                    $onlineGap = null;
                }

//                if($roll == 1) $rollName = 'Developer'; else if($roll == 2) $rollName = 'Head'; else $rollName = 'Employee';
                $rollName = User::where('id', $userId)->pluck('jobtitle');

                $avatar = 'assets/backend/images/users/'.$userId.'/user-35.jpg';
                try {
                    getimagesize($avatar);
                }
                catch(Exception $ex) {
                    $avatar = 'assets/backend/images/users/default-M/user-35.jpg';
                }

                $res[] = array(
                    'id' => $userId,
                    'name' => $name,
                    'dept' => $department,
                    'roll' => $rollName,
                    'status' => $status,
                    'avatar' => $avatar,
                    'onlinegap' => $onlineGap,
                    'timeago' => $onlineTimeAgo
                );
            }

            return json_encode($res);

        } Catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
                    
    public function getUserBasicDetails() {
        //Function data
        $functionId = 100;

        $userId = Input::get('userid');

        try {
            $name = User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname');
            $email = User::where('id', $userId)->pluck('email');
            $jobTitle = User::where('id', $userId)->pluck('jobtitle');
            $status = User::where('id', $userId)->pluck('status');
            $department = OrgDepartments::where('id', User::where('id', $userId)->pluck('dept'))->pluck('name');

            $timeAgo = new CalculationController();
            $online = User::where('id', $userId)->pluck('online');
            $online = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $online);
            $now = \Carbon\Carbon::now('Pacific/Auckland');

            $onlineGap = $diff = strtotime($now) - strtotime($online);
            $onlineTimeAgo = $timeAgo->timeAgo($online);
            if($onlineTimeAgo == '30 Nov -1') {
                $onlineTimeAgo = 'No records';
            }
            if($onlineGap > 26000) {
                $onlineGap = null;
            }

            $profilePic = 'assets/backend/images/users/'.$userId.'/user-100.jpg';
            try {
                getimagesize($profilePic);
            }
            catch(Exception $ex) {
                $profilePic = 'assets/backend/images/users/default-M/user-100.jpg';
            }

            $arr = array(
                'id' => $userId,
                'name' => $name,
                'email' => $email,
                'status' => $status,
                'jobtitle' => $jobTitle,
                'dept' => $department,
                'avatar' => $profilePic,
                'onlinegap' => $onlineGap,
                'onlinetimeago' => $onlineTimeAgo
            );

            return json_encode($arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function selectUser() {
        //Function data
        $functionId = 102;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeUserStatus() {
        //Function data
        $functionId = 105;

        //ajaxData = Input::get('name');

        try {
            //Develop your function here

            //return json_encode(arr);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeSystemAccess() {
        //Function data
        $functionId = 107;

        $userId = Input::get('selecteduser');
        $status = Input::get('status');
        $viewId = Input::get('viewid');

        try {
            if(SystemViewAccess::where('viewid', $viewId)->where('userid', $userId)->pluck('id')) {
                SystemViewAccess::where('viewid', $viewId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemViewAccess();
                $systemAccess -> viewid = $viewId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access to '.SystemView::where('id', $viewId)->pluck('view_name'));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function changeOperationAccess() {
        //Function data
        $functionId = 109;

        $userId = Input::get('selecteduser');
        $status = Input::get('status');
        $operationId = Input::get('operationid');

        try {
            if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
                SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemOperationAccess();
                $systemAccess -> operationid = $operationId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('name'));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function deleteUser() {
        //Function data
        $functionId = 111;

        $userId = Input::get('userid');

        try {
            User::where('id', $userId)->update(array(
                'status' => 0,
                'active' => 0,
                'email' => ''
            ));

            $this -> saveActivity('Deleted - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'\'s User account');

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function resendActivationEmail() {
        //Function data
        $functionId = 113;

        $userId = Input::get('userid');

        try {
            $code = User::where('id', $userId)->pluck('code');
            $orgName = 'PlusMedia';
            $fName = User::where('id', $userId)->pluck('fname');
            $lName = User::where('id', $userId)->pluck('lname');
            $email = User::where('id', $userId)->pluck('email');

            Mail::send('backend.emails.active', array(
                'link' => URL::route('active', $code),
                'orgName' => $orgName,
                'name' => $fName . ' ' . $lName,
                'homeurl' => URL::To('/')
            ), function ($message) use ($email, $fName) {
                $message->to($email, $fName)->subject('Activate your account');
            });

            return json_encode($fName);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getAccessList() {
        //Function data
        $functionId = 117;

        $userId = Input::get('userid');
        $ope = [];

        try {
            $viewList = SystemViewAccess::where('userid', $userId)->where('allow', 1)->lists('viewid');
            $operationList = SystemOperationAccess::where('userid', $userId)->where('allow', 1)->lists('operationid');

            foreach ($operationList as $operationId) {
                $choice = SystemOperationAccess::where('userid', $userId)->where('allow', 1)->where('operationid', $operationId)->pluck('choice');

                if(!$choice || $choice == null) {
                    $ope[] = array(
                        'id' => $operationId,
                        'choice' => null
                    );
                }
                else {
                    $ope[] = array(
                        'id' => $operationId,
                        'choice' => $choice
                    );
                }
            }

            $ret = array(
                'view' => $viewList,
                'ope' => $ope
            );

            return json_encode($ret);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function getActivityLog() {
        //Function data
        $functionId = 119;

        $paginate = Input::get('paginate');
        $userId = Input::get('userid');
        $arr = [];

        try {
            $activityList = UserActivityLog::where('userid', $userId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeScence = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $activity = UserActivityLog::where('id', $activityId)->pluck('activity');
                    $createdAt = UserActivityLog::where('id', $activityId)->pluck('created_at');

                    //calculations
                    $setCreatedAt = new DateTime($createdAt, new DateTimeZone('Pacific/Auckland'));
                    $setCreatedAt = $setCreatedAt->setTimezone(new DateTimeZone('Pacific/Auckland'));
                    $setCreatedAt = $setCreatedAt->format('Y-m-d H:i:s');
                    $setCreatedAt = $timeScence->timeAgo($setCreatedAt);

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

                    
    public function addUser() {
        //Function data
        $functionId = 121;

        $name = Input::get('name');
        $email = Input::get('email');
        $jobTitle = Input::get('jobtitle');
        $department = Input::get('department');


        try {
            if (User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            } else {

                $words = explode(' ', $name);
                array_shift($words);
                $lName = implode(' ', $words);
                $name = explode(" ", $name);
                $fName = $name[0];

                $roll = 4;
                $orgId = Auth::user()->orgid;
                $orgName = 'ABC Company';
                $code = str_random(60);

                $user = new User();
                $user->fname = ucfirst($fName);
                $user->lname = ucfirst($lName);
                $user->email = $email;
                $user->code = $code;
                $user->active = 0;
                $user->jobtitle = $jobTitle;
                $user->roll = $roll;
                $user->orgid = $orgId;
                $user->dept = $department;
                $user->status = 3;
                $user->created_at = \Carbon\Carbon::now('UTC');
                $user->save();

                if ($email != '') {
                    Mail::send('backend.emails.active', array(
                        'link' => URL::route('active', $code),
                        'orgName' => $orgName,
                        'name' => $fName . ' ' . $lName,
                        'homeurl' => URL::To('/')
                    ), function ($message) use ($user, $email, $fName) {
                        $message->to($email, $fName)->subject('Activate your account');
                    });
                }

                return 1;

            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }

    }

                    
    public function saveAccessChoice() {
        //Function data
        $functionId = 126;

        $choice = Input::get('choice');
        $operationId = Input::get('operationid');
        $userId = Input::get('userid');
        $allow = 1;

        try {

            if($choice == 0)
                $allow = 0;

            if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
                SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                    'choice' => $choice,
                    'allow' => $allow
                ));


            }
            else {
                $systemAccess = new SystemOperationAccess();
                $systemAccess -> operationid = $operationId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = 1;
                $systemAccess -> choice = $choice;
                $systemAccess -> allow = $allow;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
            $this -> saveActivity('Allow to '.SystemOperationChoises::where('id', $choice)->pluck('choise').' user - '.User::where('id', $userId)->pluck('fname').' '.User::where('id', $userId)->pluck('lname').'('.User::where('id', $userId)->pluck('email').') access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('name'));

            return 1;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }

    public function onlineState() {
        User::where('id', Auth::user()->id)->update(array(
            'online' => \Carbon\Carbon::now('Pacific/Auckland')
        ));

        return 1;
    }

                    
    public function onlineStateCheck() {
        //Function data
        $functionId = 145;

        //ajaxData = Input::get('name');

        try {
            $userList = null;
            $res = null;

            if (Auth::user()->roll < 2) {
                $userList = User::where('status', '!=', 0)->lists('id');
            } else {
                $access = SystemOperationAccess::where('userid', Auth::user()->id)->where('operationid', 2)->pluck('id');
                $accessAllow = SystemOperationAccess::where('id', $access)->pluck('allow');

                if ($accessAllow == 1) {
                    $choice = SystemOperationAccess::where('id', $access)->pluck('choice');

                    if ($choice == 1) {
                        $userList = User::where('status', '!=', 0)->where('roll', '>=', Auth::user()->roll)->lists('id');
                    } else if($choice == 2) {
                        $userList = User::where('status', '!=', 0)->where('roll', '==', Auth::user()->roll)->lists('id');
                    }
                    else {
                        $userList = User::where('id', Auth::user()->id)->lists('id');
                    }
                }
            }

            if($userList != null) {
                $key = array_search(Auth::user()->id, $userList);
                unset($userList[$key]);
                array_unshift($userList, Auth::user()->id);
            }
            else {
                $userList = [Auth::user()->id];
            }

            foreach ($userList as $userId) {
                $timeAgo = new CalculationController();
                $online = User::where('id', $userId)->pluck('online');
                $online = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $online);
                $now = \Carbon\Carbon::now('Pacific/Auckland');

                $onlineGap = $diff = strtotime($now) - strtotime($online);
                $onlineTimeAgo = $timeAgo->timeAgo($online);
                if($onlineGap > 26000) {
                    $onlineGap = null;
                }

                $res[] = array(
                    'id' => $userId,
                    'onlinegap' => $onlineGap
                );
            }

            return json_encode($res);

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}