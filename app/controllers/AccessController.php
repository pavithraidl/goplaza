<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {15/02/17} {10:24}.
 */
 
class AccessController extends BaseController {
    const SystemId = 5;
    const ControllerId = 1;

    public function getSecureRoute($viewId) {
        $exFunctionId = 71;

        try {
            if(SystemViewAccess::where('viewid', $viewId)->where('userid', Auth::User()->id)->where('allow', 1)->pluck('id') ||
                Auth::user()->roll == 1 ||
                SystemView::where('id', $viewId)->pluck('allow_default') == 1) {

                $systemMaintenance = System::where('id', 5)->pluck('visibility');
                if($systemMaintenance == 1) {
                    $maintenance = SystemView::where('id', $viewId)->pluck('maintenance');
                    if($maintenance == 0) {
                        return 1;
                    }
                    else {
                        if(Auth::user()->roll < 2) {
                            return 1;
                        }
                        else {
                            return -2;
                        }
                    }
                }
                else {
                    if(Auth::user()->roll < 2) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                }
            }
            else {
                return 0;
            }
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return 0;
        }

    }

    public function getOperationAccessChecker(){
        $opCode = Input::get('opcode');

        try{
            if(DB::table('operation_access')->where('opcode', $opCode)->where('userid', Auth::user()->id)->where('status', 1)->pluck('id')){
                return 1;
            }
            else{
                return 0;
            }
        }catch (Exception $ex) {
            $errorController = new ErrorController();
            $errorController->CatchError(1, $ex);
            return 0;
        }

    }

    public function operationAccessChecker($opCode){
        try{
            if(Auth::user()->rollid < 4) {
                return 1;
            }
            else if(DB::table('operation_access')->where('opcode', $opCode)->where('userid', Auth::user()->id)->where('allow', 1)->where('status', 1)->pluck('id')){
                return 1;
            }
            else{
                return 0;
            }
        }catch (Exception $ex) {
            $errorController = new ErrorController();
            $errorController->CatchError(2, $ex);
            return 0;
        }
    }

    public function operationLengthChecker($opLeCode) {
        $opLe = DB::table('operation_length_user')->where('userid', Auth::user()->id)->where('oplecode', $opLeCode)->where('allow', 1)->pluck('length');
        if($opLe == null) {
            return -1;
        }
        else {
            if($opLe == 'd') {
                $timeFilter = date("Y/m/d");
            }
            else if($opLe == 'w') {
                $timeFilter = Carbon::now()->subweeks(1);
            }
            else if($opLe == 'm') {
                $timeFilter = Carbon::now()->submonths(1);
            }
            else {
                $timeFilter = Carbon::now()->subyears(1);
            }

            return $timeFilter;
        }
    }

    public function denyAccess($urlRouteId){
        $exFunctionId = 72;

        try{
            $denyAccess = new SystemDenyAccess();
            $denyAccess -> urlrouteid = $urlRouteId;
            $denyAccess -> userid = Auth::user()->id;
            $denyAccess -> created_at = \Carbon\Carbon::now('UTC');
            $denyAccess -> save();

            return true;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return false;
        }
    }

    public function viewAccessCheck($pageId){
        $blockIp = new UserVisitController();
        if ($blockIp->getUserBlockIp()) {
            if(DB::table('system_routingaccess')->where('pageid', $pageId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') &&
                DB::table('system_views')->where('id', $pageId)->where('maintenance', 0)->pluck('id')){
                $userVisit = new UserVisitController();
                $userVisit->setUserVisit($pageId);
                return 1;
            } else {
                if (DB::table('system_views')->where('id', $pageId)->where('maintenance', 0)->pluck('id')) {
                    return 2;
                } else {
                    return -1;
                }
            }
        } else {
            return -9;
        }
    }

    //Authenticate the url access (function id - 145)
    public function urlAccessAthenticator() {
        try{
            if(DB::table('users')->where('id', Auth::user()->id)->pluck('active') != 1 || DB::table('users')->where('id',
                    Auth::user()->id)->pluck('status') != 1 || DB::table('org')->where('id', Auth::user()->orgid)->pluck('status') == 0) {
                Auth::logout();
            }
        } catch(Exception $ex) {
            Auth::logout();
        }

    }

    //View visit monitor
    public function visitMonitor($viewId) {
        $exFunctionId = 1;

        try {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $visit = new SystemViewVisit();
            $visit -> viewid = $viewId;
            $visit -> ip = $ip;
            $visit -> userid = Auth::user()->id;
            $visit -> created_at = (new DateTime("now", new DateTimeZone('Asia/Colombo')))->format('Y-m-d H:i:s');
            $visit -> save();

            return true;
        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $exFunctionId, $ex);
            return false;
        }
    }

                    
    public function apiAuthenticate() {
        //Function data
        $functionId = 320;

        try {
            foreach($_SERVER as $name => $value)
            {
                if(substr($name, 0, 5) == 'HTTP_')
                {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }

            $orgKey = $headers['Org-Key'];
            $orgSecret = $headers['Org-Secret'];

            $orgKey = substr($orgKey, 18);
            $orgKey = substr($orgKey, 0, -10);
            $orgKey = strrev($orgKey);

            $access = Org::where('active', 1)->where('orgkey', $orgKey)->where('org_secret', $orgSecret)->where('status', 1)->first();

            if($access)
                return $access->id;
            else
                return false;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}