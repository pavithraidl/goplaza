<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-15 09:29:59.
 * Controller ID - 89
 */

class MerchantController extends BaseController
{
    const SystemId = 3;
    const ControllerId = 89;

                    
    public function poliPay($orgId, $amount, $ref, $id) {
        //Function data
        $functionId = 321;

        try {
            //get org details
            $orgMerchantId = OrgMerchant::where('orgid', $orgId)->where('merchant_detail_id', 1)->where('status', 1)->pluck('merchant_id');
            $merchantPoli = OrgMerchantPoli::where('id', $orgMerchantId)->first();
            $redirectUrl = $merchantPoli->redirect_url;

            $json_builder = '{
              "Amount":"'.$amount.'",
              "CurrencyCode":"NZD",
              "MerchantReference":"'.$ref.'",
              "MerchantHomepageURL":"'.$redirectUrl.'",
              "SuccessURL":"'.$redirectUrl.'success/'.$id.'",
              "FailureURL":"'.$redirectUrl.'fail/'.$id.'",
              "CancellationURL":"'.$redirectUrl.'cancel/'.$id.'",
              "NotificationURL":"'.$redirectUrl.'nude" 
            }';

            $auth = base64_encode('SS64007779:kD3$F$ZDUM7Ry');

            $header = array();
            $header[] = 'Content-Type: application/json';
            $header[] = 'Authorization: Basic '.$auth;

            $url = 'https://poliapi.apac.paywithpoli.com/api/v2/Transaction/Initiate';
            $ch = curl_init($url);

            //The JSON data.

            //Encode the array into JSON.
            $jsonDataEncoded = json_encode($json_builder);

            //Tell cURL that we want to send a POST request.
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_builder);
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            $result = json_decode($result, true);

            return $result;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return $ex;
        }
    }
}