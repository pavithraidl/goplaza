<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-03-12 09:46:33.
 * Controller ID - 92
 */

class ExternalAPIController extends BaseController
{
    const SystemId = 3;
    const ControllerId = 92;

                    
    public function savePepperyProductInDatabase() {
        //Function data
        $functionId = 352;
        $page = 8;
        $pageSize = 250;
        $fullMode = 'true';
        $display = '';

        try {
            $url = 'https://api.pepperi.com/v1.0/items?page='.$page.'&page_size='.$pageSize.'&full_mode='.$fullMode;

            $ch = curl_init();

            //Set the URL that you want to GET by using the CURLOPT_URL option.
            curl_setopt($ch, CURLOPT_URL, $url);

            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'X-Pepperi-ConsumerKey: CpYdLvxPxmxn5nAsgYh9kTcY5JCk6SYO',
                'Authorization: Basic bml0aW5AYW56cGhhcm1hLmNvLm56Ok5pdGluMjM0',
            ));

            //Execute the request.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $result = json_decode($result, true);

            //Close the cURL handle.
            curl_close($ch);

            //Print the data out onto the page.
//            dd($result);
            foreach ($result as $data) {
                $brand = $data['MainCategoryID'];
                $externalId = $data['ExternalID'];
                $name = $data['Name'];
                $retailPrice = $data['TSARetailPrice'];
                $wholesalePrice = $data['SecondaryPrice'];
                $costPrice = $data['CostPrice'];
                $manufactureId = null;
                $description = null;
                $keywords = null;

                $product = Product::where('external_id', $externalId)->first();

                if($product) {
                    $display = $display.$name .' - '.$product->name.'<br/>';
                }
                else {
                    $display = $display.'none<br/>';

                    //setup manufacture
                    $manufactureName = strtolower($brand);
                    if($manufactureName != '') {
                        $manufacture = ProductManufactures::where('name', 'LIKE', $manufactureName.'%')->first();
                        if(!$manufacture) {
                            $manufacture = new ProductManufactures();
                            $manufacture -> name = ucwords($manufactureName);
                            $manufacture -> description = '';
                            $manufacture -> img = 1;
                            $manufacture -> status = 1;
                            $manufacture -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                            $manufacture -> save();
                            $manufactureId = $manufacture -> id;
                        }
                        else {
                            $manufactureId = $manufacture ->id;
                        }
                    }

                    //setup keywords
                    $keywords = ucwords($manufactureName);

                    $product = new Product();
                    $product -> name = $name;
                    $product -> external_id = $externalId;
                    $product -> manufacture_id = $manufactureId;
                    $product -> description = $description;
                    $product -> keywords = $keywords;
                    $product -> payments = 1;
                    $product -> status = 1;
                    $product -> new_product = 0;
                    $product -> created_by = 65;
                    $product -> orgid = 8;
                    $product -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $product -> save();
                    $productId = $product -> id;

                    $detail = new ProductDetail();
                    $detail -> productid = $productId;
                    $detail -> price = $retailPrice;
                    $detail -> wholesale_price = $wholesalePrice;
                    $detail -> cost_price = $costPrice;
                    $detail -> default_product = 1;
                    $detail -> status = 1;
                    $detail -> created_by = 65;
                    $detail -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $detail -> save();
                }
            }

            return $display;

        } Catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveException(self::SystemId, self::ControllerId, $functionId, $ex);
            return 0;
        }
    }
}