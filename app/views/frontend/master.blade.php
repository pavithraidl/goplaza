<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 3/12/17 10:53.
 */

//Defining variables and data
$orgId = 1; //should be automated
$menuId = Menu::where('orgid', $orgId)->where('positionid', 1)->pluck('id');
$menuList = MenuItem::where('menuid', $menuId)->orderBy('menu_order', 'ASC')->where('status', 1)->get();
?>

        <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    {{HTML::style('assets/frontend/css/bootstrap.css')}}
    {{HTML::style('assets/frontend/css/style.css')}}
    {{HTML::style('assets/frontend/css/dark.css')}}
    {{HTML::style('assets/frontend/css/font-icons.css')}}
    {{HTML::style('assets/frontend/css/animate.css')}}
    {{HTML::style('assets/frontend/css/magnific-popup.css')}}

    {{HTML::style('assets/frontend/css/responsive.css')}}
    <meta name="viewport" content="width=device-width, initial-scale=1" />

{{HTML::script('/assets/backend/libs/jquery/jquery-1.11.1.min.js')}}

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    {{HTML::style('assets/frontend/include/rs-plugin/css/settings.css')}}
    {{HTML::style('assets/frontend/include/rs-plugin/css/layers.css')}}
    {{HTML::style('assets/frontend/include/rs-plugin/css/navigation.css')}}
    <!-- Document Title
    ============================================= -->
    <title>Home - Chemacy</title>
    <style>
        .revo-slider-emphasis-text {
            font-size: 58px;
            font-weight: 700;
            letter-spacing: 1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }
        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }
        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }
        .tp-video-play-button { display: none !important; }

        .tp-caption { white-space: nowrap; }
    </style>
</head>
<body class="stretched">
<input type="hidden" id="orgid" value="{{$orgId}}" />
<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="sticky-style-2">

        <div class="container clearfix">

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="index.html" class="standard-logo" data-dark-logo="{{URL::To('assets/frontend/img/logo.png')}}"><img src="{{URL::To('assets/frontend/img/logo.png')}}" alt="Chemacy Logo"></a>
                <a href="index.html" class="retina-logo" data-dark-logo="{{URL::To('assets/frontend/img/logo@2x.png')}}"><img src="{{URL::To('assets/frontend/img/logo@2x.png')}}" alt="Chemacy Logo"></a>
            </div><!-- #logo end -->

            <div class="top-links">
                <ul>
                    <li class="main"><a href="#">USD</a>
                        <ul>
                            <li><a href="#">EUR</a></li>
                            <li><a href="#">AUD</a></li>
                            <li><a href="#">GBP</a></li>
                        </ul>
                    </li>
                    <li class="main"><a href="#">EN</a>
                        <ul>
                            <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/icons/flags/french.png" alt="French"> FR</a></li>
                            <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/icons/flags/italian.png" alt="Italian"> IT</a></li>
                            <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/icons/flags/german.png" alt="German"> DE</a></li>
                        </ul>
                    </li>
                    <li class="main"><a href="#">Login</a>
                        <div class="top-link-section">
                            <form id="top-login" role="form">
                                <div class="input-group" id="top-login-username">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="email" class="form-control" placeholder="Email address" required="">
                                </div>
                                <div class="input-group" id="top-login-password">
                                    <span class="input-group-addon"><i class="icon-key"></i></span>
                                    <input type="password" class="form-control" placeholder="Password" required="">
                                </div>
                                <label class="checkbox">
                                    <input type="checkbox" value="remember-me"> Remember me
                                </label>
                                <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div><!-- .top-links end -->

        </div>

        <div id="header-wrap">

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="style-2">

                <div class="container-fluid clearfix" style="text-align: center; padding: 0 30px;">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <ul>
                        @foreach($menuList as $menuItem)
                            <li class="@if($menuItem->name == 'Home') current @endif mega-menu" style="text-align: left"><a href="index.html"><div>{{$menuItem->name}}</div></a>
                                <?php
                                    $productCategories = ProductCategory::where('menu_item_id', $menuItem->id)->where('status', 1)->orderBy('list_order', 'ASC')->where('sub_item', null)->get();
                                ?>
                                @if(sizeof($productCategories) > 0)
                                    <div class="mega-menu-content style-2 clearfix row">
                                        @foreach($productCategories as $productCategory)
                                            <?php
                                                $productSubCategories = ProductCategory::where('sub_item', $productCategory->id)->get();
                                            ?>
                                            @if(sizeof($productSubCategories) > 0)
                                                <ul class="mega-menu-column col-md-3" style="padding: 5px; height: 220px; overflow-y: auto;">
                                                    <li class="mega-menu-title" style="text-align: left; padding: 10px;"><a href="#"><div>{{ $productCategory->name }}</div></a>
                                                        <ul>
                                                            @foreach($productSubCategories as $productSubCategory)
                                                                <li style="text-align: left"><a href="about.html"><div>{{ $productSubCategory->name }}</div></a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                </ul>
                                            @else
                                                <ul class="mega-menu-column col-md-3" style="padding: 0px 20px !important;">
                                                    <li style="text-align: left"><a href="about.html" style="font-weight: 600; color: #3961c1;"><div>{{ $productCategory->name }}</div></a></li>
                                                </ul>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>

                    <!-- Top Cart
                    ============================================= -->
                    <div id="top-cart">
                        <a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>5</span></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Shopping Cart</h4>
                            </div>
                            <div class="top-cart-items">
                                <div class="top-cart-item clearfix">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="images/shop/small/1.jpg" alt="Blue Round-Neck Tshirt" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <a href="#">Blue Round-Neck Tshirt</a>
                                        <span class="top-cart-item-price">$19.99</span>
                                        <span class="top-cart-item-quantity">x 2</span>
                                    </div>
                                </div>
                                <div class="top-cart-item clearfix">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="images/shop/small/6.jpg" alt="Light Blue Denim Dress" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <a href="#">Light Blue Denim Dress</a>
                                        <span class="top-cart-item-price">$24.99</span>
                                        <span class="top-cart-item-quantity">x 3</span>
                                    </div>
                                </div>
                            </div>
                            <div class="top-cart-action clearfix">
                                <span class="fleft top-checkout-price">$114.95</span>
                                <button class="button button-3d button-small nomargin fright">View Cart</button>
                            </div>
                        </div>
                    </div><!-- #top-cart end -->

                    <!-- Top Search
                    ============================================= -->
                    <div id="top-search">
                        <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                        <form action="search.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                        </form>
                    </div><!-- #top-search end -->

                </div>

            </nav><!-- #primary-menu end -->

        </div>

    </header><!-- #header end -->

    {{--Content--}}
    @yield('content')
    {{--End content--}}

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">
        <div class="container">
            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">
                <div class="col_two_third">
                    <div class="col_one_third">
                        <div class="widget clearfix">
                            <img src="{{URL::To('/')}}/assets/frontend/img/logo.png" alt="" class="footer-logo">
                            <p>We believe in <strong>Simple</strong>, <strong>Creative</strong> &amp; <strong>Flexible</strong> Design Standards.</p>
                            <div style="background: url('{{URL::To('/')}}/assets/frontend/images/world-map.png') no-repeat center center; background-size: 100%;">
                                <address>
                                    <strong>Headquarters:</strong><br>
                                    795 Folsom Ave, Suite 600<br>
                                    San Francisco, CA 94107<br>
                                </address>
                                <abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>
                                <abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>
                                <abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com
                            </div>
                        </div>
                    </div>
                    <div class="col_one_third">
                        <div class="widget widget_links clearfix">
                            <h4>Quick Links</h4>
                            <ul>
                                <li><a href="http://codex.wordpress.org/">Home</a></li>
                                <li><a href="http://wordpress.org/support/forum/requests-and-feedback">Products</a></li>
                                <li><a href="http://wordpress.org/extend/plugins/">About Us</a></li>
                                <li><a href="http://wordpress.org/support/">Our Brands</a></li>
                                <li><a href="http://wordpress.org/extend/themes/">Blog</a></li>
                                <li><a href="http://wordpress.org/news/">Contact</a></li>
                                <li><a href="http://planet.wordpress.org/">Terms of Use / Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col_one_third col_last">
                        <div class="widget clearfix">
                            <h4>Recent Posts</h4>
                            <div id="post-list-footer">
                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                        </div>
                                        <ul class="entry-meta">
                                            <li>10th July 2014</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
                                        </div>
                                        <ul class="entry-meta">
                                            <li>10th July 2014</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="spost clearfix">
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
                                        </div>
                                        <ul class="entry-meta">
                                            <li>10th July 2014</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col_one_third col_last">
                    <div class="widget clearfix" style="margin-bottom: -20px;">
                        <div class="row">
                            <div class="col-md-6 bottommargin-sm">
                                <div class="counter counter-small"><span data-from="50" data-to="15065" data-refresh-interval="50" data-speed="3000" data-comma="true"></span></div>
                                <h5 class="nobottommargin">Total Products</h5>
                            </div>
                            <div class="col-md-6 bottommargin-sm">
                                <div class="counter counter-small"><span data-from="100" data-to="1846" data-refresh-interval="10" data-speed="2000" data-comma="true"></span></div>
                                <h5 class="nobottommargin">Clients</h5>
                            </div>
                        </div>
                    </div>
                    <div class="widget subscribe-widget clearfix">
                        <h5><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
                        <div class="widget-subscribe-form-result"></div>
                        <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                            <div class="input-group divcenter">
                                <span class="input-group-addon"><i class="icon-email2"></i></span>
                                <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
                                <span class="input-group-btn">
										<button class="btn btn-primary" type="submit">Subscribe</button>
									</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- .footer-widgets-wrap end -->
        </div>
        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">
            <div class="container clearfix">
                <div class="col_half">
                    Copyrights &copy; 2014 All Rights Reserved by PlusMedia.<br>
                    <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                </div>
                <div class="col_half col_last tright">
                    <div class="fright clearfix">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-pinterest">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-github">
                            <i class="icon-github"></i>
                            <i class="icon-github"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-yahoo">
                            <i class="icon-yahoo"></i>
                            <i class="icon-yahoo"></i>
                        </a>
                        <a href="#" class="social-icon si-small si-borderless si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div>
                    <div class="clear"></div>
                    <i class="icon-envelope2"></i> info@chemacy.co.nz <span class="middot">&middot;</span> <i class="icon-headphones"></i> +64-09-6541-6369 <span class="middot">&middot;</span> <i class="icon-skype2"></i> ChemacyOnSkype
                </div>
            </div>
        </div><!-- #copyrights end -->
    </footer><!-- #footer end -->
</div><!-- #wrapper end -->
<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
{{HTML::script('assets/backend/js/custom/public/constant.js')}}
{{HTML::script('assets/backend/js/custom/public/public.functions.js')}}
{{HTML::script('assets/frontend/js/jquery.js')}}
{{HTML::script('assets/frontend/js/plugins.js')}}

<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/frontend/js/functions.js')}}

<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
{{HTML::script('assets/frontend/include/rs-plugin/js/jquery.themepunch.tools.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/jquery.themepunch.revolution.min.js')}}

{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.video.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.actions.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.migration.min.js')}}
{{HTML::script('assets/frontend/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js')}}

<script type="text/javascript">

    var tpj=jQuery;
    tpj.noConflict();

    tpj(document).ready(function() {

        var apiRevoSlider = tpj('.tp-banner').show().revolution(
            {
                sliderType:"standard",
                jsFileLocation:"include/rs-plugin/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {},
                responsiveLevels:[1200,992,768,480,320],
                gridwidth:1140,
                gridheight:500,
                lazyType:"none",
                shadow:0,
                spinner:"off",
                autoHeight:"off",
                disableProgressBar:"on",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    disableFocusListener:false,
                },
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    onHoverStop:"off",
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "ares",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '<div class="tp-title-wrap">	<span class="tp-arr-titleholder"></span> </div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    }
                }
            });

        apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
            SEMICOLON.slider.sliderParallaxDimensions();
        });

    }); //ready

</script>
@yield('scripts')

</body>
</html>