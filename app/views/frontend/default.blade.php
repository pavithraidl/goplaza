<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 11/12/17 09:34.
 */
?>

@extends('frontend.master')

@section('content')
    <section id="slider" class="slider-parallax revslider-wrap ohidden clearfix">
        <!--
        #################################
            - THEMEPUNCH BANNER -
        #################################
        -->
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Latest Collections" style="background-color: #F6F6F6;">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                             data-x="150"
                             data-y="160"
                             data-customin="x:-200;y:100;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="400"
                             data-start="1000"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style=""><img src="{{URL::To('/')}}/assets/frontend/img/slider/produts.jpg" alt="Products Image">
                        </div>
                    </li>
                    <!-- SLIDE  -->
                </ul>
            </div>
        </div>
        <!-- END REVOLUTION SLIDER -->
        <div class="section nobottommargin">
            <div class="container clearfix">
                <div class="col_one_fourth nobottommargin">
                    <div class="feature-box fbox-plain fbox-dark fbox-small">
                        <div class="fbox-icon">
                            <i class="icon-thumbs-up2"></i>
                        </div>
                        <h3>100% Original</h3>
                        <p class="notopmargin">We guarantee you the sale of Original Brands.</p>
                    </div>
                </div>
                <div class="col_one_fourth nobottommargin">
                    <div class="feature-box fbox-plain fbox-dark fbox-small">
                        <div class="fbox-icon">
                            <i class="icon-credit-cards"></i>
                        </div>
                        <h3>Payment Options</h3>
                        <p class="notopmargin">We accept Visa, MasterCard and American Express.</p>
                    </div>
                </div>
                <div class="col_one_fourth nobottommargin">
                    <div class="feature-box fbox-plain fbox-dark fbox-small">
                        <div class="fbox-icon">
                            <i class="icon-truck2"></i>
                        </div>
                        <h3>Free Shipping</h3>
                        <p class="notopmargin">Free Delivery to 100+ Locations on orders above $40.</p>
                    </div>
                </div>
                <div class="col_one_fourth nobottommargin col_last">
                    <div class="feature-box fbox-plain fbox-dark fbox-small">
                        <div class="fbox-icon">
                            <i class="icon-undo"></i>
                        </div>
                        <h3>30-Days Returns</h3>
                        <p class="notopmargin">Return or exchange items purchased within 30 days.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">

                <div id="product-grids">
                    {{--JQuery Append--}}
                </div>

                <div class="section notopborder nobottomborder nomargin nopadding nobg footer-stick">
                    <div class="container clearfix">
                        <div class="col_half nobottommargin topmargin">
                            <img src="{{URL::To('/')}}/assets/frontend/images/services/4.jpg" alt="Image"
                                 class="nobottommargin">
                        </div>
                        <div class="col_half subscribe-widget nobottommargin col_last">
                            <div class="heading-block topmargin-lg">
                                <h3><strong>GET 20% OFF*</strong></h3>
                                <span>Our App scales beautifully to different Devices.</span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet cumque, perferendis
                                accusamus porro illo exercitationem molestias.</p>
                            <div class="widget-subscribe-form-result"></div>
                            <form id="widget-subscribe-form3" action="include/subscribe.php" role="form" method="post"
                                  class="nobottommargin">
                                <div class="input-group" style="max-width:400px;">
                                    <span class="input-group-addon"><i class="icon-email2"></i></span>
                                    <input type="email" name="widget-subscribe-form-email"
                                           class="form-control required email" placeholder="Enter your Email">
                                    <span class="input-group-btn">
            <button class="btn btn-danger" type="submit">Subscribe Now</button>
            </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clear bottommargin-lg"></div>
                <div class="clear"></div>
                <div class="fancy-title title-border title-center topmargin-sm">
                    <h4>Popular Brands</h4>
                </div>
                <ul class="clients-grid grid-6 nobottommargin clearfix">
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/1.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/2.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/3.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/4.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/5.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/6.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/7.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/8.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/9.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/10.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/11.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/12.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/13.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/14.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/15.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/16.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/19.png" alt="Clients"></a></li>
                    <li><a href="#"><img src="{{URL::To('/')}}/assets/frontend/images/clients/logo/18.png" alt="Clients"></a></li>
                </ul>
            </div>
        </div>
    </section><!-- #content end -->
@endsection

@section('scripts')
    {{HTML::script('assets/js/website/home-page.js')}}
@endsection
