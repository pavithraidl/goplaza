<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-11-05 21:53:15.
 * View ID - 32
 */
 ?>

@extends('backend.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="title_left">
            <h3>Accounts <small></small></h3>
        </div>
        <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                    <span>Today Total</span>
                    <h2>$21 900</h2>
                    <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                    <span>This Week Total</span>
                    <h2 class="green">$ 289,809</h2>
                    <span class="sparkline_three" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                    <span>This Month Total</span>
                    <h2>1, 839, 083</h2>
                    <span class="sparkline_two" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                    <span>This Year Total</span>
                    <h2>11.74M</h2>
                    <span class="sparkline_four" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashboard_graph x_panel">
                        <div class="row x_title">
                            <div class="col-md-6">
                                <h3>Income <small>Cash Over-Flow</small></h3>
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="demo-container" style="height:250px">
                                <div id="placeholder3xx3" class="demo-placeholder" style="width: 100%; height:250px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Payments Due</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th style="width: 20%">Product Name</th>
                                    <th>Paid/Total</th>
                                    <th>Due Amount</th>
                                    <th>Payment Progress</th>
                                    <th>Product Status</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a style="color: #e4000e;">Devnoport Pools Ltd - Facebook Add Campaign</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        <span style="color: #e4000e;"> $600/$1 200</span>
                                    </td>
                                    <td style="color: #e4000e;">$100 Due 5th Nov/<strong>Overdue!</strong></td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="50"></div>
                                        </div>
                                        <small style="color: #e4000e;">50% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs">On Live</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-dollar"></i> Add Payment </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Big Brown Website</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        $14 000/$23 000
                                    </td>
                                    <td>$9 000 On Review Complete</td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-blue-sky" role="progressbar" data-transitiongoal="57"></div>
                                        </div>
                                        <small>57% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs">Working</button>
                                    </td>
                                    <td>
                                        <a href="{{URL::To('/')}}/projects/project" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-dollar"></i> Add Payment </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <a>Tree Magic Website</a>
                                        <br />
                                        <small>Created 01.01.2015</small>
                                    </td>
                                    <td>
                                        $1 500/$1 500
                                    </td>
                                    <td>Completed</td>
                                    <td class="project_progress">
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100"></div>
                                        </div>
                                        <small>100% Complete</small>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-xs">On Review</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-dollar"></i> Add Payment </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- end project list -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('scripts')
    <!-- bootstrap-progressbar -->
    {{HTML::script('assets/backend/libs/bootstrap-progressbar/bootstrap-progressbar.min.js')}}
    <!-- jQuery Sparklines -->
    {{HTML::script('assets/backend/libs/jquery-sparkline/dist/jquery.sparkline.min.js')}}

<script>
    $(document).ready(function () {
        $('.progress').removeAttr('style');
    });
</script>

    <script>
        $(document).ready(function() {
            //random data
            var d1 = [
                [1, 1339000],
                [2, 1539000],
                [3, 1549000],
                [4, 1349000],
                [5, 1675000],
                [6, 1348000],
                [7, 1483000],
                [8, 1935000],
                [9, 1388000],
                [10, 1379000],
                [11, ],
                [12, ]
            ];

            //flot options
            var options = {
                series: {
                    curvedLines: {
                        apply: true,
                        active: true,
                        monotonicFit: true
                    }
                },
                colors: ["#26B99A"],
                grid: {
                    borderWidth: {
                        top: 0,
                        right: 0,
                        bottom: 1,
                        left: 1
                    },
                    borderColor: {
                        bottom: "#7F8790",
                        left: "#7F8790"
                    }
                }
            };
            var plot = $.plot($("#placeholder3xx3"), [{
                label: "Registrations",
                data: d1,
                lines: {
                    fillColor: "rgba(150, 202, 89, 0.12)"
                }, //#96CA59 rgba(150, 202, 89, 0.42)
                points: {
                    fillColor: "#fff"
                }
            }], options);
        });
    </script>
    <!-- /Flot -->

    <!-- jQuery Sparklines -->
    <script>
        $(document).ready(function() {
            $(".sparkline_one").sparkline([12800, 15100, 18000, 16200, 22400, 17000, 25200, 16800,18000, 22400, 17000, 25200, 16800, 13900, 21900], {
                type: 'bar',
                height: '40',
                barWidth: 9,
                barSpacing: 2,
                barColor: '#26B99A'
            });
            $(".sparkline_three").sparkline([128, 151, 180, 162, 224, 170, 252, 168,180, 224, 170, 252, 168, 139, 289], {
                type: 'bar',
                height: '40',
                barWidth: 9,
                barSpacing: 2,
                barColor: '#26B99A'
            });

            $(".sparkline_two").sparkline([1280, 1510, 1800, 1620, 2240, 1000, 2500, 1800,1000, 2400, 1700, 2500, 1800, 1900, 1839], {
                type: 'line',
                width: '200',
                height: '40',
                lineColor: '#26B99A',
                fillColor: 'rgba(223, 223, 223, 0.57)',
                lineWidth: 2,
                spotColor: '#26B99A',
                minSpotColor: '#26B99A'
            });
            $(".sparkline_four").sparkline([12.80, 15.10, 18.00, 16.20, 22.40, 10.00, 25.00, 18.00,10.00, 24.00, 17.00, 25.00, 18.00, 19.00, 11.74], {
                type: 'line',
                width: '200',
                height: '40',
                lineColor: '#26B99A',
                fillColor: 'rgba(223, 223, 223, 0.57)',
                lineWidth: 2,
                spotColor: '#26B99A',
                minSpotColor: '#26B99A'
            });
        });
    </script>
    <!-- /jQuery Sparklines -->
@endsection
