<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-18 20:53:49.
 * View ID - 51
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/website/style.css')}}
@endsection

@section('content')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-envilope"></i> Email Enquiry</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Enquiry Inbox<small> Enquiries</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div id="enquiry-list-container" class="col-sm-3 mail_list_column" style="max-height: 600px; overflow-y: auto; overflow-x: hidden;">
                                    <p style="text-align: center">
                                        <img src="{{URL::To('/')}}/assets/backend/images/loading/ring-md.svg" />
                                    </p>

                                </div>
                                <!-- /MAIL LIST -->

                                <!-- CONTENT MAIL -->
                                <div class="col-sm-9 mail_view">
                                    <div class="inbox-body">
                                        <div class="mail_heading row">
                                            <div class="col-md-4">
                                                <p id="body-created-at" class="date"> {{--JQuery append--}}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <h4 id="body-subject"> {{--JQuery append--}}</h4>
                                            </div>
                                        </div>
                                        <div class="sender-info">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong id="body-name">{{--JQuery append--}}</strong>
                                                    <span id="body-email">{{--JQuery append--}}</span>
                                                </div>
                                            </div>
                                        </div><br/>
                                        <div class="view-mail" id="body-message">
                                            {{--JQuery append--}}
                                        </div>
                                    </div>


                                    <div class="compose col-md-12 col-xs-12" style="position: relative !important; display: block !important; margin-top: 50px;">
                                        <div class="compose-header">
                                            Reply Enquiry
                                        </div>

                                        <div class="compose-body">
                                            <div id="alerts"></div>

                                            <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                                                <div class="btn-group">
                                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                    </ul>
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a data-edit="fontSize 5">
                                                                <p style="font-size:17px">Huge</p>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a data-edit="fontSize 3">
                                                                <p style="font-size:14px">Normal</p>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a data-edit="fontSize 1">
                                                                <p style="font-size:11px">Small</p>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                                    <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                                    <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                                    <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                                    <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                                    <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                                    <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                                    <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                                    <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                                    <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                                    <div class="dropdown-menu input-append">
                                                        <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                                        <button class="btn" type="button">Add</button>
                                                    </div>
                                                    <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                                                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                                </div>

                                                <div class="btn-group">
                                                    <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                                    <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>

                                            <div id="editor" class="editor-wrapper"></div>
                                        </div>

                                        <div class="compose-footer">
                                            <button id="send" class="btn btn-sm btn-success" type="button">Send</button>
                                        </div>
                                    </div>

                                </div>
                                <!-- /CONTENT MAIL -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/js/website/enquiry.js')}}
@endsection
