<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-11 21:40:38.
 * View ID - 47
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/website/website.css')}}
@endsection

@section('content')
    <div class="right_col" role="main">
        <div id="website-sliders-block-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-film"></i> Sliders and Blocks</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-align-left"></i> Website <small>Sliders and Blocks</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12">
                                @if(Auth::user()->roll < 4)
                                <div id="website-section-add-button-container" class="col-xs-12">
                                    <button id="add-more-sliders-button" onclick="addSlider();" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus"></i> Add Slider/Block</button>
                                </div>
                                @endif
                                <div class="col-md-4">
                                    <div class="col-xs-12 bs-callout bs-callout-primary">
                                        <div class="form-group">
                                            <input id="search-filter" class="form-control" placeholder="Search..." onkeyup="searchFilter(this);" style="border-radius: 10px;"/>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                    $sliders = WebsiteSlider::where('orgid', Auth::user()->orgid)->where('status', '!=', 0)->get();
                                                    $c = 0;
                                                ?>
                                                <ul id="website-slider-container" class="website-section-list search-filter">
                                                    @if(sizeof($sliders) > 0)
                                                        @if(WebsiteSlider::where('orgid', Auth::user()->orgid)->where('id', $sliderId)->where('status', 1)->first())
                                                            @foreach($sliders as $slider)
                                                                <a href="{{URL::To('/')}}/website/slider/{{ $slider->id }}">
                                                                    <li id="section-name-{{ $slider->id }}" data-id="{{ $slider->id }}"
                                                                        @if($sliderId == $slider->id) class="selected" @endif>{{ $slider->name }}<span style="color: #dedede;"> @if(Auth::user()->roll < 4) - {{ $slider->id }} @endif</span> @if($sliderId == $slider->id)<i class="fa fa-chevron-right pull-right"></i>@endif
                                                                    </li>
                                                                </a>
                                                            @endforeach
                                                        @else
                                                            @foreach($sliders as $slider)
                                                                <a href="{{URL::To('/')}}/website/slider/{{ $slider->id }}">
                                                                    <li id="section-name-{{ $slider->id }}" data-id="{{ $slider->id }}"
                                                                        @if($c == 0) class="selected" @endif>{{ $slider->name }}<span style="color: #dedede;"> - {{ $slider->id }} @if($c == 0)<i class="fa fa-chevron-right pull-right"></i>@endif
                                                                    </li>
                                                                </a>
                                                            <?php $c++ ?>
                                                            @endforeach
                                                        @endif
                                                    @else
                                                        <li id="section-name-" class="selected" onclick="selectSlider(0)">First Slider <i class="fa fa-chevron-right pull-right"></i> </li>
                                                        <input type="hidden" id="section-id" value="0" />
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-xs-12 bs-callout bs-callout-info" id="input-section-container">
                                        <?php
                                            $slider = WebsiteSlider::where('orgid', Auth::user()->orgid)->where('id', $sliderId)->where('status', 1)->first();
                                            if(!$slider) {
                                                $slider = WebsiteSlider::where('orgid', Auth::user()->orgid)->where('status', 1)->first();
                                            }

                                            if($slider)
                                                $sliderId = $slider->id;
                                            else
                                                $sliderId = -1
                                        ?>
                                        <!-- Selected Slider id -->
                                        <input type="hidden" id="slider-id" value="@if($slider){{ $slider->id }}@endif" />

                                        <div class="col-xs-12 hidden-xs">
                                            <h4><input id="input-slider-name" type="text" class="input-trans" onkeyup="editTitles(this, 'name');" onchange="saveSlider(this, 'name', 'website_sliders');" value="@if(sizeof($slider) > 0){{ $slider->name }}@endif" placeholder="Type slider/block name here..." style="height: 25px; width: 90%;"/> </h4>
                                            @if(Auth::user()->roll < 4)
                                                <h5 id="section-id" class="pull-right" style="margin-top: -25px;"> @if(sizeof($slider) > 0)ID: {{ $slider->id }}@endif</h5>
                                            @endif
                                        </div>
                                        <div id="second-section" class="slider-second-section" style="opacity: 1;">
                                            <div id="slider-slides" class="carousel slide" data-interval="false">
                                                <!-- Wrapper for slides -->
                                                <div id="slider-slides-container" class="carousel-inner">
                                                    <?php

                                                        $slides = WebsiteSliderSlide::where('sliderid', $sliderId)->where('status', '!=', 0)->get();
                                                        $c = 1;
                                                    ?>
                                                    @if(sizeof($slides) > 0)
                                                        @foreach($slides as $slide)
                                                            {{--Selected Slide id--}}
                                                            @if($c == 1)
                                                                <input type="hidden" id="slide-id" value="{{ $slide->id }}" />
                                                            @endif
                                                            <div class="item @if($c == 1) active @endif">
                                                                @if(Auth::user()->roll < 4)
                                                                    <p style="    text-align: right; margin-bottom: -20px; color: #8a8a8a; font-size: 12px;">SlideID: {{ $slide->id }}</p>
                                                                @endif
                                                                <div id="slide-field-container-{{ $slide->id }}" class="row" style="padding: 20px">
                                                                    <h5 id="slide-number">Slide {{ str_pad($c, 2, "0", STR_PAD_LEFT) }}</h5>
                                                                    <?php
                                                                        $sliderFields = WebsiteSliderSlideField::where('slideid', $slide->id)->where('status', '!=', 0)->get();
                                                                    ?>
                                                                    @foreach($sliderFields as $field)
                                                                    <div class="form-group col-md-12">
                                                                        <label>@if(Auth::user()->roll < 4)<input id="slide-field-label-{{ $field->id }}" onchange="saveContent(this, {{ $field -> id }}, 'field_name');" type="text" class="input-trans" value="{{ $field->field_name }}" placeholder="Type your field name here..." style="width: 200px;" />@else {{ $field->field_name }} @endif </label>
                                                                        @if(Auth::user()->roll < 4)
                                                                            <i class="fa fa-minus-circle"></i>
                                                                        @endif
                                                                        @if($field->type == 1)
                                                                            <input id="slide-field-{{ $field->id }}" onchange="saveContent(this, {{ $field -> id }}, 'content');" value="{{ $field->content }}" type="text" class="form-control" placeholder="Type content here..." />
                                                                        @elseif($field->type == 2)
                                                                            <textarea id="slide-field-{{ $field->id }}" class="form-control" onchange="saveContent(this, {{ $field -> id }}, 'content');" placeholder="Type content here...">{{ $field->content }}</textarea>
                                                                        @elseif($field->type == 3)
                                                                            <div id="slide-field-{{ $field->id }}" data-id="{{ $field->id }}" class="edit">{{ $field->content }}</div>
                                                                        @elseif($field->type == 4)
                                                                            {{--Upload image start--}}
                                                                            <div class="upload-img-container" style="position: relative; width: 85%; margin: 0 auto;" onclick="openUploadFileDialog({{ $field->id }});">
                                                                                <div class="upload-img-overlay" onmouseenter="animateLivIcon({{ $field->id }});">
                                                                                    <h4 style="margin-bottom: 10%">Add/Change Slide Image</h4>
                                                                                    <div id="upload-img-livicon-{{ $field->id }}" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 65px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                                                    <p>Click Here to Upload!<br/><br/><span>{{ $field->notes }}</span></p>
                                                                                </div>
                                                                                <?php
                                                                                    $img = WebsiteSliderSlideField::where('id', $field->id)->pluck('img');
                                                                                if($img == 1) {
                                                                                    $imgSrc = Auth::user()->orgid.'/'.$field->id . ".".$field->img_ext."?timestamp=". (new DateTime())->getTimestamp();
                                                                                }
                                                                                else {
                                                                                    $imgSrc = "default.jpg";
                                                                                }
                                                                                ?>
                                                                                <img id="slider-slide-img-{{ $field->id }}" src="{{URL::To('/')}}/assets/backend/images/website/slide-block/{{ $imgSrc }}" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                                                <div id = "image-upload-progress-{{ $field->id }}" class = "progress" style = "height: 8px;width: 100%;">
                                                                                    <div id = "image-upload-progress-bar-{{ $field->id }}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                                                        <span id="image-upload-sr-only-{{ $field->id }}" class = "sr-only">0% Complete</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <form action="{{URL::Route('website-savefieldimage')}}" method="post" id="upload-image-form-{{ $field->id }}" enctype="multipart/form-data" class="slider-slide-image-upload">
                                                                                <input type="file" name="file" id="image-choose-{{ $field->id }}" onchange="submitImageUploadForm('#upload-image-form-{{ $field->id }}', {{ $field->id }});" accept="image/jpg;capture=camera" style="display: none;">
                                                                                <input type="hidden" name="fieldid" value="{{ $field->id }}" />
                                                                            </form>
                                                                            {{--Upload image end--}}
                                                                        @elseif($field->type == 5)
                                                                            {{--Icon select start--}}
                                                                            <div class="col-md-12" style="margin-bottom: 15px;">
                                                                                <div class="col-md-2">
                                                                                    <div id="selected-icon-container-{{ $field->id }}" class="icon-container">
                                                                                        <i class="fa fa-{{ ($field->content == '' ? 'square-o' : $field->content) }}"></i>
                                                                                    </div>
                                                                                    <input type="hidden" id="selected-icon-{{ $field->id }}" onclick="saveContent(this, {{ $field -> id }}, 'content');" />
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <div id="icon-list-container-{{ $field->id }}" class="col-md-12" style=" max-height: 200px; overflow-y: auto; display: none;">
                                                                                        <section id="web-application">
                                                                                            <h2 class="page-header">Web Application Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">
                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, {{ $field->id }}, 'adjust');" href="#" ><i class="fa fa-adjust"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'anchor');" href="#" ><i class="fa fa-anchor"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'archive');" href="#" ><i class="fa fa-archive"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'area-chart');" href="#" ><i class="fa fa-area-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows');" href="#" ><i class="fa fa-arrows"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows-h');" href="#" ><i class="fa fa-arrows-h"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows-v');" href="#" ><i class="fa fa-arrows-v"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'asterisk');" href="#" ><i class="fa fa-asterisk"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'at');" href="#" ><i class="fa fa-at"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'car');" href="#" ><i class="fa fa-automobile"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ban');" href="#" ><i class="fa fa-ban"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'university');" href="#" ><i class="fa fa-bank"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bar-chart');" href="#" ><i class="fa fa-bar-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bar-chart');" href="#" ><i class="fa fa-bar-chart-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'barcode');" href="#" ><i class="fa fa-barcode"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bars');" href="#" ><i class="fa fa-bars"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'beer');" href="#" ><i class="fa fa-beer"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bell');" href="#" ><i class="fa fa-bell"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bell-o');" href="#" ><i class="fa fa-bell-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bell-slash');" href="#" ><i class="fa fa-bell-slash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bell-slash-o');" href="#" ><i class="fa fa-bell-slash-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bicycle');" href="#" ><i class="fa fa-bicycle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'binoculars');" href="#" ><i class="fa fa-binoculars"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'birthday-cake');" href="#" ><i class="fa fa-birthday-cake"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bolt');" href="#" ><i class="fa fa-bolt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bomb');" href="#" ><i class="fa fa-bomb"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'book');" href="#" ><i class="fa fa-book"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bookmark');" href="#" ><i class="fa fa-bookmark"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bookmark-o');" href="#" ><i class="fa fa-bookmark-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'briefcase');" href="#" ><i class="fa fa-briefcase"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bug');" href="#" ><i class="fa fa-bug"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'building');" href="#" ><i class="fa fa-building"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'building-o');" href="#" ><i class="fa fa-building-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bullhorn');" href="#" ><i class="fa fa-bullhorn"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bullseye');" href="#" ><i class="fa fa-bullseye"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bus');" href="#" ><i class="fa fa-bus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'taxi');" href="#" ><i class="fa fa-cab"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'calculator');" href="#" ><i class="fa fa-calculator"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'calendar');" href="#" ><i class="fa fa-calendar"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'calendar-o');" href="#" ><i class="fa fa-calendar-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'camera');" href="#" ><i class="fa fa-camera"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'camera-retro');" href="#" ><i class="fa fa-camera-retro"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'car');" href="#" ><i class="fa fa-car"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-down');" href="#" ><i class="fa fa-caret-square-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-left');" href="#" ><i class="fa fa-caret-square-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-right');" href="#" ><i class="fa fa-caret-square-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-up');" href="#" ><i class="fa fa-caret-square-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc');" href="#" ><i class="fa fa-cc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'certificate');" href="#" ><i class="fa fa-certificate"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check');" href="#" ><i class="fa fa-check"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check-circle');" href="#" ><i class="fa fa-check-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check-circle-o');" href="#" ><i class="fa fa-check-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check-square');" href="#" ><i class="fa fa-check-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check-square-o');" href="#" ><i class="fa fa-check-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'child');" href="#" ><i class="fa fa-child"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle');" href="#" ><i class="fa fa-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle-o');" href="#" ><i class="fa fa-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle-o-notch');" href="#" ><i class="fa fa-circle-o-notch"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle-thin');" href="#" ><i class="fa fa-circle-thin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'clock-o');" href="#" ><i class="fa fa-clock-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'times');" href="#" ><i class="fa fa-close"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cloud');" href="#" ><i class="fa fa-cloud"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cloud-download');" href="#" ><i class="fa fa-cloud-download"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cloud-upload');" href="#" ><i class="fa fa-cloud-upload"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'code');" href="#" ><i class="fa fa-code"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'code-fork');" href="#" ><i class="fa fa-code-fork"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'coffee');" href="#" ><i class="fa fa-coffee"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cog');" href="#" ><i class="fa fa-cog"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cogs');" href="#" ><i class="fa fa-cogs"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'comment');" href="#" ><i class="fa fa-comment"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'comment-o');" href="#" ><i class="fa fa-comment-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'comments');" href="#" ><i class="fa fa-comments"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'comments-o');" href="#" ><i class="fa fa-comments-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'compass');" href="#" ><i class="fa fa-compass"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'copyright');" href="#" ><i class="fa fa-copyright"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'credit-card');" href="#" ><i class="fa fa-credit-card"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'crop');" href="#" ><i class="fa fa-crop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'crosshairs');" href="#" ><i class="fa fa-crosshairs"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cube');" href="#" ><i class="fa fa-cube"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cubes');" href="#" ><i class="fa fa-cubes"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cutlery');" href="#" ><i class="fa fa-cutlery"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tachometer');" href="#" ><i class="fa fa-dashboard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'database');" href="#" ><i class="fa fa-database"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'desktop');" href="#" ><i class="fa fa-desktop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'dot-circle-o');" href="#" ><i class="fa fa-dot-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'download');" href="#" ><i class="fa fa-download"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pencil-square-o');" href="#" ><i class="fa fa-edit"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ellipsis-h');" href="#" ><i class="fa fa-ellipsis-h"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ellipsis-v');" href="#" ><i class="fa fa-ellipsis-v"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'envelope');" href="#" ><i class="fa fa-envelope"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'envelope-o');" href="#" ><i class="fa fa-envelope-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'envelope-square');" href="#" ><i class="fa fa-envelope-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eraser');" href="#" ><i class="fa fa-eraser"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'exchange');" href="#" ><i class="fa fa-exchange"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'exclamation');" href="#" ><i class="fa fa-exclamation"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'exclamation-circle');" href="#" ><i class="fa fa-exclamation-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'exclamation-triangle');" href="#" ><i class="fa fa-exclamation-triangle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'external-link');" href="#" ><i class="fa fa-external-link"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'external-link-square');" href="#" ><i class="fa fa-external-link-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eye');" href="#" ><i class="fa fa-eye"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eye-slash');" href="#" ><i class="fa fa-eye-slash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eyedropper');" href="#" ><i class="fa fa-eyedropper"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'fax');" href="#" ><i class="fa fa-fax"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'female');" href="#" ><i class="fa fa-female"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'fighter-jet');" href="#" ><i class="fa fa-fighter-jet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-archive-o');" href="#" ><i class="fa fa-file-archive-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-audio-o');" href="#" ><i class="fa fa-file-audio-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-code-o');" href="#" ><i class="fa fa-file-code-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-excel-o');" href="#" ><i class="fa fa-file-excel-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-image-o');" href="#" ><i class="fa fa-file-image-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-video-o');" href="#" ><i class="fa fa-file-movie-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-pdf-o');" href="#" ><i class="fa fa-file-pdf-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-image-o');" href="#" ><i class="fa fa-file-photo-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-image-o');" href="#" ><i class="fa fa-file-picture-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-powerpoint-o');" href="#" ><i class="fa fa-file-powerpoint-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-audio-o');" href="#" ><i class="fa fa-file-sound-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-video-o');" href="#" ><i class="fa fa-file-video-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-word-o');" href="#" ><i class="fa fa-file-word-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-archive-o');" href="#" ><i class="fa fa-file-zip-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'film');" href="#" ><i class="fa fa-film"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'filter');" href="#" ><i class="fa fa-filter"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'fire');" href="#" ><i class="fa fa-fire"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'fire-extinguisher');" href="#" ><i class="fa fa-fire-extinguisher"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'flag');" href="#" ><i class="fa fa-flag"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'flag-checkered');" href="#" ><i class="fa fa-flag-checkered"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'flag-o');" href="#" ><i class="fa fa-flag-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bolt');" href="#" ><i class="fa fa-flash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'flask');" href="#" ><i class="fa fa-flask"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'folder');" href="#" ><i class="fa fa-folder"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'folder-o');" href="#" ><i class="fa fa-folder-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'folder-open');" href="#" ><i class="fa fa-folder-open"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'folder-open-o');" href="#" ><i class="fa fa-folder-open-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'frown-o');" href="#" ><i class="fa fa-frown-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'futbol-o');" href="#" ><i class="fa fa-futbol-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'gamepad');" href="#" ><i class="fa fa-gamepad"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'gavel');" href="#" ><i class="fa fa-gavel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cog');" href="#" ><i class="fa fa-gear"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cogs');" href="#" ><i class="fa fa-gears"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'gift');" href="#" ><i class="fa fa-gift"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'glass');" href="#" ><i class="fa fa-glass"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'globe');" href="#" ><i class="fa fa-globe"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'graduation-cap');" href="#" ><i class="fa fa-graduation-cap"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'users');" href="#" ><i class="fa fa-group"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hdd-o');" href="#" ><i class="fa fa-hdd-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'headphones');" href="#" ><i class="fa fa-headphones"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'heart');" href="#" ><i class="fa fa-heart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'heart-o');" href="#" ><i class="fa fa-heart-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'history');" href="#" ><i class="fa fa-history"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'home');" href="#" ><i class="fa fa-home"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'picture-o');" href="#" ><i class="fa fa-image"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'inbox');" href="#" ><i class="fa fa-inbox"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'info');" href="#" ><i class="fa fa-info"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'info-circle');" href="#" ><i class="fa fa-info-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'university');" href="#" ><i class="fa fa-institution"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'key');" href="#" ><i class="fa fa-key"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'keyboard-o');" href="#" ><i class="fa fa-keyboard-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'language');" href="#" ><i class="fa fa-language"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'laptop');" href="#" ><i class="fa fa-laptop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'leaf');" href="#" ><i class="fa fa-leaf"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'gavel');" href="#" ><i class="fa fa-legal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'lemon-o');" href="#" ><i class="fa fa-lemon-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'level-down');" href="#" ><i class="fa fa-level-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'level-up');" href="#" ><i class="fa fa-level-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'life-ring');" href="#" ><i class="fa fa-life-bouy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'life-ring');" href="#" ><i class="fa fa-life-buoy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'life-ring');" href="#" ><i class="fa fa-life-ring"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'life-ring');" href="#" ><i class="fa fa-life-saver"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'lightbulb-o');" href="#" ><i class="fa fa-lightbulb-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'line-chart');" href="#" ><i class="fa fa-line-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'location-arrow');" href="#" ><i class="fa fa-location-arrow"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'lock');" href="#" ><i class="fa fa-lock"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'magic');" href="#" ><i class="fa fa-magic"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'magnet');" href="#" ><i class="fa fa-magnet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share');" href="#" ><i class="fa fa-mail-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'reply');" href="#" ><i class="fa fa-mail-reply"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'reply-all');" href="#" ><i class="fa fa-mail-reply-all"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'male');" href="#" ><i class="fa fa-male"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'map-marker');" href="#" ><i class="fa fa-map-marker"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'meh-o');" href="#" ><i class="fa fa-meh-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'microphone');" href="#" ><i class="fa fa-microphone"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'microphone-slash');" href="#" ><i class="fa fa-microphone-slash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'minus');" href="#" ><i class="fa fa-minus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'minus-circle');" href="#" ><i class="fa fa-minus-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'minus-square');" href="#" ><i class="fa fa-minus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'minus-square-o');" href="#" ><i class="fa fa-minus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'mobile');" href="#" ><i class="fa fa-mobile"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'mobile');" href="#" ><i class="fa fa-mobile-phone"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'money');" href="#" ><i class="fa fa-money"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'moon-o');" href="#" ><i class="fa fa-moon-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'graduation-cap');" href="#" ><i class="fa fa-mortar-board"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'music');" href="#" ><i class="fa fa-music"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bars');" href="#" ><i class="fa fa-navicon"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'newspaper-o');" href="#" ><i class="fa fa-newspaper-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paint-brush');" href="#" ><i class="fa fa-paint-brush"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paper-plane');" href="#" ><i class="fa fa-paper-plane"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paper-plane-o');" href="#" ><i class="fa fa-paper-plane-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paw');" href="#" ><i class="fa fa-paw"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pencil');" href="#" ><i class="fa fa-pencil"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pencil-square');" href="#" ><i class="fa fa-pencil-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pencil-square-o');" href="#" ><i class="fa fa-pencil-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'phone');" href="#" ><i class="fa fa-phone"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'phone-square');" href="#" ><i class="fa fa-phone-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'picture-o');" href="#" ><i class="fa fa-photo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'picture-o');" href="#" ><i class="fa fa-picture-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pie-chart');" href="#" ><i class="fa fa-pie-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plane');" href="#" ><i class="fa fa-plane"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plug');" href="#" ><i class="fa fa-plug"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus');" href="#" ><i class="fa fa-plus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus-circle');" href="#" ><i class="fa fa-plus-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus-square');" href="#" ><i class="fa fa-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus-square-o');" href="#" ><i class="fa fa-plus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'power-off');" href="#" ><i class="fa fa-power-off"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'print');" href="#" ><i class="fa fa-print"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'puzzle-piece');" href="#" ><i class="fa fa-puzzle-piece"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'qrcode');" href="#" ><i class="fa fa-qrcode"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'question');" href="#" ><i class="fa fa-question"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'question-circle');" href="#" ><i class="fa fa-question-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'quote-left');" href="#" ><i class="fa fa-quote-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'quote-right');" href="#" ><i class="fa fa-quote-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'random');" href="#" ><i class="fa fa-random"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'recycle');" href="#" ><i class="fa fa-recycle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'refresh');" href="#" ><i class="fa fa-refresh"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'times');" href="#" ><i class="fa fa-remove"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bars');" href="#" ><i class="fa fa-reorder"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'reply');" href="#" ><i class="fa fa-reply"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'reply-all');" href="#" ><i class="fa fa-reply-all"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'retweet');" href="#" ><i class="fa fa-retweet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'road');" href="#" ><i class="fa fa-road"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rocket');" href="#" ><i class="fa fa-rocket"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rss');" href="#" ><i class="fa fa-rss"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rss-square');" href="#" ><i class="fa fa-rss-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'search');" href="#" ><i class="fa fa-search"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'search-minus');" href="#" ><i class="fa fa-search-minus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'search-plus');" href="#" ><i class="fa fa-search-plus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paper-plane');" href="#" ><i class="fa fa-send"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paper-plane-o');" href="#" ><i class="fa fa-send-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share');" href="#" ><i class="fa fa-share"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share-alt');" href="#" ><i class="fa fa-share-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share-alt-square');" href="#" ><i class="fa fa-share-alt-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share-square');" href="#" ><i class="fa fa-share-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share-square-o');" href="#" ><i class="fa fa-share-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'shield');" href="#" ><i class="fa fa-shield"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'shopping-cart');" href="#" ><i class="fa fa-shopping-cart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sign-in');" href="#" ><i class="fa fa-sign-in"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sign-out');" href="#" ><i class="fa fa-sign-out"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'signal');" href="#" ><i class="fa fa-signal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sitemap');" href="#" ><i class="fa fa-sitemap"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sliders');" href="#" ><i class="fa fa-sliders"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'smile-o');" href="#" ><i class="fa fa-smile-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'futbol-o');" href="#" ><i class="fa fa-soccer-ball-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort');" href="#" ><i class="fa fa-sort"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-alpha-asc');" href="#" ><i class="fa fa-sort-alpha-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-alpha-desc');" href="#" ><i class="fa fa-sort-alpha-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-amount-asc');" href="#" ><i class="fa fa-sort-amount-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-amount-desc');" href="#" ><i class="fa fa-sort-amount-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-asc');" href="#" ><i class="fa fa-sort-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-desc');" href="#" ><i class="fa fa-sort-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-desc');" href="#" ><i class="fa fa-sort-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-numeric-asc');" href="#" ><i class="fa fa-sort-numeric-asc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-numeric-desc');" href="#" ><i class="fa fa-sort-numeric-desc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort-asc');" href="#" ><i class="fa fa-sort-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'space-shuttle');" href="#" ><i class="fa fa-space-shuttle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'spinner');" href="#" ><i class="fa fa-spinner"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'spoon');" href="#" ><i class="fa fa-spoon"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'square');" href="#" ><i class="fa fa-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'square-o');" href="#" ><i class="fa fa-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'star');" href="#" ><i class="fa fa-star"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'star-half');" href="#" ><i class="fa fa-star-half"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'star-half-o');" href="#" ><i class="fa fa-star-half-empty"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'star-half-o');" href="#" ><i class="fa fa-star-half-full"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'star-half-o');" href="#" ><i class="fa fa-star-half-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'star-o');" href="#" ><i class="fa fa-star-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'suitcase');" href="#" ><i class="fa fa-suitcase"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sun-o');" href="#" ><i class="fa fa-sun-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'life-ring');" href="#" ><i class="fa fa-support"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tablet');" href="#" ><i class="fa fa-tablet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tachometer');" href="#" ><i class="fa fa-tachometer"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tag');" href="#" ><i class="fa fa-tag"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tags');" href="#" ><i class="fa fa-tags"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tasks');" href="#" ><i class="fa fa-tasks"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'taxi');" href="#" ><i class="fa fa-taxi"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'terminal');" href="#" ><i class="fa fa-terminal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'thumb-tack');" href="#" ><i class="fa fa-thumb-tack"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'thumbs-down');" href="#" ><i class="fa fa-thumbs-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'thumbs-o-down');" href="#" ><i class="fa fa-thumbs-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'thumbs-o-up');" href="#" ><i class="fa fa-thumbs-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'thumbs-up');" href="#" ><i class="fa fa-thumbs-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ticket');" href="#" ><i class="fa fa-ticket"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'times');" href="#" ><i class="fa fa-times"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'times-circle');" href="#" ><i class="fa fa-times-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'times-circle-o');" href="#" ><i class="fa fa-times-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tint');" href="#" ><i class="fa fa-tint"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-down');" href="#" ><i class="fa fa-toggle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-left');" href="#" ><i class="fa fa-toggle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'toggle-off');" href="#" ><i class="fa fa-toggle-off"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'toggle-on');" href="#" ><i class="fa fa-toggle-on"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-right');" href="#" ><i class="fa fa-toggle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-up');" href="#" ><i class="fa fa-toggle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'trash');" href="#" ><i class="fa fa-trash"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'trash-o');" href="#" ><i class="fa fa-trash-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tree');" href="#" ><i class="fa fa-tree"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'trophy');" href="#" ><i class="fa fa-trophy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'truck');" href="#" ><i class="fa fa-truck"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tty');" href="#" ><i class="fa fa-tty"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'umbrella');" href="#" ><i class="fa fa-umbrella"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'university');" href="#" ><i class="fa fa-university"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'unlock');" href="#" ><i class="fa fa-unlock"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'unlock-alt');" href="#" ><i class="fa fa-unlock-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'sort');" href="#" ><i class="fa fa-unsorted"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'upload');" href="#" ><i class="fa fa-upload"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'user');" href="#" ><i class="fa fa-user"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'users');" href="#" ><i class="fa fa-users"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'video-camera');" href="#" ><i class="fa fa-video-camera"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'volume-down');" href="#" ><i class="fa fa-volume-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'volume-off');" href="#" ><i class="fa fa-volume-off"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'volume-up');" href="#" ><i class="fa fa-volume-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'exclamation-triangle');" href="#" ><i class="fa fa-warning"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'wheelchair');" href="#" ><i class="fa fa-wheelchair"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'wifi');" href="#" ><i class="fa fa-wifi"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'wrench');" href="#" ><i class="fa fa-wrench"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="file-type">
                                                                                            <h2 class="page-header">File Type Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file');" href="#" ><i class="fa fa-file"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-archive-o');" href="#" ><i class="fa fa-file-archive-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-audio-o');" href="#" ><i class="fa fa-file-audio-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-code-o');" href="#" ><i class="fa fa-file-code-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-excel-o');" href="#" ><i class="fa fa-file-excel-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-image-o');" href="#" ><i class="fa fa-file-image-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-video-o');" href="#" ><i class="fa fa-file-movie-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-o');" href="#" ><i class="fa fa-file-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-pdf-o');" href="#" ><i class="fa fa-file-pdf-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-image-o');" href="#" ><i class="fa fa-file-photo-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-image-o');" href="#" ><i class="fa fa-file-picture-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-powerpoint-o');" href="#" ><i class="fa fa-file-powerpoint-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-audio-o');" href="#" ><i class="fa fa-file-sound-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-text');" href="#" ><i class="fa fa-file-text"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-text-o');" href="#" ><i class="fa fa-file-text-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-video-o');" href="#" ><i class="fa fa-file-video-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-word-o');" href="#" ><i class="fa fa-file-word-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-archive-o');" href="#" ><i class="fa fa-file-zip-o"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="spinner">
                                                                                            <h2 class="page-header">Spinner Icons</h2>

                                                                                            <div class="alert alert-success">
                                                                                                <ul class="fa-ul">
                                                                                                    <li>
                                                                                                        <i class="fa fa-info-circle fa-lg fa-li"></i> These icons work great with the <code>fa-spin</code> class. Check out the
                                                                                                        <a href="../examples/#spinning" class="alert-link">spinning icons example</a>.
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle-o-notch');" href="#" ><i class="fa fa-circle-o-notch"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cog');" href="#" ><i class="fa fa-cog"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cog');" href="#" ><i class="fa fa-gear"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'refresh');" href="#" ><i class="fa fa-refresh"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'spinner');" href="#" ><i class="fa fa-spinner"></i></a>
                                                                                                </div>

                                                                                            </div>
                                                                                        </section>

                                                                                        <section id="form-control">
                                                                                            <h2 class="page-header">Form Control Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check-square');" href="#" ><i class="fa fa-check-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'check-square-o');" href="#" ><i class="fa fa-check-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle');" href="#" ><i class="fa fa-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'circle-o');" href="#" ><i class="fa fa-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'dot-circle-o');" href="#" ><i class="fa fa-dot-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'minus-square');" href="#" ><i class="fa fa-minus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'minus-square-o');" href="#" ><i class="fa fa-minus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus-square');" href="#" ><i class="fa fa-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus-square-o');" href="#" ><i class="fa fa-plus-square-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'square');" href="#" ><i class="fa fa-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'square-o');" href="#" ><i class="fa fa-square-o"></i></a>
                                                                                                </div>

                                                                                            </div>
                                                                                        </section>

                                                                                        <section id="payment">
                                                                                            <h2 class="page-header">Payment Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-amex');" href="#" ><i class="fa fa-cc-amex"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-discover');" href="#" ><i class="fa fa-cc-discover"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-mastercard');" href="#" ><i class="fa fa-cc-mastercard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-paypal');" href="#" ><i class="fa fa-cc-paypal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-stripe');" href="#" ><i class="fa fa-cc-stripe"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-visa');" href="#" ><i class="fa fa-cc-visa"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'credit-card');" href="#" ><i class="fa fa-credit-card"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'google-wallet');" href="#" ><i class="fa fa-google-wallet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paypal');" href="#" ><i class="fa fa-paypal"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="chart">
                                                                                            <h2 class="page-header">Chart Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'area-chart');" href="#" ><i class="fa fa-area-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bar-chart');" href="#" ><i class="fa fa-bar-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bar-chart');" href="#" ><i class="fa fa-bar-chart-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'line-chart');" href="#" ><i class="fa fa-line-chart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pie-chart');" href="#" ><i class="fa fa-pie-chart"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="currency">
                                                                                            <h2 class="page-header">Currency Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'btc');" href="#" ><i class="fa fa-bitcoin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'btc');" href="#" ><i class="fa fa-btc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'jpy');" href="#" ><i class="fa fa-cny"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'usd');" href="#" ><i class="fa fa-dollar"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eur');" href="#" ><i class="fa fa-eur"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eur');" href="#" ><i class="fa fa-euro"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'gbp');" href="#" ><i class="fa fa-gbp"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ils');" href="#" ><i class="fa fa-ils"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'inr');" href="#" ><i class="fa fa-inr"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'jpy');" href="#" ><i class="fa fa-jpy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'krw');" href="#" ><i class="fa fa-krw"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'money');" href="#" ><i class="fa fa-money"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'jpy');" href="#" ><i class="fa fa-rmb"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rub');" href="#" ><i class="fa fa-rouble"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rub');" href="#" ><i class="fa fa-rub"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rub');" href="#" ><i class="fa fa-ruble"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'inr');" href="#" ><i class="fa fa-rupee"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ils');" href="#" ><i class="fa fa-shekel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ils');" href="#" ><i class="fa fa-sheqel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'try');" href="#" ><i class="fa fa-try"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'try');" href="#" ><i class="fa fa-turkish-lira"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'usd');" href="#" ><i class="fa fa-usd"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'krw');" href="#" ><i class="fa fa-won"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'jpy');" href="#" ><i class="fa fa-yen"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="text-editor">
                                                                                            <h2 class="page-header">Text Editor Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'align-center');" href="#" ><i class="fa fa-align-center"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'align-justify');" href="#" ><i class="fa fa-align-justify"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'align-left');" href="#" ><i class="fa fa-align-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'align-right');" href="#" ><i class="fa fa-align-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bold');" href="#" ><i class="fa fa-bold"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'link');" href="#" ><i class="fa fa-chain"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chain-broken');" href="#" ><i class="fa fa-chain-broken"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'clipboard');" href="#" ><i class="fa fa-clipboard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'columns');" href="#" ><i class="fa fa-columns"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'files-o');" href="#" ><i class="fa fa-copy"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'scissors');" href="#" ><i class="fa fa-cut"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'outdent');" href="#" ><i class="fa fa-dedent"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eraser');" href="#" ><i class="fa fa-eraser"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file');" href="#" ><i class="fa fa-file"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-o');" href="#" ><i class="fa fa-file-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-text');" href="#" ><i class="fa fa-file-text"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'file-text-o');" href="#" ><i class="fa fa-file-text-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'files-o');" href="#" ><i class="fa fa-files-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'floppy-o');" href="#" ><i class="fa fa-floppy-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'font');" href="#" ><i class="fa fa-font"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'header');" href="#" ><i class="fa fa-header"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'indent');" href="#" ><i class="fa fa-indent"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'italic');" href="#" ><i class="fa fa-italic"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'link');" href="#" ><i class="fa fa-link"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'list');" href="#" ><i class="fa fa-list"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'list-alt');" href="#" ><i class="fa fa-list-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'list-ol');" href="#" ><i class="fa fa-list-ol"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'list-ul');" href="#" ><i class="fa fa-list-ul"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'outdent');" href="#" ><i class="fa fa-outdent"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paperclip');" href="#" ><i class="fa fa-paperclip"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paragraph');" href="#" ><i class="fa fa-paragraph"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'clipboard');" href="#" ><i class="fa fa-paste"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'repeat');" href="#" ><i class="fa fa-repeat"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'undo');" href="#" ><i class="fa fa-rotate-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'repeat');" href="#" ><i class="fa fa-rotate-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'floppy-o');" href="#" ><i class="fa fa-save"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'scissors');" href="#" ><i class="fa fa-scissors"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'strikethrough');" href="#" ><i class="fa fa-strikethrough"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'subscript');" href="#" ><i class="fa fa-subscript"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'superscript');" href="#" ><i class="fa fa-superscript"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'table');" href="#" ><i class="fa fa-table"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'text-height');" href="#" ><i class="fa fa-text-height"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'text-width');" href="#" ><i class="fa fa-text-width"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'th');" href="#" ><i class="fa fa-th"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'th-large');" href="#" ><i class="fa fa-th-large"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'th-list');" href="#" ><i class="fa fa-th-list"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'underline');" href="#" ><i class="fa fa-underline"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'undo');" href="#" ><i class="fa fa-undo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chain-broken');" href="#" ><i class="fa fa-unlink"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="directional">
                                                                                            <h2 class="page-header">Directional Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-double-down');" href="#" ><i class="fa fa-angle-double-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-double-left');" href="#" ><i class="fa fa-angle-double-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-double-right');" href="#" ><i class="fa fa-angle-double-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-double-up');" href="#" ><i class="fa fa-angle-double-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-down');" href="#" ><i class="fa fa-angle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-left');" href="#" ><i class="fa fa-angle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-right');" href="#" ><i class="fa fa-angle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angle-up');" href="#" ><i class="fa fa-angle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-down');" href="#" ><i class="fa fa-arrow-circle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-left');" href="#" ><i class="fa fa-arrow-circle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-o-down');" href="#" ><i class="fa fa-arrow-circle-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-o-left');" href="#" ><i class="fa fa-arrow-circle-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-o-right');" href="#" ><i class="fa fa-arrow-circle-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-o-up');" href="#" ><i class="fa fa-arrow-circle-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-right');" href="#" ><i class="fa fa-arrow-circle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-circle-up');" href="#" ><i class="fa fa-arrow-circle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-down');" href="#" ><i class="fa fa-arrow-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-left');" href="#" ><i class="fa fa-arrow-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-right');" href="#" ><i class="fa fa-arrow-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrow-up');" href="#" ><i class="fa fa-arrow-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows');" href="#" ><i class="fa fa-arrows"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows-alt');" href="#" ><i class="fa fa-arrows-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows-h');" href="#" ><i class="fa fa-arrows-h"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows-v');" href="#" ><i class="fa fa-arrows-v"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-down');" href="#" ><i class="fa fa-caret-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-left');" href="#" ><i class="fa fa-caret-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-right');" href="#" ><i class="fa fa-caret-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-down');" href="#" ><i class="fa fa-caret-square-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-left');" href="#" ><i class="fa fa-caret-square-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-right');" href="#" ><i class="fa fa-caret-square-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-up');" href="#" ><i class="fa fa-caret-square-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-up');" href="#" ><i class="fa fa-caret-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-circle-down');" href="#" ><i class="fa fa-chevron-circle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-circle-left');" href="#" ><i class="fa fa-chevron-circle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-circle-right');" href="#" ><i class="fa fa-chevron-circle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-circle-up');" href="#" ><i class="fa fa-chevron-circle-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-down');" href="#" ><i class="fa fa-chevron-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-left');" href="#" ><i class="fa fa-chevron-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-right');" href="#" ><i class="fa fa-chevron-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'chevron-up');" href="#" ><i class="fa fa-chevron-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hand-o-down');" href="#" ><i class="fa fa-hand-o-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hand-o-left');" href="#" ><i class="fa fa-hand-o-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hand-o-right');" href="#" ><i class="fa fa-hand-o-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hand-o-up');" href="#" ><i class="fa fa-hand-o-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'long-arrow-down');" href="#" ><i class="fa fa-long-arrow-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'long-arrow-left');" href="#" ><i class="fa fa-long-arrow-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'long-arrow-right');" href="#" ><i class="fa fa-long-arrow-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'long-arrow-up');" href="#" ><i class="fa fa-long-arrow-up"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-down');" href="#" ><i class="fa fa-toggle-down"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-left');" href="#" ><i class="fa fa-toggle-left"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-right');" href="#" ><i class="fa fa-toggle-right"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'caret-square-o-up');" href="#" ><i class="fa fa-toggle-up"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="video-player">
                                                                                            <h2 class="page-header">Video Player Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'arrows-alt');" href="#" ><i class="fa fa-arrows-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'backward');" href="#" ><i class="fa fa-backward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'compress');" href="#" ><i class="fa fa-compress"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'eject');" href="#" ><i class="fa fa-eject"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'expand');" href="#" ><i class="fa fa-expand"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'fast-backward');" href="#" ><i class="fa fa-fast-backward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'fast-forward');" href="#" ><i class="fa fa-fast-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'forward');" href="#" ><i class="fa fa-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pause');" href="#" ><i class="fa fa-pause"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'play');" href="#" ><i class="fa fa-play"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'play-circle');" href="#" ><i class="fa fa-play-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'play-circle-o');" href="#" ><i class="fa fa-play-circle-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'step-backward');" href="#" ><i class="fa fa-step-backward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'step-forward');" href="#" ><i class="fa fa-step-forward"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'stop');" href="#" ><i class="fa fa-stop"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'youtube-play');" href="#" ><i class="fa fa-youtube-play"></i></a>
                                                                                                </div>

                                                                                            </div>

                                                                                        </section>

                                                                                        <section id="brand">
                                                                                            <h2 class="page-header">Brand Icons</h2>

                                                                                            <div class="alert alert-success">
                                                                                                <ul class="margin-bottom-none padding-left-lg">
                                                                                                    <li>All brand icons are trademarks of their respective owners.</li>
                                                                                                    <li>The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.</li>
                                                                                                </ul>

                                                                                            </div>

                                                                                            <div class="alert alert-warning">
                                                                                                <h4><i class="fa fa-warning"></i> Warning!</h4> Apparently, Adblock Plus can remove Font Awesome brand icons with their "Remove Social Media Buttons" setting. We will not use hacks to force them to display. Please
                                                                                                <a href="https://adblockplus.org/en/bugs" class="alert-link">report an issue with Adblock Plus</a> if you believe this to be an error. To work around this, you'll need to modify the social icon class names.

                                                                                            </div>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'adn');" href="#" ><i class="fa fa-adn"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'android');" href="#" ><i class="fa fa-android"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'angellist');" href="#" ><i class="fa fa-angellist"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'apple');" href="#" ><i class="fa fa-apple"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'behance');" href="#" ><i class="fa fa-behance"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'behance-square');" href="#" ><i class="fa fa-behance-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bitbucket');" href="#" ><i class="fa fa-bitbucket"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'bitbucket-square');" href="#" ><i class="fa fa-bitbucket-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'btc');" href="#" ><i class="fa fa-bitcoin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'btc');" href="#" ><i class="fa fa-btc"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-amex');" href="#" ><i class="fa fa-cc-amex"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-discover');" href="#" ><i class="fa fa-cc-discover"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-mastercard');" href="#" ><i class="fa fa-cc-mastercard"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-paypal');" href="#" ><i class="fa fa-cc-paypal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-stripe');" href="#" ><i class="fa fa-cc-stripe"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'cc-visa');" href="#" ><i class="fa fa-cc-visa"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'codepen');" href="#" ><i class="fa fa-codepen"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'css3');" href="#" ><i class="fa fa-css3"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'delicious');" href="#" ><i class="fa fa-delicious"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'deviantart');" href="#" ><i class="fa fa-deviantart"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'digg');" href="#" ><i class="fa fa-digg"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'dribbble');" href="#" ><i class="fa fa-dribbble"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'dropbox');" href="#" ><i class="fa fa-dropbox"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'drupal');" href="#" ><i class="fa fa-drupal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'empire');" href="#" ><i class="fa fa-empire"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'facebook');" href="#" ><i class="fa fa-facebook"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'facebook-square');" href="#" ><i class="fa fa-facebook-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'flickr');" href="#" ><i class="fa fa-flickr"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'foursquare');" href="#" ><i class="fa fa-foursquare"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'empire');" href="#" ><i class="fa fa-ge"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'git');" href="#" ><i class="fa fa-git"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'git-square');" href="#" ><i class="fa fa-git-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'github');" href="#" ><i class="fa fa-github"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'github-alt');" href="#" ><i class="fa fa-github-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'github-square');" href="#" ><i class="fa fa-github-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'gittip');" href="#" ><i class="fa fa-gittip"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'google');" href="#" ><i class="fa fa-google"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'google-plus');" href="#" ><i class="fa fa-google-plus"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'google-plus-square');" href="#" ><i class="fa fa-google-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'google-wallet');" href="#" ><i class="fa fa-google-wallet"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hacker-news');" href="#" ><i class="fa fa-hacker-news"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'html5');" href="#" ><i class="fa fa-html5"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'instagram');" href="#" ><i class="fa fa-instagram"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ioxhost');" href="#" ><i class="fa fa-ioxhost"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'joomla');" href="#" ><i class="fa fa-joomla"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'jsfiddle');" href="#" ><i class="fa fa-jsfiddle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'lastfm');" href="#" ><i class="fa fa-lastfm"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'lastfm-square');" href="#" ><i class="fa fa-lastfm-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'linkedin');" href="#" ><i class="fa fa-linkedin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'linkedin-square');" href="#" ><i class="fa fa-linkedin-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'linux');" href="#" ><i class="fa fa-linux"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'maxcdn');" href="#" ><i class="fa fa-maxcdn"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'meanpath');" href="#" ><i class="fa fa-meanpath"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'openid');" href="#" ><i class="fa fa-openid"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pagelines');" href="#" ><i class="fa fa-pagelines"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'paypal');" href="#" ><i class="fa fa-paypal"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pied-piper');" href="#" ><i class="fa fa-pied-piper"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pied-piper-alt');" href="#" ><i class="fa fa-pied-piper-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pinterest');" href="#" ><i class="fa fa-pinterest"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'pinterest-square');" href="#" ><i class="fa fa-pinterest-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'qq');" href="#" ><i class="fa fa-qq"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rebel');" href="#" ><i class="fa fa-ra"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'rebel');" href="#" ><i class="fa fa-rebel"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'reddit');" href="#" ><i class="fa fa-reddit"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'reddit-square');" href="#" ><i class="fa fa-reddit-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'renren');" href="#" ><i class="fa fa-renren"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share-alt');" href="#" ><i class="fa fa-share-alt"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'share-alt-square');" href="#" ><i class="fa fa-share-alt-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'skype');" href="#" ><i class="fa fa-skype"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'slack');" href="#" ><i class="fa fa-slack"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'slideshare');" href="#" ><i class="fa fa-slideshare"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'soundcloud');" href="#" ><i class="fa fa-soundcloud"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'spotify');" href="#" ><i class="fa fa-spotify"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'stack-exchange');" href="#" ><i class="fa fa-stack-exchange"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'stack-overflow');" href="#" ><i class="fa fa-stack-overflow"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'steam');" href="#" ><i class="fa fa-steam"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'steam-square');" href="#" ><i class="fa fa-steam-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'stumbleupon');" href="#" ><i class="fa fa-stumbleupon"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'stumbleupon-circle');" href="#" ><i class="fa fa-stumbleupon-circle"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tencent-weibo');" href="#" ><i class="fa fa-tencent-weibo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'trello');" href="#" ><i class="fa fa-trello"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tumblr');" href="#" ><i class="fa fa-tumblr"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'tumblr-square');" href="#" ><i class="fa fa-tumblr-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'twitch');" href="#" ><i class="fa fa-twitch"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'twitter');" href="#" ><i class="fa fa-twitter"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'twitter-square');" href="#" ><i class="fa fa-twitter-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'vimeo-square');" href="#" ><i class="fa fa-vimeo-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'vine');" href="#" ><i class="fa fa-vine"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'vk');" href="#" ><i class="fa fa-vk"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'weixin');" href="#" ><i class="fa fa-wechat"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'weibo');" href="#" ><i class="fa fa-weibo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'weixin');" href="#" ><i class="fa fa-weixin"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'windows');" href="#" ><i class="fa fa-windows"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'wordpress');" href="#" ><i class="fa fa-wordpress"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'xing');" href="#" ><i class="fa fa-xing"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'xing-square');" href="#" ><i class="fa fa-xing-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'yahoo');" href="#" ><i class="fa fa-yahoo"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'yelp');" href="#" ><i class="fa fa-yelp"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'youtube');" href="#" ><i class="fa fa-youtube"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'youtube-play');" href="#" ><i class="fa fa-youtube-play"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'youtube-square');" href="#" ><i class="fa fa-youtube-square"></i></a>
                                                                                                </div>

                                                                                            </div>
                                                                                        </section>

                                                                                        <section id="medical">
                                                                                            <h2 class="page-header">Medical Icons</h2>

                                                                                            <div class="row fontawesome-icon-list">



                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'ambulance');" href="#" ><i class="fa fa-ambulance"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'h-square');" href="#" ><i class="fa fa-h-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'hospital-o');" href="#" ><i class="fa fa-hospital-o"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'medkit');" href="#" ><i class="fa fa-medkit"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'plus-square');" href="#" ><i class="fa fa-plus-square"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'stethoscope');" href="#" ><i class="fa fa-stethoscope"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'user-md');" href="#" ><i class="fa fa-user-md"></i></a>
                                                                                                </div>

                                                                                                <div class="fa-hover col-md-1 col-sm-2 col-xs-3"><a onclick="pickedIcon({{ $field->id }}, 'wheelchair');" href="#" ><i class="fa fa-wheelchair"></i></a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </section>
                                                                                    </div>
                                                                                    <div class="show-hide-icons col-md-12" onclick="showHideIconContainer({{ $field->id }});">Show Icons <i class="fa fa-angle-double-down"></i> </div>
                                                                                </div>
                                                                            </div>
                                                                            {{--Icon select end--}}
                                                                        @endif
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            <?php $c++ ?>
                                                        @endforeach
                                                    @else
                                                        <input type="hidden" id="slide-id" value="" />
                                                        <div class="item active">
                                                            <div id="slide-field-container-0" class="row" style="padding: 20px">
                                                                {{--JQuery append--}}
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="controls testimonial_control pull-right">
                                                {{--<a id="slider-control-prev" class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#slider-slides" data-slide="prev" @if(sizeof($slides) < 2) style="opacity: 0;" @endif></a>--}}
                                                <a id="slider-control-next" class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#slider-slides" data-slide="next" @if(sizeof($slides) < 2) style="opacity: 0;" @endif></a>
                                            </div>
                                        </div>
                                        @if(Auth::user()->roll < 4)
                                        <div id="control-button-container" class="add-section-button-container" @if(sizeof($sliders) < 1) style="opacity: 0;" @endif>
                                            <p>
                                                <button class="btn btn-sm btn-info" onclick="addEditors(1);"><i class="fa fa-ellipsis-h"></i> Add Text Line</button>
                                                <button class="btn btn-sm btn-info" onclick="addEditors(2);"><i class="fa fa-align-left"></i> Add Text Area</button>
                                                <button class="btn btn-sm btn-info" onclick="addEditors(3);"><i class="fa fa-file-text"></i> Add Rich Text Area</button>
                                                <button class="btn btn-sm btn-info" onclick="addEditors(4);"><i class="fa fa-photo"></i> Add Image</button>
                                                <button class="btn btn-sm btn-info" onclick="addEditors(5);"><i class="fa fa-calendar"></i> Add Icon Picker</button>
                                                <button class="btn btn-sm btn-success pull-right" onclick="addSlide();"><i class="fa fa-film"></i> Add Slide</button>

                                            </p>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/js/website/manage-sliders.js')}}
@endsection