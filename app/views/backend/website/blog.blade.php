<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Isuru ANZPharma
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-03-21 21:29:26.
 * View ID - 53
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/website/style.css')}}
@endsection

@section('content')

    <div class="right_col" role="main">
        <div id="website-sliders-block-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-newspaper-o"></i> Blog</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-align-left"></i> Blog <small>Newsletters</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12">
                                <div id="website-section-add-button-container" class="col-xs-12">
                                    <button id="add-more-sliders-button" onclick="addBlog();" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus"></i> Add Blog/Newsletter</button>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-xs-12 bs-callout bs-callout-primary">
                                        <div class="form-group">
                                            <input id="search-filter" class="form-control" placeholder="Search..." onkeyup="searchFilter(this);" style="border-radius: 10px;"/>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                $blogs = WebsiteBlog::where('orgid', Auth::user()->orgid)->where('status', 1)->get();

                                                if($blogId != 0)
                                                    $blog = WebsiteBlog::where('id', $blogId)->first();
                                                $c = 0;
                                                ?>
                                                <ul id="website-slider-container" class="website-section-list search-filter">
                                                    @if(sizeof($blogs) > 0)
                                                        @foreach($blogs as $blog)
                                                            <a href="{{URL::To('/')}}/website/blog/{{ $blog->id }}">
                                                                <li style="cursor: pointer;" id="section-name-{{ $blog->id }}" data-id="{{ $blog->id }}" @if($blogId == $blog->id || $blogId == 0 && $c == 0) class="selected" @endif><div style="width: 80%; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{ $blog->title }} @if($blogId == $blog->id || $blogId == 0 && $c == 0)</div><i class="fa fa-chevron-right pull-right" style="position: absolute; top: 7px; right: 20px;"></i>@endif</li>
                                                            </a>
                                                            <?php
                                                                if($blogId == 0 && $c == 0) {
                                                                    $blog = WebsiteBlog::where('id', $blog->id)->first();
                                                                    $blogId = $blog->id;
                                                                }
                                                            ?>
                                                        @endforeach
                                                    @else
                                                        <li id="section-name-" class="selected" onclick="selectBlog(0)">First Blog <i class="fa fa-chevron-right pull-right"></i> </li>
                                                        <input type="hidden" id="section-id" value="0" />
                                                    @endif
                                                </ul>
                                                <input type="hidden" id="input-blog-id" value="{{ $blogId }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-xs-12 bs-callout bs-callout-info" id="input-section-container">
                                        <div class="col-md-offset-2 col-md-8 form-group">
                                            {{--Upload image start--}}
                                            <label>Blog Cover Image</label>
                                            <div class="upload-img-container" style="position: relative; width: 85%; margin: 0 auto;" onclick="openUploadFileDialog();">
                                                <div class="upload-img-overlay" onmouseenter="animateLivIcon({{ $blogId }});">
                                                    <h4 style="margin-bottom: 10%">Add/Change Blog Cover Image</h4>
                                                    <div id="upload-img-livicon-{{ $blogId }}" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 65px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                    <p>Click Here to Upload!<br/><br/></p>
                                                </div>
                                                <?php
                                                    $img = WebsiteBlogCoverImg::where('blog_id', $blogId)->first();
                                                    if($img) {
                                                        $imgSrc = Auth::user()->orgid.'/'.$img->id . ".".$img->ext."?timestamp=". (new DateTime())->getTimestamp();
                                                    }
                                                    else {
                                                        $imgSrc = "default.jpg";
                                                    }
                                                ?>
                                                <img id="slider-slide-img-{{ $blogId }}" src="{{URL::To('/')}}/assets/backend/images/website/blog/{{ $imgSrc }}" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                <div id = "image-upload-progress-{{ $blogId }}" class = "progress" style = "height: 8px;width: 100%;">
                                                    <div id = "image-upload-progress-bar-{{ $blogId }}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                        <span id="image-upload-sr-only-{{ $blogId }}" class = "sr-only">0% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <form action="{{URL::Route('website-savefieldimage')}}" method="post" id="upload-image-form-{{ $blogId }}" enctype="multipart/form-data" class="slider-slide-image-upload">
                                                <input type="file" name="file" id="image-choose-{{ $blogId }}" onchange="submitImageUploadForm('#upload-image-form-{{ $blogId }}', {{ $blogId }});" accept="image/jpg;capture=camera" style="display: none;">
                                                <input type="hidden" name="fieldid" value="{{ $blogId }}" />
                                            </form>
                                            {{--Upload image end--}}
                                        </div>
                                        <div class="col-md-12 form-group" style="margin-top: 15px;">
                                            <label for="input-blog-title">Blog Title</label>
                                            <input type="text" class="form-control" id="input-blog-title" value="@if($blog) {{ $blog->title }} @endif" />
                                        </div>
                                        <div class="col-md-12 form-group" style="margin-top: 25px;">
                                            <label for="input-blog-content">Blog Content</label>
                                            <div class="edit" >@if($blog) {{ $blog->content }} @endif</div>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 25px;">
                                            <p style="text-align: center;">
                                                <button class="btn btn-sm btn-success">Save Post</button>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{ HTML::script('assets/js/website/blog.js') }}
@endsection
