<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-12 01:23:03.
 * View ID - 42
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/website/website.css')}}
@endsection

@section('content')

    <?php
        $sections = WebsiteSection::where('orgid', Auth::user()->orgid)->where('status', 1)->get();
        $c = 0;
    ?>

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-globe"></i> Manage</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-align-left"></i> Website <small>Sections</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12">
                                <div id="website-section-add-button-container" class="col-xs-12">
                                    <button id="add-more-section-button" onclick="addSection();" class="btn btn-success btn-sm pull-right" @if(!sizeof($sections) > 0)style="opacity: 0;"@endif><i class="fa fa-plus"></i> Add Section</button>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-xs-12 bs-callout bs-callout-primary">
                                        <div class="form-group">
                                            <input id="search-filter" class="form-control" placeholder="Search..." onkeyup="searchFilter(this);" style="border-radius: 10px;"/>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul id="website-section-container" class="website-section-list search-filter">
                                                    @if(sizeof($sections) > 0)
                                                        @foreach($sections as $section)
                                                            <li id="section-name-{{ $section->id }}" onclick="selectSection({{ $section->id }})" data-id="{{ $section->id }}" @if($c == 0)class="selected"@endif>{{ $section->name }} @if($c == 0)<i class="fa fa-chevron-right pull-right"></i>@endif </li>
                                                            @if($c == 0)
                                                                <input type="hidden" id="selected-id" value="{{ $section->id }}" />
                                                            @endif
                                                            <?php $c++ ?>
                                                        @endforeach
                                                    @else
                                                        <li id="section-name-0" class="selected" onclick="selectSection(0)">First Section <i class="fa fa-chevron-right pull-right"></i> </li>
                                                        <input type="hidden" id="section-id" value="0" />
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-xs-12 bs-callout bs-callout-info" id="input-section-container">
                                        <?php
                                            $section = WebsiteSection::where('orgid', Auth::user()->orgid)->where('status', 1)->first();
                                        ?>
                                        <div class="col-xs-12 hidden-xs">
                                            <h4><input id="input-section-name" type="text" class="input-trans" onkeyup="editTitle(this, 'name');" onchange="postSection(this, 'name', 'website_sections');" value="@if(sizeof($section) > 0){{ $section->name }}@endif" placeholder="Type section name here..." style="height: 25px; width: 90%;"/> </h4>
                                            <h5 id="section-id" class="pull-right" style="margin-top: -25px;"> @if(sizeof($section) > 0)ID: {{ $section->id }}@endif</h5>
                                        </div>
                                        <div id="second-section" @if(!sizeof($section) > 0)style="opacity: 0;"@endif>
                                            <div class="col-xs-12" style="margin-top: 10px;">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input id="input-section-title" onchange="postSection(this, 'title');" value="@if(sizeof($section) > 0){{ $section->title }}@endif" class="form-control" type="text" placeholder="Section Title Type Here..." />
                                                </div>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 30px;">
                                                <label>Content</label>
                                                <div id="input-section-content" class='edit' data-rich="0">@if(sizeof($section) > 0) {{ $section->content }} @endif</div>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 20px;">
                                                <p style="text-align: center"><button id="btn-delete-section" value="0" class="btn btn-danger btn-sm" onclick="postSection(this, 'status');"><i class="fa fa-trash"></i> Delete Section</button> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{ HTML::script('assets/js/website/manage-sections.js') }}
@endsection
