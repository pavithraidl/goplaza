<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/11/17 11:06.
 */
?>
<div id=":1vy" class="ii gt ">
    <div id=":1vx" class="a3s aXjCH m161b5312d2b00ae7"><u></u>
        <div bgcolor="#e4e7ed"
             style="margin:0;padding:0;height:100%;min-width:100%;width:100%;font-size:14px;font-family:'Open Sans',Helvetica,Arial,sans-serif;color:#555555;background-color:#e4e7ed;text-align:left">

            <table class="m_6002514519833902070content-wrapper" bgcolor="#e4e7ed" width="100%" align="center"
                   style="margin:0;width:100%;max-width:100%;min-width:100%;border:0;border-spacing:0;border-collapse:collapse;font-family:'Open Sans',Helvetica,Arial,sans-serif;background-color:#e4e7ed">
                <tbody>
                <tr>
                    <td class="m_6002514519833902070content-wrapper-content" bgcolor="#e4e7ed"
                        style="margin:0;padding:15px 0;background-color:#e4e7ed;border:0">


                        <table class="m_6002514519833902070table-content m_6002514519833902070table-header" width="650"
                               bgcolor="#ffffff" align="center"
                               style="margin:0 auto;padding:0;width:650px;max-width:650px;border:0;border-spacing:0;border-collapse:collapse;background:#ffffff;border-radius:4px 4px 0 0">
                            <tbody>
                            <tr>
                                <td class="m_6002514519833902070hidden-xs" width="15"
                                    style="margin:0;padding:0;width:15px;background:none;border:0;border-bottom:1px solid #e4e7ed"></td>
                                <td class="m_6002514519833902070header" width="620"
                                    style="margin:0;padding:15px 0;width:620px;background:none;border:0;border-bottom:1px solid #e4e7ed">
                                    <a href="{{ $homeUrl }}"
                                       style="font-weight:600;color:#3498da;display:block;height:53px;width:150px;text-decoration:none;border-width:0"
                                       target="_blank">
                                        <img class="m_6002514519833902070image CToWUd" width="150" src="{{ $orgLogo }}"
                                             alt="" title="{{ $orgName }} Logo"
                                             style="border-style:none!important;display:block;height:53px;width:150px;text-decoration:none;border:0">
                                    </a>
                                </td>
                                <td class="m_6002514519833902070hidden-xs" width="15"
                                    style="margin:0;padding:0;width:15px;background:none;border:0;border-bottom:1px solid #e4e7ed"></td>
                            </tr>
                            </tbody>
                        </table>


                        <table class="m_6002514519833902070table-content m_6002514519833902070table-email-confirm"
                               cellspacing="0" border="0" align="center"
                               style="margin:0 auto;padding:0;width:650px;background:#fff;border:0;border-spacing:0;border-radius:0 0 4px 4px"
                               bgcolor="#ffffff" cellpadding="0" width="650">
                            <tbody>
                            <tr>
                                <td colspan="3" height="30"></td>
                            </tr>
                            <tr>
                                <td class="m_6002514519833902070hidden-xs" width="30"></td>
                                <td class="m_6002514519833902070content-590 m_6002514519833902070text-center"
                                    width="590"
                                    style="width:590px;margin:0;padding:0;font-family:'Open Sans',Helvetica,Arial,sans-serif;background:none;border:0;text-align:center">
                                    <h1 style="margin:0;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:28px;font-weight:300;line-height:1.2;text-align:center;color:#555555">{{ $title }}</h1>
                                    <p style="margin:15px 0 0;padding:0;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;font-size:14px;line-height:1.6;text-align:center;color:#555555">{{ $content }}</p>

                                    <p style="margin:30px 0 0;padding:0;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;font-size:14px;line-height:1.6;text-align:center;color:#555555">
                                        Kind Regards,<br>
                                        {{ $senderName }}        </p>
                                </td>
                                <td class="m_6002514519833902070hidden-xs" width="30"></td>
                            </tr>
                            <tr class="m_6002514519833902070row30">
                                <td colspan="3" height="30"></td>
                            </tr>
                            </tbody>
                        </table>


                        <table class="m_6002514519833902070table-content m_6002514519833902070table-footer" width="650"
                               align="center"
                               style="margin:0 auto;width:100%;max-width:650px;border:0;border-spacing:0;border-collapse:collapse;padding:0;background:none;border:0;text-align:center;color:#555555">
                            <tbody>
                            <tr class="m_6002514519833902070row-20 m_6002514519833902070hidden-xs">
                                <td colspan="3" height="20" style="margin:0;padding:0;background:none;border:0"></td>
                            </tr>
                            <tr class="m_6002514519833902070footer-content">
                                <td class="m_6002514519833902070hidden-xs" width="15"
                                    style="margin:0;padding:0;background:none;border:0"></td>
                                <td class="m_6002514519833902070footer-text"
                                    style="margin:0;padding:0;background:none;border:0;text-align:center;color:#555555">
                                    <p class="m_6002514519833902070social-icons"
                                       style="margin:0;padding:0;font-family:Helvetica,Arial,sans-serif,'Open Sans';font-weight:600;font-size:15px;line-height:1.6;text-align:center;color:#555555">
                                        @if($facebook != null)
                                            <a href="{{ $facebook }}"
                                               style="margin:0 5px;display:inline;height:20px;width:20px;font-weight:600;color:#555555;text-decoration:none;border:0"
                                               target="_blank"><img
                                                        src="https://ci5.googleusercontent.com/proxy/3zXwww6TULjotEZy6BxEPqJ0VEYfzNYjMAJeSWkSdBQIXWKF2SOfoAFR2YNBviHOxs771bZK6dKMpKVnhiBIQvxRESPzljDHgQ8=s0-d-e1-ft#http://cdn.eventfinda.co.nz/images/icon-facebook.gif"
                                                        alt="" title="Like {{ $orgName }} on Facebook" height="20"
                                                        width="20"
                                                        style="margin:0 5px;padding:0;display:inline;height:20px;width:20px;text-decoration:none;border:0"
                                                        class="CToWUd"></a>
                                        @endif
                                        @if($twitter != null)
                                            <a href="{{ $twitter }}"
                                               style="margin:0 5px;display:inline;height:20px;width:20px;font-weight:600;color:#555555;text-decoration:none;border:0"
                                               target="_blank"><img
                                                        src="https://ci3.googleusercontent.com/proxy/L37QJRo4kZcimske9e3o3psRusMlaVAYIjcBeR3MEV4P-rKOI4Vx2RCGUs7L8FGLnDrReZIKKguN0HLcPmLVu77vaOmKbnvLkA=s0-d-e1-ft#http://cdn.eventfinda.co.nz/images/icon-twitter.gif"
                                                        alt="" title="Follow Eventfinda on Twitter" height="20"
                                                        width="20"
                                                        style="margin:0 5px;padding:0;display:inline;height:20px;width:20px;text-decoration:none;border:0"
                                                        class="CToWUd"></a>
                                        @endif
                                    </p>
                                    <p style="margin:15px 0 0;padding:0;font-family:'Open Sans',Helvetica,Arial,sans-serif;font-weight:400;font-size:14px;line-height:1.6;text-align:center;color:#555555">
                                        Copyright © 2005–2018 PRISU PRIDE LTD
                                    </p>
                                </td>
                                <td class="m_6002514519833902070hidden-xs" width="15"
                                    style="margin:0;padding:0;background:none;border:0"></td>
                            </tr>
                            <tr class="m_6002514519833902070row-10 m_6002514519833902070hidden-xs">
                                <td colspan="3" height="10"
                                    style="margin:0;padding:0;height:10px;background:none;border:0"></td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
                </tbody>
            </table>
            <div class="yj6qo"></div>
            <div class="adL">
            </div>
        </div>
        <div class="adL">


        </div>
    </div>
</div>


