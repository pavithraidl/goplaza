<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/01/17 07:12.
 */
?>

@extends('backend.external-master')

@section('content')
    <div class="col-md-12">
        <div class="col-middle">
            <div class="text-center text-center">
                <div class="mid_center">
                    <div id="active-confirm" style="display: none;">
                        <h1>Congratulation!</h1><br/>
                        <h2>Your account has successfully activated. You can now log on to the system.</h2><br/>
                        <a href="{{URL::Route('get-login')}}"> <button class="btn btn-lg btn-info">Login</button></a>
                    </div>
                    <div id="active-form">
                        <h1 style="text-align: center"><img src="{{URL::To('/')}}/assets/backend/images/logo.png" style="width: 150px; height: auto;"/> </h1>
                        <h1>WELCOME</h1><br/>
                        <h2>PRISU CRM Activation</h2>
                        <div style="border: 1px solid #d3cfcf; border-radius: 5px; padding: 30px; margin-top: 20px;">
                            <div class = "form-group">
                                <label style="float: left !important;">First Name</label>
                                <input id = "active-fname" value="{{$fName;}}" onblur="requiredValidator(this, 'Please enter your first name.');" onkeyup="requiredValidator(this, 'Please enter your first name.');" type = "text" class = "form-control" placeholder = "Jone" maxlength = "32" required tabindex="1" style="border-radius: 5px;">
                            </div>
                            <div class = "form-group">
                                <label style="float: left !important;">Last Name</label>
                                <input id = "active-lname" value="{{$lName}}" onblur="requiredValidator(this, 'Please enter your last name.');" onkeyup="requiredValidator(this, 'Please enter your last name.');" type = "text" class = "form-control" placeholder = "Doe" maxlength = "32" required tabindex="2" style="border-radius: 5px;">
                            </div>
                            <div class = "form-group">
                                <label style="float: left !important;">Email</label>
                                <input id = "active-email" value="{{$email}}" type = "text" class = "form-control" maxlength = "32" disabled style="border-radius: 5px;">
                            </div>
                            <div class = "form-group">
                                <label style="float: left !important;">Password</label>
                                <input id = "active-password" autocomplete="off" onblur="requiredValidator(this, 'Please enter your password.');" onkeyup="requiredValidator(this, 'Please enter your password.');" type = "password" class = "form-control" placeholder = "******" maxlength = "32" required tabindex="3" style="border-radius: 5px;">
                            </div><div class = "form-group">
                                <label style="float: left !important;">Confirm Password</label>
                                <input id = "active-confirm-password" onblur="requiredValidator(this, 'Please enter the confirm password.'); confirmPassword(this);" onkeyup="requiredValidator(this, 'Please enter the confirm password.');" type = "password" class = "form-control" placeholder = "******" maxlength = "32" required tabindex="4" style="border-radius: 5px;">
                            </div>
                            <input type="hidden" id="active-code" value="{{$code}}" />
                            <p style="text-align: center">
                                <button class="btn btn-lg btn-success" style="margin-top: 20px;" onclick="activate();">Activate</button>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    {{HTML::script('assets/backend/js/custom/public/constant.js')}}
    {{HTML::script('assets/backend/js/custom/public/public.functions.js')}}
    {{HTML::script('assets/backend/js/custom/public/validator.js')}}
    {{HTML::script('assets/backend/js/custom/auth/activate.js')}}
@endsection