<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-12 08:05:46.
 * View ID - 48
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/clients/style.css')}}
@endsection

@section('content')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-folder-open"></i> Manage Clients</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">

            <button class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"></i> Add Client</button>
            
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('/assets/js/clients/manage-clients.js')}}
@endsection
