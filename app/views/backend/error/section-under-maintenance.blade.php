<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/11/17 17:23.
 */
?>
@extends('backend.master')

@section('content')
    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="animate form login_form">
                        <section class="login_content">
                            <form role="form" action="{{ URL::route('post-login') }}" method="post" id="registerForm">
                                <h1>Under Maintenance!</h1>
                                <img src="{{URL::To('/')}}/assets/backend/images/error/under-maintanance.gif" style="width: 200px;" />
                                <h3 style="font-size: 14px;">Apologize about the inconvenience. We won't take long. We will be back soon.</h3>
                                <br />
                                <br/>
                                <div>
                                    <h1><img src="{{URL::To('/')}}/assets/backend/images/logo.png" style="width: 40px; height: auto; margin-top: -25px;" /></h1>
                                    <p style="text-align: center;">©{{date('Y')}} All Rights Reserved. Privacy and Terms</p>
                                </div>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection