<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/10/17 14:13.
 */
?>

<div id="mm-add-new-operation" class="modal fade bs-new-operation-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="width: 75%;">
    <div class="modal-dialog modal-sm" style="width: 50%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New Operation</h4>
            </div>
            <div id="mm-system-view-body-container" class="modal-body" style="max-height: 250px;">
                <div class="col-md-12" style="overflow: auto;max-height: 380px;">
                    <div class="form-horizontal form" style="max-height: 380px; overflow-y: auto;">
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-ope-name">Operation Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-ope-name" placeholder="Ex: Delete user" onblur="requiredValidator(this, 'Please fill the operation name');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-ope-icon">Icon:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-ope-icon" placeholder="manage" onblur="requiredValidator(this, 'Please fill the icon for the operation');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="mm-sys-ope-description">Description:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-ope-description" placeholder="View Description" maxlength="256" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="mm-sys-ope-system-id" value="" />
                    <input type="hidden" id="mm-sys-ope-view-id" value="" />
                </div>
                <div class="modal-footer" style="margin-top: 165px;">
                    <p style="text-align: center">
                        <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                        <button id="btn-as-close" onclick="addOperation();" type="button" class="btn btn-success" data-dismiss="modal" tabindex="13">Add</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

