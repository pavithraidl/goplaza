<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/12/16 12:20.
 */
?>
<div id="mm-add-property-container" class="modal fade bs-exceptions-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 80%;left: 8%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="mm-system-exception-title"></h4>
            </div>
            <div id="mm-system-exception-body-container" class="modal-body" style="max-height: 600px;">
                <div class="col-md-12" style="overflow: auto;max-height: 500px;">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">ID </th>
                                <th class="column-title">Controller </th>
                                <th class="column-title">Function </th>
                                <th class="column-title">user </th>
                                <th class="column-title">IP </th>
                                <th class="column-title">Exception </th>
                                <th class="column-title no-link last"><span class="nobr">Status</span>
                                </th>
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                            </thead>

                            <tbody id="mm-system-exception-table">
                            {{--JQuery append--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 500px;">
                    <button class="btn btn-sm btn-success pull-left" onclick="allExceptionFixed();">All Fixed</button>
                    <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>