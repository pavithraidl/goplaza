<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 1/06/17 14:46.
 */
?>

<div id="mm-add-system" class="modal fade bs-new-system-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 170%;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New System</h4>
            </div>
            <div id="mm-system-exception-body-container" class="modal-body" style="max-height: 300px;">
                <div class="col-md-12" style="overflow: auto;max-height: 300px;">
                    <div class="form-horizontal form" style="max-height: 275px; overflow-y: auto;">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-sys-system-name">System Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-system-name" placeholder="System Name" onblur="requiredValidator(this, 'Please fill the system name');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-sys-system-icon"></span>System Icon:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mm-sys-system-icon" placeholder="fa-icon" onblur="requiredValidator(this, 'Please fill the icon');" maxlength="32" class="form-control col-md-7 col-xs-12" tabindex="2">
                            </div>
                        </div>
                        <div class="form-group margin-top-sm">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-sys-system-list-order">List Order:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mm-sys-system-list-order" maxlength="3" placeholder="1-999" min="1" max="999" value="100" class="form-control col-md-7 col-xs-12" tabindex="6">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mm-sys-system-allow-default">Allow Default:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12" tabindex="7">
                                <select id="mm-sys-system-allow-default" class="form-control col-md-7 col-xs-12">
                                    <option value="0" selected>No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 200px;">
                    <p style="text-align: center">
                        <button id="btn-as-close" type="button" class="btn btn-default" data-dismiss="modal" tabindex="13">Close</button>
                        <button id="btn-as-close" onclick="addSystem();" type="button" class="btn btn-success" data-dismiss="modal" tabindex="13">Add</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
