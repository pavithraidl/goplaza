<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 23/05/17 11:51.
 */
?>

@extends('backend.master')

@section('content')
    @include('backend.system.includes.exceptions')
    @include('backend.system.includes.new-system')
    @include('backend.system.includes.new-controller')
    @include('backend.system.includes.new-function')
    @include('backend.system.includes.new-view')
    @include('backend.system.includes.new-operation')

    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa {{System::where('id', $systemId)->pluck('icon')}}"></i> {{System::where('id',  $systemId)->pluck('name')}}</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> System Administrator <small>Manage System</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="margin-bottom: 60px !important;">
                        <div class="col-md-12">
                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <?php
                                $systemList = System::where('status', '!=', 0)->lists('id');
                                ?>
                                @foreach($systemList as $manageSystemId)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" role="tab" id="heading-{{$manageSystemId}}" data-toggle="collapse" onclick="loadSystemData({{$manageSystemId}});" data-parent="#accordion" href="#collapse{{$manageSystemId}}" aria-expanded="false" aria-controls="collapse{{$manageSystemId}}" style="position: relative;">
                                            <h4 class="panel-title"><i class="fa {{System::where('id', $manageSystemId)->pluck('icon')}}"></i>&nbsp;&nbsp; {{System::where('id', $manageSystemId)->pluck('name')}} <span style="font-size: 12px; color: #c7c7c7;">&nbsp;&nbsp;&nbsp;&nbsp;-{{$manageSystemId}}</span></h4>
                                            <?php
                                            $status = System::where('id', $manageSystemId)->pluck('status');
                                            $exceptionLed = $Systemled = '';
                                            if($status == 1) $Systemled = '-green';
                                            else if($status == 2) $Systemled = '-yellow';
                                            else if($status == 3) $Systemled = '-red r-blink-n';

                                            $exceptions = SystemException::where('systemid', $manageSystemId)->where('status', '>', 1)->max('id');
                                            if($exceptions != null || $exceptions != '') $exceptionLed = '-red r-blink-n';
                                            else $exceptionLed = '-green';

                                            $systemMaintenance = System::where('id', 5)->pluck('visibility');
                                            if($systemMaintenance == 0) {
                                                $Systemled = '-yellow y-blink-n';
                                            }
                                            else {
                                                $viewMaintenance = SystemView::where('systemid', $manageSystemId)->max('maintenance');
                                                if($viewMaintenance == 1) {
                                                    $Systemled = '-yellow y-blink-n';
                                                }
                                            }
                                            ?>
                                            <div id="system-led-status-{{$manageSystemId}}" class="led{{$Systemled}}" style="position: absolute; right: 20px; top: 16px;"></div>
                                            <div id="system-led-exceptions-{{$manageSystemId}}" class="led{{$exceptionLed}}" style="position: absolute; right: 40px; top: 16px;"></div>
                                        </a>
                                        <div id="collapse{{$manageSystemId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$manageSystemId}}">
                                            <div id="panel-body-{{$manageSystemId}}" class="panel-body">
                                                @if($manageSystemId != 5)<input data-id= "system-status-{{$manageSystemId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" @if($status == 1)checked @endif/>@endif
                                                <div class="" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: 40px;">
                                                    <ul id="tab-{{$manageSystemId}}" class="nav nav-tabs bar_tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#tab_info-{{$manageSystemId}}" id="info-tab-{{$manageSystemId}}" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a></li>
                                                        <li role="presentation" class="" onclick="getControllers({{$manageSystemId}});" ><a href="#tab_class-{{$manageSystemId}}" role="tab" id="class-tab-{{$manageSystemId}}" data-toggle="tab" aria-expanded="false"><i class="fa fa-code"></i> Controllers </a></li>
                                                        <li role="presentation" class=""><a href="#tab_view-{{$manageSystemId}}" role="tab" id="view-tab-{{$manageSystemId}}" data-toggle="tab" aria-expanded="false"><i class="fa fa-list-alt"></i> Views </a></li>
                                                        <li role="presentation" class=""><a href="#tab_access-{{$manageSystemId}}" role="tab" id="access-tab-{{$manageSystemId}}" data-toggle="tab" aria-expanded="false"><i class="fa fa-key"></i> Access </a></li>
                                                    </ul>
                                                    <div id="system-tabs-{{$manageSystemId}}" class="tab-content">
                                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_info-{{$manageSystemId}}" aria-labelledby="info-tab-{{$manageSystemId}}" style="padding: 10px;">
                                                            <div class="row top_tiles" style="margin: 10px 0;">
                                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                    <span>Total Visits</span>
                                                                    <h2 id="system-total-visits-{{$manageSystemId}}">null</h2>
                                                                    <span class="sparkline_one" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                    <span>Total Visitors</span>
                                                                    <h2 id="system-total-visitors-{{$manageSystemId}}">null</h2>
                                                                    <span class="sparkline_one" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                    <span>Total Traffic</span>
                                                                    <h2>231</h2>
                                                                    <span class="sparkline_one" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                    <span>Total bandwidth</span>
                                                                    <h2>809</h2>
                                                                    <span class="sparkline_two" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-12 bs-callout bs-callout-danger">
                                                                    <h4>System Exceptions</h4>
                                                                    <div class="form-horizontal form">
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Exception Ratio:</label>
                                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                                <progress max="100" value="0" id="system-exception-ratio-progress-{{$manageSystemId}}"></progress>
                                                                                <span style="  position: absolute; top: 10px; left: 45%; color: #91acff; font-weight: 700;" id="system-exception-ratio-text-{{$manageSystemId}}">null%</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Running Exception:</label>
                                                                            <div id="system-runing-ex-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 8px; font-size: 16px; color: #fd132f;">
                                                                                0
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Pending Exception:</label>
                                                                            <div id="system-pending-ex-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #e49a48;">
                                                                                0
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Fixed Bugs:</label>
                                                                            <div id="system-fixed-ex-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #5cd07e;">
                                                                                0
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <p style="text-align: right">
                                                                        <button class="btn btn-danger" data-toggle="modal" data-target=".bs-exceptions-modal-lg" onclick="openExceptions({{$manageSystemId}});">Exceptions</button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="col-md-12 bs-callout bs-callout-info">
                                                                    <h4>System Info</h4>
                                                                    <div class="form-horizontal form">
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">ID:</label>
                                                                            <div id="system-id-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                {{$manageSystemId}}
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Route:</label>
                                                                            <div id="system-route-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                null
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">List Order:</label>
                                                                            <div id="system-list-order-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                null
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Controllers:</label>
                                                                            <div id="system-controllers-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                null
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Functions:</label>
                                                                            <div id="system-functions-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                null
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created By:</label>
                                                                            <div id="system-created-by-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                null
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Created At:</label>
                                                                            <div id="system-created-at-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                                                                null
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane fade" id="tab_class-{{$manageSystemId}}" aria-labelledby="class-tab-{{$manageSystemId}}">
                                                            <div id="sys-controller-container-{{$manageSystemId}}" class="col-md-12" style="padding: 25px; margin-bottom: 30px;">
                                                                {{--Start of accordion--}}
                                                                <div class="accordion" id="accordion-controller-{{$manageSystemId}}" role="tablist" aria-multiselectable="true">
                                                                    {{--JQuery Append--}}
                                                                </div>
                                                                <!-- end of accordion -->
                                                                <span id="btn-new-controller-{{$manageSystemId}}" class="slide-add-circle" data-toggle="modal" data-target=".bs-new-controller-modal-lg" style="bottom: -35px !important; background-color: #1a96bb !important; width: 50px; height: 50px; padding: 10px; padding-left: 16px;"><i class="fa fa-plus"></i> </span>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane fade" id="tab_view-{{$manageSystemId}}" aria-labelledby="view-tab-{{$manageSystemId}}">
                                                            <div class="row">
                                                                <div class="col-md-12" style="padding-bottom: 35px; padding: 20px 40px;">
                                                                    <?php
                                                                        //getting the view data
                                                                        $viewList = SystemView::where('status', 1)->where('systemid', $manageSystemId)->lists('id');
                                                                    ?>

                                                                    <!-- start accordion -->
                                                                    <div class="accordion" id="accordion-view-{{$manageSystemId}}" role="tablist" aria-multiselectable="true" style="padding-bottom: 30px;">
                                                                        @foreach($viewList as $viewId)
                                                                            <?php
                                                                                $viewMaintenance = SystemView::where('id', $viewId)->pluck('maintenance');
                                                                                $viewStatus = SystemView::where('id', $viewId)->pluck('status');
                                                                                if($viewMaintenance == 1) {
                                                                                    $viewLed = '-yellow y-blink-n';
                                                                                }
                                                                                else if($viewStatus == 2) {
                                                                                    $viewLed = '-yellow';
                                                                                }
                                                                                else {
                                                                                    $viewLed = '-green';
                                                                                }
                                                                            ?>
                                                                            <div class="panel">
                                                                                <a class="panel-heading collapsed" role="tab" id="view-panel-{{$viewId}}" data-toggle="collapse" data-parent="#accordion-view-{{$manageSystemId}}" href="#collapese-view-{{$viewId}}" aria-expanded="true" aria-controls="collapese-view-{{$manageSystemId}}" style="position: relative;">
                                                                                    <h4 class="panel-title">{{ SystemView::where('id', $viewId)->pluck('menu_name') }} <span style="font-size: 12px; color: #c7c7c7;">&nbsp;&nbsp;&nbsp;&nbsp;-{{$viewId}}</span></h4>
                                                                                    <div id="view-led-status-{{$viewId}}" class="led{{$viewLed}}" style="position: absolute; right: 20px; top: 16px;"></div>
                                                                                </a>
                                                                                <div id="collapese-view-{{$viewId}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="view-panel-{{$viewId}}">
                                                                                    <div class="panel-body" id="panel-body-{{ $viewId }}">
                                                                                        <div class="col-md-12">
                                                                                            @if($manageSystemId != 5)<input data-id= "view-status-{{$viewId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-xs pull-right" @if($viewMaintenance == 0)checked @endif/>@endif
                                                                                            <div class="col-md-6">
                                                                                                <div class="bs-callout bs-callout-info">
                                                                                                    <div class="form-horizontal form">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">URL Route:</label>
                                                                                                            <div id="system-view-url-{{$viewId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 8px; font-size: 16px; color: #8e5a60;">
                                                                                                                {{ SystemUrlRoutes::where('id', SystemView::where('id', $viewId)->pluck('url_route'))->pluck('url') }}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Route As:</label>
                                                                                                            <div id="system-pending-ex-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #542ea9;">
                                                                                                                {{ SystemUrlRoutes::where('id', SystemView::where('id', $viewId)->pluck('url_route'))->pluck('routeas') }}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Uses:</label>
                                                                                                            <div id="system-pending-ex-{{$manageSystemId}}" class="col-md-8 col-sm-8 col-xs-12" style="margin-top: 10px; color: #542ea9;">
                                                                                                                {{ SystemUrlRoutes::where('id', SystemView::where('id', $viewId)->pluck('url_route'))->pluck('uses') }}
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="bs-callout bs-callout-warning" style="padding-bottom: 60px;">
                                                                                                    <h4>Operations</h4>
                                                                                                    <ul class="list-unstyled msg_list" id="operation-list-{{$viewId}}">
                                                                                                        <?php
                                                                                                        $operationList = SystemOperation::where('status', '!=', 0)->where('systemid', $manageSystemId)->where('viewid', $viewId)->lists('id');
                                                                                                        ?>
                                                                                                        @if(sizeof($operationList) < 1)
                                                                                                            <p style="text-align: center" id="msg-no-operations">No Operations Yet..!</p>
                                                                                                        @else
                                                                                                            @foreach($operationList as $operationId)
                                                                                                                <li>
                                                                                                                    <a>
                                                                                                                <span>
                                                                                                                  <span><i class="fa {{ SystemOperation::where('id', $operationId)->pluck('icon') }}"></i> {{ SystemOperation::where('id', $operationId)->pluck('name') }}</span>
                                                                                                                  <span class="time" style="right: 10%; font-size: 18px;">ID: {{ $operationId }}</span>
                                                                                                                </span>
                                                                                                                        <span class="message">
                                                                                                                    {{ SystemOperation::where('id', $operationId)->pluck('description') }}
                                                                                                                </span>
                                                                                                                        <span id="choices-container-{{$operationId}}">
                                                                                                                    <?php
                                                                                                                            $choises = SystemOperationChoises::where('operationid', $operationId)->where('status', 1)->lists('id');
                                                                                                                            ?>
                                                                                                                            @if(sizeof($choises) < 1)
                                                                                                                                <button class="btn btn-sm btn-info" style="margin-top: 10px;" onclick="showAddChoices({{ $operationId }});">Convert to Choices</button>
                                                                                                                            @else
                                                                                                                                @foreach($choises as $choiseId)
                                                                                                                                    <h4 style="font-size: 13px; margin-left: 30px; color: #888;">{{ SystemOperationChoises::where('id', $choiseId)->pluck('choise') }} - ID: {{ $choiseId }}</h4>
                                                                                                                                @endforeach
                                                                                                                                <input type="text" id="txt-choices-{{ $operationId }}" style="margin-top: 15px; width: 200px; border-radius: 10px;" /><i class="fa fa-plus-circle" style="font-size: 20px; color: #00b922; margin-left: 10px; cursor: pointer;" onclick="addOperationChoice({{ $operationId }});" ></i>
                                                                                                                            @endif
                                                                                                                </span>
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                            @endforeach
                                                                                                        @endif
                                                                                                    </ul>
                                                                                                    <span id="btn-new-operation-{{$viewId}}" class="slide-add-circle" data-toggle="modal" data-target=".bs-new-operation-modal-lg" onclick="loadNewOperation({{ $manageSystemId }}, {{$viewId}});" style="bottom: 10px !important; background-color: #8a6a6a !important; width: 50px; height: 50px; padding: 10px; padding-left: 16px;"><i class="fa fa-plus"></i> </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                    <!-- end of accordion -->
                                                                    <span id="btn-new-controller-{{$manageSystemId}}" class="slide-add-circle" data-toggle="modal" data-target=".bs-new-view-modal-lg" onclick="loadNewView({{ $manageSystemId }});" style="bottom: -10px !important; background-color: #1a96bb !important; width: 50px; height: 50px; padding: 10px; padding-left: 16px;"><i class="fa fa-plus"></i> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane fade" id="tab_access-{{$manageSystemId}}" aria-labelledby="access-tab-{{$manageSystemId}}">
                                                            <div id="sys-controller-container-{{$manageSystemId}}" class="col-md-12" style="padding: 25px; margin-bottom: 30px;">
                                                                {{--Start of accordion--}}
                                                                <div class="accordion" id="accordion-controller-{{$manageSystemId}}" role="tablist" aria-multiselectable="true">
                                                                    Access will be here soon...

                                                                </div>
                                                                <!-- end of accordion -->
                                                                <span id="btn-new-access-{{$manageSystemId}}" class="slide-add-circle" data-toggle="modal" data-target=".bs-new-controller-modal-lg" style="bottom: -35px !important; background-color: #1a96bb !important; width: 50px; height: 50px; padding: 10px; padding-left: 16px;"><i class="fa fa-plus"></i> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- end of accordion -->
                        </div>
                        <span id="btn-new-system" class="slide-add-circle" data-toggle="modal" data-target=".bs-new-system-modal-lg" style="bottom: -60px !important;"><i class="fa fa-plus"></i> </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#add-call-container').slideUp();
        });
    </script>
    {{HTML::script('assets/backend/libs/jquery-sparkline/dist/jquery.sparkline.js')}}
    {{HTML::script('assets/backend/js/custom/system/manage.js')}}
@endsection
