<?php
/************************| users.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/10/16 15:14.
 */
?>

@extends('backend.master')

@section('content')
    @include('backend.user.include.new-user')

    <div class="right_col" role="main">
        <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row" style="margin-right: 20px;">
                            <h2 class="pull-left"><i class = 'fa fa-user'></i> Manage Users</h2>
                            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-add-user-modal-lg" onclick="openAddUser();"><i class="fa fa-plus"></i> New User</button>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="user-manage-widget">
                                    <div class="col-md-3 bs-callout bs-callout-info" style="border-radius: 10px; margin-top: 10px; max-height: 600px; overflow-y: auto;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-search search-box has-feedback">
                                                    <span id="searchSpan">
                                                        <input type = "text" class = "form-control full-rounded" onkeyup = "searchFilter(this);" id = "searchText" ng-model = "searchText" placeholder = "Search..">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id = "o-m-org-list-container" class = "row" style = "margin-top: 20px;">
                                            <ul id = "user-list-container" class = "search-filter" style = "list-style-type: none; margin-left: -30px;">
                                                {{--jQuery append--}}
                                            </ul>
                                        </div>
                                    </div>
                                    <div class = "col-md-3" style = "padding: 5px;">
                                        <div class = " shadow-pane-1" style = "margin-top: -10px;">
                                            <div class = "row">
                                                <div class = "col-md-12">
                                                    @if(SystemOperationAccess::where('operationid', 4)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2)
                                                    <div data-toggle = "tooltip" title = "Active/Deactive Inspector" style = "visibility: visible; position: absolute;">
                                                        <input data-id = "user-status" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" checked hidden = "hidden"/>
                                                    </div>
                                                    @endif
                                                    <p style = "text-align: center;">
                                                        <img id="user-image" class="img-circle" src="{{URL::To('/')}}/assets/backend/images/users/default-M/user-100.jpg" style="width: 100px; height: 100px;" />
                                                    </p>
                                                </div>
                                            </div>
                                            <div class = "row" style = "margin-top: -10px;">
                                                <p style = "text-align: center; font-weight: 600; color: rgba(0, 0, 0, 0.30)" id = "user-dept">Null</p>
                                            </div>
                                            <div class = "row" style = "margin-top: -10px;">
                                                <p style = "text-align: center; font-weight: 600; color: rgba(0, 0, 0, 0.30); font-size: 9px;" id = "user-job-title">Null</p>
                                            </div>
                                            <div class = "row">
                                                <h4 class = "text-blue-3" style = "text-align: center; font-size: 18px;">
                                                    <strong id = "user-name">NULL</strong>
                                                </h4>
                                            </div>
                                            <div class = "row">
                                                <ul style = "list-style-type: none; margin-left: -20px;margin-top: 20px;">
                                                    <li style = "margin-top: 5px;">
                                                        <a href = "#">
                                                            <i id = "user-status-icon" class = "fa fa-bookmark green"></i>
                                                            &nbsp;<span id = "user-status-text">Active</span></a>
                                                    </li>
                                                    <li style = "margin-top: 5px;">
                                                        <a id="user-email" href = "#">
                                                            <i id = "o-m-account-active-status-icon" class = "fa fa-envelope text-yellow-1"></i>
                                                            &nbsp;<span id = "o-m-account-active-status-text">Email</span></a>
                                                    </li>
                                                    <li style = "margin-top: 15px;">
                                                        <button id ="user-resend-activation-email" onclick="resendActivationEmail();" class = "btn btn-default" style = "width: 90%;text-align: center; display: none;">
                                                            <i class = "fa fa-envelope"></i> Resend Activation Email
                                                        </button>
                                                        @if(SystemOperationAccess::where('operationid', 1)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2)
                                                            <button id = "user-delete-user" onclick="deleteUser();" class = "btn btn-danger" style = "width: 90%;text-align: center; visibility: visible; margin-top: 10px; display: none;">
                                                                <i class = "fa fa-trash"></i> Remove User
                                                            </button>
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "col-md-6">
                                        <ul id = "o-m-org-manage-container" class = "nav nav-tabs nav-simple">
                                            @if(SystemOperationAccess::where('operationid', 3
                                            )->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2)
                                                <li id = "user-tab-activity-log" class = "active">
                                                    <a href = "#user-tab-activity-log-container" data-toggle = "tab"><i class = "fa fa-bullhorn text-green-3"></i>
                                                        Activity Log</a>
                                                </li>
                                            @endif
                                            @if(SystemOperationAccess::where('operationid', 3)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2)
                                                <li  id="user-tab-access">
                                                    <a href = "#user-tab-access-container" data-toggle = "tab"><i class = "fa fa-sliders text-orange-3"></i>
                                                        Access</a>
                                                </li>
                                            @endif
                                                <li id="user-tab-settings">
                                                    <a href="#user-tab-settings-container" data-toggle="tab"><i class="fa fa-wrench"></i> Settings</a>
                                                </li>
                                        </ul>

                                        <div class = "tab-content" style = "padding: 20px;">
                                            <div class = "tab-pane fade" id = "user-tab-settings-container" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                                <div class="form-group">
                                                    <label for="user-email">Email:</label>
                                                    <input id = "user-settings-email" type = "email" class = "form-control" placeholder = "someone&#64;example.com" style="border: none; background-color: rgba(211, 207, 207, 0); box-shadow: none;" disabled="disabled"  maxlength = "60" required tabindex="7">
                                                </div>
                                            </div>
                                            @if(SystemOperationAccess::where('operationid', 3)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2)
                                                <!-- / .tab-pane Access-->
                                                <div class = "tab-pane fade"  id = "user-tab-access-container">
                                                    <div id="o-m-access-control-container" class = "row">
                                                        <div class="panel-group accordion-toggle" id="accordiondemo">
                                                            <?php
                                                                $systemList = System::where('status', 1)->where('visibility', 1)->orderBy('listorder', 'asc')->lists('id');
                                                            ?>
<!--                                                            Looping system list-->
                                                            @foreach($systemList as $systemId)
                                                                <?php
                                                                    $viewList = SystemView::where('systemid', $systemId)->where('status', 1)->lists('id');
                                                                ?>
                                                                @if(sizeof($viewList) > 0)
                                                                    <h4 style="margin-top: 20px; margin-bottom: 10px"><i class="fa {{ System::where('id', $systemId)->pluck('icon') }}"></i> {{ System::where('id', $systemId)->pluck('name') }}</h4>
                                                                @endif
                                                                {{--Looping View list--}}
                                                                @foreach($viewList as $viewId)
                                                                    @if(SystemViewAccess::where('viewid', $viewId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2 || System::where('id', $systemId)->where('allow_default', 1)->pluck('id'))
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion{{$viewId}}">
                                                                                        <h4 style="margin-top: 0; margin-bottom: 5px;" id="o-m-text-container-1" onclick="">
                                                                                            <i class = "fa {{ SystemView::where('id', $viewId)->pluck('icon') }}"></i> &nbsp;&nbsp;{{ SystemView::where('id', $viewId)->pluck('view_name') }}
                                                                                        </h4>
                                                                                    </a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="accordion{{$viewId}}" class="panel-collapse collapse">
                                                                                <div class="panel-body">
                                                                                    <ul style="list-style-type: none;">
                                                                                        <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px; font-size: 18px; font-weight: 600;">
                                                                                            <div class="col-md-7" id="user-operation-text-container-{{$viewId}}">
                                                                                                <i class="fa {{ SystemView::where('id', $viewId)->pluck('icon') }}"></i> Access&nbsp;&nbsp;{{ SystemView::where('id', $viewId)->pluck('view_name') }}
                                                                                            </div>
                                                                                            <div class="col-md-4" style="cursor: pointer">
                                                                                                <input id = "user-access-system-operation-{{$viewId}}" data-id = "sa-sy-{{$viewId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-md pull-left" checked/>
                                                                                            </div>
                                                                                        </li>
                                                                                        <hr>
                                                                                        <?php $systemOperationList = SystemOperation::where('status', 1)->where('viewid', $viewId)->lists('id'); ?>
                                                                                        @foreach($systemOperationList as $operationId)
                                                                                            <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px;">
                                                                                                <div class="col-md-7" id="user-operation-text-container-{{$operationId}}">
                                                                                                    <i class="fa {{SystemOperation::where('id', $operationId)->pluck('icon')}}"></i> &nbsp;{{SystemOperation::where('id', $operationId)->pluck('name')}}
                                                                                                </div>
                                                                                                <div class="col-md-4" style="cursor: pointer">
                                                                                                    <?php
                                                                                                        $systemOperationChoices = SystemOperationChoises::where('operationid', $operationId)->where('status', 1)->lists('id');
                                                                                                    ?>
                                                                                                    @if(sizeof($systemOperationChoices) < 1)
                                                                                                        <input id = "user-access-system-operation-{{$operationId}}" data-id = "sa-op-{{$operationId}}" type = "checkbox" class = "ios-switch ios-switch-primary iswitch-xs pull-left" checked/>
                                                                                                    @else
                                                                                                        <select id="user-access-system-operation-choices-{{ $operationId }}" onchange="saveAccessChoice({{ $operationId }});" style="width: 100%; height: 25px; background-color: #fbfbfb;">
                                                                                                            <option value="0">Not Allowed</option>
                                                                                                            @foreach($systemOperationChoices as $choiceId)
                                                                                                                <option value="{{ $choiceId }}">{{ SystemOperationChoises::where('id', $choiceId)->pluck('choise') }}</option>
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </li>
                                                                                        @endforeach
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(SystemOperationAccess::where('operationid', 3)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                                                <!-- / .tab-pane Activity Log-->
                                                    <div class = "tab-pane fade" id = "user-tab-activity-log-container" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                                        <div id = "user-activity-list-container" class = "row" style="padding: 10px 30px;">
                                                            <ul id = "user-activity-list" class = "media-list">

                                                            </ul>
                                                        </div>
                                                    </div>
                                            @endif
                                            <!-- / .tab-pane -->
                                        </div>
                                        <!-- / .tab-content -->
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- iCheck -->
    {{HTML::script('assets/backend/libs/iCheck/icheck.min.js')}}

    {{HTML::script('assets/backend/js/custom/users/user-manage.js')}}
@endsection
