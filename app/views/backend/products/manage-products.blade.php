<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-06 02:59:29.
 * View ID - 41
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/products/style.css')}}
@endsection

@section('content')

    @include('backend.products.includes.new-product')
    <div class="right_col" role="main">
        <div id="property-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-sliders"></i> Manage Products</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Product List</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-add-product-modal-lg" onclick="getManufactures();"><i class="fa fa-plus"></i> Add New Product</button>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="top_search">
                                    <div class="input-group">
                                        <input id="product-table-search-text" type="text" onkeyup="productTableSearchFilter(this);" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                        <button id="property-table-search-go" class="btn btn-default" type="button" onclick="serchProducts();">Go!</button>
                                    </span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <span id="search-result-count">0</span> Showing ( {{ Product::where('status', '!=', 0)->where('orgid', Auth::user()->orgid)->count() }} of All Products )
                            </div>
                            <div class="col-md-12">
                                <div class="btn-group pull-right" style="margin-right: 10px; margin-bottom: 10px; display: inline-block;">
                                    <p class="pull-left selected-count-text"> 3 selected</p>
                                    <button id="btn-clear-selection" disabled="disabled" class="btn btn-default" type="button" title="Clear Selection" onclick="clearSelection();"><i class="fa fa-square-o" style="font-weight: 700;"></i> </button>
                                    <button id="btn-delete-products" disabled="disabled" class="btn btn-default" type="button" title="Delete Selected" onclick="confirmDeleteProducts();"><i class="fa fa-trash" style="color: #e42e2c;"></i> </button>
                                    {{--<div class="btn-group">--}}
                                        {{--<button id="btn-show-more-options" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"> More <span class="caret"></span> </button>--}}
                                        {{--<ul class="dropdown-menu">--}}
                                            {{--<li><a href="#">Add to Categories</a></li>--}}
                                            {{--<li><a href="#">Publish</a></li>--}}
                                            {{--<li><a href="#">Unpublish</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action search-filter">
                                        <thead>
                                        <tr class="headings">
                                            <th></th>
                                            <th style="text-align: left;" class="column-title" onclick="sortProductList('id');">#&nbsp;&nbsp;<i id="sort-icon-id" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i> </th>
                                            <th style="text-align: left;" class="column-title" onclick="sortProductList('external_id');">Product&nbsp;ID&nbsp;&nbsp;<i id="sort-icon-external_id" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i> </th>
                                            <th class="column-title"></th>
                                            <th style="text-align: left;" class="column-title" onclick="sortProductList('name');">Name&nbsp;&nbsp;<i id="sort-icon-name" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i> </th>
                                            <th style="text-align: right; cursor: default !important;" class="column-title">Price&nbsp;&nbsp;<i id="sort-icon-price" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i></th>
                                            <th style="text-align: right; cursor: default !important;" class="column-title">W/S&nbsp;Price&nbsp;&nbsp;<i id="sort-icon-wholesale_price" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i></th>
                                            <th style="text-align: center;" class="column-title" onclick="sortProductList('status');">Status&nbsp;&nbsp;<i id="sort-icon-status" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i></th>
                                            <th style="text-align: right;" class="column-title" onclick="sortProductList('created_at');">Created&nbsp;At&nbsp;&nbsp;<i id="sort-icon-created_at" class="fa fa-caret-up sort-icon" style="opacity: 0;"></i></th>
                                        </tr>
                                        </thead>

                                        <tbody id="products-row-container">
                                            {{--JQuery Append--}}
                                            <tr>
                                                <td id="onstart-loading" colspan="9" align="center"><img src="{{URL::To('/')}}/assets/backend/images/loading/ring-sm.svg"/> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('assets/backend/libs/fastclick/lib/fastclick.js')}}
    <!-- NProgress -->
    {{HTML::script('assets/backend/libs/nprogress/nprogress.js')}}
    <!-- iCheck -->
    {{HTML::script('assets/backend/libs/iCheck/icheck.min.js')}}

    <!-- Custom Theme Scripts -->
    {{HTML::script('assets/backend/js/custom.min.js')}}
    {{HTML::script('assets/js/products/products.js')}}
@endsection
