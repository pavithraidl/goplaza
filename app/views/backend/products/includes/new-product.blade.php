<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/17 09:46.
 */
?>

<div id="model-add-new-product" class="modal fade bs-add-product-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" id="model-add-new-product-content">

            <div class="modal-header">
                <button id="product-modal-close" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Product</h4>
            </div>
            <div id="modal-add-user-body" class="modal-body" style="padding: 20px 30px;">
                <div class = "row">
                    <div class = "form-group">
                        <label>Product Name</label>
                        <input id = "mm-product-name" onblur="requiredValidator(this, 'Please enter the product\'s name.');" onkeyup="requiredValidator(this, 'Please enter the product\'s name.');" type = "text" class = "form-control" placeholder = "Product name here..." maxlength = "32" required tabindex="1">
                    </div>
                </div>
                <p style = "text-align: center; margin-top: 40px;">
                    <button id = "mm-btn-user-cancel" onclick="cancelProduct();" class = "btn btn-danger md-close">Cancel</button>
                    <button id = "mm-btn-user-add" onclick="addProduct();" type = "button" class = "btn btn-success" tabindex="4">Add Product</button>
                </p>
            </div>
        </div>
    </div>
</div><!-- End div .md-modal .md-3d-flip-vertical-->

