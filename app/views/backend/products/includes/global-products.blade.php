<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 8/03/18 15:18.
 */
?>

<div id="model-show-global-products" class="modal fade show-global-products" tabindex="-1" role="dialog" aria-hidden="true" style="left: 595px; top: 83.0781px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="model-global-product-list" style="width: 580px;">

            <div class="modal-header">
                <button id="product-modal-close" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">GoPlaza Global Products Database</h4>
            </div>
            <div id="modal-add-user-body" class="modal-body" style="padding: 20px 30px;">
                <h5 style="margin-bottom: 15px;">Is there your correct product below? Click on the product to add product details automatically</h5>
                <div id="global-product-container" class = "row" style="max-height: 320px; overflow: auto;">
                    {{--JQuery append--}}
                </div>
                <p style = "text-align: center; margin-top: 10px;">
                    <button id = "mm-btn-user-cancel" onclick="cancelProduct();" class = "btn btn-danger md-close">Cancel</button>
                </p>
            </div>
        </div>
    </div>
</div><!-- End div .md-modal .md-3d-flip-vertical-->
