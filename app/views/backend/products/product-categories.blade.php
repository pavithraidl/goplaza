<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-02-05 02:02:10.
 * View ID - 52
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/products/style.css')}}
@endsection

@section('content')
    @include('backend.products.includes.new-product')
    <div class="right_col" role="main">
        <div id="category-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-sitemap"></i> Product Categories</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="product-top-name">Categories</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php
                        $menuId = Menu::where('orgid', Auth::user()->orgid)->where('positionid', 1)->pluck('id');
                        $menuList = MenuItem::where('menuid', $menuId)->orderBy('status', 'DESC')->orderBy('menu_order', 'ASC')->where('status', '!=', 0)->get();
                        $counter = 1;
                        $gridX = 0;
                        $gridY = sizeof($menuList);
                        ?>
                        <div class="row" style="padding: 30px; padding-bottom: 10px !important;">
                            <button id="add-new-widget" class="btn btn-sm btn-success pull-right" onclick="addMenuItem({{ $menuId }})"><i class="fa fa-plus"></i> Add Menu Item</button>
                        </div>
                        <div class="row" style="padding: 30px; padding-top: 10px !important;">
                            <div id="menu-item-grid-stack" class="grid-stack">
                                @foreach($menuList as $menuItem)
                                    <div id="menu-item-{{ $menuItem->id }}" class="grid-stack-item menu-item" data-menuid="{{ $menuItem->id }}" data-gs-x="0" data-gs-y="{{ $gridY }}" data-gs-width="12" data-gs-height="6" data-gs-no-resize="true" style="padding: 10px;">
                                        <div class="grid-stack-item-content flip" style="box-shadow: 1px 1px 2px 3px #f7f6f6; border-radius: 10px; padding: 20px; position: relative; height: 100%;">
                                            <h4 style="text-align: center;">@if($menuItem->status == 2) <span id="menu-item-number-{{ $menuItem->id }}" class="menu-item-draft">Draft - </span> @else <span id="menu-item-number-{{ $menuItem->id }}">{{ $counter++ }}.</span> @endif <input id="txt-menu-item-name-{{ $menuItem->id }}" type="text" class="input-trans" value="{{ $menuItem->name }}" style="height: 25px;" onchange="updateMenuItemName({{ $menuItem->id }});" /></h4>
                                            <div style="position: absolute; right: 10px; top: 10px;">
                                                <i class="fa fa-gear pull-right" style="font-size: 23px; cursor: pointer;" onclick="flipDiv({{ $menuItem->id }});"></i>
                                                <input data-id = "menu-item-status-{{ $menuItem->id }}" type = "checkbox" class = "ios-switch ios-switch-primary iswitch-xs pull-right" title="Publish Menu Item" @if($menuItem->status == 1) checked @endif />
                                            </div>
                                            <div id="flipped-class-{{ $menuItem->id }}" class="card" style="margin-top: 50px;">
                                                <div class="face front">
                                                    <div class="bs-callout bs-callout-info col-md-12">
                                                        <div class="col-md-4">
                                                            {{--Upload Img--}}
                                                            <?php
                                                            if($menuItem->img == 1)
                                                                $imgUrl = URL::To('/').'/assets/backend/images/website/menu/'.Auth::user()->orgid.'/'.$menuItem->id.'.'.$menuItem->img_ext;
                                                            else
                                                                $imgUrl = URL::To('/').'/assets/backend/images/website/slide-block/default.jpg';
                                                            ?>
                                                            <label>Menu Image</label>
                                                            <div class="upload-img-container" style="position: relative; height: 200px; overflow: hidden;" onclick="$('#menu-image-choose-{{ $menuItem->id }}').trigger('click');">
                                                                <div class="upload-img-overlay" onmouseenter="animateLivIcon({{ $menuItem->id }});">
                                                                    <h4 style="margin-bottom: 10%; font-size: 13px;">Add/Change Banner Image</h4>
                                                                    <div id="menu-upload-img-livicon-{{ $menuItem->id }}" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 45px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                                    <p>Click Here to Upload!<br/><br/><span>notes</span></p>
                                                                </div>
                                                                <img id="menu-img-{{ $menuItem->id }}" src="{{ $imgUrl }}" style="width: 100%; height: auto; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                                <div id="menu-image-upload-progress-{{ $menuItem->id }}" class = "progress" style = "height: 8px;width: 100%;">
                                                                    <div id="menu-upload-progress-bar-{{ $menuItem->id }}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                                        <span id="menu-upload-sr-only-{{ $menuItem->id }}" class = "sr-only">0% Complete</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <form action="{{URL::Route('products-savemenuimage')}}" method="post" id="menu-upload-image-form-{{ $menuItem->id }}" enctype="multipart/form-data" class="menu-item-img-upload">
                                                                <input type="file" name="file" id="menu-image-choose-{{ $menuItem->id }}" onchange="menuImgFormSubmit({{ $menuItem->id }}, 'banner');" accept="image/jpg;capture=camera" style="display: none;">
                                                                <input type="hidden" name="menuid" value="{{ $menuItem->id }}" />
                                                            </form>
                                                            {{--End of image upload--}}
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label>Description</label>
                                                                <textarea id="menu-item-description-{{ $menuItem->id }}" class="form-control" data-rich="0">{{ $menuItem->description }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="margin-bottom: 30px;">
                                                        <button class="btn btn-sm btn-info pull-right" onclick="addCategory({{$menuItem->id}}, null);"><i class="fa fa-plus"></i> Add Category</button>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <!-- start accordion -->
                                                            <?php
                                                            $productCategories = ProductCategory::where('menu_item_id', $menuItem->id)->where('status', 1)->where('sub_item', null)->get();
                                                            ?>
                                                            <div class="accordion" id="accordion-menu-category-{{ $menuItem->id }}" data-menuid="{{ $menuItem->id }}" data-container="0" role="tablist" aria-multiselectable="true">
                                                                @foreach($productCategories as $productCategory)
                                                                    <div class="panel" data-menuitemid="{{ $productCategory->id }}">
                                                                        <a class="panel-heading collapsed" role="tab" id="category-{{ $productCategory->id }}" data-toggle="collapse" data-parent="#accordion-menu-category-{{ $menuItem->id }}" href="#collapse-{{ $productCategory->id }}" aria-expanded="true" aria-controls="collapse-{{ $productCategory->id }}">
                                                                            <h4 id="panel-title-{{ $productCategory->id }}" class="panel-title">{{ $productCategory->name }}</h4>
                                                                        </a>
                                                                        <div id="collapse-{{ $productCategory->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $productCategory->id }}">
                                                                            <div class="panel-body">
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-4">
                                                                                        {{--Image Upload--}}
                                                                                        <?php
                                                                                        $categoryImg = $productCategory->img;
                                                                                        if($categoryImg == 1)
                                                                                            $imgUrl = URL::To('/').'/assets/backend/images/products/categories/'.Auth::user()->orgid.'/'.$productCategory->id.'.'.$productCategory->img_ext;
                                                                                        else
                                                                                            $imgUrl = URL::To('/').'/assets/backend/images/website/slide-block/default.jpg';


                                                                                        $categoryBanner = $productCategory->banner;
                                                                                        ?>
                                                                                        <label>Category Image</label>
                                                                                        <div class="upload-img-container" style="position: relative; height: 200px; overflow: hidden;" onclick="$('#image-choose-{{ $productCategory->id }}').trigger('click');">
                                                                                            <div class="upload-img-overlay" onmouseenter="animateLivIcon({{ $productCategory->id }});">
                                                                                                <h4 style="margin-bottom: 10%; font-size: 13px;">Add/Change Category Image</h4>
                                                                                                <div id="upload-img-livicon-{{ $productCategory->id }}" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 45px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                                                                <p>Click Here to Upload!<br/><br/><span>notes</span></p>
                                                                                            </div>
                                                                                            <img id="category-img-{{ $productCategory->id }}" src="{{ $imgUrl }}" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                                                            <div id="category-image-upload-progress-{{ $productCategory->id }}" class = "progress" style = "height: 8px;width: 100%;">
                                                                                                <div id="category-upload-progress-bar-{{ $productCategory->id }}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                                                                    <span id="category-upload-sr-only-{{ $productCategory->id }}" class = "sr-only">0% Complete</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <form action="{{URL::Route('products-savecategoryimg')}}" method="post" id="upload-image-form-{{ $productCategory->id }}" enctype="multipart/form-data" class="slider-slide-image-upload">
                                                                                            <input type="file" name="file" id="image-choose-{{ $productCategory->id }}" onchange="categoryImgFormSubmit({{ $productCategory->id }}, 'category');" accept="image/jpg;capture=camera" style="display: none;">
                                                                                            <input type="hidden" name="categoryid" value="{{ $productCategory->id }}" />
                                                                                            <input type="hidden" name="imgtype" value="1" />
                                                                                        </form>
                                                                                        {{--End of image upload--}}
                                                                                    </div>
                                                                                    <div class="col-md-8">
                                                                                        {{--Image Upload--}}
                                                                                        <?php
                                                                                        $bannerImg = $productCategory->banner;
                                                                                        if($bannerImg == 1)
                                                                                            $imgUrl = URL::To('/').'/assets/backend/images/products/categories/'.Auth::user()->orgid.'/'.$productCategory->id.'-banner.'.$productCategory->banner_ext;
                                                                                        else
                                                                                            $imgUrl = URL::To('/').'/assets/backend/images/website/slide-block/default.jpg';


                                                                                        $categoryBanner = $productCategory->banner;
                                                                                        ?>
                                                                                        <label>Banner Image</label>
                                                                                        <div class="upload-img-container" style="position: relative; height: 200px; overflow: hidden;" onclick="$('#banner-image-choose-{{ $productCategory->id }}').trigger('click');">
                                                                                            <div class="upload-img-overlay" onmouseenter="animateLivIcon({{ $productCategory->id }});">
                                                                                                <h4 style="margin-bottom: 10%; font-size: 13px;">Add/Change Banner Image</h4>
                                                                                                <div id="banner-upload-img-livicon-{{ $productCategory->id }}" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 45px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                                                                <p>Click Here to Upload!<br/><br/><span>notes</span></p>
                                                                                            </div>
                                                                                            <img id="banner-img-{{ $productCategory->id }}" src="{{ $imgUrl }}" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                                                            <div id="banner-image-upload-progress-{{ $productCategory->id }}" class = "progress" style = "height: 8px;width: 100%;">
                                                                                                <div id="banner-upload-progress-bar-{{ $productCategory->id }}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                                                                    <span id="banner-upload-sr-only-{{ $productCategory->id }}" class = "sr-only">0% Complete</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <form action="{{URL::Route('products-savecategoryimg')}}" method="post" id="banner-upload-image-form-{{ $productCategory->id }}" enctype="multipart/form-data" class="slider-slide-image-upload">
                                                                                            <input type="file" name="file" id="banner-image-choose-{{ $productCategory->id }}" onchange="categoryImgFormSubmit({{ $productCategory->id }}, 'banner');" accept="image/jpg;capture=camera" style="display: none;">
                                                                                            <input type="hidden" name="categoryid" value="{{ $productCategory->id }}" />
                                                                                            <input type="hidden" name="imgtype" value="2" />
                                                                                        </form>
                                                                                        {{--End of image upload--}}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group" style="margin-top: 40px;">
                                                                                    <label>Category Name</label>
                                                                                    <input  id="category-name-{{ $productCategory->id }}" type="text" class="form-control" value="{{ $productCategory->name }}"  onchange="updateCategoryDetails(this, {{ $productCategory->id }}, 'name')" />
                                                                                </div>
                                                                                <div class="form-group" style="margin-top: 40px;">
                                                                                    <label>Category Short Description</label>
                                                                                    <textarea id="category-description-{{ $productCategory->id }}" onchange="updateCategoryDetails(this, {{ $productCategory->id }}, 'description')" class="form-control" data-rich="0">{{ $productCategory->description }}</textarea>
                                                                                </div>
                                                                                <div class="form-group" style="margin-top: 40px;">
                                                                                    <label>Category Long Description</label>
                                                                                    <textarea id="category-long-description-{{ $productCategory->id }}" onchange="updateCategoryDetails(this, {{ $productCategory->id }}, 'long_description')" class="form-control" data-rich="0">{{ $productCategory->long_description }}</textarea>
                                                                                </div>
                                                                                <?php
                                                                                $productSubCategories = ProductCategory::where('sub_item', $productCategory->id)->get();
                                                                                ?>
                                                                                <h3 style="margin-top: 50px; font-size: 18px;">Sub Categories</h3>
                                                                                <div class="col-md-12" style="margin-bottom: 30px;">
                                                                                    <button class="btn btn-sm btn-info pull-right" onclick="addCategory({{$menuItem->id}}, {{ $productCategory->id }});"><i class="fa fa-plus"></i> Add Sub Category</button>
                                                                                </div>
                                                                                <div></div>
                                                                                <div class="accordion" id="accordion-menu-sub-category-{{ $productCategory->id }}" data-menuid="{{ $productCategory->id }}" data-container="{{ $productCategory->id }}" role="tablist" aria-multiselectable="true" style="margin-bottom: 100px; padding: 5px; margin-top: 50px;">
                                                                                    @foreach($productSubCategories as $productSubCategory)
                                                                                        <div class="panel" data-menuitemid="{{ $productSubCategory->id }}">
                                                                                            <a class="panel-heading collapsed" role="tab" id="sub-category-{{ $productSubCategory->id }}" data-toggle="collapse" data-parent="#accordion-menu-sub-category-{{ $productCategory->id }}" href="#collapse-{{ $productSubCategory->id }}" aria-expanded="true" aria-controls="collapse-{{ $productSubCategory->id }}" style="background-color: #f5f2f7;">
                                                                                                <h4 id="panel-title-{{ $productSubCategory->id }}" class="panel-title">{{ $productSubCategory->name }}</h4>
                                                                                            </a>
                                                                                            <div id="collapse-{{ $productSubCategory->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $productSubCategory->id }}">
                                                                                                <div class="panel-body">
                                                                                                    <div class="col-md-12">
                                                                                                        <div style="width: 270px; height: auto; margin: 0 auto;">
                                                                                                            {{--Image Upload--}}
                                                                                                            <?php
                                                                                                            $productSubCategoryImg = $productSubCategory->img;
                                                                                                            if($productSubCategoryImg == 1)
                                                                                                                $imgUrl = URL::To('/').'/assets/backend/images/products/categories/'.Auth::user()->orgid.'/'.$productSubCategory->id.'.'.$productSubCategory->img_ext;
                                                                                                            else
                                                                                                                $imgUrl = URL::To('/').'/assets/backend/images/website/slide-block/default.jpg';
                                                                                                            ?>
                                                                                                            <label>Sub-Category Image</label>
                                                                                                            <div class="upload-img-container" style="position: relative; height: 200px; overflow: hidden;" onclick="$('#image-choose-{{ $productSubCategory->id }}').trigger('click');">
                                                                                                                <div class="upload-img-overlay" onmouseenter="animateLivIcon({{ $productSubCategory->id }});">
                                                                                                                    <h4 style="margin-bottom: 10%; font-size: 13px;">Add/Change Category Image</h4>
                                                                                                                    <div id="upload-img-livicon-{{ $productSubCategory->id }}" class="livicon-evo" data-options="name: cloud-upload.svg; style: filled; size: 45px; strokeColor: #22A7F0;" style="margin: 0 auto;"></div>
                                                                                                                    <p>Click Here to Upload!<br/><br/><span>notes</span></p>
                                                                                                                </div>
                                                                                                                <img id="category-img-{{ $productSubCategory->id }}" src="{{ $imgUrl }}" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                                                                                                <div id="category-image-upload-progress-{{ $productSubCategory->id }}" class = "progress" style = "height: 8px;width: 100%;">
                                                                                                                    <div id="category-upload-progress-bar-{{ $productSubCategory->id }}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                                                                                        <span id="category-upload-sr-only-{{ $productSubCategory->id }}" class = "sr-only">0% Complete</span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <form action="{{URL::Route('products-savecategoryimg')}}" method="post" id="upload-image-form-{{ $productSubCategory->id }}" enctype="multipart/form-data" class="slider-slide-image-upload">
                                                                                                                <input type="file" name="file" id="image-choose-{{ $productSubCategory->id }}" onchange="categoryImgFormSubmit({{ $productSubCategory->id }}, 'category');" accept="image/jpg;capture=camera" style="display: none;">
                                                                                                                <input type="hidden" name="categoryid" value="{{ $productSubCategory->id }}" />
                                                                                                                <input type="hidden" name="imgtype" value="1" />
                                                                                                            </form>
                                                                                                            {{--End of image upload--}}
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="form-group" style="margin-top: 40px;">
                                                                                                        <label>Sub-Category Name</label>
                                                                                                        <input  id="category-name-{{ $productSubCategory->id }}" type="text" class="form-control" value="{{ $productSubCategory->name }}"  onchange="updateCategoryDetails(this, {{ $productSubCategory->id }}, 'name')" />
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label>Description</label>
                                                                                                        <textarea id="category-description-{{ $productSubCategory->id }}" onchange="updateCategoryDetails(this, {{ $productSubCategory->id }}, 'description')" class="form-control" data-rich="0">{{ $productSubCategory->description }}</textarea>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <!-- end of accordion -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="face back">
                                                    <div class="bs-callout bs-callout-info">
                                                        <div class="form-group">
                                                            <label>URL</label>
                                                            <input class="form-control input-trans" placeholder="custom url (Ex: yourdomain.com)" value="{{ $menuItem->href }}" />
                                                        </div>
                                                        <p style="text-align: center;">
                                                            <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Remove</button>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                        $gridY--;
                                    ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- iCheck -->
    {{HTML::script('assets/backend/libs/iCheck/icheck.min.js')}}

    <!-- Custom Theme Scripts -->
    {{HTML::script('assets/backend/js/custom.min.js')}}
    {{HTML::script('assets/js/products/category.js')}}

    <script>
        $( function() {
            $( "ul.droptrue" ).sortable({
                connectWith: "ul"
            });

            $( "ul.dropfalse" ).sortable({
                connectWith: "ul",
                dropOnEmpty: false
            });

            // $( "#sortable1, #sortable2, #sortable3" ).disableSelection();
        } );
    </script>
@endsection
