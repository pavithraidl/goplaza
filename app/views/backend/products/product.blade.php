<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024] IDL Creations Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - IDL Creaitons (Pavithra Isuru Liyanage)
 * Created on - 2017-12-12 22:56:46.
 * View ID - 43
 */
?>

@extends('backend.master')

@section('styles')

@endsection

@section('content')
    @include('backend/products/includes/global-products')

    <?php
        $product = Product::where('id', $productId)->where('status', '!=', 0)->first();
        $productDetailsId = ProductDetail::where('productid', $productId)->where('default_product', 1)->where('status', '!=', 0)->pluck('id');
        $productDetails = ProductDetail::where('id', $productDetailsId)->first();
    ?>
    <input id="product-id" type="hidden" value="{{ $productId }}" />
    <input id="product-detail-id" type="hidden" value="{{ $productDetailsId }}" />
    <input id="product-detail-payment" type="hidden" value="{{ $product->payments }}" />
    <input id="product-new" type="hidden" value="{{ $product->new_product }}" />
    <button id="trigger-global-product-list" type="button" class="btn btn-primary pull-right hidden" data-toggle="modal" data-target=".show-global-products">Product</button>

    <div class="right_col" role="main">
        <div id="product-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-cubes"></i> Products</h3>
            </div>
            <div class="clearfix"></div>
            <h5><a href="{{URL::To('/')}}/products/manage"> <i class="fa fa-angle-left"></i> Back To All Products </a></h5>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 id="product-top-name"><strong>{{ $product->external_id }}</strong> ::: {{ $product->name }}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div style="display: none;">
                                <form action="{{URL::Route('general-uploadimage')}}" method="post" id="upload-image-form" enctype="multipart/form-data" class="image-upload">
                                    <input type="file" name="file" id="image-choose" onchange="submitImageUploadForm('#upload-image-form');" accept="image/*;capture=camera|video/*;capture=camera" style="display: none;">
                                    <input type="hidden" id="img-productid" name="productid" value="{{ $product->id }}" />
                                    <input type="hidden" id="img-product-img-type" name="imgtype" value="1" />
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <h3 id="product-img-title">Main Image</h3>
                                <div class="product-image" style="position: relative;">
                                    {{--Image url setup--}}
                                    <?php
                                        $productImages = ProductImgRelation::where('productid', $productId)->where('status', 1)->where('global_img', 0)->lists('imgid');
                                        $mainImgId = ProductImgRelation::where('productid', $productId)->where('status', 1)->where('global_img', 0)->where('type', 1)->pluck('imgid');
                                        $mainImgExt = ProductImage::where('id', $mainImgId)->pluck('ext');

                                        if($mainImgId)
                                            $mainImgUrl = $mainImgId.'.'.$mainImgExt;
                                        else
                                            $mainImgUrl = 'default.png';
                                    ?>


                                    <img id="img-main-img" src="{{URL::To('/')}}/assets/backend/images/products/products/{{ $mainImgUrl }}" alt="..." />
                                    <div id = "image-upload-progress" class = "progress" style = "height: 8px;width: 100%;">
                                        <div id = "image-upload-progress-bar" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                            <span id="image-upload-sr-only" class = "sr-only">0% Complete</span>
                                        </div>
                                    </div>
                                    <div class="dark-overlay" onclick="$('#image-choose').trigger('click');">
                                        <h4>Click Here To Upload Product Main Image</h4>
                                        <h5><i class="fa fa-upload"></i></h5>
                                        <p>For better preview in the website, upload image should be in 720 x 960 (px) Resolution</p>
                                    </div>
                                </div>
                                <div class="product_gallery">
                                    @for($i = 0; $i < 4; $i++)
                                        <?php
                                            if(array_key_exists($i, $productImages)) {
                                                $imgId = $productImages[$i];
                                                $ext = ProductImage::where('id', $imgId)->pluck('ext');
                                                $imgUrl = $imgId.'.'.$ext;
                                            }
                                            else {
                                                $imgUrl = 'default.png';
                                            }

                                            if($i == 0)
                                                $type = 'Main';
                                            else if($i == 1)
                                                $type = 'Hover';
                                            else
                                                $type = 'Other'
                                        ?>
                                        <a id="product-img-thumb-{{ $i }}" style="position: relative;" onclick="changeSelectedImage({{ $i }});">
                                            <img id="img-thumb-{{ $i }}" src="{{URL::To('/')}}/assets/backend/images/products/products/{{ $imgUrl }}" alt="Product Image" />
                                            <span id="product-img-thumb-overlay-{{ $i }}" class="{{ $i != 0? 'light-overlay':'' }} dark-thumb-overlay">{{ $type }} Image</span>
                                        </a>
                                    @endfor
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-8 col-xs-12" style="border:0px solid #e5e5e5;">
                                <div class="col-md-12">
                                    <input data-id = "product-status" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" title="Publish Product" @if($product->status == 1) checked @endif />
                                </div>
                                <h3 class="prod_title" id="product-middle-name">{{ $product->name }}</h3>
                                <textarea id="product-small-description" placeholder="Please Enter Product Basic Description Here..." onchange="saveProductInfo(this, 'small_description');" class="form-control">{{ $product->small_description }}</textarea>
                                <br />
                                {{--<div class="">--}}
                                {{--<h2>Available Colors</h2>--}}
                                {{--<ul class="list-inline prod_color">--}}
                                {{--<li>--}}
                                {{--<p>Green</p>--}}
                                {{--<div class="color bg-green"></div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<p>Blue</p>--}}
                                {{--<div class="color bg-blue"></div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<p>Red</p>--}}
                                {{--<div class="color bg-red"></div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<p>Orange</p>--}}
                                {{--<div class="color bg-orange"></div>--}}
                                {{--</li>--}}

                                {{--</ul>--}}
                                {{--</div>--}}
                                <br />

                                <div id="product-price-container">
                                    <h4 style="text-align: center;">Product Price</h4>
                                    <div class="form-group">
                                        <label for="">Select Product Options</label>
                                        <select id="product-price-options" class="form-control" onchange="ChangeProductPriceOption();">
                                            <option value="1" @if($product->payments == 1) selected="selected" @endif>By Each (Charge Per Item)</option>
                                            <option value="2" @if($product->payments == 2) selected="selected" @endif>By Volume (Charge Per ml)</option>
                                            <option value="3" @if($product->payments == 3) selected="selected" @endif>By Weight (Charge Per kg)</option>
                                            <option value="5" @if($product->payments == 5) selected="selected" @endif>By Area (Charge Per m2)</option>
                                            <option value="4" @if($product->payments == 4) selected="selected" @endif>By Size (Charge Per Sizes)</option>
                                        </select>
                                    </div>
                                    {{--Each--}}
                                    <div id="price-each" @if($product->payments != 1) style="display: none;" @endif>
                                        <div class="col-md-4" style="padding: 15px;">
                                            <div class="form-group col-md-12">
                                                <label for="product-price">Retail Price</label>
                                                <h1 class="price" style="font-size: 28px; position: relative;"><span style="display: inline-block; position: absolute;">$</span><input id="product-price" type="text" onchange="saveProductDetails(this, 'price', {{ $productDetails->id }});" value="{{ $productDetails->price }}"  class="form-control input-trans only-digits" style="font-size: 28px; font-weight: 400; color: #26B99A; width: 100%;display: inline-block; padding-left: 20px;" /></h1>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="padding: 15px;">
                                            <div class="form-group col-md-12">
                                                <label for="product-ws-price">Wholesale Price</label>
                                                <h1 class="price" style="font-size: 28px; position: relative;"><span style="display: inline-block; position: absolute;">$</span><input id="product-ws-price" type="text" onchange="saveProductDetails(this, 'wholesale_price', {{ $productDetails->id }});" value="{{ $productDetails->wholesale_price }}"  class="form-control input-trans only-digits" style="font-size: 28px; font-weight: 400; color: #26B99A; width: 100%;display: inline-block; padding-left: 20px;" /></h1>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="padding: 15px;">
                                            <div class="form-group col-md-12">
                                                <label for="product-cost-price">Cost Price</label>
                                                <h1 class="price" style="font-size: 28px; position: relative;"><span style="display: inline-block; position: absolute;">$</span><input id="product-cost-price" type="text" onchange="saveProductDetails(this, 'cost_price', {{ $productDetails->id }});" value="{{ $productDetails->cost_price }}"  class="form-control input-trans only-digits" style="font-size: 28px; font-weight: 400; color: #26B99A; width: 100%;display: inline-block; padding-left: 20px;" /></h1>
                                            </div>
                                        </div>
                                    </div>
                                    {{--Volume--}}
                                    <div id="price-volume" @if($product->payments != 2) style="display: none;" @endif>
                                        <div class="form-group col-md-6">
                                            <label>Volume (liter)</label>
                                            <input id="product-volume-volume" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'volume', {{ $productDetails->id }});" value="{{ $productDetails->volume }}" />
                                        </div>
                                        <div class="col-md-5 form-group">
                                            <label>Price ($)</label>
                                            <input id="product-volume-price" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetails->id }});" value="{{ $productDetails->price }}" />
                                        </div>

                                        <?php
                                        $productDetailList = ProductDetail::where('productid', $product->id)->where('status', 1)->where('volume', '!=', '')->where('price', '!=', 0)->where('default_product', 0)->get();
                                        ?>
                                        @foreach($productDetailList as $productDetail)
                                            <div id="product-volume-container-{{$productDetail->id}}">
                                                <div class="form-group col-md-6">
                                                    <label>Volume (ml)</label>
                                                    <input id="product-volume-volume-{{$productDetail->id}}" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'volume', {{ $productDetail->id }});" value="{{ $productDetail->volume }}" />
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>Price ($)</label>
                                                    <input id="product-volume-price-{{$productDetail->id}}" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetail->id }});" value="{{ $productDetail->price }}" />
                                                </div>
                                                <div class="col-md-1">
                                                    <i id="product-volume-remove-icon-{{$productDetail->id}}" class="fa fa-minus-circle remove-icon" data-toggle="tooltip" title="Remove This" onclick="removePriceCombination('volume', {{$productDetail->id}});"></i>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                    {{--Weight--}}
                                    <div id="price-weight" @if($product->payments != 3) style="display: none;" @endif>
                                        <div class="form-group col-md-6">
                                            <label>Weight (kg)</label>
                                            <input id="product-weight-weight" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'weight', {{ $productDetails->id }});" value="{{ $productDetails->weight }}" />
                                        </div>
                                        <div class="col-md-5 form-group">
                                            <label>Price ($)</label>
                                            <input id="product-weight-price" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetails->id }});" value="{{ $productDetails->price }}" />
                                        </div>

                                        <?php
                                        $productDetailList = ProductDetail::where('productid', $product->id)->where('status', 1)->where('weight', '!=', '')->where('price', '!=', 0)->where('default_product', 0)->get();
                                        ?>
                                        @foreach($productDetailList as $productDetail)
                                            <div id="product-weight-container-{{$productDetail->id}}">
                                                <div class="form-group col-md-6">
                                                    <label>Weight (kg)</label>
                                                    <input id="product-weight-weight-{{$productDetail->id}}" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'weight', {{ $productDetail->id }});" value="{{ $productDetail->weight }}" />
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>Price ($)</label>
                                                    <input id="product-weight-price-{{$productDetail->id}}" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetail->id }});" value="{{ $productDetail->price }}" />
                                                </div>
                                                <div class="col-md-1">
                                                    <i id="product-weight-remove-icon-{{$productDetail->id}}" class="fa fa-minus-circle remove-icon" data-toggle="tooltip" title="Remove This" onclick="removePriceCombination('weight', {{$productDetail->id}});"></i>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                    {{--Area--}}
                                    <div id="price-area" @if($product->payments != 5) style="display: none;" @endif>
                                        <div class="form-group col-md-6">
                                            <label>Area (m2)</label>
                                            <input id="product-area-area" type="text" class="form-control" onchange="saveProductDetails(this, 'area', {{ $productDetails->id }});" value="{{ $productDetails->area }}" />
                                        </div>
                                        <div class="col-md-5 form-group">
                                            <label>Price ($)</label>
                                            <input id="product-area-price" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetails->id }});" value="{{ $productDetails->price }}" />
                                        </div>
                                        <?php
                                            $productDetailList = ProductDetail::where('productid', $product->id)->where('status', 1)->where('area', '!=', '')->where('price', '!=', 0)->where('default_product', 0)->get();
                                        ?>
                                        @foreach($productDetailList as $productDetail)
                                            <div id="product-area-container-{{$productDetail->id}}">
                                                <div class="form-group col-md-6">
                                                    <label>Area (m2)</label>
                                                    <input id="product-area-area-{{$productDetail->id}}" type="text" class="form-control only-digits" onchange="saveProductDetails(this, 'area', {{ $productDetail->id }});" value="{{ $productDetail->area }}" />
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>Price ($)</label>
                                                    <input id="product-area-price-{{$productDetail->id}}" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetail->id }});" value="{{ $productDetail->price }}" />
                                                </div>
                                                <div class="col-md-1">
                                                    <i id="product-area-remove-icon-{{$productDetail->id}}" class="fa fa-minus-circle remove-icon" data-toggle="tooltip" title="Remove This" onclick="removePriceCombination('area', {{$productDetail->id}});"></i>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    {{--Size--}}
                                    <div id="price-size" @if($product->payments != 4) style="display: none;" @endif>
                                        <div class="form-group col-md-6">
                                            <label>Size Name (Ex: Small, Large)</label>
                                            <input id="product-size-size" type="text" class="form-control" onchange="saveProductDetails(this, 'size', {{ $productDetails->id }});" value="{{ $productDetails->size }}" />
                                        </div>
                                        <div class="col-md-5 form-group">
                                            <label>Price ($)</label>
                                            <input id="product-size-price" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetails->id }});" value="{{ $productDetails->price }}" />
                                        </div>


                                        <?php
                                        $productDetailList = ProductDetail::where('productid', $product->id)->where('status', 1)->where('size', '!=', '')->where('price', '!=', 0)->where('default_product', 0)->get();
                                        ?>
                                        @foreach($productDetailList as $productDetail)
                                            <div id="product-size-container-{{$productDetail->id}}">
                                                <div class="form-group col-md-6">
                                                    <label>Size Name (Ex: Small, Large)</label>
                                                    <input id="product-size-size-{{$productDetail->id}}" type="text" class="form-control only-digits" onchange="saveProductDetails(this, 'size', {{ $productDetail->id }});" value="{{ $productDetail->size }}" />
                                                </div>
                                                <div class="col-md-5 form-group">
                                                    <label>Price ($)</label>
                                                    <input id="product-size-price-{{$productDetail->id}}" type="number" class="form-control only-digits" onchange="saveProductDetails(this, 'price', {{ $productDetail->id }});" value="{{ $productDetail->price }}" />
                                                </div>
                                                <div class="col-md-1">
                                                    <i id="product-size-remove-icon-{{$productDetail->id}}" class="fa fa-minus-circle remove-icon" data-toggle="tooltip" title="Remove This" onclick="removePriceCombination('size', {{$productDetail->id}});"></i>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                    <div class="col-xs-12" style="margin-bottom: 40px;">
                                        <button id="product-add-more-icon"  @if($product->payments == 1) style="display: none;" @endif class="btn btn-xs btn-success" data-toggle="tooltip" title="Add More" onclick="addMorePriceCategories();"><i class="fa fa-plus"></i> Add More</button>
                                    </div>
                                    <div id="select-category-container" class="col-md-12" style="margin-top: 20px;">
                                        <div class="form-group col-md-6">
                                            <label>Category</label>
                                            <select id="drp-product-category" class="form-control" onchange="saveProductCategory();">
                                                <?php
                                                $categories = ProductCategory::where('orgid', Auth::user()->orgid)->where('sub_item', null)->where('status', 1)->get();
                                                $categoryOn = 0;
                                                $options = '';
                                                $selectedCategory = null;
                                                ?>
                                                @foreach($categories as $category)
                                                    <?php
                                                    $optionSelected = '';
                                                    $productCategory = ProductCategoryRelation::where('product_category_id', $category->id)->where('product_id', $product->id)->first();
                                                    if($productCategory) {
                                                        $categoryOn = 1;
                                                        $optionSelected = 'selected';
                                                        $selectedCategory = $category -> id;
                                                        $selectedSubCategory = $productCategory -> sub_category_id;
                                                    }
                                                    $options = $options.'<option id="'.$category->id.'" '.$optionSelected.' value="'.$category->id.'">'.$category->name.'</option>';
                                                    ?>
                                                @endforeach

                                                @if($categoryOn == 0)
                                                    <option disabled selected>Non-selected</option>
                                                @endif
                                                {{ $options }}
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Sub Category</label>
                                            <select id="drp-product-sub-cateogory" class="form-control" onchange="saveSubCategory();">
                                                <?php
                                                    $subCategoryOptions = '';
                                                    if($selectedCategory != null) {
                                                        $subCategories = ProductCategory::where('sub_item', $selectedCategory)->get();
                                                        foreach ($subCategories as $subCategory) {
                                                            $selectedText = '';
                                                            if($selectedSubCategory == $subCategory -> id)
                                                                $selectedText = 'selected';

                                                            $subCategoryOptions = $subCategoryOptions.'<option value="'.$subCategory->id.'" '.$selectedText.'>'.$subCategory->name.'</option>';
                                                        }
                                                    }
                                                ?>
                                                <option value="0">Non-selected</option>
                                                {{ $subCategoryOptions }}
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top: 50px;">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="product-tabs" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_statistics" id="statistics-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-line-chart"></i> Statistics</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_description" role="tab" id="description-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-align-justify"></i> Product Detailed Description</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_additional" role="tab" id="additional-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-info-circle"></i> Additional Information</a>
                                        </li>
                                        {{--<li role="presentation" class=""><a href="#tab_categories" role="tab" id="categories-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-dedent"></i> Product Categories</a>--}}
                                        {{--</li>--}}
                                        <li role="presentation" class=""><a href="#tab_settings" role="tab" id="settings-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-wrench"></i> Settings</a>
                                        </li>
                                    </ul>
                                    <div id="product-tab-content" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_statistics" aria-labelledby="statistics-tab">
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 20px;">
                                                <div id="echart_line" style="height:350px;"></div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_description" aria-labelledby="description-tab">
                                            <div class="form-group"style="padding: 20px;" id="detailed-description-container">
                                                <div id="detailed-description" class='edit' data-rich="0">{{ $product->description }}</div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_additional" aria-labelledby="additional-tab">
                                            <div class="form-group"style="padding: 20px;" id="additional-info-container">
                                                <div id="additional-info" class='edit' data-rich="0">{{ $product->additional_info }}</div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_categories" aria-labelledby="categories-tab">
                                            <?php
                                            $menuId = Menu::where('orgid', Auth::user()->orgid)->where('positionid', 1)->pluck('id');
                                            $menuList = MenuItem::where('menuid', $menuId)->orderBy('status', 'DESC')->orderBy('menu_order', 'ASC')->where('status', '!=', 0)->get();
                                            $counter = 1;
                                            $gridX = 0;
                                            $gridY = 0;
                                            ?>
                                            <div class="row" style="padding: 30px; padding-bottom: 10px !important;">
                                                <button id="add-new-widget" class="btn btn-sm btn-success pull-right" onclick="addMenuItem({{ $menuId }})"><i class="fa fa-plus"></i> Add Menu Item</button>
                                            </div>
                                            <div class="row" style="padding: 30px; padding-top: 10px !important;">
                                                <div id="menu-item-grid-stack" class="grid-stack">
                                                    @foreach($menuList as $menuItem)
                                                        <div id="menu-item-{{ $menuItem->id }}" class="grid-stack-item menu-item" data-menuid="{{ $menuItem->id }}" data-gs-x="{{ $gridX }}" data-gs-y="{{ $gridY }}" data-gs-width="4" data-gs-height="4" data-gs-no-resize="true" style="padding: 10px;">
                                                            <div class="grid-stack-item-content flip" style="box-shadow: 1px 1px 2px 3px #f7f6f6; border-radius: 10px; padding: 20px; position: relative; height: 100%;">
                                                                <h4 style="text-align: center;">@if($menuItem->status == 2) <span id="menu-item-number-{{ $menuItem->id }}" class="menu-item-draft">Draft - </span> @else <span id="menu-item-number-{{ $menuItem->id }}">{{ $counter++ }}.</span> @endif <input id="txt-menu-item-name-{{ $menuItem->id }}" type="text" class="input-trans" value="{{ $menuItem->name }}" style="height: 25px;" onchange="updateMenuItemName({{ $menuItem->id }});" /></h4>
                                                                <div style="position: absolute; right: 10px; top: 10px;">
                                                                    <i class="fa fa-gear pull-right" style="font-size: 23px; cursor: pointer;" onclick="flipDiv({{ $menuItem->id }});"></i>
                                                                    <input data-id = "menu-item-status-{{ $menuItem->id }}" type = "checkbox" class = "ios-switch ios-switch-primary iswitch-xs pull-right" title="Publish Menu Item" @if($menuItem->status == 1) checked @endif />
                                                                </div>
                                                                <div id="flipped-class-{{ $menuItem->id }}" class="card">
                                                                    <div class="face front">
                                                                        <!-- start accordion -->
                                                                        <?php
                                                                        $productCategories = ProductCategory::where('menu_item_id', $menuItem->id)->where('status', 1)->where('sub_item', null)->get();
                                                                        ?>
                                                                        @if(sizeof($productCategories) > 0)
                                                                            <div class="accordion" id="accordion-menu-category-{{ $menuItem->id }}" data-menuid="{{ $menuItem->id }}" data-container="0" role="tablist" aria-multiselectable="true">
                                                                                @foreach($productCategories as $productCategory)
                                                                                    <div class="panel" data-menuitemid="{{ $productCategory->id }}">
                                                                                        <a class="panel-heading collapsed" role="tab" id="category-{{ $productCategory->id }}" data-toggle="collapse" data-parent="#accordion-menu-category-{{ $menuItem->id }}" href="#collapse-{{ $productCategory->id }}" aria-expanded="true" aria-controls="collapse-{{ $productCategory->id }}">
                                                                                            <h4 class="panel-title">{{ $productCategory->name }}</h4>
                                                                                        </a>
                                                                                        <div id="collapse-{{ $productCategory->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $productCategory->id }}">
                                                                                            <div class="panel-body">
                                                                                                <div class="form-group">
                                                                                                    <label>Description</label>
                                                                                                    <textarea id="category-description-{{ $productCategory->id }}" class="form-control" data-rich="0">{{ $productCategory->description }}</textarea>
                                                                                                </div>
                                                                                                <?php
                                                                                                $productSubCategories = ProductCategory::where('sub_item', $productCategory->id)->get();
                                                                                                ?>
                                                                                                <div class="accordion" id="accordion-menu-sub-category-{{ $productCategory->id }}" data-menuid="{{ $productCategory->id }}" data-container="{{ $productCategory->id }}" role="tablist" aria-multiselectable="true">
                                                                                                    @foreach($productSubCategories as $productSubCategory)
                                                                                                        <div class="panel" data-menuitemid="{{ $productSubCategory->id }}">
                                                                                                            <a class="panel-heading collapsed" role="tab" id="sub-category-{{ $productSubCategory->id }}" data-toggle="collapse" data-parent="#accordion-menu-sub-category-{{ $productCategory->id }}" href="#collapse-{{ $productSubCategory->id }}" aria-expanded="true" aria-controls="collapse-{{ $productSubCategory->id }}" style="background-color: #f5f2f7;">
                                                                                                                <h4 class="panel-title">{{ $productSubCategory->name }}</h4>
                                                                                                            </a>
                                                                                                            <div id="collapse-{{ $productSubCategory->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $productSubCategory->id }}">
                                                                                                                <div class="panel-body">
                                                                                                                    <div class="form-group">
                                                                                                                        <label>Description</label>
                                                                                                                        <textarea id="category-description-{{ $productSubCategory->id }}" class="form-control" data-rich="0">{{ $productSubCategory->description }}</textarea>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        @else
                                                                            <p style="text-align: left; padding-left: 15px; color: #b1b1b1;">
                                                                                No Sub categories under this menu item
                                                                            </p>
                                                                    @endif
                                                                    <!-- end of accordion -->
                                                                    </div>
                                                                    <div class="face back">
                                                                        <div class="bs-callout bs-callout-info">
                                                                            <div class="form-group">
                                                                                <label>URL</label>
                                                                                <input class="form-control input-trans" placeholder="custom url (Ex: yourdomain.com)" value="{{ $menuItem->href }}" />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Description</label>
                                                                                <textarea id="menu-item-description-{{ $menuItem->id }}" class="form-control" data-rich="0">{{ $menuItem->description }}</textarea>
                                                                            </div>
                                                                            <p style="text-align: center;">
                                                                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Remove</button>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <?php
                                                        if($gridX == 0) {
                                                            $gridX = 4;
                                                        }
                                                        else if($gridX == 4) {
                                                            $gridX = 8;
                                                        }
                                                        else if($gridX == 8) {
                                                            $gridX = 0;
                                                            $gridY = $gridY+4;
                                                        }
                                                        ?>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_settings" aria-labelledby="settings-tab">
                                            <div class="form-group"style="padding: 20px;" id="search-keywords-container">
                                                <label>Search Keywords</label>
                                                <textarea id="search-keywords" class="form-control" placeholder="Type product search keywords here..." onchange="saveProductInfo(this, 'keywords')">{{ $product->keywords }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>

@endsection

@section('scripts')

    <!-- ECharts -->
    {{HTML::script('/assets/backend/libs/echarts/dist/echarts.js')}}
    {{HTML::script('/assets/backend/libs/echarts/map/js/world.js')}}

    {{HTML::script('assets/js/products/single-product.js')}}


    <script>
        $(function () {
            var options = {
                cellHeight: 80,
                verticalMargin: 10
            };
            $('.grid-stack').gridstack(options);
        });

        $(function  () {
            $(".accordion").sortable({
                stop: function () {
                    window.calledFromMove = 1;
//                    var menuid = $(this).attr('data-menuid');
                    sortMenuSubItems();
                },
                connectWith: $('.accordion')
            });
        });
    </script>
@endsection