<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 16/01/18 21:44.
 */
?>

<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 26/01/17 08:10.
 */
?>

<div id="model-add-new-org" class="modal fade bs-add-org-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button id="org-modal-close" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Organization</h4>
            </div>
            <div id="modal-add-org-body" class="modal-body" style="padding: 20px 30px;">
                <div class = "row">
                    <div class = "form-group">
                        <label>Organization Name</label>
                        <input id = "mm-org-name" onblur="requiredValidator(this, 'Please enter the organizations\'s name.');" onkeyup="requiredValidator(this, 'Please enter the organization\'s name.');" type = "text" class = "form-control" placeholder = "Prisu Pride LTD" maxlength = "64" required tabindex="1">
                    </div>
                    <div class = "form-group">
                        <label>Account Admin Name</label>
                        <input id = "mm-admin-name" onblur="requiredValidator(this, 'Please enter the admin\'s name.');" onkeyup="requiredValidator(this, 'Please enter the admin\'s name.');" type = "text" class = "form-control" placeholder="John" maxlength = "32" required tabindex="2">
                    </div>
                    <div class = "form-group">
                        <label>Admin Email</label>
                        <input id = "mm-admin-email" onblur="requiredValidator(this, 'Please enter the email address.')" onkeyup="emailValidator(this, 'Invalid email address.');" type = "email" class = "form-control" placeholder = "john&#64;example.com" maxlength = "60" required tabindex="3">
                    </div>
                    <p style = "text-align: center; margin-top: 40px;">
                        <button id = "mm-btn-user-cancel" onclick="cancelUser();" class = "btn btn-danger md-close">Cancel</button>
                        <button id = "mm-btn-user-add" onclick="addOrganization();" type = "button" class = "btn btn-success" tabindex="4">Add</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div><!-- End div .md-modal .md-3d-flip-vertical-->


