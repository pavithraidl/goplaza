<?php
/*************************************************************************
 *
 * IDL CONFIDENTIAL
 * __________________
 *
 *  [2018] - [2028] THE PRISU PRIDE LIMITED
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of THE PRISU PRIDE LIMITED and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to THE PRISU PRIDE LIMITED
 * and its suppliers and may be covered by New Zealand and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from THE PRISU PRIDE LIMITED.
 *
 * Developed by - Pavithra Isuru
 * Authorized by - THE PRISU PRIDE LIMITED (Pavithra Isuru Liyanage)
 * Created on - 2018-01-12 08:17:09.
 * View ID - 49
 */
 ?>

@extends('backend.master')

@section('styles')
    {{HTML::style('assets/css/organizations/style.css')}}
@endsection

@section('content')

    @include('backend.organizations.include.new-org')

    <div class="right_col" role="main">
        <div id="org-container">
            <div class="col-xs-12">
                <h3 style="text-align: center; font-size: 28px;"><i class="fa fa-building-o"></i> Organizations</h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Organization List</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="opacity: 0;">
                        <div class="row">
                            <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target=".bs-add-org-modal-lg" onclick="openAddOrg();"><i class="fa fa-plus"></i> New Organization</button>
                        </div>
                        <!-- start accordion -->
                        <div class="accordion" id="accordion-orgs" role="tablist" aria-multiselectable="true">
                            <?php
                                if(Auth::user()->roll < 4) {
                                    $orgs = Org::where('status', '!=', 0)->get();
                                }
                                else {
                                    $orgs = Org::where('status', '!=', 0)->where('parent', Auth::user()->org)->get();
                                }
                            ?>
                            @foreach($orgs as $org)
                                <?php
                                    $orgAdmin = User::where('id', $org->admin)->first();
                                    ?>
                            <div class="panel">
                                <a class="panel-heading collapsed" role="tab" id="heading-{{ $org->id }}" data-toggle="collapse" data-parent="#accordion-orgs" href="#collapse-{{ $org->id }}" aria-expanded="false" aria-controls="collapse-{{ $org->id }}" onclick="selectOrg({{ $org->id }});">
                                    <h4 class="panel-title">{{ str_pad($org->id, 4, '0', STR_PAD_LEFT) }} {{ $org->name }} <i class="fa fa-circle pull-right" id="org-status-indicator-{{ $org->id }}" style="color: @if($org->status == 1)  #00c219 @else #9c9c9c @endif;" ></i> </h4>
                                </a>
                                <div id="collapse-{{ $org->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $org->id }}">
                                    <div class="panel-body" id="panel-{{ $org->id }}" style="margin-bottom: 100px;">
                                        @if($org->id != Auth::user()->orgid)
                                            <input data-id = "org-status" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" @if($org->status == 1)checked @endif hidden = "hidden"/>
                                        @endif
                                        <div class="" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: 40px;">
                                            <ul id="org-tab-{{ $org->id }}" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content-info-{{ $org->id }}" id="info-tabb-{{ $org->id }}" role="tab" data-toggle="tab" aria-controls="info-{{ $org->id }}" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a></li>
                                                @if($org->id != Auth::user()->orgid) <li role="presentation" class=""><a href="#tab_content-access-{{ $org->id }}" role="tab" id="access-tabb-{{ $org->id }}" data-toggle="tab" aria-controls="access-{{ $org->id }}" aria-expanded="false"><i class="fa fa-key"></i> Access</a></li> @endif
                                                <li role="presentation" class=""><a href="#tab_content-settings-{{ $org->id }}" role="tab" id="settings-tabb-{{ $org->id }}" data-toggle="tab" aria-controls="settings-{{ $org->id }}" aria-expanded="false"><i class="fa fa-cog"></i> Settings</a></li>
                                            </ul>
                                            <div id="org-tab-content-{{ $org->id }}" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content-info-{{ $org->id }}" aria-labelledby="info-tab-{{ $org->id }}">
                                                    <div class="col-md-12">
                                                        <div class="row top_tiles" style="margin: 10px 0;">
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total Visits</span>
                                                                <h2 id="system-total-visits-{{ $org->id }}">121</h2>
                                                                <span class="sparkline_one" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total Visitors</span>
                                                                <h2 id="system-total-visitors-{{ $org->id }}">12</h2>
                                                                <span class="sparkline_one" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total Traffic</span>
                                                                <h2>231</h2>
                                                                <span class="sparkline_one" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3 col-xs-6 tile">
                                                                <span>Total bandwidth</span>
                                                                <h2>809</h2>
                                                                <span class="sparkline_two" style="height: 160px;">
                                                                    <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <div class="bs-callout bs-callout-info">
                                                                <div class="info-section">
                                                                    <div>
                                                                        <span class="prompt"><i class="fa fa-circle"></i> Name</span>
                                                                        <span class="separator">:</span>
                                                                        <span class="value">{{ $org->name }}</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="prompt"><i class="fa fa-circle"></i> Contact Name</span>
                                                                        <span class="separator">:</span>
                                                                        <span class="value">{{ $orgAdmin->fname }} {{ $orgAdmin->lname }}</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="prompt"><i class="fa fa-circle"></i> Contact Email</span>
                                                                        <span class="separator">:</span>
                                                                        <span class="value">{{ $orgAdmin->email }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="bs-callout bs-callout-primary">
                                                                <div>
                                                                    <span class="prompt"><i class="fa fa-circle"></i> Subscription</span>
                                                                    <span class="separator">:</span>
                                                                    <span id="org-activation-status-{{ $org->id }}" class="value"
                                                                        @if($org->status == 1 && $org->activation_link == 0)
                                                                            > <button onclick="sendActivationEmail();" class="btn btn-sm btn-default">Send Activation Email</button>
                                                                        @elseif($org->status == 1 && $org->activation_link == 2)
                                                                            > <button onclick="sendActivationEmail();" class="btn btn-sm btn-default">Re-Send Activation Email</button>
                                                                        @elseif($org->status == 1 && $org->activation_link == 1)
                                                                            style="color: #00d719;"> Active
                                                                        @elseif($org->status == 2)
                                                                            style="color: #D5000E;"> Deactivated @endif </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($org->id != Auth::user()->orgid)
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content-access-{{ $org->id }}" aria-labelledby="access-tab-{{ $org->id }}">
                                                    <!-- / .tab-pane Access-->
                                                    <div id="o-m-access-control-container" class = "row" style="padding: 20px 15%;">
                                                        <div class="panel-group accordion-toggle" id="accordiondemo-{{ $org->id }}">
                                                        <?php
                                                        $systemList = System::where('status', 1)->where('visibility', 1)->orderBy('listorder', 'asc')->lists('id');
                                                        ?>
                                                        <!--                                                            Looping system list-->
                                                            @foreach($systemList as $systemId)
                                                                <?php
                                                                $viewList = SystemView::where('systemid', $systemId)->where('status', 1)->lists('id');
                                                                ?>
                                                                @if(sizeof($viewList) > 0)
                                                                    <h4 style="margin-top: 20px; margin-bottom: 10px"><i class="fa {{ System::where('id', $systemId)->pluck('icon') }}"></i> {{ System::where('id', $systemId)->pluck('name') }}</h4>
                                                                @endif
                                                                {{--Looping View list--}}
                                                                @foreach($viewList as $viewId)
                                                                    @if(SystemViewAccess::where('viewid', $viewId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 2 || System::where('id', $systemId)->where('allow_default', 1)->pluck('id'))
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading" style="background-color: #ececec">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordiondemo-{{ $org->id }}" href="#accordion-{{ $org->id }}-{{$viewId}}">
                                                                                        <h4 style="margin-top: 0; margin-bottom: 5px;" id="o-m-text-container-1" onclick="">
                                                                                            <i class = "fa {{ SystemView::where('id', $viewId)->pluck('icon') }}"></i> &nbsp;&nbsp;{{ SystemView::where('id', $viewId)->pluck('view_name') }}
                                                                                        </h4>
                                                                                    </a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="accordion-{{ $org->id }}-{{$viewId}}" class="panel-collapse collapse">
                                                                                <div class="panel-body">
                                                                                    <ul style="list-style-type: none;">
                                                                                        <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px; font-size: 18px; font-weight: 600;">
                                                                                            <div class="col-md-7" id="user-operation-text-container-{{ $org->id }}-{{$viewId}}">
                                                                                                <i class="fa {{ SystemView::where('id', $viewId)->pluck('icon') }}"></i> Access&nbsp;&nbsp;{{ SystemView::where('id', $viewId)->pluck('view_name') }}
                                                                                            </div>
                                                                                            <div class="col-md-4" style="cursor: pointer">
                                                                                                <?php
                                                                                                    //get status for bellow view
                                                                                                    $viewAccess = SystemViewAccess::where('viewid', $viewId)->where('userid', $org->admin)->pluck('allow');
                                                                                                ?>
                                                                                                <input id = "user-access-system-views-{{ $org->id }}-{{$viewId}}" data-id = "sa-sy-{{$viewId}}" name="view" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-md pull-left" @if($viewAccess == 1) checked @endif/>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content-settings-{{ $org->id }}" aria-labelledby="settings-tab-{{ $org->id }}">
                                                    <div class="col-xs-2">
                                                        <!-- required for floating -->
                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs tabs-left">
                                                            <li class="active"><a href="#website-{{ $org->id }}" data-toggle="tab"><i class="fa fa-globe"></i> Website</a></li>
                                                            <li><a href="#mail-{{ $org->id }}" data-toggle="tab"><i class="fa fa-envelope"></i> Mail</a></li>
                                                            <li><a href="#merchant-{{ $org->id }}" data-toggle="tab"><i class="fa fa-credit-card"></i> Merchant</a></li>

                                                        </ul>
                                                    </div>

                                                    <div class="col-xs-10">
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            {{--Website Tab Content--}}
                                                            <div class="tab-pane active" id="website-{{$org->id}}">
                                                                <form class="form-horizontal form-label-left">
                                                                    <?php
                                                                        $website = WebsiteSettings::where('orgid', $org->id)->where('status', 1)->first();
                                                                    ?>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Style Sheet Links</label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <textarea id="org-website-styles-{{ $org->id }}" onchange="saveSettings(this, 'style_sheets', 'website_settings');" value="@if($website){{ $website->style_sheets }}@endif" type="text" class="form-control" placeholder="Ex: <link media='all' type='text/css' rel='stylesheet' href='your_style.css'>">@if($website){{ $website->style_sheets }}@endif</textarea>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            {{-- Mail settings tab content--}}
                                                            <div class="tab-pane" id="mail-{{ $org->id }}">
                                                                <p class="lead" style="text-align: center;">Mail Settings</p>
                                                                <form class="form-horizontal form-label-left">
                                                                    <?php
                                                                    $mail = OrgMail::where('orgid', $org->id)->where('status', 1)->first();
                                                                    ?>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Driver</label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-driver-{{ $org->id }}" onchange="saveSettings(this, 'driver', 'org_mail');" value="@if($mail){{ $mail->driver }}@endif" type="text" class="form-control" placeholder="smtp">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Host </label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-host-{{ $org->id }}" onchange="saveSettings(this, 'host', 'org_mail');" value="@if($mail){{ $mail->host }}@endif" type="text" class="form-control" placeholder="yourhost.co.nz">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Port</label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-port-{{ $org->id }}" onchange="saveSettings(this, 'port', 'org_mail');" value="@if($mail){{ $mail->port }}@endif" type="number" class="form-control" placeholder="587">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">From Address </label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-address-{{ $org->id }}" onchange="saveSettings(this, 'from_address', 'org_mail');" value="@if($mail){{ $mail->from_address }}@endif" type="text" class="form-control" placeholder="noreply@yourhost.co.nz">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">From Name </label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-name-{{ $org->id }}" onchange="saveSettings(this, 'from_name', 'org_mail');" value="@if($mail){{ $mail->from_name }}@endif" type="text" class="form-control" placeholder="Your Company Pvt Ltd">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Encryption </label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-encryption-{{ $org->id }}" onchange="saveSettings(this, 'encryption', 'org_mail');" value="@if($mail){{ $mail->encryption }}@endif" type="text" class="form-control" placeholder="tls">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Username </label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-username-{{ $org->id }}" onchange="saveSettings(this, 'username', 'org_mail');" value="@if($mail){{ $mail->username }}@endif" type="text" class="form-control" placeholder="your email username">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password </label>
                                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                                            <input id="org-mail-password-{{ $org->id }}" onchange="saveSettings(this, 'password', 'org_mail');" value="@if($mail){{ $mail->password }}@endif" type="text" class="form-control" placeholder="your email password">
                                                                        </div>
                                                                    </div>
                                                                    <div class="ln_solid"></div>
                                                                    <div class="form-group">
                                                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                            <span id="org-mail-reset-{{ $org->id }}" onclick="saveSettings(this, 'status', 'org_mail');" value="0" class="btn btn-danger">Reset to Default</span>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            {{--Merchant Tab Content--}}
                                                            <div class="tab-pane" id="merchant-{{$org->id}}">
                                                                <form class="form-horizontal form-label-left">
                                                                    <?php
                                                                        $merchants = OrgMerchantDetail::where('status', 1)->get();
                                                                    ?>
                                                                    @foreach($merchants as $merchant)
                                                                        @if($merchant->id == 1)
                                                                            <?php
                                                                                $poliId = OrgMerchant::where('merchant_detail_id', $merchant->id)->where('orgid', $org->id)->where('status', 1)->pluck('merchant_id');
                                                                                $poli = OrgMerchantPoli::where('id', $poliId)->first();
                                                                            ?>
                                                                            <div class="section" style="margin-left: 20px; position: relative;">
                                                                                <h4 id="merchant-title-{{ $org->id }}" style="font-size: 20px; @if(!$poli || $poli->status == 0) color: #7d7d7d; @endif"><img id="merchant-logo-{{ $org->id }}" @if(!$poli || $poli->status == 0) class="gray-scale" @endif src="https://www.polipayments.com/Images/Shared/Logo.png" style="width: 130px;"/> Poli Payment</h4>
                                                                                <input data-id = "payment-status-{{ $org->id }}-{{ str_pad($merchant->id, 2, '0', STR_PAD_LEFT) }}" type = "checkbox" class = "ios-switch ios-switch-info iswitch-xs pull-right iswitch-merchant-margin" @if($poli) @if($poli->status == 1)checked @endif @endif/>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 merchant-labels" style="@if(!$poli || $poli->status == 0) color: #7d7d7d; @endif">Merchant Code </label>
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input id="org-merchant-code-{{ $org->id }}" onchange="saveSettings(this, 'auth_key', 'org_merchant_poli');" value="@if($poli){{ $poli->auth_key }}@endif" type="text" class="form-control" placeholder="Enter Poli Merchant Code here..." />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 merchant-labels" style="@if(!$poli || $poli->status == 0) color: #7d7d7d; @endif">Authentication Code </label>
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input id="org-merchant-authentication-{{ $org->id }}" onchange="saveSettings(this, 'auth_secret', 'org_merchant_poli');" value="@if($poli){{ $poli->auth_secret }}@endif" type="text" class="form-control" placeholder="Enter Poli Authentication Code here..." />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 merchant-labels" style="@if(!$poli || $poli->status == 0) color: #7d7d7d; @endif">Redirect URL </label>
                                                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                        <input id="org-merchant-redirect-url-{{ $org->id }}" onchange="saveSettings(this, 'redirect_url', 'org_merchant_poli');" value="@if($poli){{ $poli->redirect_url }}@endif" type="text" class="form-control" placeholder="Enter Poli Redirect URL here..." />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- end of accordion -->
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('/assets/backend/js/custom/organizations/manage-organizations.js')}}
@endsection
